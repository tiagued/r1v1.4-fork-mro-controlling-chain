﻿Public Class FormAdmin
    Public ctrl As CAdminViewDelegate
    Private Sub cancel_save_bt_Click(sender As Object, e As EventArgs) Handles cancel_save_bt.Click
        MConstants.GLB_MSTR_CTRL.save_controller.cancelAsync()
    End Sub

    Private Sub ace_driver_version_bt_Click(sender As Object, e As EventArgs) Handles ace_driver_version_bt.Click
        ctrl.getACEVersion()
    End Sub

    Private Sub culture_info_bt_Click(sender As Object, e As EventArgs) Handles culture_info_bt.Click
        ctrl.getCultureInfo()
    End Sub
End Class
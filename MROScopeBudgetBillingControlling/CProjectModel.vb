﻿
Imports System.ComponentModel
Imports System.Runtime.CompilerServices


Public Class CProjectModel
    Implements INotifyPropertyChanged
    Public Sub New()
        _confProjView = New CProjectView(Me)
        _project_listO = New List(Of String)

        'at initalization datetimepicker format is " ", checked is false 
        _curr_input_date_picker_format = " "
        _curr_input_date_picker_checked = False
        _end_fcst_date_picker_format = " "
        _end_fcst_date_picker_checked = False
        'by default dassault text boxes are disabled
        _dass_enable = True = False
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Private _confProjView As CProjectView

    Public Property confProjView() As CProjectView
        Get
            Return _confProjView
        End Get
        Set(ByVal value As CProjectView)
            _confProjView = value
        End Set
    End Property

    Private _ac_reg As String
    Public Property ac_reg() As String
        Get
            Return _ac_reg
        End Get
        Set(ByVal value As String)
            If Not _ac_reg = value Then
                _ac_reg = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("ac_reg")
            End If
        End Set
    End Property

    Private _ac_master As String
    Public Property ac_master() As String
        Get
            Return _ac_master
        End Get
        Set(ByVal value As String)
            If Not _ac_master = value Then
                _ac_master = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("ac_master")
            End If
        End Set
    End Property

    Private _ac_group As String
    Public Property ac_group() As String
        Get
            Return _ac_group
        End Get
        Set(ByVal value As String)
            If Not _ac_group = value OrElse Not MConstants.GLOB_APP_LOADED Then
                _ac_group = value
                'textbox applicable to dass
                If _ac_group.Contains(MConstants.constantConf(MConstants.CONST_CONF_PROJ_AC_GRP_DASS_KEY).value) Then
                    Me.dass_enable = True
                Else
                    Me.dass_enable = False
                    Me.ac_cycles = 0
                    Me.ac_flight_hours = 0
                    Me.dass_sales_order = ""
                End If
                'update markup list
                _confProjView.applicable_material_markup = New BindingSource
                'filter
                If MConstants.GLOB_APP_LOADED Then
                    master_controller.billing_cond_controller.markupValueChanged()
                End If
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("ac_group")
            End If
        End Set
    End Property
    'to enable or disable dassault textboxes
    Private _dass_enable As Boolean
    Public Property dass_enable() As Boolean
        Get
            Return _dass_enable
        End Get
        Set(ByVal value As Boolean)
            If Not _dass_enable = value Then
                _dass_enable = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property


    Private _project_listO As List(Of String)
    Public Property project_list_str() As String
        Get
            Return String.Join(MConstants.SEP_COMMA, _project_listO.ToArray)
        End Get
        Set(ByVal value As String)
            If Not MConstants.GLOB_APP_LOADED And Not String.IsNullOrWhiteSpace(value) Then
                _project_listO = value.Split(MConstants.SEP_COMMA).ToList
            End If
            NotifyPropertyChanged()
        End Set
    End Property
    'read the list
    Public ReadOnly Property project_list() As List(Of String)
        Get
            Return _project_listO
        End Get
    End Property
    'add item to project list and notify
    Public Sub projectListAddItem(_item As String)
        If Not _project_listO.Contains(_item) Then
            _project_listO.Add(_item)
            MConstants.GLB_MSTR_CTRL.project_controller.refreshPorjList()
            'simulate change on project list
            Me.project_list_str = ""
            MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("project_list_str")
        End If
    End Sub
    'remove item to project list and notify
    Public Sub projectListRemoveItem(_item As String)
        If _project_listO.Contains(_item) Then
            _project_listO.Remove(_item)
            MConstants.GLB_MSTR_CTRL.project_controller.refreshPorjList()
            'simulate change on project list
            Me.project_list_str = ""
            MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("project_list_str")
        End If
    End Sub

    Private _project_def As String
    Public Property project_def() As String
        Get
            Return _project_def
        End Get
        Set(ByVal value As String)
            If Not _project_def = value Then
                _project_def = value
                Me.sf_planning_task_file = MConstants.constantConf(MConstants.CONST_CONF_PLAN_TASK_F).value.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, _project_def)
                Me.sf_planning_proj_file = MConstants.constantConf(MConstants.CONST_CONF_PLAN_PROJ_F).value.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, _project_def)
                'update links of planning files
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.project_controller.resetPlanningFileLinks()
                End If
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("project_def")
            End If
        End Set
    End Property

    Private _project_desc As String
    Public Property project_desc() As String
        Get
            Return _project_desc
        End Get
        Set(ByVal value As String)
            If Not _project_desc = value Then
                _project_desc = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("project_desc")
            End If
        End Set
    End Property

    Private _input_date As Date
    Public Property input_date() As Date
        Get
            Return _input_date
        End Get
        Set(ByVal value As Date)
            If Not _input_date = value Or Not Me.curr_input_date_picker_checked Then
                _input_date = value
                'handle display
                If Not _input_date = MConstants.APP_NULL_DATE Then
                    Me.curr_input_date_picker_format = MConstants.constantConf(MConstants.CONST_CONF_DATE_FORMAT).value
                    Me.curr_input_date_picker_checked = True
                Else
                    _input_date = Today
                    Me.curr_input_date_picker_format = " "
                    Me.curr_input_date_picker_checked = False
                End If
            End If
            NotifyPropertyChanged()
            MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("input_date")
        End Set
    End Property
    'input date picker format
    Private _curr_input_date_picker_format As String
    Public Property curr_input_date_picker_format() As String
        Get
            Return _curr_input_date_picker_format
        End Get
        Set(ByVal value As String)
            If Not _curr_input_date_picker_format = value Then
                _curr_input_date_picker_format = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property
    'input date picker checked
    Private _curr_input_date_picker_checked As Boolean
    Public Property curr_input_date_picker_checked() As Boolean
        Get
            Return _curr_input_date_picker_checked
        End Get
        Set(ByVal value As Boolean)
            If Not _curr_input_date_picker_checked = value Then
                _curr_input_date_picker_checked = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _fcst_finish_date As Date
    Public Property fcst_finish_date() As Date
        Get
            Return _fcst_finish_date
        End Get
        Set(ByVal value As Date)
            If Not _fcst_finish_date = value Or Not Me.end_fcst_date_picker_checked Then
                _fcst_finish_date = value
                'handle display
                If Not _fcst_finish_date = MConstants.APP_NULL_DATE Then
                    Me.end_fcst_date_picker_format = MConstants.constantConf(MConstants.CONST_CONF_DATE_FORMAT).value
                    Me.end_fcst_date_picker_checked = True
                Else
                    _fcst_finish_date = Today
                    Me.end_fcst_date_picker_format = " "
                    Me.end_fcst_date_picker_checked = False
                End If
                NotifyPropertyChanged()
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("fcst_finish_date")
        End Set
    End Property
    'end_fcst date picker format
    Private _end_fcst_date_picker_format As String
    Public Property end_fcst_date_picker_format() As String
        Get
            Return _end_fcst_date_picker_format
        End Get
        Set(ByVal value As String)
            If Not _end_fcst_date_picker_format = value Then
                _end_fcst_date_picker_format = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property
    'end_fcst date picker checked
    Private _end_fcst_date_picker_checked As Boolean
    Public Property end_fcst_date_picker_checked() As Boolean
        Get
            Return _end_fcst_date_picker_checked
        End Get
        Set(ByVal value As Boolean)
            If Not _end_fcst_date_picker_checked = value Then
                _end_fcst_date_picker_checked = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _tagk_date As Date
    Public Property tagk_date() As Date
        Get
            Return _tagk_date
        End Get
        Set(ByVal value As Date)
            If Not _tagk_date = value Or Not Me.tagk_date_format_checked Then
                _tagk_date = value
                'handle display
                If Not _tagk_date = MConstants.APP_NULL_DATE Then
                    Me.tagk_date_format = MConstants.constantConf(MConstants.CONST_CONF_DATE_FORMAT).value
                    Me.tagk_date_format_checked = True
                Else
                    _tagk_date = Today
                    Me.tagk_date_format = " "
                    Me.tagk_date_format_checked = False
                End If
                NotifyPropertyChanged()
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("tagk_date")
        End Set
    End Property
    'tagk date picker format
    Private _tagk_date_format As String
    Public Property tagk_date_format() As String
        Get
            Return _tagk_date_format
        End Get
        Set(ByVal value As String)
            If Not _tagk_date_format = value Then
                _tagk_date_format = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property
    'tagk date picker checked
    Private _tagk_date_format_checked As Boolean
    Public Property tagk_date_format_checked() As Boolean
        Get
            Return _tagk_date_format_checked
        End Get
        Set(ByVal value As Boolean)
            If Not _tagk_date_format_checked = value Then
                _tagk_date_format_checked = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _purchase_order As String
    Public Property purchase_order() As String
        Get
            Return _purchase_order
        End Get
        Set(ByVal value As String)
            If Not _purchase_order = value Then
                _purchase_order = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("purchase_order")
            End If
        End Set
    End Property

    'quotation number
    Public ReadOnly Property quotation_number() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.ini_scope_controller.getProjectQuoteNumber()
        End Get
    End Property

    Private _customer As String
    Public Property customer() As String
        Get
            Return _customer
        End Get
        Set(ByVal value As String)
            If Not _customer = value Then
                _customer = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("customer")
            End If
        End Set
    End Property

    Private _customer_rep_name As String
    Public Property customer_rep_name() As String
        Get
            Return _customer_rep_name
        End Get
        Set(ByVal value As String)
            If Not _customer_rep_name = value Then
                _customer_rep_name = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("customer_rep_name")
            End If
        End Set
    End Property

    Private _customer_email As String
    Public Property customer_email() As String
        Get
            Return _customer_email
        End Get
        Set(ByVal value As String)
            If Not _customer_email = value Then
                _customer_email = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("customer_email")
            End If
        End Set
    End Property

    Private _awq_specialist As String
    Public Property awq_specialist() As String
        Get
            Return _awq_specialist
        End Get
        Set(ByVal value As String)
            If Not _awq_specialist = value Then
                _awq_specialist = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("awq_specialist")
            End If
        End Set
    End Property

    Private _awq_specialist_email As String
    Public Property awq_specialist_email() As String
        Get
            Return _awq_specialist_email
        End Get
        Set(ByVal value As String)
            If Not _awq_specialist_email = value Then
                _awq_specialist_email = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("awq_specialist_email")
            End If
        End Set
    End Property

    Private _billing_group As String
    Public Property billing_group() As String
        Get
            Return _billing_group
        End Get
        Set(ByVal value As String)
            If Not _billing_group = value Then
                _billing_group = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("billing_group")
            End If
        End Set
    End Property

    Private _sf_planning_task_file As String
    Public Property sf_planning_task_file() As String
        Get
            Return MConstants.constantConf(MConstants.CONST_CONF_PLAN_TASK_F).value.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, _project_def)
        End Get
        Set(ByVal value As String)
            If Not _sf_planning_task_file = value Then
                _sf_planning_task_file = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _sf_planning_proj_file As String
    Public Property sf_planning_proj_file() As String
        Get
            Return MConstants.constantConf(MConstants.CONST_CONF_PLAN_PROJ_F).value.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, _project_def)
        End Get
        Set(ByVal value As String)
            If Not _sf_planning_proj_file = value Then
                _sf_planning_proj_file = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _ac_flight_hours As Long
    Public Property ac_flight_hours() As Long
        Get
            Return _ac_flight_hours
        End Get
        Set(ByVal value As Long)
            If Not _ac_flight_hours = value Then
                _ac_flight_hours = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("ac_flight_hours")
            End If
        End Set
    End Property

    Private _ac_cycles As Long
    Public Property ac_cycles() As Long
        Get
            Return _ac_cycles
        End Get
        Set(ByVal value As Long)
            If Not _ac_cycles = value Then
                _ac_cycles = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("ac_cycles")
            End If
        End Set
    End Property

    Private _dass_sales_order As String
    Public Property dass_sales_order() As String
        Get
            Return _dass_sales_order
        End Get
        Set(ByVal value As String)
            If Not _dass_sales_order = value Then
                _dass_sales_order = value
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("dass_sales_order")
            End If
        End Set
    End Property

    Private _lab_curr As String
    Public Property lab_curr() As String
        Get
            Return _lab_curr
        End Get
        Set(ByVal value As String)
            If Not _lab_curr = value Then
                _lab_curr = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("lab_curr")
            End If
        End Set
    End Property

    Private _mat_curr As String
    Public Property mat_curr() As String
        Get
            Return _mat_curr
        End Get
        Set(ByVal value As String)
            If Not _mat_curr = value Then
                _mat_curr = value
                Me.freight_curr = value
                Me.third_party_curr = value
                Me.repair_curr = value
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                NotifyPropertyChanged()
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("mat_curr")
            End If
        End Set
    End Property

    Private _freight_curr As String
    Public Property freight_curr() As String
        Get
            Return _freight_curr
        End Get
        Set(ByVal value As String)
            If Not _freight_curr = value Then
                _freight_curr = value
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("freight_curr")
            End If
        End Set
    End Property

    Private _3rd_party_curr As String
    Public Property third_party_curr() As String
        Get
            Return _3rd_party_curr
        End Get
        Set(ByVal value As String)
            If Not _3rd_party_curr = value Then
                _3rd_party_curr = value
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("third_party_curr")
            End If
        End Set
    End Property

    Private _repair_curr As String
    Public Property repair_curr() As String
        Get
            Return _repair_curr
        End Get
        Set(ByVal value As String)
            If Not _repair_curr = value Then
                _repair_curr = value
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("repair_curr")
            End If
        End Set
    End Property

    Private _material_markup As String
    Public Property material_markup() As String
        Get
            Return _material_markup
        End Get
        Set(ByVal value As String)
            If Not _material_markup = value Then
                _material_markup = value
                If MConstants.GLOB_APP_LOADED Then
                    master_controller.billing_cond_controller.markupValueChanged()
                End If
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("material_markup")
            End If
        End Set
    End Property

    Private _master_controller As CMasterController
    Public Property master_controller() As CMasterController
        Get
            Return _master_controller
        End Get
        Set(ByVal value As CMasterController)
            _master_controller = value
        End Set
    End Property

    'is markup capped at line level ? if yes consider value and currency
    Private _is_line_mat_markup_cap As Boolean
    Public Property is_line_mat_markup_cap() As Boolean
        Get
            Return _is_line_mat_markup_cap
        End Get
        Set(ByVal value As Boolean)
            If Not _is_line_mat_markup_cap = value Then
                _is_line_mat_markup_cap = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("is_line_mat_markup_cap")
            End If
        End Set
    End Property
    Private _line_mat_markup_cap_value As Double
    Public Property line_mat_markup_cap_value() As Double
        Get
            Return _line_mat_markup_cap_value
        End Get
        Set(ByVal value As Double)
            If Not _line_mat_markup_cap_value = value Then
                _line_mat_markup_cap_value = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("line_mat_markup_cap_value")
            End If
        End Set
    End Property
    Private _line_mat_markup_cap_curr As String
    Public Property line_mat_markup_cap_curr() As String
        Get
            Return _line_mat_markup_cap_curr
        End Get
        Set(ByVal value As String)
            If Not _line_mat_markup_cap_curr = value Then
                _line_mat_markup_cap_curr = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("line_mat_markup_cap_curr")
            End If
        End Set
    End Property

    'is markup capped at project level ? if yes consider value and currency
    Private _is_proj_mat_markup_cap As Boolean
    Public Property is_proj_mat_markup_cap() As Boolean
        Get
            Return _is_proj_mat_markup_cap
        End Get
        Set(ByVal value As Boolean)
            If Not _is_proj_mat_markup_cap = value Then
                _is_proj_mat_markup_cap = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("is_proj_mat_markup_cap")
            End If
        End Set
    End Property
    Private _proj_mat_markup_cap_value As Double
    Public Property proj_mat_markup_cap_value() As Double
        Get
            Return _proj_mat_markup_cap_value
        End Get
        Set(ByVal value As Double)
            If Not _proj_mat_markup_cap_value = value Then
                _proj_mat_markup_cap_value = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("proj_mat_markup_cap_value")
            End If
        End Set
    End Property
    Private _proj_mat_markup_cap_curr As String
    Public Property proj_mat_markup_cap_curr() As String
        Get
            Return _proj_mat_markup_cap_curr
        End Get
        Set(ByVal value As String)
            If Not _proj_mat_markup_cap_curr = value Then
                _proj_mat_markup_cap_curr = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("proj_mat_markup_cap_curr")
            End If
        End Set
    End Property

    Private _freight_rate As Double
    Public Property freight_rate() As Double
        Get
            Return _freight_rate
        End Get
        Set(ByVal value As Double)
            If Not _freight_rate = value Then
                _freight_rate = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("freight_rate")
            End If
        End Set
    End Property
    'is freight capped at line level ? if yes consider value and currency
    Private _is_line_freight_cap As Boolean
    Public Property is_line_freight_cap() As Boolean
        Get
            Return _is_line_freight_cap
        End Get
        Set(ByVal value As Boolean)
            If Not _is_line_freight_cap = value Then
                _is_line_freight_cap = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("is_line_freight_cap")
            End If
        End Set
    End Property
    Private _line_freight_cap_value As Double
    Public Property line_freight_cap_value() As Double
        Get
            Return _line_freight_cap_value
        End Get
        Set(ByVal value As Double)
            If Not _line_freight_cap_value = value Then
                _line_freight_cap_value = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("line_freight_cap_value")
            End If
        End Set
    End Property
    Private _line_freight_cap_curr As String
    Public Property line_freight_cap_curr() As String
        Get
            Return _line_freight_cap_curr
        End Get
        Set(ByVal value As String)
            If Not _line_freight_cap_curr = value Then
                _line_freight_cap_curr = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("line_freight_cap_curr")
            End If
        End Set
    End Property

    'is freight capped at project level ? if yes consider value and currency
    Private _is_proj_freight_cap As Boolean
    Public Property is_proj_freight_cap() As Boolean
        Get
            Return _is_proj_freight_cap
        End Get
        Set(ByVal value As Boolean)
            If Not _is_proj_freight_cap = value Then
                _is_proj_freight_cap = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("is_proj_freight_cap")
            End If
        End Set
    End Property
    Private _proj_freight_cap_value As Double
    Public Property proj_freight_cap_value() As Double
        Get
            Return _proj_freight_cap_value
        End Get
        Set(ByVal value As Double)
            If Not _proj_freight_cap_value = value Then
                _proj_freight_cap_value = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("proj_freight_cap_value")
            End If
        End Set
    End Property
    Private _proj_freight_cap_curr As String
    Public Property proj_freight_cap_curr() As String
        Get
            Return _proj_freight_cap_curr
        End Get
        Set(ByVal value As String)
            If Not _proj_freight_cap_curr = value Then
                _proj_freight_cap_curr = value
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("proj_freight_cap_curr")
            End If
        End Set
    End Property

    Private _gum As String
    Public Property gum() As String
        Get
            Return _gum
        End Get
        Set(ByVal value As String)
            If Not _gum = value Then
                _gum = value
                If MConstants.GLOB_APP_LOADED Then
                    master_controller.billing_cond_controller.filterGUMView()
                End If
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("gum")
            End If
        End Set
    End Property


    Private _eco_tax As String
    Public Property eco_tax() As String
        Get
            Return _eco_tax
        End Get
        Set(ByVal value As String)
            If Not _eco_tax = value Then
                _eco_tax = value
                If MConstants.GLOB_APP_LOADED Then
                    master_controller.billing_cond_controller.filterECOView()
                End If
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("eco_tax")
            End If
        End Set
    End Property

    Private _labor_rate_std As String
    Public Property labor_rate_std() As String
        Get
            Return _labor_rate_std
        End Get
        Set(ByVal value As String)
            If Not _labor_rate_std = value Then
                _labor_rate_std = value
                If MConstants.GLOB_APP_LOADED Then
                    master_controller.lab_rate_controller.filterLaborRateStd()
                End If
                NotifyPropertyChanged()
                If MConstants.GLOB_APP_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                MConstants.GLB_MSTR_CTRL.csDB.add_project_item_to_be_updated("labor_rate_std")
            End If
        End Set
    End Property
End Class

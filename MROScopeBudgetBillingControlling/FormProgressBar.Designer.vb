﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormProgressBar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.loading_prog_lbl = New System.Windows.Forms.Label()
        Me.progress_bar_new = New System.Windows.Forms.ProgressBar()
        Me.insert_new_lbl = New System.Windows.Forms.Label()
        Me.updating_existing_lbl = New System.Windows.Forms.Label()
        Me.progress_bar_update = New System.Windows.Forms.ProgressBar()
        Me.final_message_txt = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'loading_prog_lbl
        '
        Me.loading_prog_lbl.AutoSize = True
        Me.loading_prog_lbl.Location = New System.Drawing.Point(314, 9)
        Me.loading_prog_lbl.Name = "loading_prog_lbl"
        Me.loading_prog_lbl.Size = New System.Drawing.Size(170, 20)
        Me.loading_prog_lbl.TabIndex = 0
        Me.loading_prog_lbl.Text = "Loading Progression ..."
        '
        'progress_bar_new
        '
        Me.progress_bar_new.Location = New System.Drawing.Point(25, 59)
        Me.progress_bar_new.Name = "progress_bar_new"
        Me.progress_bar_new.Size = New System.Drawing.Size(863, 46)
        Me.progress_bar_new.TabIndex = 1
        '
        'insert_new_lbl
        '
        Me.insert_new_lbl.AutoSize = True
        Me.insert_new_lbl.Location = New System.Drawing.Point(21, 36)
        Me.insert_new_lbl.Name = "insert_new_lbl"
        Me.insert_new_lbl.Size = New System.Drawing.Size(101, 20)
        Me.insert_new_lbl.TabIndex = 2
        Me.insert_new_lbl.Text = "Insert New ..."
        '
        'updating_existing_lbl
        '
        Me.updating_existing_lbl.AutoSize = True
        Me.updating_existing_lbl.Location = New System.Drawing.Point(21, 124)
        Me.updating_existing_lbl.Name = "updating_existing_lbl"
        Me.updating_existing_lbl.Size = New System.Drawing.Size(149, 20)
        Me.updating_existing_lbl.TabIndex = 4
        Me.updating_existing_lbl.Text = "Updating Existing ..."
        '
        'progress_bar_update
        '
        Me.progress_bar_update.Location = New System.Drawing.Point(25, 147)
        Me.progress_bar_update.Name = "progress_bar_update"
        Me.progress_bar_update.Size = New System.Drawing.Size(863, 46)
        Me.progress_bar_update.TabIndex = 3
        '
        'final_message_txt
        '
        Me.final_message_txt.Location = New System.Drawing.Point(25, 231)
        Me.final_message_txt.Multiline = True
        Me.final_message_txt.Name = "final_message_txt"
        Me.final_message_txt.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.final_message_txt.Size = New System.Drawing.Size(863, 433)
        Me.final_message_txt.TabIndex = 5
        '
        'FormProgressBar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(905, 676)
        Me.Controls.Add(Me.final_message_txt)
        Me.Controls.Add(Me.updating_existing_lbl)
        Me.Controls.Add(Me.progress_bar_update)
        Me.Controls.Add(Me.insert_new_lbl)
        Me.Controls.Add(Me.progress_bar_new)
        Me.Controls.Add(Me.loading_prog_lbl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "FormProgressBar"
        Me.Text = "Progression"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents loading_prog_lbl As Label
    Friend WithEvents progress_bar_new As ProgressBar
    Friend WithEvents insert_new_lbl As Label
    Friend WithEvents updating_existing_lbl As Label
    Friend WithEvents progress_bar_update As ProgressBar
    Friend WithEvents final_message_txt As TextBox
End Class

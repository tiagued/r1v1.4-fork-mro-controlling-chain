﻿Public Class CAWQuotationCalucaltor
    Public previousAWQ_Before_Disc As CScopeAtivityCalcEntry
    Public thisAWQ_Bef_Disc As CScopeAtivityCalcEntry
    Public thisAWQ_Disc As CScopeAtivityCalcEntry
    Public thisAWQ_After_Disc As CScopeAtivityCalcEntry

    Public Sub New(awq_ As CAWQuotation)
        _awq = awq_
        previousAWQ_Before_Disc = New CScopeAtivityCalcEntry(CScopeActivityCalculator.EMPTY_LINE)
        thisAWQ_Bef_Disc = New CScopeAtivityCalcEntry(CScopeActivityCalculator.EMPTY_LINE)
        thisAWQ_Disc = New CScopeAtivityCalcEntry(CScopeActivityCalculator.EMPTY_LINE)
        thisAWQ_After_Disc = New CScopeAtivityCalcEntry(CScopeActivityCalculator.EMPTY_LINE)
    End Sub


    Private _awq As CAWQuotation
    Public Property awq() As CAWQuotation
        Get
            Return _awq
        End Get
        Set(ByVal value As CAWQuotation)
            _awq = value
        End Set
    End Property

    Public Sub update(Optional _payer As String = "", Optional fillLabMatList As Boolean = False)
        Try

            Dim payer As String
            If String.IsNullOrWhiteSpace(_payer) Then
                payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
            Else
                payer = _payer
            End If

            Dim listO As List(Of CScopeAtivityCalcEntry) = CAWQuotationCalucaltor.getAWQPayerSummary(_awq, payer, fillLabMatList)

            'if list is nothing, means awq values are null for this payer
            If IsNothing(listO) Then
                listO = New List(Of CScopeAtivityCalcEntry)
                listO.Add(CScopeAtivityCalcEntry.getEmpty)
                listO.Add(CScopeAtivityCalcEntry.getEmpty)
                listO.Add(CScopeAtivityCalcEntry.getEmpty)
                listO.Add(CScopeAtivityCalcEntry.getEmpty)
            End If

            If listO.Count = 4 Then
                previousAWQ_Before_Disc = listO(0)
                thisAWQ_Bef_Disc = listO(1)
                thisAWQ_Disc = listO(2)
                thisAWQ_After_Disc = listO(3)
            End If

        Catch ex As Exception
            Throw New Exception("Updating awq calculations" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Shared Function getAWQPayerSummary(awq As CAWQuotation, payer As String, Optional fillLabMatList As Boolean = False) As List(Of CScopeAtivityCalcEntry)
        Dim res As New List(Of CScopeAtivityCalcEntry)
        Try
            Dim is_not_nothing As Boolean = False

            Dim entryList As List(Of CScopeAtivityCalcEntry)

            Dim releasedAWQ As List(Of String) = awq.getPreviouApprovedRevs
            Dim _previousAWQ_Before_Disc = CScopeAtivityCalcEntry.getEmpty
            Dim _thisAWQ_Bef_Disc = CScopeAtivityCalcEntry.getEmpty
            Dim _thisAWQ_Disc = CScopeAtivityCalcEntry.getEmpty
            Dim _thisAWQ_After_Disc = CScopeAtivityCalcEntry.getEmpty

            If IsNothing(awq.master_obj) Then
                Return Nothing
            End If

            'recalculate by including lists
            If fillLabMatList Then
                For Each act As CScopeActivity In awq.master_obj.dependantActList
                    act.calculator.update(True)
                Next
            End If

            For Each act As CScopeActivity In awq.master_obj.dependantActList
                entryList = act.calculator.getEntry(payer, releasedAWQ, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                For Each entry As CScopeAtivityCalcEntry In entryList
                    If Not entry.is_labor_and_mat_text Then
                        _previousAWQ_Before_Disc.addValuesFrom(entry)
                        is_not_nothing = True
                    End If
                Next
            Next

            'current AWQ
            Dim my_entry As CScopeAtivityCalcEntry = Nothing
            For Each act As CScopeActivity In awq.master_obj.dependantActList
                my_entry = act.calculator.getEntry(payer, awq.reference_revision, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                If Not IsNothing(my_entry) AndAlso Not my_entry.is_labor_and_mat_text Then
                    _thisAWQ_Bef_Disc.addValuesFrom(my_entry)
                    is_not_nothing = True
                End If

                my_entry = act.calculator.getEntry(payer, awq.reference_revision, CScopeActivityCalculator.DISC_SUFF)
                If Not IsNothing(my_entry) AndAlso Not my_entry.is_labor_and_mat_text Then
                    _thisAWQ_Disc.addValuesFrom(my_entry)
                    is_not_nothing = True
                End If

                my_entry = act.calculator.getEntry(payer, awq.reference_revision, CScopeActivityCalculator.AFTER_DISC_SUFF)
                If Not IsNothing(my_entry) AndAlso Not my_entry.is_labor_and_mat_text Then
                    _thisAWQ_After_Disc.addValuesFrom(my_entry)
                    is_not_nothing = True
                End If
            Next

            If Not is_not_nothing Then
                Return Nothing
            End If

            res.Add(_previousAWQ_Before_Disc)
            res.Add(_thisAWQ_Bef_Disc)
            res.Add(_thisAWQ_Disc)
            res.Add(_thisAWQ_After_Disc)
        Catch ex As Exception
            Throw New Exception("Updating awq calculations" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function
End Class
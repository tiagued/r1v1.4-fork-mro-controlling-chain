﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormReportCustomerRelease
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.report_pages_group = New System.Windows.Forms.GroupBox()
        Me.select_page_nte_details_chk = New System.Windows.Forms.CheckBox()
        Me.select_page_down_payment_chk = New System.Windows.Forms.CheckBox()
        Me.select_page_awq_list_chk = New System.Windows.Forms.CheckBox()
        Me.select_page_proj_details_chk = New System.Windows.Forms.CheckBox()
        Me.select_page_proj_sum_chk = New System.Windows.Forms.CheckBox()
        Me.run_bt = New System.Windows.Forms.Button()
        Me.cancel_bt = New System.Windows.Forms.Button()
        Me.awq_settings_grp = New System.Windows.Forms.GroupBox()
        Me.awq_report_latest_only_chk = New System.Windows.Forms.CheckBox()
        Me.proj_details_settings_grp = New System.Windows.Forms.GroupBox()
        Me.proj_details_fill_corr_act_chk = New System.Windows.Forms.CheckBox()
        Me.report_payer_grp = New System.Windows.Forms.GroupBox()
        Me.cb_mat_curr = New System.Windows.Forms.ComboBox()
        Me.cb_lab_curr = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cb_payer = New System.Windows.Forms.ComboBox()
        Me.payer_lbl = New System.Windows.Forms.Label()
        Me.logs_grp = New System.Windows.Forms.GroupBox()
        Me.log_bt = New System.Windows.Forms.TextBox()
        Me.report_pages_group.SuspendLayout()
        Me.awq_settings_grp.SuspendLayout()
        Me.proj_details_settings_grp.SuspendLayout()
        Me.report_payer_grp.SuspendLayout()
        Me.logs_grp.SuspendLayout()
        Me.SuspendLayout()
        '
        'report_pages_group
        '
        Me.report_pages_group.Controls.Add(Me.select_page_nte_details_chk)
        Me.report_pages_group.Controls.Add(Me.select_page_down_payment_chk)
        Me.report_pages_group.Controls.Add(Me.select_page_awq_list_chk)
        Me.report_pages_group.Controls.Add(Me.select_page_proj_details_chk)
        Me.report_pages_group.Controls.Add(Me.select_page_proj_sum_chk)
        Me.report_pages_group.Location = New System.Drawing.Point(8, 103)
        Me.report_pages_group.Name = "report_pages_group"
        Me.report_pages_group.Size = New System.Drawing.Size(578, 150)
        Me.report_pages_group.TabIndex = 0
        Me.report_pages_group.TabStop = False
        Me.report_pages_group.Text = "Select Report Pages"
        '
        'select_page_nte_details_chk
        '
        Me.select_page_nte_details_chk.AutoSize = True
        Me.select_page_nte_details_chk.Location = New System.Drawing.Point(6, 115)
        Me.select_page_nte_details_chk.Name = "select_page_nte_details_chk"
        Me.select_page_nte_details_chk.Size = New System.Drawing.Size(83, 17)
        Me.select_page_nte_details_chk.TabIndex = 4
        Me.select_page_nte_details_chk.Text = "NTE Details"
        Me.select_page_nte_details_chk.UseVisualStyleBackColor = True
        '
        'select_page_down_payment_chk
        '
        Me.select_page_down_payment_chk.AutoSize = True
        Me.select_page_down_payment_chk.Location = New System.Drawing.Point(6, 92)
        Me.select_page_down_payment_chk.Name = "select_page_down_payment_chk"
        Me.select_page_down_payment_chk.Size = New System.Drawing.Size(139, 17)
        Me.select_page_down_payment_chk.TabIndex = 3
        Me.select_page_down_payment_chk.Text = "Project Down Payments"
        Me.select_page_down_payment_chk.UseVisualStyleBackColor = True
        '
        'select_page_awq_list_chk
        '
        Me.select_page_awq_list_chk.AutoSize = True
        Me.select_page_awq_list_chk.Location = New System.Drawing.Point(6, 68)
        Me.select_page_awq_list_chk.Name = "select_page_awq_list_chk"
        Me.select_page_awq_list_chk.Size = New System.Drawing.Size(169, 17)
        Me.select_page_awq_list_chk.TabIndex = 2
        Me.select_page_awq_list_chk.Text = "Additional Work Quotation List"
        Me.select_page_awq_list_chk.UseVisualStyleBackColor = True
        '
        'select_page_proj_details_chk
        '
        Me.select_page_proj_details_chk.AutoSize = True
        Me.select_page_proj_details_chk.Location = New System.Drawing.Point(6, 44)
        Me.select_page_proj_details_chk.Name = "select_page_proj_details_chk"
        Me.select_page_proj_details_chk.Size = New System.Drawing.Size(116, 17)
        Me.select_page_proj_details_chk.TabIndex = 1
        Me.select_page_proj_details_chk.Text = "Project Details Tab"
        Me.select_page_proj_details_chk.UseVisualStyleBackColor = True
        '
        'select_page_proj_sum_chk
        '
        Me.select_page_proj_sum_chk.AutoSize = True
        Me.select_page_proj_sum_chk.Location = New System.Drawing.Point(5, 20)
        Me.select_page_proj_sum_chk.Name = "select_page_proj_sum_chk"
        Me.select_page_proj_sum_chk.Size = New System.Drawing.Size(127, 17)
        Me.select_page_proj_sum_chk.TabIndex = 0
        Me.select_page_proj_sum_chk.Text = "Project Summary Tab"
        Me.select_page_proj_sum_chk.UseVisualStyleBackColor = True
        '
        'run_bt
        '
        Me.run_bt.Location = New System.Drawing.Point(135, 448)
        Me.run_bt.Name = "run_bt"
        Me.run_bt.Size = New System.Drawing.Size(92, 23)
        Me.run_bt.TabIndex = 1
        Me.run_bt.Text = "Run"
        Me.run_bt.UseVisualStyleBackColor = True
        '
        'cancel_bt
        '
        Me.cancel_bt.Location = New System.Drawing.Point(366, 448)
        Me.cancel_bt.Name = "cancel_bt"
        Me.cancel_bt.Size = New System.Drawing.Size(92, 23)
        Me.cancel_bt.TabIndex = 2
        Me.cancel_bt.Text = "Cancel"
        Me.cancel_bt.UseVisualStyleBackColor = True
        '
        'awq_settings_grp
        '
        Me.awq_settings_grp.Controls.Add(Me.awq_report_latest_only_chk)
        Me.awq_settings_grp.Location = New System.Drawing.Point(8, 268)
        Me.awq_settings_grp.Name = "awq_settings_grp"
        Me.awq_settings_grp.Size = New System.Drawing.Size(265, 100)
        Me.awq_settings_grp.TabIndex = 3
        Me.awq_settings_grp.TabStop = False
        Me.awq_settings_grp.Text = "Additional Work Quotation"
        '
        'awq_report_latest_only_chk
        '
        Me.awq_report_latest_only_chk.AutoSize = True
        Me.awq_report_latest_only_chk.Location = New System.Drawing.Point(7, 20)
        Me.awq_report_latest_only_chk.Name = "awq_report_latest_only_chk"
        Me.awq_report_latest_only_chk.Size = New System.Drawing.Size(161, 17)
        Me.awq_report_latest_only_chk.TabIndex = 0
        Me.awq_report_latest_only_chk.Text = "Only Latest AWQ Revision ?"
        Me.awq_report_latest_only_chk.UseVisualStyleBackColor = True
        '
        'proj_details_settings_grp
        '
        Me.proj_details_settings_grp.Controls.Add(Me.proj_details_fill_corr_act_chk)
        Me.proj_details_settings_grp.Location = New System.Drawing.Point(310, 268)
        Me.proj_details_settings_grp.Name = "proj_details_settings_grp"
        Me.proj_details_settings_grp.Size = New System.Drawing.Size(276, 100)
        Me.proj_details_settings_grp.TabIndex = 4
        Me.proj_details_settings_grp.TabStop = False
        Me.proj_details_settings_grp.Text = "Project Details"
        '
        'proj_details_fill_corr_act_chk
        '
        Me.proj_details_fill_corr_act_chk.AutoSize = True
        Me.proj_details_fill_corr_act_chk.Location = New System.Drawing.Point(7, 20)
        Me.proj_details_fill_corr_act_chk.Name = "proj_details_fill_corr_act_chk"
        Me.proj_details_fill_corr_act_chk.Size = New System.Drawing.Size(141, 17)
        Me.proj_details_fill_corr_act_chk.TabIndex = 0
        Me.proj_details_fill_corr_act_chk.Text = "Fill Correctives Actions ?"
        Me.proj_details_fill_corr_act_chk.UseVisualStyleBackColor = True
        '
        'report_payer_grp
        '
        Me.report_payer_grp.Controls.Add(Me.cb_mat_curr)
        Me.report_payer_grp.Controls.Add(Me.cb_lab_curr)
        Me.report_payer_grp.Controls.Add(Me.Label2)
        Me.report_payer_grp.Controls.Add(Me.Label1)
        Me.report_payer_grp.Controls.Add(Me.cb_payer)
        Me.report_payer_grp.Controls.Add(Me.payer_lbl)
        Me.report_payer_grp.Location = New System.Drawing.Point(8, 18)
        Me.report_payer_grp.Name = "report_payer_grp"
        Me.report_payer_grp.Size = New System.Drawing.Size(578, 58)
        Me.report_payer_grp.TabIndex = 5
        Me.report_payer_grp.TabStop = False
        Me.report_payer_grp.Text = "Report Payer"
        '
        'cb_mat_curr
        '
        Me.cb_mat_curr.FormattingEnabled = True
        Me.cb_mat_curr.Location = New System.Drawing.Point(481, 22)
        Me.cb_mat_curr.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_mat_curr.Name = "cb_mat_curr"
        Me.cb_mat_curr.Size = New System.Drawing.Size(65, 21)
        Me.cb_mat_curr.TabIndex = 6
        '
        'cb_lab_curr
        '
        Me.cb_lab_curr.FormattingEnabled = True
        Me.cb_lab_curr.Location = New System.Drawing.Point(325, 22)
        Me.cb_lab_curr.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_lab_curr.Name = "cb_lab_curr"
        Me.cb_lab_curr.Size = New System.Drawing.Size(67, 21)
        Me.cb_lab_curr.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(422, 24)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Mat Curr. :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(266, 24)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Lab Curr. :"
        '
        'cb_payer
        '
        Me.cb_payer.FormattingEnabled = True
        Me.cb_payer.Location = New System.Drawing.Point(81, 22)
        Me.cb_payer.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_payer.Name = "cb_payer"
        Me.cb_payer.Size = New System.Drawing.Size(171, 21)
        Me.cb_payer.TabIndex = 1
        '
        'payer_lbl
        '
        Me.payer_lbl.AutoSize = True
        Me.payer_lbl.Location = New System.Drawing.Point(7, 24)
        Me.payer_lbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.payer_lbl.Name = "payer_lbl"
        Me.payer_lbl.Size = New System.Drawing.Size(73, 13)
        Me.payer_lbl.TabIndex = 0
        Me.payer_lbl.Text = "Select Payer :"
        '
        'logs_grp
        '
        Me.logs_grp.Controls.Add(Me.log_bt)
        Me.logs_grp.Location = New System.Drawing.Point(8, 384)
        Me.logs_grp.Name = "logs_grp"
        Me.logs_grp.Size = New System.Drawing.Size(578, 43)
        Me.logs_grp.TabIndex = 7
        Me.logs_grp.TabStop = False
        Me.logs_grp.Text = "Logs"
        '
        'log_bt
        '
        Me.log_bt.Enabled = False
        Me.log_bt.Location = New System.Drawing.Point(5, 19)
        Me.log_bt.Name = "log_bt"
        Me.log_bt.Size = New System.Drawing.Size(566, 20)
        Me.log_bt.TabIndex = 0
        '
        'FormReportCustomerRelease
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(605, 488)
        Me.Controls.Add(Me.logs_grp)
        Me.Controls.Add(Me.report_payer_grp)
        Me.Controls.Add(Me.proj_details_settings_grp)
        Me.Controls.Add(Me.awq_settings_grp)
        Me.Controls.Add(Me.cancel_bt)
        Me.Controls.Add(Me.run_bt)
        Me.Controls.Add(Me.report_pages_group)
        Me.Name = "FormReportCustomerRelease"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.Text = "Customer Release Report"
        Me.report_pages_group.ResumeLayout(False)
        Me.report_pages_group.PerformLayout()
        Me.awq_settings_grp.ResumeLayout(False)
        Me.awq_settings_grp.PerformLayout()
        Me.proj_details_settings_grp.ResumeLayout(False)
        Me.proj_details_settings_grp.PerformLayout()
        Me.report_payer_grp.ResumeLayout(False)
        Me.report_payer_grp.PerformLayout()
        Me.logs_grp.ResumeLayout(False)
        Me.logs_grp.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents report_pages_group As GroupBox
    Friend WithEvents select_page_down_payment_chk As CheckBox
    Friend WithEvents select_page_awq_list_chk As CheckBox
    Friend WithEvents select_page_proj_details_chk As CheckBox
    Friend WithEvents select_page_proj_sum_chk As CheckBox
    Friend WithEvents run_bt As Button
    Friend WithEvents cancel_bt As Button
    Friend WithEvents awq_settings_grp As GroupBox
    Friend WithEvents awq_report_latest_only_chk As CheckBox
    Friend WithEvents proj_details_settings_grp As GroupBox
    Friend WithEvents proj_details_fill_corr_act_chk As CheckBox
    Friend WithEvents report_payer_grp As GroupBox
    Friend WithEvents cb_payer As ComboBox
    Friend WithEvents payer_lbl As Label
    Friend WithEvents cb_mat_curr As ComboBox
    Friend WithEvents cb_lab_curr As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents select_page_nte_details_chk As CheckBox
    Friend WithEvents logs_grp As GroupBox
    Friend WithEvents log_bt As TextBox
End Class

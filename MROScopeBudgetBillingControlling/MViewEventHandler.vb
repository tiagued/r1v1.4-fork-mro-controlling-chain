﻿Imports System.Reflection

Module MViewEventHandler
    'add event handler to ignore mouse wheel event on some controls like combobox
    Public Sub addDefaultComboBoxEventHandler(control As ComboBox)
        AddHandler control.MouseWheel, AddressOf IgnoreMouseWheel_OnMouseWheel
        AddHandler control.DropDownClosed, AddressOf ComboCloseFocusParent_DropDownClosed
    End Sub

    'event handler to ignore mouse wheel event on some controls like combobox
    Public Sub IgnoreMouseWheel_OnMouseWheel(sender As Object, ByVal e As MouseEventArgs)
        Try
            Dim mwe As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            mwe.Handled = True
        Catch ex As Exception
        End Try
    End Sub
    'event handler to ignore mouse wheel event on  combobox by seting focus on the parent container after combobox is closed
    Public Sub ComboCloseFocusParent_DropDownClosed(sender As Object, ByVal e As EventArgs)
        Try
            sender.Parent.Focus()
        Catch ex As Exception

        End Try
    End Sub
    'add event handler to get combobox column list in one click rather than clicking three times
    Public Sub addDefaultDatagridEventHandler(dgrid As DataGridView)
        'set default datagridview settings and add event handlers
        'do not autogenerate columns
        dgrid.AllowUserToDeleteRows = False
        dgrid.AutoGenerateColumns = False
        dgrid.AllowUserToResizeColumns = True
        dgrid.AllowUserToResizeRows = True
        dgrid.AllowUserToOrderColumns = False
        dgrid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText
        dgrid.ContextMenuStrip = MContextMenu.GENERIC_CONTEXT_MENU
        Try
            'set as double buffered
            GetType(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.SetProperty, Nothing, dgrid, New Object() {True})
        Catch ex As Exception
        End Try


        AddHandler dgrid.MouseDown, AddressOf datagridview_MouseDown
        AddHandler dgrid.CellDoubleClick, AddressOf getDoubleClickComboBoxList_CellDoubleClick
        AddHandler dgrid.EditingControlShowing, AddressOf handleEditing_EditingControlShowing
        AddHandler dgrid.CurrentCellDirtyStateChanged, AddressOf DataGridView_CurrentCellDirtyStateChanged
        AddHandler dgrid.SelectionChanged, AddressOf DataGridView_SelectionChanged
    End Sub

    'event handler to get combobox column list in one click rather than clicking three times
    Public Sub getDoubleClickComboBoxList_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Try
            If Not (e.ColumnIndex > 0 AndAlso e.RowIndex > 0) Then
                Exit Sub
            End If

            Dim dgv As DataGridView = CType(sender, DataGridView)
            If dgv(e.ColumnIndex, e.RowIndex).EditType IsNot Nothing Then
                If dgv(e.ColumnIndex, e.RowIndex).EditType.ToString() = "System.Windows.Forms.DataGridViewComboBoxEditingControl" Then
                    'send key only if enter for the first time
                    If IsNothing(dgv.EditingControl) Then
                        SendKeys.Send("{F4}")
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    'event handler catch right click that trigger context menu and  reset selection if that cell is not selected
    Public Sub datagridview_MouseDown(ByVal dgv As DataGridView, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            If e.Button = MouseButtons.Right Then
                Dim cellHit As DataGridView.HitTestInfo = dgv.HitTest(e.X, e.Y)
                If Not IsNothing(cellHit) AndAlso cellHit.Type = DataGridViewHitTestType.Cell Then
                    Dim currentCell As DataGridViewCell = dgv(cellHit.ColumnIndex, cellHit.RowIndex)
                    If Not dgv.SelectedCells.Contains(currentCell) Then
                        dgv.ClearSelection()
                        currentCell.Selected = True
                    End If
                Else
                    dgv.ClearSelection()
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    'event handler to get combobox when edited a combocolumn and add an handler to the dropdown close event
    Public Sub handleEditing_EditingControlShowing(ByVal sender As DataGridView, ByVal e As DataGridViewEditingControlShowingEventArgs)
        Try
            Dim rowObj As IGenericInterfaces.IDataGridViewEditable = Nothing
            'cancel edit if item is no more editable
            Try
                If Not IsNothing(sender.Rows(sender.CurrentCell.RowIndex).DataBoundItem) Then
                    rowObj = sender.Rows(sender.CurrentCell.RowIndex).DataBoundItem.Object
                End If
            Catch ex As Exception
            End Try
            If Not IsNothing(rowObj) Then
                If Not rowObj.isEditable.Key Then
                    MGeneralFuntionsViewControl.displayMessage("Not Editable!", rowObj.isEditable.Value, MsgBoxStyle.Critical)
                    sender.EndEdit()
                    Exit Sub
                End If
            End If

            'handle view when it is a date column
            If sender.CurrentCell.OwningColumn.ValueType = GetType(Date) Then
                If Not IsNothing(sender.CurrentCell.Value) AndAlso IsDate(sender.CurrentCell.Value) _
                    AndAlso sender.CurrentCell.Value > MConstants.APP_NULL_DATE AndAlso sender.CurrentCell.Value < Date.MaxValue Then
                    MConstants.DATE_PICKER.Value = sender.CurrentCell.Value
                    MConstants.DATE_PICKER.Checked = True
                Else
                    MConstants.DATE_PICKER.Value = Date.Today
                    MConstants.DATE_PICKER.Checked = False
                End If

                MConstants.DATE_PICKER.Parent = sender
                MConstants.DATE_PICKER.current_cell = sender.CurrentCell
                MConstants.DATE_PICKER.Location = sender.GetCellDisplayRectangle(sender.CurrentCell.ColumnIndex, sender.CurrentCell.RowIndex, False).Location
                MConstants.DATE_PICKER.Width = sender.CurrentCell.OwningColumn.Width
                MConstants.DATE_PICKER.BringToFront()
            End If
        Catch ex As Exception
        End Try
    End Sub


    'Ends Edit Mode So CellValueChanged Event Can Fire
    Private Sub DataGridView_CurrentCellDirtyStateChanged(sender As DataGridView, e As EventArgs)
        If sender.CurrentCell.OwningColumn.GetType.Equals(GetType(DataGridViewTextBoxColumn)) Then
            Exit Sub
        End If

        'for check boxes cancel edit if not editable (should be part of handleEditing_EditingControlShowing but checkbox does not trigger showedit event)
        If sender.CurrentCell.OwningColumn.GetType.Equals(GetType(DataGridViewCheckBoxColumn)) Then
            Dim rowObj As IGenericInterfaces.IDataGridViewEditable = Nothing
            'cancel edit if item is no more editable
            Try
                rowObj = sender.Rows(sender.CurrentCell.RowIndex).DataBoundItem.Object
            Catch ex As Exception
            End Try
            If Not IsNothing(rowObj) Then
                'To be done more systematically: skip modification check in the case of labor or material in an AWQ edit form
                If Not sender.FindForm().GetType Is GetType(FormAWQEdit) Then
                    If Not rowObj.isEditable.Key Then
                        MGeneralFuntionsViewControl.displayMessage("Not Editable!", rowObj.isEditable.Value, MsgBoxStyle.Critical)
                        sender.CancelEdit()
                        Exit Sub
                    End If
                End If
            End If
        End If

        'if current cell of grid is dirty, commits edit
        If sender.IsCurrentCellDirty Then
            sender.CommitEdit(DataGridViewDataErrorContexts.Commit)
            sender.EndEdit()
        End If
    End Sub
    'datagridview selection changed event
    Private Sub DataGridView_SelectionChanged(ByVal sender As DataGridView, ByVal e As EventArgs)
        Try

            Dim lbl As ToolStripStatusLabel = GLB_MSTR_CTRL.mainForm.status_strip_dgrid_selected_sum
            Dim cell_count As Integer
            Dim cell_avg_count As Integer
            Dim cell_sum As Double
            Dim cell_avg As Double

            For Each cell As DataGridViewCell In sender.SelectedCells
                cell_count = cell_count + 1
                If cell.ValueType Is GetType(Double) Then
                    cell_avg_count = cell_avg_count + 1
                    cell_sum = cell_sum + cell.Value
                End If
            Next
            If cell_avg_count > 0 Then
                cell_avg = cell_sum / cell_avg_count
            End If
            lbl.Text = "        Count: " & cell_count & "   Sum: " & cell_sum.ToString("N2") & "    Average: " & cell_avg.ToString("N2")
        Catch ex As Exception

        End Try
    End Sub

    'refresh scope dependant views when dgrid is visible. Use for instance to refresh activity list when material grid is displayed
    Public Sub addScopeListRefreshHandlerWhenVisible(dgrid As DataGridView)
        AddHandler dgrid.VisibleChanged, AddressOf datagridViewVisible_RefreshActivityDependantLists
    End Sub

    'event when a datagridview is visible and need to refresh scope activities lists
    Private Sub datagridViewVisible_RefreshActivityDependantLists(ByVal sender As Object, ByVal e As EventArgs)
        Dim dgrid As DataGridView = CType(sender, DataGridView)
        If dgrid.Visible Then
            MConstants.GLB_MSTR_CTRL.add_scope_controller.refreshScopeDependantViews()
            Try
                dgrid.DataSource.Refresh()
            Catch ex As Exception
                MGeneralFuntionsViewControl.displayMessage("Error", "An error occured while refreshing view of material list" & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    'load labor controlling view when showing the datagrid
    Public Sub laborControlLoadHandlerWhenVisible(dgrid As DataGridView)
        AddHandler dgrid.VisibleChanged, AddressOf datagridViewVisible_LoadLaborControlling
    End Sub

    'load labor controlling view
    Private Sub datagridViewVisible_LoadLaborControlling(ByVal sender As Object, ByVal e As EventArgs)
        Dim dgrid As DataGridView = CType(sender, DataGridView)
        If dgrid.Visible Then
            Try
                'activate the loading
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.LABOR_CONTROL_VIEW_LOADABLE = True
                'load
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.loadControllingModule()
            Catch ex As Exception
                MGeneralFuntionsViewControl.displayMessage("Error", "An error occured while loading labor controlling view" & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    'event that handle click on button of quote entries
    'handle set activity button quoze summary grid
    Public Sub init_scope_pricing_sum_grid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)
        Dim init_scope_pricing_sum_grid As DataGridView = CType(sender, DataGridView)
        ' Ignore clicks that are not on button cells. 
        If e.RowIndex >= 0 AndAlso init_scope_pricing_sum_grid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_QUOTE_ENTRY_SET_ACT Then
            Dim quoteE As CSalesQuoteEntry = Nothing
            Try
                quoteE = CType(init_scope_pricing_sum_grid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CSalesQuoteEntry)).Object
            Catch ex As Exception
            End Try

            If IsNothing(quoteE) OrElse quoteE.calc_is_in_error_state Then
                MGeneralFuntionsViewControl.displayMessage("Error!", "Fix All Errors in the current row before launching activity mapping", MsgBoxStyle.Critical)
            Else
                MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.launchEditForm(quoteE)
            End If
        Else
            Return
        End If
    End Sub

    'add event handler to manage datetime picker validation
    Public Sub addDefaultDateTimePickerEventHandler(dtpk As DateTimePicker)
        AddHandler dtpk.CloseUp, AddressOf DateTimePicker_CloseUp
    End Sub

    Private Sub DateTimePicker_CloseUp(sender As DateTimePicker, e As EventArgs)
        handleDateTimePickerDisplay(sender)
    End Sub

    'handle datetime picker display
    Public Sub handleDateTimePickerDisplay(sender As DateTimePicker, Optional value As Date = Nothing)
        'if data have binded and that the control has not yet been showed, sender value is always today's date. Value is used to pass the real date value
        If value = Date.MinValue OrElse IsNothing(value) Then
            value = sender.Value
        End If
        If value >= sender.MinDate And value <= sender.MaxDate And value > MConstants.APP_NULL_DATE Then
            sender.Format = DateTimePickerFormat.Custom
            sender.CustomFormat = MConstants.constantConf(MConstants.CONST_CONF_DATE_FORMAT).value
            sender.Checked = True
        Else
            sender.Format = DateTimePickerFormat.Custom
            sender.CustomFormat = " "
            sender.Checked = False
        End If
        'validate
        CEditPanelViewRowValidationHandler.validateControl(sender)
    End Sub

    'refresh AWQ View when visible
    Public Sub addAWQListRefreshHandlerWhenVisible(dgrid As DataGridView)
        AddHandler dgrid.VisibleChanged, AddressOf AWQ_datagridViewVisible_Refresh
    End Sub

    'event when a datagridview is visible and need to refresh scope activities lists
    Private Sub AWQ_datagridViewVisible_Refresh(ByVal sender As Object, ByVal e As EventArgs)
        Dim dgrid As DataGridView = CType(sender, DataGridView)
        If dgrid.Visible Then
            Try
                dgrid.DataSource.Refresh()
            Catch ex As Exception
                MGeneralFuntionsViewControl.displayMessage("Error", "An error occured while refreshing AWQ view" & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

End Module

﻿Public Class FormAWQEdit
    'navigate awq with arrows
    Private Sub awq_select_left_pic_Click(sender As Object, e As EventArgs) Handles awq_select_left_pic.Click
        GLB_MSTR_CTRL.awq_edit_ctrl.selectedAWQBackWard()
    End Sub
    'navigate awq with arrows
    Private Sub awq_select_right_pic_Click(sender As Object, e As EventArgs) Handles awq_select_right_pic.Click
        GLB_MSTR_CTRL.awq_edit_ctrl.selectedAWQForward()
    End Sub
    'awq change event
    Private Sub cb_awq_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_awq.SelectedIndexChanged
        GLB_MSTR_CTRL.awq_edit_ctrl.selectedAWQChanged()
    End Sub
    'awq error text
    Private Sub awq_error_state_pic_Click(sender As Object, e As EventArgs) Handles awq_error_state_pic.Click
        MsgBox(awq_error_state_pic.Tag)
    End Sub

    'add or remove dependant activities
    Private Sub select_act_right_Click(sender As Object, e As EventArgs) Handles select_act_right.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.addDependantActivities()
    End Sub
    'add or remove dependant activities
    Private Sub select_act_left_Click(sender As Object, e As EventArgs) Handles select_act_left.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.RemoveDependantActivities()
    End Sub
    'clear values and save when closing form
    Private Sub FormAWQEdit_FormClosed(sender As Object, e As EventArgs) Handles MyBase.FormClosed
        'MConstants.GLB_MSTR_CTRL.writeAll()
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.clearResources()
    End Sub

    'selected activity in related act list, refresh hours and material views
    'select an item among dependant activities to edit hours
    Private Sub act_dep_selected_listb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles act_dep_selected_listb.SelectedIndexChanged
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.updateHoursAndMaterialsViews()
    End Sub

    'change main activity
    Private Sub select_main_act_cb_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles select_main_act_cb.SelectionChangeCommitted
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.main_activity_changed()
    End Sub

    Private Sub add_scope_main_datagrid_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs) Handles act_add_lab_dgrid.DataError
        sender = sender
        e = e
    End Sub

    'as datetimepicker does not natively implement error validator, simulate validation when calendar closes
    Private Sub datetimepicker_val_cancel_Click(sender As PictureBox, e As EventArgs) _
        Handles offer_validity_val_cancel.Click, cust_sent_date_val_cancel.Click, cust_fdbk_due_date_val_cancel.Click,
        due_date_val_cancel.Click, issue_date_val_cancel.Click, cust_feedback_actual_val_cancel.Click
        'clear value
        Dim datePick As DateTimePicker = Nothing
        Try
            datePick = CType(sender.FindForm.Controls.Find(sender.Name.Replace("_cancel", ""), True).FirstOrDefault, DateTimePicker)
            If datePick.Enabled Then
                datePick.Value = MConstants.APP_NULL_DATE
                MViewEventHandler.handleDateTimePickerDisplay(datePick)
            End If
        Catch ex As Exception
            If IsNothing(datePick) Then
                MGeneralFuntionsViewControl.displayMessage("No DateTimePicker object linked to this cancel button", "Error!", MsgBoxStyle.Critical)
            End If
        End Try
    End Sub
    'cancel
    Private Sub cancel_bt_Click(sender As Object, e As EventArgs) Handles cancel_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.cancelAWQ()
    End Sub
    'archive
    Private Sub archive_bt_Click(sender As Object, e As EventArgs) Handles awq_archive_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.archiveAWQ()
    End Sub
    'revise
    Private Sub revise_bt_Click(sender As Object, e As EventArgs) Handles revise_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.reviseAWQ()
    End Sub
    'release
    Private Sub release_bt_Click(sender As Object, e As EventArgs) Handles release_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.releaseAWQ()
    End Sub
    'close
    Private Sub close_bt_Click(sender As Object, e As EventArgs) Handles close_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.closeAWQ()
    End Sub

    Private Sub map_sap_act_bt_Click(sender As Object, e As EventArgs) Handles map_sap_act_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.launchFakeActMap()
    End Sub

    Private Sub awq_form_bt_Click(sender As Object, e As EventArgs) Handles awq_form_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.launchAWQForm()
    End Sub

    Private Sub datetimepicker_val_cancel_Click(sender As Object, e As EventArgs) Handles offer_validity_val_cancel.Click, issue_date_val_cancel.Click, due_date_val_cancel.Click, cust_sent_date_val_cancel.Click, cust_feedback_actual_val_cancel.Click, cust_fdbk_due_date_val_cancel.Click

    End Sub
End Class
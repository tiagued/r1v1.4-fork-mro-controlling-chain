﻿Imports System.ComponentModel
Imports System.Timers

Public Class CSaveBackGroundWorker
    Public Sub New()
        _pending_exec_list = New List(Of CSaveWorkerArgs)
    End Sub

    Public Sub init()
        Try
            _worker = New BackgroundWorker
            _worker.WorkerReportsProgress = True
            _worker.WorkerSupportsCancellation = True

            AddHandler _worker.DoWork, AddressOf DoWork
            AddHandler _worker.ProgressChanged, AddressOf ProgressChanged
            AddHandler _worker.RunWorkerCompleted, AddressOf RunWorkerCompleted

            'init timer
            _auto_save_timer = New Timers.Timer(CDbl(MConstants.constantConf(MConstants.CONST_CONF_AUTO_SAVE_MODE_ELAPSE_MS).value))
            'repeat infinitely
            _auto_save_timer.AutoReset = True
            'enable 
            _auto_save_timer.Enabled = CBool(MConstants.constantConf(MConstants.CONST_CONF_AUTO_SAVE_MODE_ENABLE).value)

            AddHandler _auto_save_timer.Elapsed, AddressOf AutoSaveOnTimeEvent
        Catch ex As Exception
            Throw New Exception("An exception occured whine initializing save controller " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
    Private WithEvents _worker As BackgroundWorker
    Public ReadOnly Property NewProperty() As BackgroundWorker
        Get
            Return _worker
        End Get
    End Property

    'fifo
    Private _pending_exec_list As List(Of CSaveWorkerArgs)

    Public ReadOnly Property pending_exec_list() As List(Of CSaveWorkerArgs)
        Get
            Return _pending_exec_list
        End Get
    End Property

    'additional progress bar
    Private progBar As ProgressBar

    Private _is_user_action As Boolean
    Public Property is_user_action() As Boolean
        Get
            Return _is_user_action
        End Get
        Set(ByVal value As Boolean)
            _is_user_action = value
        End Set
    End Property

    Private _auto_save_timer As System.Timers.Timer
    Public Property auto_save_timer() As System.Timers.Timer
        Get
            Return _auto_save_timer
        End Get
        Set(ByVal value As System.Timers.Timer)
            _auto_save_timer = value
        End Set
    End Property

    Public Property auto_save_timer_enable() As Boolean
        Get
            Return _auto_save_timer.Enabled
        End Get
        Set(ByVal value As Boolean)
            If Not _auto_save_timer.Enabled = value Then
                _auto_save_timer.Enabled = value
            End If
        End Set
    End Property

    Public Property auto_save_timer_elapse() As Double
        Get
            Return _auto_save_timer.Interval
        End Get
        Set(ByVal value As Double)
            If Not _auto_save_timer.Interval = value Then
                _auto_save_timer.Interval = value
            End If
        End Set
    End Property

    'call auto save
    Private Sub AutoSaveOnTimeEvent(source As Object, e As ElapsedEventArgs)
        Me.startAsync(CSaveWorkerArgs.getInsertAll)
        Me.startAsync(CSaveWorkerArgs.getAutomaticUpdateDeleteAll)
    End Sub

    Private _is_worker_busy As Boolean
    Public ReadOnly Property is_worker_busy() As Boolean
        Get
            Return _worker.IsBusy
        End Get
    End Property

    Public Sub startAsync(arg As CSaveWorkerArgs)
        Try
            If _worker.IsBusy <> True Then
                MConstants.GLB_MSTR_CTRL.statusBar.progress_bar_visible = True
                MConstants.GLB_MSTR_CTRL.statusBar.status_strip_main_label = "Saving Data To file Ongoing..."
                ' Start the asynchronous operation.
                _worker.RunWorkerAsync(arg)
            Else
                'if busy add to queue
                If Not _pending_exec_list.Contains(arg) Then
                    _pending_exec_list.Add(arg)
                End If
            End If
        Catch ex As Exception
            'busy
        End Try
    End Sub

    Public Sub cancelAsync()
        If _worker.WorkerSupportsCancellation = True Then
            ' Cancel the asynchronous operation.
            _worker.CancelAsync()
        End If
    End Sub

    ' This event handler is where the time-consuming work is done.
    Private Sub DoWork(ByVal sender As System.Object, ByVal e As DoWorkEventArgs)
        Try
            Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)

            Dim arg As CSaveWorkerArgs = CType(e.Argument, CSaveWorkerArgs)
            DoWorkDelegate(worker, arg, e)

            'check the loop and launch
            While _pending_exec_list.Count > 0
                Dim arg_loop As CSaveWorkerArgs = _pending_exec_list(0)
                DoWorkDelegate(worker, arg_loop, e)
                _pending_exec_list.Remove(arg_loop)
                If arg_loop.calculation = CSaveWorkerArgs.WAIT_DONE Then
                    CMasterController.saverDoneEvent.Set()
                End If
            End While

        Catch ex As Exception
            Throw New Exception("An error occured while saving the data to the DB file" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Private Sub DoWorkDelegate(ByVal worker As BackgroundWorker, ByVal arg As CSaveWorkerArgs, e As DoWorkEventArgs)
        Try
            Dim hasBeenUpdated As Boolean = False

            'any secondary progress bar ?
            progBar = arg.progBar

            Me.is_user_action = arg.is_user_action

            'access db
            Dim progress_unit As Integer = 10
            Dim progress_unit_cnt As Double
            Dim processed_cnt As Integer = 0
            Dim progress_step As Integer = 1

            Dim updateAll As Boolean = False
            Dim deleteAll As Boolean = False
            Dim insertAll As Boolean = False

            If arg.calculation = CSaveWorkerArgs.UPD_DEL_ALL Then
                updateAll = True
                deleteAll = True
            ElseIf arg.calculation = CSaveWorkerArgs.INSERT_ALL Then
                insertAll = True
            End If

            'if mass update or delete
            If updateAll Then

                MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Recording Updated Items..."

                worker.ReportProgress(0)
                'copy list to avoid interferences
                Dim localListUpdate As New List(Of Object)(MConstants.GLB_MSTR_CTRL.csDB.items_to_be_updated)
                Dim localProjectListUpdate As New List(Of CViewModelMap)(MConstants.GLB_MSTR_CTRL.csDB.items_project_to_be_updated)

                progress_unit_cnt = (localListUpdate.Count + localProjectListUpdate.Count) / 10
                processed_cnt = 0
                For Each obj As Object In localListUpdate
                    If (worker.CancellationPending = True) Then
                        e.Cancel = True
                        Exit For
                    Else
                        MConstants.GLB_MSTR_CTRL.csDB.performUpdate(obj)
                        processed_cnt = processed_cnt + 1
                        If processed_cnt >= progress_unit_cnt * progress_step Then
                            If progress_step * progress_unit <= 100 Then
                                worker.ReportProgress(progress_step * progress_unit)
                            Else
                                worker.ReportProgress(100)
                            End If
                            progress_step = progress_step + 1
                        End If
                    End If

                    'remove from list
                    If MConstants.GLB_MSTR_CTRL.csDB.items_to_be_updated.Contains(obj) Then
                        MConstants.GLB_MSTR_CTRL.csDB.items_to_be_updated.Remove(obj)
                        If Not hasBeenUpdated Then
                            hasBeenUpdated = True
                        End If
                    End If
                Next

                'update project
                For Each map As CViewModelMap In localProjectListUpdate
                    If (worker.CancellationPending = True) Then
                        e.Cancel = True
                        Exit For
                    Else
                        MConstants.GLB_MSTR_CTRL.csDB.performUpdateProject(map)

                        processed_cnt = processed_cnt + 1
                        If processed_cnt >= progress_unit_cnt * progress_step Then
                            If progress_step * progress_unit <= 100 Then
                                worker.ReportProgress(progress_step * progress_unit)
                            Else
                                worker.ReportProgress(100)
                            End If
                            progress_step = progress_step + 1
                        End If
                    End If

                    'remove from list
                    If MConstants.GLB_MSTR_CTRL.csDB.items_project_to_be_updated.Contains(map) Then
                        MConstants.GLB_MSTR_CTRL.csDB.items_project_to_be_updated.Remove(map)
                        If Not hasBeenUpdated Then
                            hasBeenUpdated = True
                        End If
                    End If
                Next
            End If
            If insertAll Then
                MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Recording New Items..."
                processed_cnt = 0
                progress_unit_cnt = MConstants.GLB_MSTR_CTRL.csDB.items_to_be_inserted.Count / 10

                worker.ReportProgress(0)
                'copy list to avoid interferences
                Dim localListInsert As New List(Of Object)(MConstants.GLB_MSTR_CTRL.csDB.items_to_be_inserted)
                For Each obj As Object In localListInsert
                    If (worker.CancellationPending = True) Then
                        e.Cancel = True
                        Exit For
                    Else
                        MConstants.GLB_MSTR_CTRL.csDB.performInsert(obj)
                        processed_cnt = processed_cnt + 1
                        If processed_cnt >= progress_unit_cnt * progress_step Then
                            If progress_step * progress_unit <= 100 Then
                                worker.ReportProgress(progress_step * progress_unit)
                            Else
                                worker.ReportProgress(100)
                            End If
                            progress_step = progress_step + 1
                        End If
                    End If

                    'remove from list
                    If MConstants.GLB_MSTR_CTRL.csDB.items_to_be_inserted.Contains(obj) Then
                        MConstants.GLB_MSTR_CTRL.csDB.items_to_be_inserted.Remove(obj)
                        If Not hasBeenUpdated Then
                            hasBeenUpdated = True
                        End If
                    End If
                Next
            End If
            If deleteAll Then
                MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Recording Deleted Items..."
                processed_cnt = 0
                progress_unit_cnt = MConstants.GLB_MSTR_CTRL.csDB.items_to_be_deleted.Count / 10

                worker.ReportProgress(0)
                'copy list to avoid interferences
                Dim localListDeleted As New List(Of Object)(MConstants.GLB_MSTR_CTRL.csDB.items_to_be_deleted)
                For Each obj As Object In localListDeleted
                    If (worker.CancellationPending = True) Then
                        e.Cancel = True
                        Exit For
                    Else
                        MConstants.GLB_MSTR_CTRL.csDB.performDelete(obj)
                        processed_cnt = processed_cnt + 1
                        If processed_cnt >= progress_unit_cnt * progress_step Then
                            If progress_step * progress_unit <= 100 Then
                                worker.ReportProgress(progress_step * progress_unit)
                            Else
                                worker.ReportProgress(100)
                            End If
                            progress_step = progress_step + 1
                        End If
                    End If

                    'remove from list
                    If MConstants.GLB_MSTR_CTRL.csDB.items_to_be_deleted.Contains(obj) Then
                        MConstants.GLB_MSTR_CTRL.csDB.items_to_be_deleted.Remove(obj)
                        If Not hasBeenUpdated Then
                            hasBeenUpdated = True
                        End If
                    End If
                Next
            End If
            'do this because ReportProgress call another event thread which may loose the value of progBar object
            setAdditionalProgressBar(100)

            worker.ReportProgress(100)

            'log latest update time
            If hasBeenUpdated Then
                MConstants.GLB_MSTR_CTRL.sys_data_ctrl.latest_save_time.value = Now.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATE_TIME_FORMAT).value) & " (By " & MConstants.GLB_MSTR_CTRL.sys_data_ctrl.session_signature_log & ")"
                MConstants.GLB_MSTR_CTRL.csDB.performUpdate(MConstants.GLB_MSTR_CTRL.sys_data_ctrl.latest_save_time)
            End If

        Catch ex As Exception
            Throw New Exception("An error occured while saving the data to the DB file" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    ' This event handler updates the progress.
    Private Sub ProgressChanged(ByVal sender As System.Object, ByVal e As ProgressChangedEventArgs)
        Dim value As Integer = e.ProgressPercentage
        If e.ProgressPercentage > 100 Then
            value = 100
        End If
        MConstants.GLB_MSTR_CTRL.statusBar.progress_bar_value = value

        If Not IsNothing(progBar) Then
            setAdditionalProgressBar(value)
        End If
    End Sub

    Delegate Sub safeCallDelegate(value As Integer)
    Private Sub setAdditionalProgressBar(value As Integer)
        Try
            If Not IsNothing(progBar) Then
                If progBar.InvokeRequired Then
                    Dim deleg As New safeCallDelegate(AddressOf setAdditionalProgressBar)
                    progBar.Invoke(deleg, New Object() {value})
                Else
                    progBar.Value = value
                    Application.DoEvents()
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    ' This event handler deals with the results of the background operation.
    Private Sub RunWorkerCompleted(ByVal sender As System.Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim msgboxStyte As MsgBoxStyle = MsgBoxStyle.Information
        If e.Cancelled = True Then
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Save has been Canceled!"
            MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Tag = "Save has been cancelled"
            MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Image = My.Resources.no_errors
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_main_label = "End Save"
        ElseIf e.Error IsNot Nothing Then
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "An Error occured while saving!"
            MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Tag = "An Error occured while saving!" & Chr(10) & e.Error.Message & Chr(10) & e.Error.StackTrace
            MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Image = My.Resources.yes_errors
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_main_label = "End Save"
            msgboxStyte = MsgBoxStyle.Critical
            'display error in case of exceptions even if it is not user action
            If Not Me.is_user_action Then
                MsgBox(MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Tag, msgboxStyte)
            End If
        Else
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Save has run successfully!"
            MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Tag = "Latest Save Successfully done at " & Now.ToString("yyyy/MMM/dd HH:mm:ss")
            MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Image = My.Resources.no_errors
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_main_label = "End Save"
            MConstants.GLB_MSTR_CTRL.statusBar.latest_update_date_label = "Latest Save : " & Now.ToString("dd/MMM/yyyy HH:mm:ss")
        End If

        If Me.is_user_action Then
            MsgBox(MConstants.GLB_MSTR_CTRL.mainForm.top_menu_save_status.Tag, msgboxStyte)
        End If

        MConstants.GLB_MSTR_CTRL.statusBar.progress_bar_visible = False

    End Sub

End Class

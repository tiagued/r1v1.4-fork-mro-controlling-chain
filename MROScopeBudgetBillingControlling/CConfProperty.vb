﻿Public Class CConfProperty
    Implements IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not _is_editable Then
            res = False
            mess = "Cannot be modified"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(False, "")
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
    End Sub

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = "It is not allowed to delete LOV objects"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _key = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
    End Function

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return _db_table
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
            End If
        End Set
    End Property

    Private _db_table As String
    Public Property db_table() As String
        Get
            Return _db_table
        End Get
        Set(ByVal value As String)
            If Not _db_table = value Then
                _db_table = value
            End If
        End Set
    End Property

    Public Shared Function getEmpty()
        Dim empty = New CConfProperty
        empty.key = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        empty.value = " "
        Return empty
    End Function

    Private _key As String
    Public Property key() As String
        Get
            Return _key
        End Get
        Set(ByVal _t_value As String)
            If Not key = _t_value Then
                _key = _t_value
                'add item to be updated
                If Not String.IsNullOrWhiteSpace(_db_table) AndAlso listOfDBTblConfPropWritable.Contains(_db_table) Then
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                End If
            End If
        End Set
    End Property

    Private _value As String
    Public Property value() As String
        Get
            Return _value
        End Get
        Set(ByVal _t_value As String)
            If Not _value = _t_value Then
                _value = _t_value
                'add item to be updated
                If Not String.IsNullOrWhiteSpace(_db_table) AndAlso listOfDBTblConfPropWritable.Contains(_db_table) Then
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                End If
            End If
        End Set
    End Property

    Private _list As String
    Public Property list() As String
        Get
            Return _list
        End Get
        Set(ByVal _t_value As String)
            If Not _list = _t_value Then
                _list = _t_value
                'add item to be updated
                If Not String.IsNullOrWhiteSpace(_db_table) AndAlso listOfDBTblConfPropWritable.Contains(_db_table) Then
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                End If
            End If
        End Set
    End Property

    Private _lov_type As String
    Public Property lov_type() As String
        Get
            Return _lov_type
        End Get
        Set(ByVal value As String)
            If Not _lov_type = value Then
                _lov_type = value
                'add item to be updated
                If Not String.IsNullOrWhiteSpace(_db_table) AndAlso listOfDBTblConfPropWritable.Contains(_db_table) Then
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                End If
            End If
        End Set
    End Property

    Private _desc As String
    Public Property desc() As String
        Get
            Return _desc
        End Get
        Set(ByVal value As String)
            If Not _desc = value Then
                _desc = value
                'add item to be updated
                If Not String.IsNullOrWhiteSpace(_db_table) AndAlso listOfDBTblConfPropWritable.Contains(_db_table) Then
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                End If
            End If
        End Set
    End Property

    Private _is_editable As Boolean
    Public Property is_editable() As Boolean
        Get
            Return _is_editable
        End Get
        Set(ByVal value As Boolean)
            If Not _is_editable = value Then
                _is_editable = value
                'add item to be updated
                If Not String.IsNullOrWhiteSpace(_db_table) AndAlso listOfDBTblConfPropWritable.Contains(_db_table) Then
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                End If
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "Key: " & _key & ", Value: " & _value & ",list " & _list & ", lov type " & _lov_type
    End Function
End Class

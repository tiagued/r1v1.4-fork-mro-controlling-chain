﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTransferLabMat
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panel = New System.Windows.Forms.Panel()
        Me.material_map_dgrid = New System.Windows.Forms.DataGridView()
        Me.awq_material_dgrid = New System.Windows.Forms.DataGridView()
        Me.labor_map_dgrid = New System.Windows.Forms.DataGridView()
        Me.awq_labor_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_lab_mat_grp = New System.Windows.Forms.GroupBox()
        Me.select_dependant_act_grp = New System.Windows.Forms.GroupBox()
        Me.temp_act_lbl = New System.Windows.Forms.Label()
        Me.temp_act_val = New System.Windows.Forms.TextBox()
        Me.select_main_act_cb = New System.Windows.Forms.ComboBox()
        Me.main_act_lbl = New System.Windows.Forms.Label()
        Me.select_act_right = New System.Windows.Forms.PictureBox()
        Me.select_act_left = New System.Windows.Forms.PictureBox()
        Me.act_dep_all_listb = New System.Windows.Forms.ListBox()
        Me.act_dep_selected_listb = New System.Windows.Forms.ListBox()
        Me.awr_applicability_ly = New System.Windows.Forms.FlowLayoutPanel()
        Me.bottom_panel = New System.Windows.Forms.Panel()
        Me.cancel_bt = New System.Windows.Forms.Button()
        Me.apply_transfer_bt = New System.Windows.Forms.Button()
        Me.panel.SuspendLayout()
        CType(Me.material_map_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.awq_material_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.labor_map_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.awq_labor_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_lab_mat_grp.SuspendLayout()
        Me.select_dependant_act_grp.SuspendLayout()
        CType(Me.select_act_right, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.select_act_left, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.awr_applicability_ly.SuspendLayout()
        Me.bottom_panel.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel
        '
        Me.panel.AutoScroll = True
        Me.panel.AutoSize = True
        Me.panel.Controls.Add(Me.material_map_dgrid)
        Me.panel.Controls.Add(Me.awq_material_dgrid)
        Me.panel.Controls.Add(Me.labor_map_dgrid)
        Me.panel.Controls.Add(Me.awq_labor_dgrid)
        Me.panel.Location = New System.Drawing.Point(8, 27)
        Me.panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.panel.Name = "panel"
        Me.panel.Size = New System.Drawing.Size(2811, 854)
        Me.panel.TabIndex = 2
        '
        'material_map_dgrid
        '
        Me.material_map_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.material_map_dgrid.Location = New System.Drawing.Point(1635, 403)
        Me.material_map_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.material_map_dgrid.Name = "material_map_dgrid"
        Me.material_map_dgrid.Size = New System.Drawing.Size(1172, 446)
        Me.material_map_dgrid.TabIndex = 3
        '
        'awq_material_dgrid
        '
        Me.awq_material_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.awq_material_dgrid.Location = New System.Drawing.Point(4, 409)
        Me.awq_material_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.awq_material_dgrid.Name = "awq_material_dgrid"
        Me.awq_material_dgrid.Size = New System.Drawing.Size(1586, 440)
        Me.awq_material_dgrid.TabIndex = 2
        '
        'labor_map_dgrid
        '
        Me.labor_map_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.labor_map_dgrid.Location = New System.Drawing.Point(1635, -2)
        Me.labor_map_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.labor_map_dgrid.Name = "labor_map_dgrid"
        Me.labor_map_dgrid.Size = New System.Drawing.Size(1172, 377)
        Me.labor_map_dgrid.TabIndex = 1
        '
        'awq_labor_dgrid
        '
        Me.awq_labor_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.awq_labor_dgrid.Location = New System.Drawing.Point(4, 5)
        Me.awq_labor_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.awq_labor_dgrid.Name = "awq_labor_dgrid"
        Me.awq_labor_dgrid.Size = New System.Drawing.Size(1586, 377)
        Me.awq_labor_dgrid.TabIndex = 0
        '
        'act_lab_mat_grp
        '
        Me.act_lab_mat_grp.AutoSize = True
        Me.act_lab_mat_grp.Controls.Add(Me.panel)
        Me.act_lab_mat_grp.Location = New System.Drawing.Point(8, 554)
        Me.act_lab_mat_grp.Margin = New System.Windows.Forms.Padding(8)
        Me.act_lab_mat_grp.Name = "act_lab_mat_grp"
        Me.act_lab_mat_grp.Padding = New System.Windows.Forms.Padding(8)
        Me.act_lab_mat_grp.Size = New System.Drawing.Size(2831, 913)
        Me.act_lab_mat_grp.TabIndex = 3
        Me.act_lab_mat_grp.TabStop = False
        Me.act_lab_mat_grp.Text = "Labor And Material Applicabilities"
        '
        'select_dependant_act_grp
        '
        Me.select_dependant_act_grp.AutoSize = True
        Me.select_dependant_act_grp.Controls.Add(Me.temp_act_lbl)
        Me.select_dependant_act_grp.Controls.Add(Me.temp_act_val)
        Me.select_dependant_act_grp.Controls.Add(Me.select_main_act_cb)
        Me.select_dependant_act_grp.Controls.Add(Me.main_act_lbl)
        Me.select_dependant_act_grp.Controls.Add(Me.select_act_right)
        Me.select_dependant_act_grp.Controls.Add(Me.select_act_left)
        Me.select_dependant_act_grp.Controls.Add(Me.act_dep_all_listb)
        Me.select_dependant_act_grp.Controls.Add(Me.act_dep_selected_listb)
        Me.select_dependant_act_grp.Location = New System.Drawing.Point(4, 5)
        Me.select_dependant_act_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_dependant_act_grp.Name = "select_dependant_act_grp"
        Me.select_dependant_act_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_dependant_act_grp.Size = New System.Drawing.Size(2668, 536)
        Me.select_dependant_act_grp.TabIndex = 2
        Me.select_dependant_act_grp.TabStop = False
        Me.select_dependant_act_grp.Text = "Select Dependant Activities"
        '
        'temp_act_lbl
        '
        Me.temp_act_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.temp_act_lbl.AutoSize = True
        Me.temp_act_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_act_lbl.Location = New System.Drawing.Point(9, 36)
        Me.temp_act_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.temp_act_lbl.Name = "temp_act_lbl"
        Me.temp_act_lbl.Size = New System.Drawing.Size(146, 20)
        Me.temp_act_lbl.TabIndex = 12
        Me.temp_act_lbl.Text = "Temp. Activity  :"
        '
        'temp_act_val
        '
        Me.temp_act_val.Location = New System.Drawing.Point(186, 29)
        Me.temp_act_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.temp_act_val.Name = "temp_act_val"
        Me.temp_act_val.Size = New System.Drawing.Size(822, 26)
        Me.temp_act_val.TabIndex = 11
        '
        'select_main_act_cb
        '
        Me.select_main_act_cb.FormattingEnabled = True
        Me.select_main_act_cb.Location = New System.Drawing.Point(2152, 249)
        Me.select_main_act_cb.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_main_act_cb.Name = "select_main_act_cb"
        Me.select_main_act_cb.Size = New System.Drawing.Size(508, 28)
        Me.select_main_act_cb.TabIndex = 0
        '
        'main_act_lbl
        '
        Me.main_act_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.main_act_lbl.AutoSize = True
        Me.main_act_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.main_act_lbl.Location = New System.Drawing.Point(2318, 156)
        Me.main_act_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.main_act_lbl.Name = "main_act_lbl"
        Me.main_act_lbl.Size = New System.Drawing.Size(135, 20)
        Me.main_act_lbl.TabIndex = 9
        Me.main_act_lbl.Text = "Main Activity  :"
        '
        'select_act_right
        '
        Me.select_act_right.Cursor = System.Windows.Forms.Cursors.Hand
        Me.select_act_right.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.select_act_right.Location = New System.Drawing.Point(1040, 188)
        Me.select_act_right.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_act_right.Name = "select_act_right"
        Me.select_act_right.Size = New System.Drawing.Size(54, 49)
        Me.select_act_right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.select_act_right.TabIndex = 4
        Me.select_act_right.TabStop = False
        '
        'select_act_left
        '
        Me.select_act_left.Cursor = System.Windows.Forms.Cursors.Hand
        Me.select_act_left.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.select_act_left.Location = New System.Drawing.Point(1040, 283)
        Me.select_act_left.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_act_left.Name = "select_act_left"
        Me.select_act_left.Size = New System.Drawing.Size(54, 49)
        Me.select_act_left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.select_act_left.TabIndex = 3
        Me.select_act_left.TabStop = False
        '
        'act_dep_all_listb
        '
        Me.act_dep_all_listb.FormattingEnabled = True
        Me.act_dep_all_listb.ItemHeight = 20
        Me.act_dep_all_listb.Location = New System.Drawing.Point(9, 83)
        Me.act_dep_all_listb.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.act_dep_all_listb.Name = "act_dep_all_listb"
        Me.act_dep_all_listb.Size = New System.Drawing.Size(998, 424)
        Me.act_dep_all_listb.TabIndex = 1
        '
        'act_dep_selected_listb
        '
        Me.act_dep_selected_listb.FormattingEnabled = True
        Me.act_dep_selected_listb.ItemHeight = 20
        Me.act_dep_selected_listb.Location = New System.Drawing.Point(1132, 83)
        Me.act_dep_selected_listb.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.act_dep_selected_listb.Name = "act_dep_selected_listb"
        Me.act_dep_selected_listb.Size = New System.Drawing.Size(992, 424)
        Me.act_dep_selected_listb.TabIndex = 0
        '
        'awr_applicability_ly
        '
        Me.awr_applicability_ly.AutoScroll = True
        Me.awr_applicability_ly.Controls.Add(Me.select_dependant_act_grp)
        Me.awr_applicability_ly.Controls.Add(Me.act_lab_mat_grp)
        Me.awr_applicability_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.awr_applicability_ly.Location = New System.Drawing.Point(8, 8)
        Me.awr_applicability_ly.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.awr_applicability_ly.Name = "awr_applicability_ly"
        Me.awr_applicability_ly.Size = New System.Drawing.Size(1908, 949)
        Me.awr_applicability_ly.TabIndex = 10
        '
        'bottom_panel
        '
        Me.bottom_panel.Controls.Add(Me.cancel_bt)
        Me.bottom_panel.Controls.Add(Me.apply_transfer_bt)
        Me.bottom_panel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.bottom_panel.Location = New System.Drawing.Point(8, 957)
        Me.bottom_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.bottom_panel.Name = "bottom_panel"
        Me.bottom_panel.Size = New System.Drawing.Size(1908, 85)
        Me.bottom_panel.TabIndex = 4
        '
        'cancel_bt
        '
        Me.cancel_bt.Location = New System.Drawing.Point(1099, 28)
        Me.cancel_bt.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cancel_bt.Name = "cancel_bt"
        Me.cancel_bt.Size = New System.Drawing.Size(128, 40)
        Me.cancel_bt.TabIndex = 1
        Me.cancel_bt.Text = "Cancel"
        Me.cancel_bt.UseVisualStyleBackColor = True
        '
        'apply_transfer_bt
        '
        Me.apply_transfer_bt.Location = New System.Drawing.Point(501, 28)
        Me.apply_transfer_bt.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.apply_transfer_bt.Name = "apply_transfer_bt"
        Me.apply_transfer_bt.Size = New System.Drawing.Size(128, 40)
        Me.apply_transfer_bt.TabIndex = 0
        Me.apply_transfer_bt.Text = "Apply Transfer"
        Me.apply_transfer_bt.UseVisualStyleBackColor = True
        '
        'FormTransferLabMat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1924, 1050)
        Me.Controls.Add(Me.awr_applicability_ly)
        Me.Controls.Add(Me.bottom_panel)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "FormTransferLabMat"
        Me.Padding = New System.Windows.Forms.Padding(8)
        Me.Text = "Transfer Labor & Material"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel.ResumeLayout(False)
        CType(Me.material_map_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.awq_material_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.labor_map_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.awq_labor_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_lab_mat_grp.ResumeLayout(False)
        Me.act_lab_mat_grp.PerformLayout()
        Me.select_dependant_act_grp.ResumeLayout(False)
        Me.select_dependant_act_grp.PerformLayout()
        CType(Me.select_act_right, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.select_act_left, System.ComponentModel.ISupportInitialize).EndInit()
        Me.awr_applicability_ly.ResumeLayout(False)
        Me.awr_applicability_ly.PerformLayout()
        Me.bottom_panel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panel As Panel
    Friend WithEvents act_lab_mat_grp As GroupBox
    Friend WithEvents select_dependant_act_grp As GroupBox
    Friend WithEvents main_act_lbl As Label
    Friend WithEvents select_act_right As PictureBox
    Friend WithEvents select_act_left As PictureBox
    Friend WithEvents act_dep_all_listb As ListBox
    Friend WithEvents act_dep_selected_listb As ListBox
    Friend WithEvents awr_applicability_ly As FlowLayoutPanel
    Friend WithEvents temp_act_val As TextBox
    Friend WithEvents select_main_act_cb As ComboBox
    Friend WithEvents material_map_dgrid As DataGridView
    Friend WithEvents awq_material_dgrid As DataGridView
    Friend WithEvents labor_map_dgrid As DataGridView
    Friend WithEvents awq_labor_dgrid As DataGridView
    Friend WithEvents bottom_panel As Panel
    Friend WithEvents cancel_bt As Button
    Friend WithEvents apply_transfer_bt As Button
    Friend WithEvents temp_act_lbl As Label
End Class

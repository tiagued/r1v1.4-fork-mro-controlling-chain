﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CAWQuotationEditDelegate
    Public form As FormAWQEdit
    Private latestEditedAWQ As CAWQuotation
    Private latestUpdatedAct As CScopeActivity
    Private allActSelectedItems As List(Of CScopeActivity)
    Private depActSelectedItems As List(Of CScopeActivity)
    Private bloclActViewRefresh As Boolean
    Private blockAWQViewRefresh As Boolean
    Private load_awq_list As Boolean
    Private awq_list_bds As BindingSource
    Private awq_rev_list As List(Of CAWQuotation)
    Private awqListHandler As BindingListView(Of CAWQuotation)

    'launch form
    Public Sub launchEditForm(awq As CAWQuotation, Optional _loadList As Boolean = False)
        Try
            load_awq_list = _loadList
            latestEditedAWQ = Nothing
            latestUpdatedAct = Nothing
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "preparing view to edit awq..."

            allActSelectedItems = New List(Of CScopeActivity)
            depActSelectedItems = New List(Of CScopeActivity)

            form = New FormAWQEdit

            'draw hours grid views.
            drawAddHoursgrid()
            drawMaterialgrid()

            'cb event handler
            form.cb_awq.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            form.cb_awq.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_awq)
            'refresh cb list
            awq_list_bds = New BindingSource
            If load_awq_list Then
                'initialize activity selection combobox
                awqListHandler = New BindingListView(Of CAWQuotation)(MConstants.GLB_MSTR_CTRL.awq_controller.awq_list.DataSource)
                awqListHandler.ApplySort("reference ASC, revision ASC")
            Else
                awq_rev_list = New List(Of CAWQuotation)
                awq_list_bds.DataSource = awq_rev_list
            End If

            'set combobox list
            form.cb_awq.DataSource = awq_list_bds
            bind_awq_list(awq)

            'awq data validator
            'add error handlers
            Dim awqEditMap As CViewModelMapList
            Dim hdler As CEditPanelViewRowValidationHandler

            awqEditMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_EDIT_VIEW_DISPLAY)
            For Each map As CViewModelMap In awqEditMap.list.Values
                Dim _ctrl As Control = Nothing
                _ctrl = form.Controls.Find(map.view_control, True).FirstOrDefault
                If Not IsNothing(_ctrl) Then
                    hdler = CEditPanelViewRowValidationHandler.addNew(_ctrl, CAWQuotationRowValidator.getInstance, awqEditMap, awq.GetType, form.awq_error_state_pic, form)
                    hdler.validateAddDelegate = AddressOf Me.awq_ValidateAdd
                End If
            Next

            'fill data in views
            updateView()

            form.ShowDialog()
            form.BringToFront()
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End preparing view to edit awq"
        Catch ex As Exception
            MsgBox("An error occured while preparing the windows to edit awq " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    Private Sub bind_awq_list(awq As CAWQuotation)
        Try
            'avoid AWQ view refresh while reseting datasource to avoid stackoverflow (reset cb list > refresh view > calculate AWQ > refresh AWQ object > reset cb list)
            blockAWQViewRefresh = True
            If load_awq_list Then
                awqListHandler.Refresh()
                awq_list_bds.DataSource = MGeneralFuntionsViewControl.getAWQFromListView(awqListHandler)
                awq_list_bds.ResetBindings(False)
            Else
                awq_rev_list.Clear()
                If Not IsNothing(awq.master_obj) AndAlso awq.master_obj.revision_list.Count > 1 Then
                    For Each _awq_rev As CAWQuotation In awq.master_obj.revision_list
                        awq_rev_list.Add(_awq_rev)
                    Next
                Else
                    awq_rev_list.Add(awq)
                End If
                awq_list_bds.ResetBindings(False)
            End If
            'enable update before new AWQ
            blockAWQViewRefresh = False
            'set selected object
            form.cb_awq.SelectedItem = awq
        Catch ex As Exception
            MsgBox("An error occured while binding AWQ combo box list " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'if index changed selected activity
    Public Sub selectedAWQChanged()
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        'use this to update view only when selected activity change. cause due to object binding, when any field of actiivity is updated, all the controls are updated even the selected activity combobox
        If latestEditedAWQ IsNot Nothing AndAlso Not latestEditedAWQ.Equals(awq) Then
            updateView()
        End If

    End Sub
    'Update page
    Public Sub updateView()
        Try
            'if editing dependant activities not update
            If blockAWQViewRefresh Then
                Exit Sub
            End If

            latestUpdatedAct = Nothing
            clearResources()
            'activity details
            Dim awq As CAWQuotation = form.cb_awq.SelectedItem
            awq.updateCal()

            'bind activity data
            Dim awqEditMap As CViewModelMapList

            'enable actions
            If awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY).value Then
                form.cancel_bt.Enabled = False
                form.release_bt.Enabled = False
                form.revise_bt.Enabled = False
                form.close_bt.Enabled = False
                form.awq_archive_bt.Enabled = False
                form.map_sap_act_bt.Enabled = False
                form.awq_form_bt.Enabled = False
            Else
                If awq.is_latest_rev_calc Then
                    form.revise_bt.Enabled = True
                    'latest rev cannot be SUPERSEDED (internal status)
                    If awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value _
                        Or awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value _
                        Or awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
                        form.cancel_bt.Enabled = False
                        form.release_bt.Enabled = False
                        form.close_bt.Enabled = False

                        If awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value Then
                            form.close_bt.Enabled = True
                            form.cancel_bt.Enabled = True
                        End If
                    Else
                        form.cancel_bt.Enabled = True
                        form.release_bt.Enabled = True
                        form.close_bt.Enabled = True
                    End If
                Else
                    form.cancel_bt.Enabled = False
                    form.release_bt.Enabled = False
                    form.revise_bt.Enabled = False
                    form.close_bt.Enabled = False
                End If

                'archive only items with no closed awq
                form.awq_archive_bt.Enabled = False
                If Not IsNothing(awq.master_obj) AndAlso Not IsNothing(awq.master_obj.latest_REV) AndAlso awq.master_obj.latest_REV.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value Then
                    Dim anyClosed As Boolean = False
                    For Each awq_rev As CAWQuotation In awq.master_obj.revision_list
                        If Not Object.Equals(awq.master_obj.latest_REV, awq_rev) AndAlso awq_rev.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value Then
                            anyClosed = True
                        End If
                    Next
                    form.awq_archive_bt.Enabled = Not anyClosed
                End If
            End If


            'Bind current activity to the view
            awqEditMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_EDIT_VIEW_DISPLAY)
            updateAWQEditView(awq, awqEditMap, form)
            updateDependantActivitiesView()
            latestEditedAWQ = awq
        Catch ex As Exception
            MsgBox("An error occured while loading data of activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'update activity edit view
    Public Sub updateAWQEditView(awq As CAWQuotation, awqEditMap As CViewModelMapList, edit_form As Form)
        Try
            Dim master_awq As CAWQuotation = form.cb_awq.SelectedItem
            'dependant activities and main activities are stored on the master object, except for a new created first revision
            If Not IsNothing(awq.master_obj) Then
                master_awq = awq.master_obj
            Else
                master_awq = awq
            End If
            Dim act As CScopeActivity = master_awq.main_activity_obj

            'map fake activity button
            If Not IsNothing(act) AndAlso act.is_fake_activity Then
                form.map_sap_act_bt.Visible = True
                form.map_sap_act_bt.Enabled = True
            Else
                form.map_sap_act_bt.Enabled = False
                form.map_sap_act_bt.Visible = False
            End If

            'bind awq data
            Dim bds As BindingSource
            'Bind current awq to the view
            Dim tbbd As Binding
            For Each map As CViewModelMap In awqEditMap.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim textBox As TextBox
                    textBox = CType(edit_form.Controls.Find(map.view_control, True).FirstOrDefault, TextBox)
                    If Not IsNothing(textBox.DataBindings) Then
                        textBox.DataBindings.Clear()
                    End If
                    tbbd = textBox.DataBindings.Add("Text", awq, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    'format double
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        tbbd.FormatInfo = MConstants.CULT_INFO
                        tbbd.FormatString = map.col_format
                        tbbd.FormattingEnabled = True
                    End If
                    If Not awq.is_latest_rev_calc OrElse Not awq.is_in_work Then
                        textBox.Enabled = False
                    Else
                        textBox.Enabled = map.col_editable
                    End If
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim comboBox As ComboBox
                    comboBox = CType(edit_form.Controls.Find(map.view_control, True).FirstOrDefault, ComboBox)
                    If Not IsNothing(comboBox.DataBindings) Then
                        comboBox.DataBindings.Clear()
                    End If
                    comboBox.AutoCompleteMode = AutoCompleteMode.None
                    comboBox.DropDownStyle = ComboBoxStyle.DropDownList
                    If Not awq.is_latest_rev_calc OrElse Not awq.is_in_work Then
                        comboBox.Enabled = False
                    Else
                        comboBox.Enabled = map.col_editable
                    End If
                    MViewEventHandler.addDefaultComboBoxEventHandler(comboBox)

                    bds = New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "value"
                        comboBox.ValueMember = "key"
                        comboBox.DataBindings.Add("SelectedValue", awq, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "value"
                        comboBox.ValueMember = "key"
                        comboBox.DataBindings.Add("SelectedValue", awq, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    Else
                        Throw New Exception("Error occured when binding AWQ view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim chk As CheckBox
                    chk = CType(edit_form.Controls.Find(map.view_control, True).FirstOrDefault, CheckBox)
                    If Not IsNothing(chk.DataBindings) Then
                        chk.DataBindings.Clear()
                    End If
                    chk.DataBindings.Add("Checked", awq, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    If Not awq.is_latest_rev_calc OrElse Not awq.is_in_work Then
                        chk.Enabled = False
                    Else
                        chk.Enabled = map.col_editable
                    End If
                    'always alow PM Go Ahead
                    If map.class_field = "is_pm_go_ahead" Then
                        chk.Enabled = True
                    End If
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_DATE).value Then
                        Dim datePick As DateTimePicker
                    datePick = CType(edit_form.Controls.Find(map.view_control, True).FirstOrDefault, DateTimePicker)
                    If Not IsNothing(datePick.DataBindings) Then
                        datePick.DataBindings.Clear()
                    End If
                    datePick.DataBindings.Add("Value", awq, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    If Not awq.is_latest_rev_calc OrElse Not awq.is_in_work Then
                        datePick.Enabled = False
                    Else
                        datePick.Enabled = map.col_editable
                    End If
                    Dim value As Date
                    'validate picker
                    value = CallByName(awq, map.class_field, CallType.Get)
                    MViewEventHandler.handleDateTimePickerDisplay(datePick, value)
                    MViewEventHandler.addDefaultDateTimePickerEventHandler(datePick)
                End If
            Next

            'if released and not yet closed allow customer feedback field
            If awq.is_latest_rev_calc And awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value Then
                form.customer_comment_val.Enabled = True
                form.cust_feedback_actual_val.Enabled = True
                form.cust_feedback_val.Enabled = True
            End If

            'DP Lab & Mat always editable
            form.dp_labor_val.Enabled = True
            form.dp_material_val.Enabled = True

        Catch ex As Exception
            MsgBox("An error occured while loading data of AWQ " & awq.reference_revision & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub updateDependantActivitiesView()
        Dim bds As New BindingSource
        Dim sortHandler As BindingListView(Of CScopeActivity)
        Try
            Dim awq As CAWQuotation = form.cb_awq.SelectedItem
            Dim master_awq As CAWQuotation = form.cb_awq.SelectedItem
            'dependant activities and main activities are stored on the master object, except for a new created first revision
            If Not IsNothing(awq.master_obj) Then
                master_awq = awq.master_obj
            Else
                master_awq = awq
            End If

            'editable
            If Not awq.is_latest_rev_calc OrElse Not awq.is_in_work OrElse master_awq.main_activity_obj.is_fake_activity Then
                form.select_act_right.Enabled = False
                form.select_act_left.Enabled = False
                form.select_main_act_cb.Enabled = False
            Else
                form.select_act_right.Enabled = True
                form.select_act_left.Enabled = True
                If awq.revision > 0 Then
                    form.select_main_act_cb.Enabled = False
                Else
                    form.select_main_act_cb.Enabled = True
                End If
            End If

            'potentials act
            'fill all activities view
            sortHandler = New BindingListView(Of CScopeActivity)(GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource)
            bds = New BindingSource
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterAllPotentialDependantAct)
            sortHandler.ApplySort("act_notif_opcode ASC")
            bds.DataSource = MGeneralFuntionsViewControl.getActFromListView(sortHandler)
            form.act_dep_all_listb.DataSource = bds
            form.act_dep_all_listb.SelectionMode = SelectionMode.MultiExtended
            form.act_dep_all_listb.HorizontalScrollbar = True
            form.act_dep_all_listb.SelectedItems.Clear()
            'pre select act
            For Each act As CScopeActivity In allActSelectedItems
                form.act_dep_all_listb.SelectedItems.Add(act)
            Next

            allActSelectedItems.Clear()
            form.act_dep_all_listb.Refresh()

            'current selected act
            'block sold labor and material refresh to avoid several calls when selecting activites
            bloclActViewRefresh = True
            sortHandler = New BindingListView(Of CScopeActivity)(master_awq.dependantActList)
            bds = New BindingSource
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterCurrentDependantAct)
            sortHandler.ApplySort("act_notif_opcode ASC")
            Dim listO As List(Of CScopeActivity) = MGeneralFuntionsViewControl.getActFromListView(sortHandler)
            bds.DataSource = listO
            form.act_dep_selected_listb.DataSource = bds
            form.act_dep_selected_listb.SelectionMode = SelectionMode.MultiExtended
            form.act_dep_selected_listb.HorizontalScrollbar = True
            form.act_dep_selected_listb.SelectedItems.Clear()

            'pre select act
            For Each act As CScopeActivity In depActSelectedItems
                form.act_dep_selected_listb.SelectedItems.Add(act)
            Next
            depActSelectedItems.Clear()
            form.act_dep_selected_listb.Refresh()
            'deblock row refresh

            bloclActViewRefresh = False

            'primary activity
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterCurrentDependantActPotentialMain)
            sortHandler.ApplySort("act_notif_opcode ASC")
            listO = MGeneralFuntionsViewControl.getActFromListView(sortHandler)

            form.select_main_act_cb.Text = ""
            bds = New BindingSource
            bds.DataSource = listO
            form.select_main_act_cb.DataSource = bds
            form.select_main_act_cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            'set current value. if null put first list value
            If IsNothing(master_awq.main_activity_obj) Then
                'if no main activity set, take first one
                If listO.Count > 0 Then
                    form.select_main_act_cb.SelectedItem = listO(0)
                    master_awq.main_activity_obj = listO(0)
                End If
            Else
                form.select_main_act_cb.SelectedItem = master_awq.main_activity_obj
            End If

            'update hours view
            updateHoursAndMaterialsViews()

        Catch ex As Exception
            MsgBox("An error occured while loading data of dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'filter potential dependant activities
    Private Function filterAllPotentialDependantAct(act As CScopeActivity) As Boolean
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        If (act.awq_master_id <= 0) And act.system_status <> MConstants.CONST_SYS_ROW_DELETED And Not act.is_fake_activity Then
            'case main activitity has been set 
            If Not IsNothing(awq.main_activity_obj) AndAlso awq.main_activity_obj.Equals(act) Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
    End Function
    'filter current dependant activities
    Private Function filterCurrentDependantAct(act As CScopeActivity) As Boolean
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        If act.awq_master_id = awq.master_id And act.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
            Return True
        Else
            'case main activitity has been set 
            If Not IsNothing(awq.main_activity_obj) AndAlso awq.main_activity_obj.Equals(act) Then
                Return True
            Else
                Return False
            End If
        End If
    End Function
    'filter potential primary activities activities
    Private Function filterCurrentDependantActPotentialMain(act As CScopeActivity) As Boolean
        If filterCurrentDependantAct(act) AndAlso Not act.is_cost_collector Then
            Return True
        Else
            Return False
        End If
    End Function

    'navigate forward awq
    Public Sub selectedAWQForward()
        If Not form.cb_awq.SelectedIndex + 1 > form.cb_awq.Items.Count - 1 Then
            form.cb_awq.SelectedIndex = form.cb_awq.SelectedIndex + 1
        End If
    End Sub
    'navigate backward awq
    Public Sub selectedAWQBackWard()
        If Not form.cb_awq.SelectedIndex - 1 < 0 Then
            form.cb_awq.SelectedIndex = form.cb_awq.SelectedIndex - 1
        End If
    End Sub

    'add activities to the quote
    Public Sub addDependantActivities()
        Try
            Dim awq As CAWQuotation = form.cb_awq.SelectedItem
            If Not awq.id > 0 Then
                MGeneralFuntionsViewControl.displayMessage("AWQ in Error State!", "Cannot perform action. Please fix all errors before starting activities mapping", MsgBoxStyle.Critical)
                Exit Sub
            End If
            For Each act As CScopeActivity In form.act_dep_all_listb.SelectedItems
                'if it is the first time that activities are binded to the quote, define a primary activity to avoid error in activity price caluclation (seting the product sum will triger calculation on act that need primary act)
                If IsNothing(awq.main_activity_obj) AndAlso Not act.is_cost_collector Then
                    awq.main_activity_obj = act
                End If

                act.awq_master_obj = awq.master_obj
                awq.master_obj.dependantActList.Add(act)
                act.updateCal()
                depActSelectedItems.Add(act)
            Next
            updateDependantActivitiesView()
        Catch ex As Exception
            MsgBox("An error occured while adding data to dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'remove activities from the quote
    Public Sub RemoveDependantActivities()
        Try
            Dim awq As CAWQuotation = form.cb_awq.SelectedItem
            latestUpdatedAct = Nothing
            If Not awq.id > 0 Then
                MGeneralFuntionsViewControl.displayMessage("AWQ in Error State!", "Cannot perform action. Please fix all errors before starting activities mapping", MsgBoxStyle.Critical)
                Exit Sub
            End If
            For Each act As CScopeActivity In form.act_dep_selected_listb.SelectedItems
                If awq.master_obj.main_activity_id = act.id Then
                    MsgBox("Cannot remove activity " & act.tostring & " because it is set as primary activity")
                ElseIf awq.isActUsed(act) Then
                    MsgBox("Cannot remove activity " & act.tostring & " because it is already used in this AWQ")
                Else
                    awq.master_obj.dependantActList.Remove(act)
                    act.awq_master_obj = Nothing
                    act.updateCal()
                    allActSelectedItems.Add(act)
                End If
            Next
            updateDependantActivitiesView()
        Catch ex As Exception
            MsgBox("An error occured while removing data from dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'update calculation view
    Public Sub updateHoursAndMaterialsViews()
        Try
            'if editing dependant activities not update
            If bloclActViewRefresh Then
                Exit Sub
            End If
            'if no act selected block view edit
            If form.act_dep_selected_listb.SelectedItems.Count = 0 Then
                'reset views
                form.act_add_lab_dgrid.DataSource = Nothing
                form.act_materials_dgrid.DataSource = Nothing
                Exit Sub
            End If
            Dim act As CScopeActivity = form.act_dep_selected_listb.SelectedItems(0)
            If Not IsNothing(latestUpdatedAct) AndAlso latestUpdatedAct.Equals(act) Then
                Exit Sub
            End If
            latestUpdatedAct = act

            Dim awq As CAWQuotation = form.cb_awq.SelectedItem

            'fill views
            'editable ?
            If Not awq.is_latest_rev_calc OrElse Not awq.is_in_work Then
                form.act_add_lab_dgrid.Columns(MConstants.CONST_CONF_AWQ_ADD_LABMAT).ReadOnly = True
                form.act_add_lab_dgrid.Columns(MConstants.CONST_CONF_AWQ_REMOVE_LABMAT).ReadOnly = True
                form.act_materials_dgrid.Columns(MConstants.CONST_CONF_AWQ_ADD_LABMAT).ReadOnly = True
                form.act_materials_dgrid.Columns(MConstants.CONST_CONF_AWQ_REMOVE_LABMAT).ReadOnly = True
            Else
                form.act_add_lab_dgrid.Columns(MConstants.CONST_CONF_AWQ_ADD_LABMAT).ReadOnly = False
                form.act_add_lab_dgrid.Columns(MConstants.CONST_CONF_AWQ_REMOVE_LABMAT).ReadOnly = False
                form.act_materials_dgrid.Columns(MConstants.CONST_CONF_AWQ_ADD_LABMAT).ReadOnly = False
                form.act_materials_dgrid.Columns(MConstants.CONST_CONF_AWQ_REMOVE_LABMAT).ReadOnly = False
            End If

            Dim addView As New BindingListView(Of CSoldHours)(act.sold_hour_list)
            addView.ApplyFilter(AddressOf filterAddScope)
            form.act_add_lab_dgrid.DataSource = addView
            form.act_add_lab_dgrid.Refresh()

            Dim matView As New BindingListView(Of CMaterial)(act.material_list)
            matView.ApplyFilter(AddressOf filterAddMat)
            form.act_materials_dgrid.DataSource = matView
            form.act_materials_dgrid.Refresh()

        Catch ex As Exception
            MsgBox("An error occured while loading labor and materials of the selected activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    'filter add materials
    Public Function filterAddMat(ByVal mat As CMaterial)
        'delegate for filtering
        If mat.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf IsNothing(mat.init_quote_entry_obj) Then
            Return True
        Else
            Return False
        End If
    End Function

    'filter add scope
    Private Function filterAddScope(sold_hr As CSoldHours) As Boolean
        If sold_hr.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf IsNothing(sold_hr.init_quote_entry_obj) Then
            Return True
        Else
            Return False
        End If
    End Function

    'validate new
    Public Sub awq_ValidateAdd(awq As CAWQuotation)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(awq) Then
                'if new item
                awq.setID()
                'master id
                'same code handle in setID
                If IsNothing(awq.master_obj) OrElse awq.master_id <= 0 Then
                    'first rev
                    awq.master_obj = awq
                    awq.latest_REV = awq
                    awq.setReference()
                    'set data on activity
                    For Each act As CScopeActivity In awq.dependantActList
                        act.awq_master_obj = awq.master_obj
                    Next

                End If
                'revision list
                awq.master_obj.revision_list.Add(awq)

                'add to list
                MConstants.GLB_MSTR_CTRL.awq_controller.awq_list.DataSource.Add(awq)
                MConstants.GLB_MSTR_CTRL.awq_controller.awq_list.Refresh()
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new material entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'handle check box event
    Private Sub DataGridView_CurrentCellDirtyStateChanged(sender As DataGridView, e As EventArgs)
        'if current cell of grid is dirty, commits edit
        If sender.IsCurrentCellDirty Then
            sender.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    'change event cannot be handled in dirty cell state change becase changes are not yet commited
    Private Sub DataGridView_CellValueChanged(ByVal sender As DataGridView, ByVal e As DataGridViewCellEventArgs)
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        Dim chkb As DataGridViewCheckBoxCell
        Dim chck_value As Boolean
        Dim cancelMapping As Boolean
        Dim currentCell As DataGridViewCell = sender.Rows(e.RowIndex).Cells(e.ColumnIndex)
        If currentCell.OwningColumn.Name = MConstants.CONST_CONF_AWQ_ADD_LABMAT Then
            chkb = CType(currentCell, DataGridViewCheckBoxCell)
            chck_value = Not chkb.Value
            cancelMapping = False
        ElseIf currentCell.OwningColumn.Name = MConstants.CONST_CONF_AWQ_REMOVE_LABMAT Then
            chkb = CType(currentCell, DataGridViewCheckBoxCell)
            chck_value = Not chkb.Value
            cancelMapping = True
        Else
            Exit Sub
        End If

        'try to cast databound object
        Dim rowObj As Object = Nothing
        Try
            rowObj = sender.Rows(e.RowIndex).DataBoundItem.Object
        Catch ex As Exception
        End Try

        'if no bounded object, end
        If IsNothing(rowObj) Then
            Exit Sub
        End If
        If rowObj.GetType Is GetType(CSoldHours) Then
            'check if item could be edited
            Dim soldH As CSoldHours = CType(rowObj, CSoldHours)
            If Not cancelMapping Then
                'try to add/remove lab to awq
                If Not IsNothing(soldH.awq_obj) AndAlso soldH.awq_id <> awq.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to change AWQ content from a former revision", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            Else
                'cancelling a previous item
                If Not IsNothing(soldH.remove_awq_obj) AndAlso soldH.remove_awq_obj.id <> awq.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to change AWQ content from a former revision", MsgBoxStyle.Critical)
                    Exit Sub
                ElseIf IsNothing(soldH.awq_obj) OrElse soldH.awq_obj.id >= awq.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to cancel an item not bound to a previous revision", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            End If
            setLaborAWQ(chck_value, soldH, cancelMapping)
        ElseIf rowObj.GetType Is GetType(CMaterial) Then
            'check if item could be edited
            Dim mat As CMaterial = CType(rowObj, CMaterial)
            If Not cancelMapping Then
                'try to add/remove lab to awq
                If Not IsNothing(mat.awq_obj) AndAlso mat.awq_id <> awq.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to change AWQ content from a former revision", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            Else
                'cancelling a previous item
                If Not IsNothing(mat.remove_awq_obj) AndAlso mat.remove_awq_obj.id <> awq.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to change AWQ content from a former revision", MsgBoxStyle.Critical)
                    Exit Sub
                ElseIf IsNothing(mat.awq_obj) OrElse mat.awq_obj.id >= awq.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to cancel an item not bound to a previous revision", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            End If
            setMaterialAWQ(chck_value, mat, cancelMapping)
        End If

    End Sub
    'clear resources
    Public Sub clearResources()
        'clear bindings
        Dim awqEditMap As CViewModelMapList = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_EDIT_VIEW_DISPLAY)
        For Each map As CViewModelMap In awqEditMap.list.Values
            Dim ctrl As Control
            ctrl = form.Controls.Find(map.view_control, True).FirstOrDefault
            If Not IsNothing(ctrl.DataBindings) Then
                ctrl.DataBindings.Clear()
            End If
        Next
    End Sub

    'set applicability for sold hours
    Public Sub setLaborAWQ(checkValue As Boolean, soldH As CSoldHours, cancelItem As Boolean)
        'if value is checked, items is added, if not checked item is deleted
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        If awq.is_latest_rev_calc Then
            If Not cancelItem Then
                If checkValue Then
                    soldH.awq_id = awq.id
                    soldH.is_reported = True
                    If Not awq.sold_hour_list.Contains(soldH) Then
                        awq.sold_hour_list.Add(soldH)
                    End If
                Else
                    If soldH.awq_id = awq.id Then
                        soldH.awq_obj = Nothing
                        If soldH.act_object.is_cost_collector Then
                            soldH.is_reported = False
                        End If
                    End If
                    If awq.sold_hour_list.Contains(soldH) Then
                        awq.sold_hour_list.Remove(soldH)
                    End If
                End If
            Else
                If checkValue Then
                    soldH.remove_awq_id = awq.id
                    If Not awq.sold_hour_removed_list.Contains(soldH) Then
                        awq.sold_hour_removed_list.Add(soldH)
                    End If
                Else
                    If soldH.remove_awq_id = awq.id Then
                        soldH.remove_awq_obj = Nothing
                    End If
                    If awq.sold_hour_removed_list.Contains(soldH) Then
                        awq.sold_hour_removed_list.Remove(soldH)
                    End If
                End If
            End If
            awq.updateCal()
        End If
    End Sub
    'set applicability for sold hours
    Public Sub setMaterialAWQ(checkValue As Boolean, mat As CMaterial, cancelItem As Boolean)
        'if value is checked, items is added, if not checked item is deleted
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        If awq.is_latest_rev_calc Then
            If Not cancelItem Then
                If checkValue Then
                    mat.awq_id = awq.id
                    mat.is_reported = True
                    If Not awq.material_list.Contains(mat) Then
                        awq.material_list.Add(mat)
                    End If
                Else
                    If mat.awq_id = awq.id Then
                        mat.awq_obj = Nothing
                        If mat.activity_obj.is_cost_collector Then
                            mat.is_reported = False
                        End If
                    End If
                    If awq.material_list.Contains(mat) Then
                        awq.material_list.Remove(mat)
                    End If
                End If
            Else
                If checkValue Then
                    mat.remove_awq_id = awq.id
                    If Not awq.material_removed_list.Contains(mat) Then
                        awq.material_removed_list.Add(mat)
                    End If
                Else
                    If mat.remove_awq_id = awq.id Then
                        mat.remove_awq_obj = Nothing
                    End If
                    If awq.material_removed_list.Contains(mat) Then
                        awq.material_removed_list.Remove(mat)
                    End If
                End If
            End If
            awq.updateCal()
        End If
    End Sub
    'main activity changed
    Public Sub main_activity_changed()
        Try
            Dim act As CScopeActivity = form.select_main_act_cb.SelectedItem
            Dim previousMainAct As CScopeActivity
            Dim awq As CAWQuotation = form.cb_awq.SelectedItem
            If awq.revision > 0 Then
                Exit Sub
            End If

            previousMainAct = awq.main_activity_obj
            awq.main_activity_obj = act
            awq.setReference()
            'update calc
            act.updateCal()
            If Not IsNothing(previousMainAct) Then
                previousMainAct.updateCal()
            End If
            form.act_dep_selected_listb.SelectedItems.Clear()
            'will trigger hours grid refresh
            form.act_dep_selected_listb.SelectedItem = act
        Catch ex As Exception
            MsgBox("An error occured while setting the new primary activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'release AWQ
    Public Sub releaseAWQ()
        'release AWQ
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        If Not awq.is_latest_rev_calc Then
            Exit Sub
        End If
        Try
            Dim dialogRes As DialogResult
            dialogRes = MessageBox.Show("This action will freeze the AWQ and will not be editable anymore. Are you sure you want to release this AWQ revision ? " & Chr(10) & "Click OK to continue, or Cancel to leave.", "Confirm AWQ release", MessageBoxButtons.OKCancel)
            If dialogRes = DialogResult.Cancel Then
                Exit Sub
            End If

            'set status before to avoid error on errorprodivder side when checking is status equals system status
            awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value

            awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value
            awq.cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_PENDING_KEY).value
            If CEditPanelViewRowValidationHandler.validateControl(form.internal_status_val) Then
                awq.cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_PENDING_KEY).value
                awq.master_obj.main_activity_obj.updateCal()
                MGeneralFuntionsViewControl.displayMessage("Release Success", "AWQ Revision has been successfully released !", MsgBoxStyle.Information)
                'force update to freeze data
                latestUpdatedAct = Nothing
                updateView()
            Else
                awq.system_status = ""
                MGeneralFuntionsViewControl.displayMessage("Release Failed", "AWQ Revision release has failed. Please fill all required fields and try again !", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            If Not IsNothing(awq) Then
                awq.system_status = ""
            End If
            MsgBox("An error occured while releasing AWQ " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try

    End Sub

    'release AWQ
    Public Sub closeAWQ()
        'release AWQ
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        If Not awq.is_latest_rev_calc Then
            Exit Sub
        End If
        Try
            Dim dialogRes As DialogResult
            dialogRes = MessageBox.Show("This action will approve the AWQ and make its figures visible in the customer statement. Are you sure you want to close this AWQ revision  ? " & Chr(10) & "Click OK to continue, or Cancel to leave.", "Confirm AWQ Closure", MessageBoxButtons.OKCancel)
            If dialogRes = DialogResult.Cancel Then
                Exit Sub
            End If

            'set status before to avoid error on errorprodivder side when checking is status equals system status
            awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value

            awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value
            awq.cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY).value
            If CEditPanelViewRowValidationHandler.validateControl(form.internal_status_val) Then
                'cust feedback
                awq.cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY).value
                MGeneralFuntionsViewControl.displayMessage("Close Success", "AWQ Revision has been successfully closed !", MsgBoxStyle.Information)
                'update main act calculate
                awq.master_obj.main_activity_obj.updateCal()
                'force update to freeze data
                latestUpdatedAct = Nothing
                updateView()
            Else
                awq.system_status = ""
                MGeneralFuntionsViewControl.displayMessage("Closure Failed", "AWQ Closure release has failed. Please fill all required fields and try again !", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            If Not IsNothing(awq) Then
                awq.system_status = ""
            End If
            MsgBox("An error occured while closing AWQ " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try

    End Sub

    'cancel AWQ
    Public Sub cancelAWQ()
        'cancel AWQ
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        If Not awq.is_latest_rev_calc Then
            Exit Sub
        End If
        Try
            Dim dialogRes As DialogResult
            dialogRes = MessageBox.Show("This action will freeze the AWQ and will not be editable anymore. Are you sure you want to cancel this AWQ revision ? " & Chr(10) & "Click OK to continue, or Cancel to leave.", "Confirm AWQ cancellation", MessageBoxButtons.OKCancel)
            If dialogRes = DialogResult.Cancel Then
                Exit Sub
            End If
            If awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value Then
                awq.cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_DECLINED_KEY).value
            End If
            awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value
            'set status before to avoid error on errorprodivder side when checking is status equals system status
            awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value
            'remove binded items
            awq.clearAWQBindedItems()
            'force update to freeze data
            latestUpdatedAct = Nothing
            awq.master_obj.main_activity_obj.updateCal()
            updateView()
            MGeneralFuntionsViewControl.displayMessage("Cancel Operation Successfully", "AWQ Revision has been successfully cancelled !", MsgBoxStyle.Information)

        Catch ex As Exception
            If Not IsNothing(awq) Then
                awq.system_status = ""
            End If
            MsgBox("An error occured while canceling AWQ " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'cancel AWQ
    Public Sub archiveAWQ()
        'archive AWQ
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        Try
            Dim dialogRes As DialogResult
            dialogRes = MessageBox.Show("This action will definitively cancel the AWQ and all related revisions and will not be editable anymore. Are you sure you want to archive this AWQ ? " & Chr(10) & "Click OK to continue, or Cancel to leave.", "Confirm AWQ archived", MessageBoxButtons.OKCancel)
            If dialogRes = DialogResult.Cancel Then
                Exit Sub
            End If

            If Not IsNothing(awq.master_obj) Then
                For Each act As CScopeActivity In awq.master_obj.dependantActList
                    act.awq_master_obj = Nothing
                Next
                awq.master_obj.main_activity_obj = Nothing
                awq.master_obj.dependantActList.Clear()
            End If
            awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY).value
            awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY).value

            MGeneralFuntionsViewControl.displayMessage("Archive Operation Successfully", "AWQ and all related revisions have been successfully archived !", MsgBoxStyle.Information)

        Catch ex As Exception
            If Not IsNothing(awq) Then
                awq.system_status = ""
            End If
            MsgBox("An error occured while archiving AWQ " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'revise AWQ
    Public Sub reviseAWQ()
        Dim awq As CAWQuotation = form.cb_awq.SelectedItem
        Try
            Dim dialogRes As DialogResult
            Dim message As String
            If Not awq.is_latest_rev_calc Then
                Exit Sub
            End If
            Dim is_replaced As Boolean
            If awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value Or
            awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
                message = "This action will create a new AWQ revision. Are you sure you want to create a new revision ?"
            Else
                message = "The current AWQ revision has not been closed. Creating a new revision will transfer current changes to the new one. Are you sure you want to create a new revision ?"
                is_replaced = True
            End If
            dialogRes = MessageBox.Show(message & Chr(10) & "Click OK to continue, or Cancel to leave.", "Confirm AWQ Revision", MessageBoxButtons.OKCancel)
            If dialogRes = DialogResult.Cancel Then
                Exit Sub
            End If
            'new rev AWQ
            If is_replaced Then
                If awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value Then
                    awq.cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_SUPERS_REVISED_KEY).value
                End If

                awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_SUPERS_REVISED_KEY).value
                'set status before to avoid error on errorprodivder side when checking is status equals system status
                awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_SUPERS_REVISED_KEY).value
            End If
            Dim newRev As New CAWQuotation
            newRev.master_obj = awq.master_obj
            newRev.reference = awq.reference
            newRev.revision = awq.revision + 1
            newRev.comments = awq.comments
            newRev.description = awq.description
            newRev.ground_time_unit = awq.ground_time_unit
            newRev.ground_time = awq.ground_time
            newRev.airworthiness = awq.airworthiness
            newRev.weight_and_balance = awq.weight_and_balance
            newRev.prioriry = awq.prioriry
            newRev.due_date = awq.due_date
            newRev.pricing = awq.pricing
            newRev.payment_terms = awq.payment_terms

            awq_ValidateAdd(newRev)
            newRev.master_obj.latest_REV = newRev

            If is_replaced Then
                awq.replaceAWQBindedItems(newRev)
            End If

            'to allow view refresh
            latestUpdatedAct = Nothing
            'update act calc
            awq.master_obj.main_activity_obj.updateCal()

            bind_awq_list(newRev)
            MGeneralFuntionsViewControl.displayMessage("New Revision Created Successfully", "AWQ New Revision has been successfully created !", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox("An error occured while creating new REV AWQ " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'launch form to map fake activity with sap activty
    Public Sub launchFakeActMap()
        Try
            Dim awq As CAWQuotation = form.cb_awq.SelectedItem
            Dim master_obj As CAWQuotation
            If Not IsNothing(awq.master_obj) Then
                master_obj = awq.master_obj
            Else
                master_obj = awq
            End If
            Dim act As CScopeActivity = master_obj.main_activity_obj
            If Not act.is_fake_activity Then
                Exit Sub
            End If
            Dim mapFakeActDelegate As New CAWQFakeActivityTransferDelegate
            mapFakeActDelegate.launchEditForm(act)
        Catch ex As Exception
            MsgBox("An error occured while loading form to map temporary activity" & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub launchAWQForm()
        Try
            Dim awq As CAWQuotation = form.cb_awq.SelectedItem
            CReportingAWQForm.getInstance.prepareAWQFormView(awq)
        Catch ex As Exception
            MsgBox("An error occured while generating AWQ Fotm" & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'draw grid
    Private Sub drawAddHoursgrid()
        Dim gridMap As CViewModelMapList
        'get  map view
        gridMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ADD_HOURS_VIEW_DISPLAY)

        ' datagridview
        form.act_add_lab_dgrid.AllowUserToAddRows = False
        MViewEventHandler.addDefaultDatagridEventHandler(form.act_add_lab_dgrid)

        MConstants.GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.drawGenericAddHoursgrid(form.act_add_lab_dgrid, gridMap)
        'set view as read only
        For Each col As DataGridViewColumn In form.act_add_lab_dgrid.Columns
            col.ReadOnly = True
        Next

        'add applicability columns
        addApplicabilityCols(form.act_add_lab_dgrid)

        MGeneralFuntionsViewControl.resizeDataGridWidth(form.act_add_lab_dgrid)

    End Sub

    Private Sub drawMaterialgrid()
        form.act_materials_dgrid.AllowUserToAddRows = False
        MViewEventHandler.addDefaultDatagridEventHandler(form.act_materials_dgrid)
        MConstants.GLB_MSTR_CTRL.material_controller.bind_mat_generic_view(form.act_materials_dgrid)
        'set view as read only
        For Each col As DataGridViewColumn In form.act_materials_dgrid.Columns
            col.ReadOnly = True
            If col.Name = MConstants.CONST_CONF_MAT_EDIT Then
                col.Visible = False
            End If
        Next
        'add applicability columns
        addApplicabilityCols(form.act_materials_dgrid)
        MGeneralFuntionsViewControl.resizeDataGridWidth(form.act_materials_dgrid)
    End Sub
    'add two checkbox columns at the beginning of the datagrid to allow user to add or delete a new labor or material to the awq
    Private Sub addApplicabilityCols(dgrid As DataGridView)
        Dim dgcol As DataGridViewCheckBoxColumn
        dgcol = New DataGridViewCheckBoxColumn
        dgcol.Width = 50
        dgcol.Name = MConstants.CONST_CONF_AWQ_REMOVE_LABMAT
        dgcol.HeaderText = "Remove"
        dgcol.SortMode = DataGridViewColumnSortMode.Automatic
        dgcol.ReadOnly = False
        dgcol.DataPropertyName = "is_removed_awq"
        dgrid.Columns.Insert(0, dgcol)

        dgcol = New DataGridViewCheckBoxColumn
        dgcol.Width = 50
        dgcol.Name = MConstants.CONST_CONF_AWQ_ADD_LABMAT
        dgcol.HeaderText = "Add"
        dgcol.SortMode = DataGridViewColumnSortMode.Automatic
        dgcol.ReadOnly = False
        dgcol.DataPropertyName = "is_awq"
        dgrid.Columns.Insert(0, dgcol)
        'event to handle applicability
        AddHandler dgrid.CurrentCellDirtyStateChanged, AddressOf DataGridView_CurrentCellDirtyStateChanged
        AddHandler dgrid.CellValueChanged, AddressOf DataGridView_CellValueChanged
    End Sub
End Class

﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CGridViewRowValidationHandler
    Public Shared handler_list As Dictionary(Of String, CGridViewRowValidationHandler)
    Public Shared current_handled_grid_list As List(Of String)

    Public Shared Function addNew(_dgrid As DataGridView, _validator As Object, _mapList As CViewModelMapList)
        Dim hdler As CGridViewRowValidationHandler
        Try
            If Not CGridViewRowValidationHandler.handler_list.ContainsKey(_dgrid.Name) Then
                hdler = New CGridViewRowValidationHandler
                hdler.validator = _validator
                hdler.mapList = _mapList
                CGridViewRowValidationHandler.handler_list.Add(_dgrid.Name, hdler)
            Else
                hdler = CGridViewRowValidationHandler.handler_list(_dgrid.Name)
            End If
            'add handler to validate row when living row, if row is not validated force does not allow user to select another row
            AddHandler _dgrid.RowValidating, AddressOf CGridViewRowValidationHandler.rowEndEditing
            'use this to validate row after cell end cell editing
            AddHandler _dgrid.CellEndEdit, AddressOf CGridViewRowValidationHandler.cellEndEditing
            'use this to validate row  cell value changed
            AddHandler _dgrid.CellValueChanged, AddressOf CGridViewRowValidationHandler.cellEndEditing
            'use this to validate row  deleted
            AddHandler _dgrid.RowsRemoved, AddressOf CGridViewRowValidationHandler.DataGridView_RowsRemoved
            'use this to validate row  added
            AddHandler _dgrid.RowsAdded, AddressOf CGridViewRowValidationHandler.DataGridView_RowsAdded
            'when gridview is loaded
            AddHandler _dgrid.VisibleChanged, AddressOf CGridViewRowValidationHandler.datagridview_visible
            'losse focus=> force validation to set id in case of focus lost without ending editing
            AddHandler _dgrid.LostFocus, AddressOf CGridViewRowValidationHandler.datagridview_visible
        Catch ex As Exception
            Throw New Exception("An error occured while adding a new error validator event on datagridview " & _dgrid.Name & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return hdler
    End Function

    Public Shared Sub DataGridView_RowsAdded(sender As DataGridView, e As DataGridViewRowsAddedEventArgs)
        initializeGridErrorState(sender)
    End Sub
    Public Shared Sub DataGridView_RowsRemoved(sender As DataGridView, e As DataGridViewRowsRemovedEventArgs)
        initializeGridErrorState(sender)
    End Sub
    'browse all rows and validate. call when grid is shown the first time
    Public Shared Sub initializeGridErrorState(dgrid As DataGridView)
        Try
            If CGridViewRowValidationHandler.handler_list.ContainsKey(dgrid.Name) And dgrid.RowCount > 1 Then
                For i As Integer = 0 To dgrid.RowCount - 2
                    validateRow(dgrid, i, CGridViewRowValidationHandler.handler_list(dgrid.Name))
                Next
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while initializing grid error state " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'event when a datagridview is visible and need to refresh scope activities lists
    Public Shared Sub datagridview_visible(ByVal sender As Object, ByVal e As EventArgs)
        Dim dgrid As DataGridView = CType(sender, DataGridView)
        If dgrid.Visible Then
            CGridViewRowValidationHandler.initializeGridErrorState(dgrid)
        End If
    End Sub

    Public Shared Sub cellEndEditing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Try
            Dim dgrid As DataGridView = CType(sender, DataGridView)

            'check if this grid view is being shown
            If Not dgrid.Visible OrElse Not dgrid.Focused Then
                Exit Sub
            End If

            Dim rowObj As Object = Nothing
            Try
                rowObj = dgrid.Rows(e.RowIndex).DataBoundItem
            Catch ex As Exception

            End Try
            'if no bounded object, end
            If IsNothing(rowObj) Then
                Exit Sub
            End If

            If CGridViewRowValidationHandler.handler_list.ContainsKey(dgrid.Name) Then
                Dim hdler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.handler_list(dgrid.Name)
                If (validateRow(dgrid, e.RowIndex, hdler)) Then
                    'check if it is a new item and validate it
                    If Not IsNothing(hdler.validateAddDelegate) Then
                        hdler.validateAddDelegate(rowObj, e.RowIndex)
                    End If
                End If
            End If

        Catch ex As Exception
            Throw New Exception("An error occured while validating cell row " & e.RowIndex & " column " & e.ColumnIndex & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub


    Public Shared Sub rowEndEditing(ByVal sender As Object, ByVal data As DataGridViewCellCancelEventArgs)
        Try
            Dim dgrid As DataGridView = CType(sender, DataGridView)

            'check if this grid view is being shown OrElse Not dgrid.Focused
            If Not dgrid.Visible OrElse Not dgrid.Focused Then
                data.Cancel = False
                Exit Sub
            End If

            Dim i As String = dgrid.Name
            Dim rowObj As Object = Nothing
            Try
                rowObj = dgrid.Rows(data.RowIndex).DataBoundItem
            Catch ex As Exception
            End Try
            'if no bounded object, end
            If IsNothing(rowObj) Then
                data.Cancel = False
                Exit Sub
            End If

            If CGridViewRowValidationHandler.handler_list.ContainsKey(dgrid.Name) Then
                Dim hdler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.handler_list(dgrid.Name)
                validateRow(dgrid, data.RowIndex, hdler)
                data.Cancel = False
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while validating row " & data.RowIndex & " column " & data.ColumnIndex & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub


    Public Shared Function validateRow(dgrid As DataGridView, rowIndex As Integer, hdler As CGridViewRowValidationHandler) As Boolean

        Try
            Dim rowVaildated As Boolean = True
            Dim rowErr As String = ""
            Dim rowObj As Object
            Dim rowObjView As Object

            Dim otype As Type = hdler.validator.GetType
            'skip cases where user click on the latest row without editing => object not yet bound
            If Not IsNothing(dgrid.Rows(rowIndex).DataBoundItem) Then
                rowObjView = dgrid.Rows(rowIndex).DataBoundItem
                rowObj = rowObjView.Object
            Else
                'when a row is new there is no databoud item till the edit mode has been activated and ended
                Return True
            End If

            Dim validRes As KeyValuePair(Of Boolean, String)
            rowErr = "Error in column(s): "
            Dim method As MethodInfo
            dgrid.Rows(rowIndex).ErrorText = ""

            'init error state 
            method = otype.GetMethod("initErrorState")
            If method IsNot Nothing Then
                method.Invoke(hdler.validator, New Object() {rowObj})
            End If

            For Each map As CViewModelMap In hdler.mapList.list.Values
                method = otype.GetMethod(map.class_field)
                If method IsNot Nothing Then
                    validRes = method.Invoke(hdler.validator, New Object() {rowObj})
                    If Not validRes.Key Then
                        dgrid.Rows(rowIndex).Cells(dgrid.Columns(map.view_col_sys_name).Index).ErrorText = validRes.Value
                        rowErr = rowErr & map.view_col & ", "
                    Else
                        dgrid.Rows(rowIndex).Cells(dgrid.Columns(map.view_col_sys_name).Index).ErrorText = String.Empty
                    End If
                    rowVaildated = rowVaildated And validRes.Key
                End If
            Next
            If Not rowVaildated Then
                If rowErr.EndsWith(", ") Then
                    rowErr = Left(rowErr, rowErr.Length - 2)
                End If
                dgrid.Rows(rowIndex).ErrorText = rowErr
            Else
                'if row is validated, check if there is any runtime error that happened when calculating
                method = otype.GetMethod("errorText")
                If method IsNot Nothing Then
                    validRes = method.Invoke(hdler.validator, New Object() {rowObj})
                    dgrid.Rows(rowIndex).ErrorText = validRes.Value
                    rowVaildated = validRes.Key
                End If
            End If

            If rowVaildated Then
                'check if it is a new item and validate it
                If Not IsNothing(hdler.validateAddDelegate) Then
                    hdler.validateAddDelegate(rowObjView, rowIndex)
                End If
            End If

            Return rowVaildated
        Catch ex As Exception
            Throw New Exception("An error occured while validating row " & rowIndex & " of datagridview " & dgrid.Name & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function


    Public Shared Sub validateRowObj(rowObj As Object, validator As Object, mapList As CViewModelMapList)
        Try
            Dim otype As Type = validator.GetType
            Dim method As MethodInfo
            Dim validRes As KeyValuePair(Of Boolean, String)

            Dim errString As String = ""
            Dim errBool As Boolean = False

            For Each map As CViewModelMap In mapList.list.Values
                method = otype.GetMethod(map.class_field)
                If method IsNot Nothing Then
                    validRes = method.Invoke(validator, New Object() {rowObj})
                    If Not validRes.Key Then
                        If String.IsNullOrWhiteSpace(errString) Then
                            errString = validRes.Value
                        Else
                            errString = errString & Chr(10) & validRes.Value
                        End If
                    End If
                    errBool = errBool OrElse Not validRes.Key
                End If
            Next
            Dim record As IGenericInterfaces.IDataGridViewRecordable = CType(rowObj, IGenericInterfaces.IDataGridViewRecordable)
            If Not IsNothing(record) Then
                record.setErrorState(errBool, errString)
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while validating Object " & rowObj.ToString & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Delegate Sub ValidateNewItem(obj As Object, rowIndex As Integer)
    Delegate Function CancelNewItem(obj As Object, rowIndex As Integer) As Boolean

    Public validator As Object
    Public mapList As CViewModelMapList
    'delegate function to be used for adding new item
    Public validateAddDelegate As ValidateNewItem
    Public cancelAddDelegate As CancelNewItem
End Class

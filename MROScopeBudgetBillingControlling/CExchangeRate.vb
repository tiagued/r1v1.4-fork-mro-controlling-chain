﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CExchangeRate
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = "It is not allowed to delete exchange rate objects"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function
    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property


    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_EXCHG_RATE_DB_READ
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
            End If
        End Set
    End Property

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Return New KeyValuePair(Of Boolean, String)(True, "")
    End Function

    Private _from_curr As String
    Public Property from_curr() As String
        Get
            Return _from_curr
        End Get
        Set(ByVal value As String)
            If Not _from_curr = value Then
                _from_curr = value
            End If
        End Set
    End Property
    'use for view
    Public ReadOnly Property from_curr_view() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_from_curr).value
        End Get
    End Property

    Private _to_curr As String
    Public Property to_curr() As String
        Get
            Return _to_curr
        End Get
        Set(ByVal value As String)
            If Not _to_curr = value Then
                _to_curr = value
            End If
        End Set
    End Property
    Public ReadOnly Property to_curr_view() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_to_curr).value
        End Get
    End Property

    Private _exchange_rate As Double
    Public Property exchange_rate() As Double
        Get
            Return If(_from_curr = _to_curr, 1, _exchange_rate)
        End Get
        Set(ByVal value As Double)
            If Not _exchange_rate = value Then
                If _to_curr = _from_curr Then
                    _exchange_rate = 1
                Else
                    _exchange_rate = value
                End If
                NotifyPropertyChanged()
                If MConstants.GLOB_EXCHG_RATE_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _exchange_rate_date As Date

    Public Property exchange_rate_date() As Date
        Get
            Return _exchange_rate_date
        End Get
        Set(ByVal value As Date)
            If Not _exchange_rate_date = value Then
                _exchange_rate_date = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "from curr: " & Me.from_curr_view & ", to curr: " & Me.to_curr_view & ", TAGK " & _exchange_rate
    End Function

End Class

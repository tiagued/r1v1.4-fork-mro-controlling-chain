﻿Imports System.ComponentModel

Public Class CNotificationHelper
    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private ReadOnly invokeDelegate As ISynchronizeInvoke

    Public Sub New(ByVal invokeDelegate As ISynchronizeInvoke)
        Me.invokeDelegate = invokeDelegate
    End Sub

    Public Sub OnPropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Try
            If invokeDelegate.InvokeRequired Then
                invokeDelegate.Invoke(New PropertyChangedEventHandler(AddressOf OnPropertyChanged), {sender, e})
                Return
            End If
            RaiseEvent PropertyChanged(Me, e)
        Catch ex As Exception
        End Try
    End Sub

End Class

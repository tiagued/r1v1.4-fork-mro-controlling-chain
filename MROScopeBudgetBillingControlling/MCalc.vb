﻿Module MCalc
    'convert value from curr to labor curr
    Public Function toLabCurr(value As Double, curr As String)
        Try
            Return toCurr(value, curr, GLB_MSTR_CTRL.project_controller.project.lab_curr)
        Catch ex As Exception
            Throw New Exception("An error occured when converting value " & value & ", curr " & curr & " to labor currency" & Chr(10) & ex.Message, ex)
        End Try
    End Function

    'convert value from curr to material curr
    Public Function toMatCurr(value As Double, curr As String)
        Try
            Return toCurr(value, curr, GLB_MSTR_CTRL.project_controller.project.mat_curr)
        Catch ex As Exception
            Throw New Exception("An error occured when converting value " & value & ", curr " & curr & " to material currency" & Chr(10) & ex.Message, ex)
        End Try
    End Function

    'convert value from curr to freight curr
    Public Function toFreightCurr(value As Double, curr As String)
        Try
            Return toCurr(value, curr, GLB_MSTR_CTRL.project_controller.project.freight_curr)
        Catch ex As Exception
            Throw New Exception("An error occured when converting value " & value & ", curr " & curr & " to freight currency" & Chr(10) & ex.Message, ex)
        End Try
    End Function

    'convert value from curr to serv curr
    Public Function toServCurr(value As Double, curr As String)
        Try
            Return toCurr(value, curr, GLB_MSTR_CTRL.project_controller.project.third_party_curr)
        Catch ex As Exception
            Throw New Exception("An error occured when converting value " & value & ", curr " & curr & " to service currency" & Chr(10) & ex.Message, ex)
        End Try
    End Function

    'convert value from curr to  curr
    Public Function toCurr(value As Double, from_curr As String, to_curr As String)
        Try
            Return value * GLB_MSTR_CTRL.billing_cond_controller.getTagk(from_curr, to_curr)
        Catch ex As Exception
            Throw New Exception("An error occured when converting value " & value & ",from curr " & from_curr & " to curr " & to_curr & Chr(10) & ex.Message, ex)
        End Try
    End Function

    'retrieve cc rate convert to lab curr and apply sold hours. project default labor rate catalog is used
    Public Function ccRateToLabCurr(hours As Double, cc As CCostCenter, payer As CPayer)
        Try
            If payer.payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value Then
                Return ccRateToLabCurr(GLB_MSTR_CTRL.project_controller.project.labor_rate_std, hours, cc, payer)
            Else
                Return ccRateToLabCurr(payer.payer, hours, cc, payer)
            End If
        Catch ex As Exception
            Throw New Exception("An error occured calculating cc rate price. cc " & cc.cc & ", payer " & payer.payer & Chr(10) & ex.Message, ex)
        End Try
    End Function

    'retrieve cc rate convert to lab curr and apply sold hours. specified labor rate catalog is used
    Public Function ccRateToLabCurr(labor_rate_catalog As String, hours As Double, cc As CCostCenter, payer As CPayer)
        Try
            If Not GLB_MSTR_CTRL.lab_rate_controller._labor_rate_dict.ContainsKey(CLaborRate.getLaborRateKey(labor_rate_catalog, payer.payer, cc.sap_id)) Then
                Throw New Exception("Labor rate list does not contains key for: catalog " & labor_rate_catalog & " cc " & cc.cc & ", payer " & payer.payer)
            End If
            Dim lr As CLaborRate = GLB_MSTR_CTRL.lab_rate_controller._labor_rate_dict(CLaborRate.getLaborRateKey(labor_rate_catalog, payer.payer, cc.sap_id))
            Return toLabCurr(hours * lr.rate, lr.value_curr)
        Catch ex As Exception
            Throw New Exception("An error occured calculating cc rate price. rate list " & labor_rate_catalog & " cc " & cc.cc & ", payer " & payer.payer & Chr(10) & ex.Message, ex)
        End Try
    End Function

End Module

﻿Public Class CReportConfObject
    Implements IGenericInterfaces.IDataGridViewRecordable

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(False, "")
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
    End Sub

    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_REPORT_CONF_DB_READ
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_DISCOUNT_LOADED Then
                    CDiscount.currentMaxID = Math.Max(CDiscount.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _report_name As String
    Public Property report_name() As String
        Get
            Return _report_name
        End Get
        Set(ByVal value As String)
            _report_name = value
        End Set
    End Property

    Private _report_item As String
    Public Property report_item() As String
        Get
            Return _report_item
        End Get
        Set(ByVal value As String)
            _report_item = value
        End Set
    End Property

    Private _item_list As String
    Public Property item_list() As String
        Get
            Return _item_list
        End Get
        Set(ByVal value As String)
            _item_list = value
        End Set
    End Property

    Private _list_start_cell As String
    Public Property list_start_cell() As String
        Get
            Return _list_start_cell
        End Get
        Set(ByVal value As String)
            _list_start_cell = value
        End Set
    End Property


    Private _object As String
    Public Property item_object() As String
        Get
            Return _object
        End Get
        Set(ByVal value As String)
            _object = value
        End Set
    End Property

    Private _item_type As String
    Public Property item_type() As String
        Get
            Return _item_type
        End Get
        Set(ByVal value As String)
            _item_type = value
        End Set
    End Property

    Private _item_property As String
    Public Property item_property() As String
        Get
            Return _item_property
        End Get
        Set(ByVal value As String)
            _item_property = value
        End Set
    End Property

    Private _item_row As String
    Public Property item_row() As String
        Get
            Return _item_row
        End Get
        Set(ByVal value As String)
            _item_row = value
        End Set
    End Property

    Private _item_column As String
    Public Property item_column() As String
        Get
            Return _item_column
        End Get
        Set(ByVal value As String)
            _item_column = value
        End Set
    End Property

    Private _item_format As String
    Public Property item_format() As String
        Get
            Return _item_format
        End Get
        Set(ByVal value As String)
            _item_format = value
        End Set
    End Property

    Public Shared Sub loadReportConfObject()

        Dim listO As List(Of Object)
        MConstants.listOfReportConf = New Dictionary(Of String, Dictionary(Of String, List(Of CReportConfObject)))
        Try
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform query to report conf table..."
            listO = MConstants.GLB_MSTR_CTRL.csDB.performSelectObject(MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_REPORT_CONF_DB_READ).getDefaultSelect, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_REPORT_CONF_DB_READ), True)

            Dim sheetLists As Dictionary(Of String, List(Of CReportConfObject))
            Dim listItems As List(Of CReportConfObject)

            For Each confO As CReportConfObject In listO
                If MConstants.listOfReportConf.ContainsKey(confO.report_name) Then
                    sheetLists = MConstants.listOfReportConf(confO.report_name)
                Else
                    sheetLists = New Dictionary(Of String, List(Of CReportConfObject))
                    MConstants.listOfReportConf.Add(confO.report_name, sheetLists)
                End If
                Dim listName = confO.item_list
                If confO.item_type = CONST_REPORT_CONF_ITEM_TYPE_CELL Then
                    listName = MConstants.REPORT_CONF_SUMMARY_LIST_NAME
                End If

                If sheetLists.ContainsKey(listName) Then
                    listItems = sheetLists(listName)
                Else
                    listItems = New List(Of CReportConfObject)
                    sheetLists.Add(listName, listItems)
                End If
                listItems.Add(confO)
            Next
        Catch ex As Exception
            Throw New Exception("Error occured when reading report conf data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform query to report conf table"
    End Sub
End Class
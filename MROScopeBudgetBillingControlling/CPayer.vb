﻿Imports MROScopeBudgetBillingControlling

Public Class CPayer
    Implements IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewRecordable

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
    End Function

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_PAYER_DB_READ
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
            End If
        End Set
    End Property

    Public Shared Function getEmptypayer() As CPayer
        Dim empty As New CPayer
        empty.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        'if label nothing it will cause issue in combobox selection
        empty.label = " "
        Return empty
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(False, "")
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
    End Sub

    Private _payer As String
    Public Property payer() As String
        Get
            Return _payer
        End Get
        Set(ByVal value As String)
            If Not _payer = value Then
                _payer = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _friendly_label As String
    Public Property friendly_label() As String
        Get
            Return _friendly_label
        End Get
        Set(ByVal value As String)
            If Not _friendly_label = value Then
                _friendly_label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _warr_text As String
    Public Property warr_text() As String
        Get
            Return _warr_text
        End Get
        Set(ByVal value As String)
            If Not _warr_text = value Then
                _warr_text = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "Payer : " & _label
    End Function
End Class


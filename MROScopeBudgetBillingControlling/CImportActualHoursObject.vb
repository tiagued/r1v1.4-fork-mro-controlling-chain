﻿
Public Class CImportActualHoursObject

    Public Sub New()
        'set combo box
        'set first cc in list
        'use Me. to trigger object retrieval from id
        _cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        _posting_date = MConstants.APP_NULL_DATE
    End Sub

    Private _cost_object As String
    Public Property cost_object() As String
        Get
            Return _cost_object
        End Get
        Set(ByVal value As String)
            If Not _cost_object = value Then
                _cost_object = value
            End If
        End Set
    End Property

    Private _act_id As Long
    Public Property act_id() As Long
        Get
            Return _act_id
        End Get
        Set(ByVal value As Long)
            If Not _act_id = value Then
                _act_id = value
            End If
        End Set
    End Property

    Private _act_obj As CScopeActivity
    Public Property act_obj() As CScopeActivity
        Get
            Return _act_obj
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(value, _act_obj) Then
                _act_obj = value
            End If
        End Set
    End Property

    Private _cc As String
    Public Property cc() As String
        Get
            Return _cc
        End Get
        Set(ByVal value As String)
            If Not _cc = value Then
                _cc = value
            End If
        End Set
    End Property

    Private _personal_number As String
    Public Property personal_number() As String
        Get
            Return _personal_number
        End Get
        Set(ByVal value As String)
            If Not _personal_number = value Then
                _personal_number = value
            End If
        End Set
    End Property

    Private _wbs_element As String
    Public Property wbs_element() As String
        Get
            Return _wbs_element
        End Get
        Set(ByVal value As String)
            If Not _wbs_element = value Then
                _wbs_element = value
            End If
        End Set
    End Property


    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            If Not _qty = value Then
                _qty = value
            End If
        End Set
    End Property

    Private _posting_number As Long
    Public Property posting_number() As Long
        Get
            Return _posting_number
        End Get
        Set(ByVal value As Long)
            If Not _posting_number = value Then
                _posting_number = value
            End If
        End Set
    End Property

    Private _posting_date As Date
    Public Property posting_date() As Date
        Get
            Return _posting_date
        End Get
        Set(ByVal value As Date)
            If Not _posting_date = value Then
                _posting_date = value
            End If
        End Set
    End Property


    Public Function getActualHoursObject() As CActualHours
        Dim res As New CActualHours
        res.cost_object = _cost_object
        res.cc = _cc
        res.personal_number = _personal_number
        res.wbs_element = _wbs_element
        res.qty = _qty
        res.posting_number = _posting_number
        res.posting_date = _posting_date
        Return res
    End Function

End Class

﻿Imports System.ComponentModel
Imports Equin.ApplicationFramework
Public Class CViewRefreshHandler
    Private Shared handler_list As Dictionary(Of Object, CViewRefreshHandler)

    Public Shared Sub initiateList()
        handler_list = New Dictionary(Of Object, CViewRefreshHandler)
    End Sub
    Public Shared Sub addEntry(Of T)(entry As CViewRefreshHandler)
        Try
            Dim bindingLV As BindingListView(Of T) = CType(entry.listView, BindingListView(Of T))
            If Not handler_list.ContainsKey(entry.datagridView.Name) Then
                handler_list.Add(entry.listView, entry)
                AddHandler bindingLV.ListChanged, AddressOf CViewRefreshHandler.refreshView
                If Not IsNothing(entry.addingNewItemDelegate) Then
                    AddHandler bindingLV.AddingNew, AddressOf CViewRefreshHandler.addNew
                End If

            End If
        Catch ex As Exception
            Throw New Exception("An error occured while adding view refresh handler " & entry.datagridView.Name & Chr(10) & ex.Message, ex)
        End Try
    End Sub

    'event handler list changed
    Public Shared Sub refreshView(ByVal sender As Object, ByVal e As ListChangedEventArgs)
        Try
            If handler_list.ContainsKey(sender) Then
                Dim handler As CViewRefreshHandler = handler_list(sender)
                If e.ListChangedType = ListChangedType.ItemAdded OrElse e.ListChangedType = ListChangedType.ItemDeleted OrElse e.ListChangedType = ListChangedType.Reset Then
                    handler.refreshHandlerViewsHeight()

                Else
                    handler.refreshHandlerViews()
                End If
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while refreshing view" & Chr(10) & ex.Message, ex)
        End Try
    End Sub
    'event handler adding new
    Public Shared Sub addNew(ByVal sender As Object, ByVal e As AddingNewEventArgs)
        Try
            If handler_list.ContainsKey(sender) Then
                Dim handler As CViewRefreshHandler = handler_list(sender)
                handler.addingNewItemDelegate.Invoke(sender, e)
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while adding new item to view" & Chr(10) & ex.Message, ex)
        End Try
    End Sub


    Delegate Sub refreshDependantViews()
    Delegate Sub addingNewItem(ByVal sender As Object, ByVal e As AddingNewEventArgs)

    Public Sub New()
        bindingListViewList = New List(Of Object)
        delegateList = New List(Of refreshDependantViews)
    End Sub
    'list view that have been updated (remove, add, update, filter...)
    Public listView As Object

    ' control that height need to be updated. could be a panel containing the gridview
    Public flexibleHeigtControl As Control

    'datagridview that display the current binding list view
    Public datagridView As DataGridView

    'dependant list of view to be refreshed
    Public bindingListViewList As List(Of Object)

    'list of delegate to be executed
    Public delegateList As List(Of refreshDependantViews)

    'delegate function to be used for adding new item
    Public addingNewItemDelegate As addingNewItem


    Public Sub refreshHandlerViews()
        Try
            'invoke delegate to refresh depending bindingsources 
            For Each delegateFunction As refreshDependantViews In delegateList
                delegateFunction.Invoke
            Next
            'refresh depending views
            For Each obj As Object In bindingListViewList
                If obj IsNot Nothing Then
                    Try
                        obj.GetType.GetMethod("Refresh").Invoke(obj, New Object() {})
                    Catch ex As Exception
                        MGeneralFuntionsViewControl.displayMessage("Error!", "Error while refreshing dependant view " & obj.ToString & Chr(10) & "Check that binded table is not in dirty state." & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
                    End Try
                End If
            Next
        Catch ex As Exception
            Throw New Exception("An error occured while refreshing view" & Chr(10) & ex.Message, ex)
        End Try
    End Sub

    'add or delete
    Public Sub refreshHandlerViewsHeight()
        Try
            'set height
            Dim delta As Long
            delta =  datagridView.RowTemplate.Height * (listView.Count + 3) - datagridView.Height
            flexibleHeigtControl.Height = flexibleHeigtControl.Height + delta

            'update other
            refreshHandlerViews()
        Catch ex As Exception
            Throw New Exception("An error occured while resizing datagrid view" & Chr(10) & ex.Message, ex)
        End Try
    End Sub

End Class

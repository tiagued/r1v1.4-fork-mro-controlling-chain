﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormAWQEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.top_panel_activity = New System.Windows.Forms.Panel()
        Me.awq_info = New System.Windows.Forms.Panel()
        Me.awq_info_grp = New System.Windows.Forms.GroupBox()
        Me.product_details_lay = New System.Windows.Forms.TableLayoutPanel()
        Me.pm_go_ahead_val = New System.Windows.Forms.CheckBox()
        Me.pm_go_ahead_lbl = New System.Windows.Forms.Label()
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel()
        Me.offer_validity_val = New System.Windows.Forms.DateTimePicker()
        Me.offer_validity_val_cancel = New System.Windows.Forms.PictureBox()
        Me.offer_validity_lbl = New System.Windows.Forms.Label()
        Me.mat_appr_val = New System.Windows.Forms.TextBox()
        Me.dp_material_val = New System.Windows.Forms.TextBox()
        Me.comments_val = New System.Windows.Forms.TextBox()
        Me.mat_appr_lbl = New System.Windows.Forms.Label()
        Me.description_val = New System.Windows.Forms.TextBox()
        Me.revision_val = New System.Windows.Forms.TextBox()
        Me.reference_val = New System.Windows.Forms.TextBox()
        Me.comments_lbl = New System.Windows.Forms.Label()
        Me.description_lbl = New System.Windows.Forms.Label()
        Me.reference_lbl = New System.Windows.Forms.Label()
        Me.revision_lbl = New System.Windows.Forms.Label()
        Me.is_latest_rev_lbl = New System.Windows.Forms.Label()
        Me.is_latest_rev_calc_val = New System.Windows.Forms.CheckBox()
        Me.ground_time_lbl = New System.Windows.Forms.Label()
        Me.ground_time_tb_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.ground_time_val = New System.Windows.Forms.TextBox()
        Me.ground_time_unit_val = New System.Windows.Forms.ComboBox()
        Me.airworthiness_lbl = New System.Windows.Forms.Label()
        Me.airworthiness_val = New System.Windows.Forms.ComboBox()
        Me.weight_and_balance_lbl = New System.Windows.Forms.Label()
        Me.weight_and_balance_val = New System.Windows.Forms.TextBox()
        Me.priority_lbl = New System.Windows.Forms.Label()
        Me.priority_val = New System.Windows.Forms.ComboBox()
        Me.due_date_lbl = New System.Windows.Forms.Label()
        Me.due_date_val_tb_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.due_date_val_cancel = New System.Windows.Forms.PictureBox()
        Me.due_date_val = New System.Windows.Forms.DateTimePicker()
        Me.internal_status_lbl = New System.Windows.Forms.Label()
        Me.internal_status_val = New System.Windows.Forms.ComboBox()
        Me.issue_date_lbl = New System.Windows.Forms.Label()
        Me.issue_date_tb_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.issue_date_val_cancel = New System.Windows.Forms.PictureBox()
        Me.issue_date_val = New System.Windows.Forms.DateTimePicker()
        Me.cust_sent_date_lbl = New System.Windows.Forms.Label()
        Me.cust_sent_date_val_tb_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.cust_sent_date_val_cancel = New System.Windows.Forms.PictureBox()
        Me.cust_sent_date_val = New System.Windows.Forms.DateTimePicker()
        Me.cust_feedback_lbl = New System.Windows.Forms.Label()
        Me.cust_feedback_val = New System.Windows.Forms.ComboBox()
        Me.cust_fdbk_due_date_lbl = New System.Windows.Forms.Label()
        Me.cust_fdbk_due_date_tb_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.cust_fdbk_due_date_val_cancel = New System.Windows.Forms.PictureBox()
        Me.cust_fdbk_due_date_val = New System.Windows.Forms.DateTimePicker()
        Me.cust_feedback_actual_lbl = New System.Windows.Forms.Label()
        Me.cust_feedback_actual_tb_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.cust_feedback_actual_val_cancel = New System.Windows.Forms.PictureBox()
        Me.cust_feedback_actual_val = New System.Windows.Forms.DateTimePicker()
        Me.customer_comment_lbl = New System.Windows.Forms.Label()
        Me.customer_comment_val = New System.Windows.Forms.TextBox()
        Me.pricing_lbl = New System.Windows.Forms.Label()
        Me.pricing_val = New System.Windows.Forms.ComboBox()
        Me.dp_labor_val = New System.Windows.Forms.TextBox()
        Me.dp_labor_lbl = New System.Windows.Forms.Label()
        Me.payement_terms_lbl = New System.Windows.Forms.Label()
        Me.payement_terms_val = New System.Windows.Forms.TextBox()
        Me.dp_material_lbl = New System.Windows.Forms.Label()
        Me.total_material_appr_val = New System.Windows.Forms.TextBox()
        Me.total_material_appr_lbl = New System.Windows.Forms.Label()
        Me.total_material_val = New System.Windows.Forms.TextBox()
        Me.total_material_lbl = New System.Windows.Forms.Label()
        Me.labor_lbl = New System.Windows.Forms.Label()
        Me.labor_val = New System.Windows.Forms.TextBox()
        Me.appr_labor_lbl = New System.Windows.Forms.Label()
        Me.appr_labor_val = New System.Windows.Forms.TextBox()
        Me.service_appr_val = New System.Windows.Forms.TextBox()
        Me.service_appr_lbl = New System.Windows.Forms.Label()
        Me.service_val = New System.Windows.Forms.TextBox()
        Me.service_lbl = New System.Windows.Forms.Label()
        Me.freight_appr_val = New System.Windows.Forms.TextBox()
        Me.freight_appr_lbl = New System.Windows.Forms.Label()
        Me.freight_lbl = New System.Windows.Forms.Label()
        Me.freight_val = New System.Windows.Forms.TextBox()
        Me.repair_val = New System.Windows.Forms.TextBox()
        Me.repair_lbl = New System.Windows.Forms.Label()
        Me.material_lbl = New System.Windows.Forms.Label()
        Me.material_val = New System.Windows.Forms.TextBox()
        Me.repair_appr_lbl = New System.Windows.Forms.Label()
        Me.repair_appr_val = New System.Windows.Forms.TextBox()
        Me.oem_feedback_lbl = New System.Windows.Forms.Label()
        Me.oem_feedback_val = New System.Windows.Forms.ComboBox()
        Me.top_panel = New System.Windows.Forms.Panel()
        Me.select_activity_grp = New System.Windows.Forms.GroupBox()
        Me.awq_archive_bt = New System.Windows.Forms.Button()
        Me.awq_form_bt = New System.Windows.Forms.Button()
        Me.map_sap_act_bt = New System.Windows.Forms.Button()
        Me.close_bt = New System.Windows.Forms.Button()
        Me.release_bt = New System.Windows.Forms.Button()
        Me.revise_bt = New System.Windows.Forms.Button()
        Me.cancel_bt = New System.Windows.Forms.Button()
        Me.awq_error_state_pic = New System.Windows.Forms.PictureBox()
        Me.awq_select_right_pic = New System.Windows.Forms.PictureBox()
        Me.awq_select_left_pic = New System.Windows.Forms.PictureBox()
        Me.cb_awq = New System.Windows.Forms.ComboBox()
        Me.act_lab_mat_panel = New System.Windows.Forms.Panel()
        Me.launch_awq_bt = New System.Windows.Forms.TabControl()
        Me.act_matlab_addlab_page = New System.Windows.Forms.TabPage()
        Me.act_add_lab_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_matlab_mat_page = New System.Windows.Forms.TabPage()
        Me.act_materials_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_lab_mat_grp = New System.Windows.Forms.GroupBox()
        Me.dep_act_grp = New System.Windows.Forms.GroupBox()
        Me.select_main_act_cb = New System.Windows.Forms.ComboBox()
        Me.main_act_lbl = New System.Windows.Forms.Label()
        Me.select_act_right = New System.Windows.Forms.PictureBox()
        Me.select_act_left = New System.Windows.Forms.PictureBox()
        Me.act_dep_all_listb = New System.Windows.Forms.ListBox()
        Me.act_dep_selected_listb = New System.Windows.Forms.ListBox()
        Me.awr_applicability_ly = New System.Windows.Forms.FlowLayoutPanel()
        Me.awq_lab_mat_ctrl = New System.Windows.Forms.TabControl()
        Me.awq_lab_mat_ctrl_dependant_act_selection_page = New System.Windows.Forms.TabPage()
        Me.awq_lab_mat_ctrl_awq_price_sum_page = New System.Windows.Forms.TabPage()
        Me.awq_summary_ly = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.top_panel_activity.SuspendLayout()
        Me.awq_info.SuspendLayout()
        Me.awq_info_grp.SuspendLayout()
        Me.product_details_lay.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        CType(Me.offer_validity_val_cancel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ground_time_tb_ly.SuspendLayout()
        Me.due_date_val_tb_ly.SuspendLayout()
        CType(Me.due_date_val_cancel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.issue_date_tb_ly.SuspendLayout()
        CType(Me.issue_date_val_cancel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cust_sent_date_val_tb_ly.SuspendLayout()
        CType(Me.cust_sent_date_val_cancel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cust_fdbk_due_date_tb_ly.SuspendLayout()
        CType(Me.cust_fdbk_due_date_val_cancel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cust_feedback_actual_tb_ly.SuspendLayout()
        CType(Me.cust_feedback_actual_val_cancel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.top_panel.SuspendLayout()
        Me.select_activity_grp.SuspendLayout()
        CType(Me.awq_error_state_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.awq_select_right_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.awq_select_left_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_lab_mat_panel.SuspendLayout()
        Me.launch_awq_bt.SuspendLayout()
        Me.act_matlab_addlab_page.SuspendLayout()
        CType(Me.act_add_lab_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_matlab_mat_page.SuspendLayout()
        CType(Me.act_materials_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_lab_mat_grp.SuspendLayout()
        Me.dep_act_grp.SuspendLayout()
        CType(Me.select_act_right, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.select_act_left, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.awr_applicability_ly.SuspendLayout()
        Me.awq_lab_mat_ctrl.SuspendLayout()
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.SuspendLayout()
        Me.awq_lab_mat_ctrl_awq_price_sum_page.SuspendLayout()
        Me.awq_summary_ly.SuspendLayout()
        Me.SuspendLayout()
        '
        'top_panel_activity
        '
        Me.top_panel_activity.AutoSize = True
        Me.top_panel_activity.Controls.Add(Me.awq_info)
        Me.top_panel_activity.Controls.Add(Me.top_panel)
        Me.top_panel_activity.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel_activity.Location = New System.Drawing.Point(5, 5)
        Me.top_panel_activity.Name = "top_panel_activity"
        Me.top_panel_activity.Size = New System.Drawing.Size(1273, 331)
        Me.top_panel_activity.TabIndex = 1
        '
        'awq_info
        '
        Me.awq_info.AutoScroll = True
        Me.awq_info.AutoSize = True
        Me.awq_info.Controls.Add(Me.awq_info_grp)
        Me.awq_info.Dock = System.Windows.Forms.DockStyle.Fill
        Me.awq_info.Location = New System.Drawing.Point(0, 55)
        Me.awq_info.Name = "awq_info"
        Me.awq_info.Size = New System.Drawing.Size(1273, 276)
        Me.awq_info.TabIndex = 3
        '
        'awq_info_grp
        '
        Me.awq_info_grp.AutoSize = True
        Me.awq_info_grp.Controls.Add(Me.product_details_lay)
        Me.awq_info_grp.Location = New System.Drawing.Point(5, 5)
        Me.awq_info_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.awq_info_grp.Name = "awq_info_grp"
        Me.awq_info_grp.Size = New System.Drawing.Size(1602, 266)
        Me.awq_info_grp.TabIndex = 1
        Me.awq_info_grp.TabStop = False
        Me.awq_info_grp.Text = "Additional Work Quotation Details"
        '
        'product_details_lay
        '
        Me.product_details_lay.AutoScroll = True
        Me.product_details_lay.ColumnCount = 8
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.18163!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.35049!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.567872!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.31253!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.417884!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.99185!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.219587!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.95815!))
        Me.product_details_lay.Controls.Add(Me.pm_go_ahead_val, 1, 9)
        Me.product_details_lay.Controls.Add(Me.pm_go_ahead_lbl, 0, 9)
        Me.product_details_lay.Controls.Add(Me.TableLayoutPanel7, 5, 3)
        Me.product_details_lay.Controls.Add(Me.offer_validity_lbl, 4, 3)
        Me.product_details_lay.Controls.Add(Me.mat_appr_val, 3, 7)
        Me.product_details_lay.Controls.Add(Me.dp_material_val, 7, 5)
        Me.product_details_lay.Controls.Add(Me.comments_val, 7, 0)
        Me.product_details_lay.Controls.Add(Me.mat_appr_lbl, 2, 7)
        Me.product_details_lay.Controls.Add(Me.description_val, 5, 0)
        Me.product_details_lay.Controls.Add(Me.revision_val, 3, 0)
        Me.product_details_lay.Controls.Add(Me.reference_val, 1, 0)
        Me.product_details_lay.Controls.Add(Me.comments_lbl, 6, 0)
        Me.product_details_lay.Controls.Add(Me.description_lbl, 4, 0)
        Me.product_details_lay.Controls.Add(Me.reference_lbl, 0, 0)
        Me.product_details_lay.Controls.Add(Me.revision_lbl, 2, 0)
        Me.product_details_lay.Controls.Add(Me.is_latest_rev_lbl, 0, 1)
        Me.product_details_lay.Controls.Add(Me.is_latest_rev_calc_val, 1, 1)
        Me.product_details_lay.Controls.Add(Me.ground_time_lbl, 2, 1)
        Me.product_details_lay.Controls.Add(Me.ground_time_tb_ly, 3, 1)
        Me.product_details_lay.Controls.Add(Me.airworthiness_lbl, 4, 1)
        Me.product_details_lay.Controls.Add(Me.airworthiness_val, 5, 1)
        Me.product_details_lay.Controls.Add(Me.weight_and_balance_lbl, 6, 1)
        Me.product_details_lay.Controls.Add(Me.weight_and_balance_val, 7, 1)
        Me.product_details_lay.Controls.Add(Me.priority_lbl, 0, 2)
        Me.product_details_lay.Controls.Add(Me.priority_val, 1, 2)
        Me.product_details_lay.Controls.Add(Me.due_date_lbl, 2, 2)
        Me.product_details_lay.Controls.Add(Me.due_date_val_tb_ly, 3, 2)
        Me.product_details_lay.Controls.Add(Me.internal_status_lbl, 4, 2)
        Me.product_details_lay.Controls.Add(Me.internal_status_val, 5, 2)
        Me.product_details_lay.Controls.Add(Me.issue_date_lbl, 0, 3)
        Me.product_details_lay.Controls.Add(Me.issue_date_tb_ly, 1, 3)
        Me.product_details_lay.Controls.Add(Me.cust_sent_date_lbl, 2, 3)
        Me.product_details_lay.Controls.Add(Me.cust_sent_date_val_tb_ly, 3, 3)
        Me.product_details_lay.Controls.Add(Me.cust_feedback_lbl, 0, 4)
        Me.product_details_lay.Controls.Add(Me.cust_feedback_val, 1, 4)
        Me.product_details_lay.Controls.Add(Me.cust_fdbk_due_date_lbl, 6, 3)
        Me.product_details_lay.Controls.Add(Me.cust_fdbk_due_date_tb_ly, 7, 3)
        Me.product_details_lay.Controls.Add(Me.cust_feedback_actual_lbl, 2, 4)
        Me.product_details_lay.Controls.Add(Me.cust_feedback_actual_tb_ly, 3, 4)
        Me.product_details_lay.Controls.Add(Me.customer_comment_lbl, 4, 4)
        Me.product_details_lay.Controls.Add(Me.customer_comment_val, 5, 4)
        Me.product_details_lay.Controls.Add(Me.pricing_lbl, 0, 5)
        Me.product_details_lay.Controls.Add(Me.pricing_val, 1, 5)
        Me.product_details_lay.Controls.Add(Me.dp_labor_val, 5, 5)
        Me.product_details_lay.Controls.Add(Me.dp_labor_lbl, 4, 5)
        Me.product_details_lay.Controls.Add(Me.payement_terms_lbl, 2, 5)
        Me.product_details_lay.Controls.Add(Me.payement_terms_val, 3, 5)
        Me.product_details_lay.Controls.Add(Me.dp_material_lbl, 6, 5)
        Me.product_details_lay.Controls.Add(Me.total_material_appr_val, 7, 6)
        Me.product_details_lay.Controls.Add(Me.total_material_appr_lbl, 6, 6)
        Me.product_details_lay.Controls.Add(Me.total_material_val, 5, 6)
        Me.product_details_lay.Controls.Add(Me.total_material_lbl, 4, 6)
        Me.product_details_lay.Controls.Add(Me.labor_lbl, 0, 6)
        Me.product_details_lay.Controls.Add(Me.labor_val, 1, 6)
        Me.product_details_lay.Controls.Add(Me.appr_labor_lbl, 2, 6)
        Me.product_details_lay.Controls.Add(Me.appr_labor_val, 3, 6)
        Me.product_details_lay.Controls.Add(Me.service_appr_val, 7, 8)
        Me.product_details_lay.Controls.Add(Me.service_appr_lbl, 6, 8)
        Me.product_details_lay.Controls.Add(Me.service_val, 5, 8)
        Me.product_details_lay.Controls.Add(Me.service_lbl, 4, 8)
        Me.product_details_lay.Controls.Add(Me.freight_appr_val, 3, 8)
        Me.product_details_lay.Controls.Add(Me.freight_appr_lbl, 2, 8)
        Me.product_details_lay.Controls.Add(Me.freight_lbl, 0, 8)
        Me.product_details_lay.Controls.Add(Me.freight_val, 1, 8)
        Me.product_details_lay.Controls.Add(Me.repair_val, 5, 7)
        Me.product_details_lay.Controls.Add(Me.repair_lbl, 4, 7)
        Me.product_details_lay.Controls.Add(Me.material_lbl, 0, 7)
        Me.product_details_lay.Controls.Add(Me.material_val, 1, 7)
        Me.product_details_lay.Controls.Add(Me.repair_appr_lbl, 6, 7)
        Me.product_details_lay.Controls.Add(Me.repair_appr_val, 7, 7)
        Me.product_details_lay.Controls.Add(Me.oem_feedback_lbl, 2, 9)
        Me.product_details_lay.Controls.Add(Me.oem_feedback_val, 3, 9)
        Me.product_details_lay.Location = New System.Drawing.Point(12, 19)
        Me.product_details_lay.Name = "product_details_lay"
        Me.product_details_lay.RowCount = 10
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00068!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00068!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00068!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00068!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00068!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00068!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999685!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.996534!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.996534!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00315!))
        Me.product_details_lay.Size = New System.Drawing.Size(1584, 225)
        Me.product_details_lay.TabIndex = 1
        '
        'pm_go_ahead_val
        '
        Me.pm_go_ahead_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pm_go_ahead_val.AutoSize = True
        Me.pm_go_ahead_val.Location = New System.Drawing.Point(164, 204)
        Me.pm_go_ahead_val.Name = "pm_go_ahead_val"
        Me.pm_go_ahead_val.Size = New System.Drawing.Size(15, 14)
        Me.pm_go_ahead_val.TabIndex = 11
        Me.pm_go_ahead_val.UseVisualStyleBackColor = True
        '
        'pm_go_ahead_lbl
        '
        Me.pm_go_ahead_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pm_go_ahead_lbl.AutoSize = True
        Me.pm_go_ahead_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pm_go_ahead_lbl.Location = New System.Drawing.Point(3, 205)
        Me.pm_go_ahead_lbl.Name = "pm_go_ahead_lbl"
        Me.pm_go_ahead_lbl.Size = New System.Drawing.Size(104, 13)
        Me.pm_go_ahead_lbl.TabIndex = 11
        Me.pm_go_ahead_lbl.Text = "PM Go Ahead ? :"
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 2
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.3253!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.6747!))
        Me.TableLayoutPanel7.Controls.Add(Me.offer_validity_val, 0, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.offer_validity_val_cancel, 1, 0)
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(914, 66)
        Me.TableLayoutPanel7.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        Me.TableLayoutPanel7.RowCount = 1
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(176, 22)
        Me.TableLayoutPanel7.TabIndex = 15
        '
        'offer_validity_val
        '
        Me.offer_validity_val.Location = New System.Drawing.Point(3, 3)
        Me.offer_validity_val.Name = "offer_validity_val"
        Me.offer_validity_val.Size = New System.Drawing.Size(100, 20)
        Me.offer_validity_val.TabIndex = 39
        '
        'offer_validity_val_cancel
        '
        Me.offer_validity_val_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.offer_validity_val_cancel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.offer_validity_val_cancel.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.cancel
        Me.offer_validity_val_cancel.Location = New System.Drawing.Point(143, 0)
        Me.offer_validity_val_cancel.Margin = New System.Windows.Forms.Padding(0)
        Me.offer_validity_val_cancel.Name = "offer_validity_val_cancel"
        Me.offer_validity_val_cancel.Size = New System.Drawing.Size(33, 21)
        Me.offer_validity_val_cancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.offer_validity_val_cancel.TabIndex = 16
        Me.offer_validity_val_cancel.TabStop = False
        '
        'offer_validity_lbl
        '
        Me.offer_validity_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.offer_validity_lbl.AutoSize = True
        Me.offer_validity_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.offer_validity_lbl.Location = New System.Drawing.Point(784, 70)
        Me.offer_validity_lbl.Name = "offer_validity_lbl"
        Me.offer_validity_lbl.Size = New System.Drawing.Size(88, 13)
        Me.offer_validity_lbl.TabIndex = 10
        Me.offer_validity_lbl.Text = "Offer Validity :"
        '
        'mat_appr_val
        '
        Me.mat_appr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_appr_val.Location = New System.Drawing.Point(542, 157)
        Me.mat_appr_val.Name = "mat_appr_val"
        Me.mat_appr_val.Size = New System.Drawing.Size(100, 20)
        Me.mat_appr_val.TabIndex = 55
        '
        'dp_material_val
        '
        Me.dp_material_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.dp_material_val.Location = New System.Drawing.Point(1300, 113)
        Me.dp_material_val.Name = "dp_material_val"
        Me.dp_material_val.Size = New System.Drawing.Size(100, 20)
        Me.dp_material_val.TabIndex = 53
        '
        'comments_val
        '
        Me.comments_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.comments_val.Location = New System.Drawing.Point(1300, 3)
        Me.comments_val.Name = "comments_val"
        Me.comments_val.Size = New System.Drawing.Size(216, 20)
        Me.comments_val.TabIndex = 13
        '
        'mat_appr_lbl
        '
        Me.mat_appr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_appr_lbl.AutoSize = True
        Me.mat_appr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_appr_lbl.Location = New System.Drawing.Point(391, 158)
        Me.mat_appr_lbl.Name = "mat_appr_lbl"
        Me.mat_appr_lbl.Size = New System.Drawing.Size(100, 13)
        Me.mat_appr_lbl.TabIndex = 52
        Me.mat_appr_lbl.Text = "Mat. Prev Appr.:"
        '
        'description_val
        '
        Me.description_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.description_val.Location = New System.Drawing.Point(917, 3)
        Me.description_val.Name = "description_val"
        Me.description_val.Size = New System.Drawing.Size(163, 20)
        Me.description_val.TabIndex = 14
        '
        'revision_val
        '
        Me.revision_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.revision_val.Location = New System.Drawing.Point(542, 3)
        Me.revision_val.Name = "revision_val"
        Me.revision_val.Size = New System.Drawing.Size(100, 20)
        Me.revision_val.TabIndex = 15
        '
        'reference_val
        '
        Me.reference_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.reference_val.Location = New System.Drawing.Point(164, 3)
        Me.reference_val.Name = "reference_val"
        Me.reference_val.Size = New System.Drawing.Size(161, 20)
        Me.reference_val.TabIndex = 1
        '
        'comments_lbl
        '
        Me.comments_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.comments_lbl.AutoSize = True
        Me.comments_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comments_lbl.Location = New System.Drawing.Point(1154, 4)
        Me.comments_lbl.Name = "comments_lbl"
        Me.comments_lbl.Size = New System.Drawing.Size(72, 13)
        Me.comments_lbl.TabIndex = 6
        Me.comments_lbl.Text = "Comments :"
        '
        'description_lbl
        '
        Me.description_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.description_lbl.AutoSize = True
        Me.description_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.description_lbl.Location = New System.Drawing.Point(784, 4)
        Me.description_lbl.Name = "description_lbl"
        Me.description_lbl.Size = New System.Drawing.Size(79, 13)
        Me.description_lbl.TabIndex = 4
        Me.description_lbl.Text = "Description :"
        '
        'reference_lbl
        '
        Me.reference_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.reference_lbl.AutoSize = True
        Me.reference_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.reference_lbl.Location = New System.Drawing.Point(3, 4)
        Me.reference_lbl.Name = "reference_lbl"
        Me.reference_lbl.Size = New System.Drawing.Size(74, 13)
        Me.reference_lbl.TabIndex = 0
        Me.reference_lbl.Text = "Reference :"
        '
        'revision_lbl
        '
        Me.revision_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.revision_lbl.AutoSize = True
        Me.revision_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.revision_lbl.Location = New System.Drawing.Point(391, 4)
        Me.revision_lbl.Name = "revision_lbl"
        Me.revision_lbl.Size = New System.Drawing.Size(64, 13)
        Me.revision_lbl.TabIndex = 2
        Me.revision_lbl.Text = "Revision :"
        '
        'is_latest_rev_lbl
        '
        Me.is_latest_rev_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.is_latest_rev_lbl.AutoSize = True
        Me.is_latest_rev_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.is_latest_rev_lbl.Location = New System.Drawing.Point(3, 26)
        Me.is_latest_rev_lbl.Name = "is_latest_rev_lbl"
        Me.is_latest_rev_lbl.Size = New System.Drawing.Size(88, 13)
        Me.is_latest_rev_lbl.TabIndex = 10
        Me.is_latest_rev_lbl.Text = "Latest Rev ? :"
        '
        'is_latest_rev_calc_val
        '
        Me.is_latest_rev_calc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.is_latest_rev_calc_val.AutoSize = True
        Me.is_latest_rev_calc_val.Location = New System.Drawing.Point(164, 26)
        Me.is_latest_rev_calc_val.Name = "is_latest_rev_calc_val"
        Me.is_latest_rev_calc_val.Size = New System.Drawing.Size(15, 14)
        Me.is_latest_rev_calc_val.TabIndex = 10
        Me.is_latest_rev_calc_val.UseVisualStyleBackColor = True
        '
        'ground_time_lbl
        '
        Me.ground_time_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.ground_time_lbl.AutoSize = True
        Me.ground_time_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ground_time_lbl.Location = New System.Drawing.Point(391, 26)
        Me.ground_time_lbl.Name = "ground_time_lbl"
        Me.ground_time_lbl.Size = New System.Drawing.Size(87, 13)
        Me.ground_time_lbl.TabIndex = 7
        Me.ground_time_lbl.Text = "Ground Time :"
        '
        'ground_time_tb_ly
        '
        Me.ground_time_tb_ly.ColumnCount = 2
        Me.ground_time_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.71429!))
        Me.ground_time_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.28571!))
        Me.ground_time_tb_ly.Controls.Add(Me.ground_time_val, 0, 0)
        Me.ground_time_tb_ly.Controls.Add(Me.ground_time_unit_val, 1, 0)
        Me.ground_time_tb_ly.Location = New System.Drawing.Point(539, 22)
        Me.ground_time_tb_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.ground_time_tb_ly.Name = "ground_time_tb_ly"
        Me.ground_time_tb_ly.RowCount = 1
        Me.ground_time_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.ground_time_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.ground_time_tb_ly.Size = New System.Drawing.Size(175, 22)
        Me.ground_time_tb_ly.TabIndex = 10
        '
        'ground_time_val
        '
        Me.ground_time_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.ground_time_val.Location = New System.Drawing.Point(3, 3)
        Me.ground_time_val.Name = "ground_time_val"
        Me.ground_time_val.Size = New System.Drawing.Size(100, 20)
        Me.ground_time_val.TabIndex = 8
        '
        'ground_time_unit_val
        '
        Me.ground_time_unit_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.ground_time_unit_val.FormattingEnabled = True
        Me.ground_time_unit_val.Location = New System.Drawing.Point(111, 3)
        Me.ground_time_unit_val.Name = "ground_time_unit_val"
        Me.ground_time_unit_val.Size = New System.Drawing.Size(61, 21)
        Me.ground_time_unit_val.TabIndex = 44
        '
        'airworthiness_lbl
        '
        Me.airworthiness_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.airworthiness_lbl.AutoSize = True
        Me.airworthiness_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.airworthiness_lbl.Location = New System.Drawing.Point(784, 26)
        Me.airworthiness_lbl.Name = "airworthiness_lbl"
        Me.airworthiness_lbl.Size = New System.Drawing.Size(90, 13)
        Me.airworthiness_lbl.TabIndex = 12
        Me.airworthiness_lbl.Text = "Airworthiness :"
        '
        'airworthiness_val
        '
        Me.airworthiness_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.airworthiness_val.FormattingEnabled = True
        Me.airworthiness_val.Location = New System.Drawing.Point(917, 25)
        Me.airworthiness_val.Name = "airworthiness_val"
        Me.airworthiness_val.Size = New System.Drawing.Size(100, 21)
        Me.airworthiness_val.TabIndex = 45
        '
        'weight_and_balance_lbl
        '
        Me.weight_and_balance_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.weight_and_balance_lbl.AutoSize = True
        Me.weight_and_balance_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.weight_and_balance_lbl.Location = New System.Drawing.Point(1154, 26)
        Me.weight_and_balance_lbl.Name = "weight_and_balance_lbl"
        Me.weight_and_balance_lbl.Size = New System.Drawing.Size(107, 13)
        Me.weight_and_balance_lbl.TabIndex = 5
        Me.weight_and_balance_lbl.Text = "Weight And Bal. :"
        '
        'weight_and_balance_val
        '
        Me.weight_and_balance_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.weight_and_balance_val.Location = New System.Drawing.Point(1300, 25)
        Me.weight_and_balance_val.Name = "weight_and_balance_val"
        Me.weight_and_balance_val.Size = New System.Drawing.Size(173, 20)
        Me.weight_and_balance_val.TabIndex = 9
        '
        'priority_lbl
        '
        Me.priority_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.priority_lbl.AutoSize = True
        Me.priority_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.priority_lbl.Location = New System.Drawing.Point(3, 48)
        Me.priority_lbl.Name = "priority_lbl"
        Me.priority_lbl.Size = New System.Drawing.Size(54, 13)
        Me.priority_lbl.TabIndex = 13
        Me.priority_lbl.Text = "Priority :"
        '
        'priority_val
        '
        Me.priority_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.priority_val.FormattingEnabled = True
        Me.priority_val.Location = New System.Drawing.Point(164, 47)
        Me.priority_val.Name = "priority_val"
        Me.priority_val.Size = New System.Drawing.Size(100, 21)
        Me.priority_val.TabIndex = 46
        '
        'due_date_lbl
        '
        Me.due_date_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.due_date_lbl.AutoSize = True
        Me.due_date_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.due_date_lbl.Location = New System.Drawing.Point(391, 48)
        Me.due_date_lbl.Name = "due_date_lbl"
        Me.due_date_lbl.Size = New System.Drawing.Size(69, 13)
        Me.due_date_lbl.TabIndex = 10
        Me.due_date_lbl.Text = "Due Date :"
        '
        'due_date_val_tb_ly
        '
        Me.due_date_val_tb_ly.ColumnCount = 2
        Me.due_date_val_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.3253!))
        Me.due_date_val_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.6747!))
        Me.due_date_val_tb_ly.Controls.Add(Me.due_date_val_cancel, 1, 0)
        Me.due_date_val_tb_ly.Controls.Add(Me.due_date_val, 0, 0)
        Me.due_date_val_tb_ly.Location = New System.Drawing.Point(539, 44)
        Me.due_date_val_tb_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.due_date_val_tb_ly.Name = "due_date_val_tb_ly"
        Me.due_date_val_tb_ly.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        Me.due_date_val_tb_ly.RowCount = 1
        Me.due_date_val_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.due_date_val_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.due_date_val_tb_ly.Size = New System.Drawing.Size(175, 22)
        Me.due_date_val_tb_ly.TabIndex = 20
        '
        'due_date_val_cancel
        '
        Me.due_date_val_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.due_date_val_cancel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.due_date_val_cancel.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.cancel
        Me.due_date_val_cancel.Location = New System.Drawing.Point(142, 0)
        Me.due_date_val_cancel.Margin = New System.Windows.Forms.Padding(0)
        Me.due_date_val_cancel.Name = "due_date_val_cancel"
        Me.due_date_val_cancel.Size = New System.Drawing.Size(33, 21)
        Me.due_date_val_cancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.due_date_val_cancel.TabIndex = 16
        Me.due_date_val_cancel.TabStop = False
        '
        'due_date_val
        '
        Me.due_date_val.Location = New System.Drawing.Point(3, 3)
        Me.due_date_val.Name = "due_date_val"
        Me.due_date_val.Size = New System.Drawing.Size(100, 20)
        Me.due_date_val.TabIndex = 40
        '
        'internal_status_lbl
        '
        Me.internal_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.internal_status_lbl.AutoSize = True
        Me.internal_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.internal_status_lbl.Location = New System.Drawing.Point(784, 48)
        Me.internal_status_lbl.Name = "internal_status_lbl"
        Me.internal_status_lbl.Size = New System.Drawing.Size(98, 13)
        Me.internal_status_lbl.TabIndex = 10
        Me.internal_status_lbl.Text = "Internal Status :"
        '
        'internal_status_val
        '
        Me.internal_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.internal_status_val.FormattingEnabled = True
        Me.internal_status_val.Location = New System.Drawing.Point(917, 47)
        Me.internal_status_val.Name = "internal_status_val"
        Me.internal_status_val.Size = New System.Drawing.Size(100, 21)
        Me.internal_status_val.TabIndex = 47
        '
        'issue_date_lbl
        '
        Me.issue_date_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.issue_date_lbl.AutoSize = True
        Me.issue_date_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.issue_date_lbl.Location = New System.Drawing.Point(3, 70)
        Me.issue_date_lbl.Name = "issue_date_lbl"
        Me.issue_date_lbl.Size = New System.Drawing.Size(76, 13)
        Me.issue_date_lbl.TabIndex = 6
        Me.issue_date_lbl.Text = "Issue Date :"
        '
        'issue_date_tb_ly
        '
        Me.issue_date_tb_ly.ColumnCount = 2
        Me.issue_date_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.3253!))
        Me.issue_date_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.6747!))
        Me.issue_date_tb_ly.Controls.Add(Me.issue_date_val_cancel, 1, 0)
        Me.issue_date_tb_ly.Controls.Add(Me.issue_date_val, 0, 0)
        Me.issue_date_tb_ly.Location = New System.Drawing.Point(161, 66)
        Me.issue_date_tb_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.issue_date_tb_ly.Name = "issue_date_tb_ly"
        Me.issue_date_tb_ly.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        Me.issue_date_tb_ly.RowCount = 1
        Me.issue_date_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.issue_date_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.issue_date_tb_ly.Size = New System.Drawing.Size(166, 22)
        Me.issue_date_tb_ly.TabIndex = 21
        '
        'issue_date_val_cancel
        '
        Me.issue_date_val_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.issue_date_val_cancel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.issue_date_val_cancel.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.cancel
        Me.issue_date_val_cancel.Location = New System.Drawing.Point(135, 0)
        Me.issue_date_val_cancel.Margin = New System.Windows.Forms.Padding(0)
        Me.issue_date_val_cancel.Name = "issue_date_val_cancel"
        Me.issue_date_val_cancel.Size = New System.Drawing.Size(31, 21)
        Me.issue_date_val_cancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.issue_date_val_cancel.TabIndex = 16
        Me.issue_date_val_cancel.TabStop = False
        '
        'issue_date_val
        '
        Me.issue_date_val.Location = New System.Drawing.Point(3, 3)
        Me.issue_date_val.Name = "issue_date_val"
        Me.issue_date_val.Size = New System.Drawing.Size(100, 20)
        Me.issue_date_val.TabIndex = 41
        '
        'cust_sent_date_lbl
        '
        Me.cust_sent_date_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cust_sent_date_lbl.AutoSize = True
        Me.cust_sent_date_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cust_sent_date_lbl.Location = New System.Drawing.Point(391, 70)
        Me.cust_sent_date_lbl.Name = "cust_sent_date_lbl"
        Me.cust_sent_date_lbl.Size = New System.Drawing.Size(105, 13)
        Me.cust_sent_date_lbl.TabIndex = 11
        Me.cust_sent_date_lbl.Text = "Cust. Sent Date :"
        '
        'cust_sent_date_val_tb_ly
        '
        Me.cust_sent_date_val_tb_ly.ColumnCount = 2
        Me.cust_sent_date_val_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.3253!))
        Me.cust_sent_date_val_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.6747!))
        Me.cust_sent_date_val_tb_ly.Controls.Add(Me.cust_sent_date_val_cancel, 1, 0)
        Me.cust_sent_date_val_tb_ly.Controls.Add(Me.cust_sent_date_val, 0, 0)
        Me.cust_sent_date_val_tb_ly.Location = New System.Drawing.Point(539, 66)
        Me.cust_sent_date_val_tb_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.cust_sent_date_val_tb_ly.Name = "cust_sent_date_val_tb_ly"
        Me.cust_sent_date_val_tb_ly.RowCount = 1
        Me.cust_sent_date_val_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.cust_sent_date_val_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.cust_sent_date_val_tb_ly.Size = New System.Drawing.Size(175, 22)
        Me.cust_sent_date_val_tb_ly.TabIndex = 18
        '
        'cust_sent_date_val_cancel
        '
        Me.cust_sent_date_val_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cust_sent_date_val_cancel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cust_sent_date_val_cancel.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.cancel
        Me.cust_sent_date_val_cancel.Location = New System.Drawing.Point(142, 0)
        Me.cust_sent_date_val_cancel.Margin = New System.Windows.Forms.Padding(0)
        Me.cust_sent_date_val_cancel.Name = "cust_sent_date_val_cancel"
        Me.cust_sent_date_val_cancel.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        Me.cust_sent_date_val_cancel.Size = New System.Drawing.Size(33, 22)
        Me.cust_sent_date_val_cancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.cust_sent_date_val_cancel.TabIndex = 16
        Me.cust_sent_date_val_cancel.TabStop = False
        '
        'cust_sent_date_val
        '
        Me.cust_sent_date_val.Location = New System.Drawing.Point(3, 3)
        Me.cust_sent_date_val.Name = "cust_sent_date_val"
        Me.cust_sent_date_val.Size = New System.Drawing.Size(100, 20)
        Me.cust_sent_date_val.TabIndex = 42
        '
        'cust_feedback_lbl
        '
        Me.cust_feedback_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cust_feedback_lbl.AutoSize = True
        Me.cust_feedback_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cust_feedback_lbl.Location = New System.Drawing.Point(3, 92)
        Me.cust_feedback_lbl.Name = "cust_feedback_lbl"
        Me.cust_feedback_lbl.Size = New System.Drawing.Size(80, 13)
        Me.cust_feedback_lbl.TabIndex = 9
        Me.cust_feedback_lbl.Text = "Cust. Fbck. :"
        '
        'cust_feedback_val
        '
        Me.cust_feedback_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cust_feedback_val.FormattingEnabled = True
        Me.cust_feedback_val.Location = New System.Drawing.Point(164, 91)
        Me.cust_feedback_val.Name = "cust_feedback_val"
        Me.cust_feedback_val.Size = New System.Drawing.Size(100, 21)
        Me.cust_feedback_val.TabIndex = 48
        '
        'cust_fdbk_due_date_lbl
        '
        Me.cust_fdbk_due_date_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cust_fdbk_due_date_lbl.AutoSize = True
        Me.cust_fdbk_due_date_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cust_fdbk_due_date_lbl.Location = New System.Drawing.Point(1154, 70)
        Me.cust_fdbk_due_date_lbl.Name = "cust_fdbk_due_date_lbl"
        Me.cust_fdbk_due_date_lbl.Size = New System.Drawing.Size(107, 13)
        Me.cust_fdbk_due_date_lbl.TabIndex = 10
        Me.cust_fdbk_due_date_lbl.Text = "Cust. Fbck. Due :"
        '
        'cust_fdbk_due_date_tb_ly
        '
        Me.cust_fdbk_due_date_tb_ly.ColumnCount = 2
        Me.cust_fdbk_due_date_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.3253!))
        Me.cust_fdbk_due_date_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.6747!))
        Me.cust_fdbk_due_date_tb_ly.Controls.Add(Me.cust_fdbk_due_date_val_cancel, 1, 0)
        Me.cust_fdbk_due_date_tb_ly.Controls.Add(Me.cust_fdbk_due_date_val, 0, 0)
        Me.cust_fdbk_due_date_tb_ly.Location = New System.Drawing.Point(1297, 66)
        Me.cust_fdbk_due_date_tb_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.cust_fdbk_due_date_tb_ly.Name = "cust_fdbk_due_date_tb_ly"
        Me.cust_fdbk_due_date_tb_ly.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        Me.cust_fdbk_due_date_tb_ly.RowCount = 1
        Me.cust_fdbk_due_date_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.cust_fdbk_due_date_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.cust_fdbk_due_date_tb_ly.Size = New System.Drawing.Size(175, 22)
        Me.cust_fdbk_due_date_tb_ly.TabIndex = 19
        '
        'cust_fdbk_due_date_val_cancel
        '
        Me.cust_fdbk_due_date_val_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cust_fdbk_due_date_val_cancel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cust_fdbk_due_date_val_cancel.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.cancel
        Me.cust_fdbk_due_date_val_cancel.Location = New System.Drawing.Point(142, 0)
        Me.cust_fdbk_due_date_val_cancel.Margin = New System.Windows.Forms.Padding(0)
        Me.cust_fdbk_due_date_val_cancel.Name = "cust_fdbk_due_date_val_cancel"
        Me.cust_fdbk_due_date_val_cancel.Size = New System.Drawing.Size(33, 21)
        Me.cust_fdbk_due_date_val_cancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.cust_fdbk_due_date_val_cancel.TabIndex = 16
        Me.cust_fdbk_due_date_val_cancel.TabStop = False
        '
        'cust_fdbk_due_date_val
        '
        Me.cust_fdbk_due_date_val.Location = New System.Drawing.Point(3, 3)
        Me.cust_fdbk_due_date_val.Name = "cust_fdbk_due_date_val"
        Me.cust_fdbk_due_date_val.Size = New System.Drawing.Size(100, 20)
        Me.cust_fdbk_due_date_val.TabIndex = 41
        '
        'cust_feedback_actual_lbl
        '
        Me.cust_feedback_actual_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cust_feedback_actual_lbl.AutoSize = True
        Me.cust_feedback_actual_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cust_feedback_actual_lbl.Location = New System.Drawing.Point(391, 92)
        Me.cust_feedback_actual_lbl.Name = "cust_feedback_actual_lbl"
        Me.cust_feedback_actual_lbl.Size = New System.Drawing.Size(107, 13)
        Me.cust_feedback_actual_lbl.TabIndex = 10
        Me.cust_feedback_actual_lbl.Text = "Cust. Fbck. Act. :"
        '
        'cust_feedback_actual_tb_ly
        '
        Me.cust_feedback_actual_tb_ly.ColumnCount = 2
        Me.cust_feedback_actual_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.3253!))
        Me.cust_feedback_actual_tb_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.6747!))
        Me.cust_feedback_actual_tb_ly.Controls.Add(Me.cust_feedback_actual_val_cancel, 1, 0)
        Me.cust_feedback_actual_tb_ly.Controls.Add(Me.cust_feedback_actual_val, 0, 0)
        Me.cust_feedback_actual_tb_ly.Location = New System.Drawing.Point(539, 88)
        Me.cust_feedback_actual_tb_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.cust_feedback_actual_tb_ly.Name = "cust_feedback_actual_tb_ly"
        Me.cust_feedback_actual_tb_ly.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        Me.cust_feedback_actual_tb_ly.RowCount = 1
        Me.cust_feedback_actual_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.cust_feedback_actual_tb_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.cust_feedback_actual_tb_ly.Size = New System.Drawing.Size(175, 22)
        Me.cust_feedback_actual_tb_ly.TabIndex = 22
        '
        'cust_feedback_actual_val_cancel
        '
        Me.cust_feedback_actual_val_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cust_feedback_actual_val_cancel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cust_feedback_actual_val_cancel.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.cancel
        Me.cust_feedback_actual_val_cancel.Location = New System.Drawing.Point(142, 0)
        Me.cust_feedback_actual_val_cancel.Margin = New System.Windows.Forms.Padding(0)
        Me.cust_feedback_actual_val_cancel.Name = "cust_feedback_actual_val_cancel"
        Me.cust_feedback_actual_val_cancel.Size = New System.Drawing.Size(33, 21)
        Me.cust_feedback_actual_val_cancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.cust_feedback_actual_val_cancel.TabIndex = 16
        Me.cust_feedback_actual_val_cancel.TabStop = False
        '
        'cust_feedback_actual_val
        '
        Me.cust_feedback_actual_val.Location = New System.Drawing.Point(3, 3)
        Me.cust_feedback_actual_val.Name = "cust_feedback_actual_val"
        Me.cust_feedback_actual_val.Size = New System.Drawing.Size(100, 20)
        Me.cust_feedback_actual_val.TabIndex = 42
        '
        'customer_comment_lbl
        '
        Me.customer_comment_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.customer_comment_lbl.AutoSize = True
        Me.customer_comment_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.customer_comment_lbl.Location = New System.Drawing.Point(784, 92)
        Me.customer_comment_lbl.Name = "customer_comment_lbl"
        Me.customer_comment_lbl.Size = New System.Drawing.Size(99, 13)
        Me.customer_comment_lbl.TabIndex = 9
        Me.customer_comment_lbl.Text = "Cust. Comment :"
        '
        'customer_comment_val
        '
        Me.customer_comment_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.customer_comment_val.Location = New System.Drawing.Point(917, 91)
        Me.customer_comment_val.Name = "customer_comment_val"
        Me.customer_comment_val.Size = New System.Drawing.Size(172, 20)
        Me.customer_comment_val.TabIndex = 4
        '
        'pricing_lbl
        '
        Me.pricing_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pricing_lbl.AutoSize = True
        Me.pricing_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pricing_lbl.Location = New System.Drawing.Point(3, 114)
        Me.pricing_lbl.Name = "pricing_lbl"
        Me.pricing_lbl.Size = New System.Drawing.Size(54, 13)
        Me.pricing_lbl.TabIndex = 8
        Me.pricing_lbl.Text = "Pricing :"
        '
        'pricing_val
        '
        Me.pricing_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pricing_val.FormattingEnabled = True
        Me.pricing_val.Location = New System.Drawing.Point(164, 113)
        Me.pricing_val.Name = "pricing_val"
        Me.pricing_val.Size = New System.Drawing.Size(100, 21)
        Me.pricing_val.TabIndex = 49
        '
        'dp_labor_val
        '
        Me.dp_labor_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.dp_labor_val.Location = New System.Drawing.Point(917, 113)
        Me.dp_labor_val.Name = "dp_labor_val"
        Me.dp_labor_val.Size = New System.Drawing.Size(100, 20)
        Me.dp_labor_val.TabIndex = 51
        '
        'dp_labor_lbl
        '
        Me.dp_labor_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.dp_labor_lbl.AutoSize = True
        Me.dp_labor_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dp_labor_lbl.Location = New System.Drawing.Point(784, 114)
        Me.dp_labor_lbl.Name = "dp_labor_lbl"
        Me.dp_labor_lbl.Size = New System.Drawing.Size(68, 13)
        Me.dp_labor_lbl.TabIndex = 50
        Me.dp_labor_lbl.Text = "Labor DP :"
        '
        'payement_terms_lbl
        '
        Me.payement_terms_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.payement_terms_lbl.AutoSize = True
        Me.payement_terms_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.payement_terms_lbl.Location = New System.Drawing.Point(391, 114)
        Me.payement_terms_lbl.Name = "payement_terms_lbl"
        Me.payement_terms_lbl.Size = New System.Drawing.Size(101, 13)
        Me.payement_terms_lbl.TabIndex = 17
        Me.payement_terms_lbl.Text = "Payment Terms :"
        '
        'payement_terms_val
        '
        Me.payement_terms_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.payement_terms_val.Location = New System.Drawing.Point(542, 113)
        Me.payement_terms_val.Name = "payement_terms_val"
        Me.payement_terms_val.Size = New System.Drawing.Size(216, 20)
        Me.payement_terms_val.TabIndex = 18
        '
        'dp_material_lbl
        '
        Me.dp_material_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.dp_material_lbl.AutoSize = True
        Me.dp_material_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dp_material_lbl.Location = New System.Drawing.Point(1154, 114)
        Me.dp_material_lbl.Name = "dp_material_lbl"
        Me.dp_material_lbl.Size = New System.Drawing.Size(61, 13)
        Me.dp_material_lbl.TabIndex = 52
        Me.dp_material_lbl.Text = "Mat. DP :"
        '
        'total_material_appr_val
        '
        Me.total_material_appr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_material_appr_val.Location = New System.Drawing.Point(1300, 135)
        Me.total_material_appr_val.Name = "total_material_appr_val"
        Me.total_material_appr_val.Size = New System.Drawing.Size(100, 20)
        Me.total_material_appr_val.TabIndex = 55
        '
        'total_material_appr_lbl
        '
        Me.total_material_appr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_material_appr_lbl.AutoSize = True
        Me.total_material_appr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total_material_appr_lbl.Location = New System.Drawing.Point(1154, 136)
        Me.total_material_appr_lbl.Name = "total_material_appr_lbl"
        Me.total_material_appr_lbl.Size = New System.Drawing.Size(119, 13)
        Me.total_material_appr_lbl.TabIndex = 52
        Me.total_material_appr_lbl.Text = "Tot.Mat. Prev App :"
        '
        'total_material_val
        '
        Me.total_material_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_material_val.Location = New System.Drawing.Point(917, 135)
        Me.total_material_val.Name = "total_material_val"
        Me.total_material_val.Size = New System.Drawing.Size(100, 20)
        Me.total_material_val.TabIndex = 54
        '
        'total_material_lbl
        '
        Me.total_material_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_material_lbl.AutoSize = True
        Me.total_material_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total_material_lbl.Location = New System.Drawing.Point(784, 136)
        Me.total_material_lbl.Name = "total_material_lbl"
        Me.total_material_lbl.Size = New System.Drawing.Size(93, 13)
        Me.total_material_lbl.TabIndex = 51
        Me.total_material_lbl.Text = "Total Material :"
        '
        'labor_lbl
        '
        Me.labor_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.labor_lbl.AutoSize = True
        Me.labor_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labor_lbl.Location = New System.Drawing.Point(3, 136)
        Me.labor_lbl.Name = "labor_lbl"
        Me.labor_lbl.Size = New System.Drawing.Size(47, 13)
        Me.labor_lbl.TabIndex = 51
        Me.labor_lbl.Text = "Labor :"
        '
        'labor_val
        '
        Me.labor_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.labor_val.Location = New System.Drawing.Point(164, 135)
        Me.labor_val.Name = "labor_val"
        Me.labor_val.Size = New System.Drawing.Size(100, 20)
        Me.labor_val.TabIndex = 54
        '
        'appr_labor_lbl
        '
        Me.appr_labor_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.appr_labor_lbl.AutoSize = True
        Me.appr_labor_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.appr_labor_lbl.Location = New System.Drawing.Point(391, 136)
        Me.appr_labor_lbl.Name = "appr_labor_lbl"
        Me.appr_labor_lbl.Size = New System.Drawing.Size(104, 13)
        Me.appr_labor_lbl.TabIndex = 52
        Me.appr_labor_lbl.Text = "Lab. Prev Appr. :"
        '
        'appr_labor_val
        '
        Me.appr_labor_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.appr_labor_val.Location = New System.Drawing.Point(542, 135)
        Me.appr_labor_val.Name = "appr_labor_val"
        Me.appr_labor_val.Size = New System.Drawing.Size(100, 20)
        Me.appr_labor_val.TabIndex = 55
        '
        'service_appr_val
        '
        Me.service_appr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.service_appr_val.Location = New System.Drawing.Point(1300, 179)
        Me.service_appr_val.Name = "service_appr_val"
        Me.service_appr_val.Size = New System.Drawing.Size(100, 20)
        Me.service_appr_val.TabIndex = 55
        '
        'service_appr_lbl
        '
        Me.service_appr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.service_appr_lbl.AutoSize = True
        Me.service_appr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.service_appr_lbl.Location = New System.Drawing.Point(1154, 180)
        Me.service_appr_lbl.Name = "service_appr_lbl"
        Me.service_appr_lbl.Size = New System.Drawing.Size(109, 13)
        Me.service_appr_lbl.TabIndex = 52
        Me.service_appr_lbl.Text = "Serv. Prev Appr. :"
        '
        'service_val
        '
        Me.service_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.service_val.Location = New System.Drawing.Point(917, 179)
        Me.service_val.Name = "service_val"
        Me.service_val.Size = New System.Drawing.Size(100, 20)
        Me.service_val.TabIndex = 54
        '
        'service_lbl
        '
        Me.service_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.service_lbl.AutoSize = True
        Me.service_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.service_lbl.Location = New System.Drawing.Point(784, 180)
        Me.service_lbl.Name = "service_lbl"
        Me.service_lbl.Size = New System.Drawing.Size(58, 13)
        Me.service_lbl.TabIndex = 51
        Me.service_lbl.Text = "Service :"
        '
        'freight_appr_val
        '
        Me.freight_appr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_appr_val.Location = New System.Drawing.Point(542, 179)
        Me.freight_appr_val.Name = "freight_appr_val"
        Me.freight_appr_val.Size = New System.Drawing.Size(100, 20)
        Me.freight_appr_val.TabIndex = 55
        '
        'freight_appr_lbl
        '
        Me.freight_appr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_appr_lbl.AutoSize = True
        Me.freight_appr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_appr_lbl.Location = New System.Drawing.Point(391, 180)
        Me.freight_appr_lbl.Name = "freight_appr_lbl"
        Me.freight_appr_lbl.Size = New System.Drawing.Size(108, 13)
        Me.freight_appr_lbl.TabIndex = 52
        Me.freight_appr_lbl.Text = "Frgh. Prev Appr. :"
        '
        'freight_lbl
        '
        Me.freight_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_lbl.AutoSize = True
        Me.freight_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_lbl.Location = New System.Drawing.Point(3, 180)
        Me.freight_lbl.Name = "freight_lbl"
        Me.freight_lbl.Size = New System.Drawing.Size(54, 13)
        Me.freight_lbl.TabIndex = 51
        Me.freight_lbl.Text = "Freight :"
        '
        'freight_val
        '
        Me.freight_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_val.Location = New System.Drawing.Point(164, 179)
        Me.freight_val.Name = "freight_val"
        Me.freight_val.Size = New System.Drawing.Size(100, 20)
        Me.freight_val.TabIndex = 54
        '
        'repair_val
        '
        Me.repair_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.repair_val.Location = New System.Drawing.Point(917, 157)
        Me.repair_val.Name = "repair_val"
        Me.repair_val.Size = New System.Drawing.Size(100, 20)
        Me.repair_val.TabIndex = 54
        '
        'repair_lbl
        '
        Me.repair_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.repair_lbl.AutoSize = True
        Me.repair_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.repair_lbl.Location = New System.Drawing.Point(784, 158)
        Me.repair_lbl.Name = "repair_lbl"
        Me.repair_lbl.Size = New System.Drawing.Size(74, 13)
        Me.repair_lbl.TabIndex = 51
        Me.repair_lbl.Text = "Small Mat. :"
        '
        'material_lbl
        '
        Me.material_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.material_lbl.AutoSize = True
        Me.material_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.material_lbl.Location = New System.Drawing.Point(3, 158)
        Me.material_lbl.Name = "material_lbl"
        Me.material_lbl.Size = New System.Drawing.Size(60, 13)
        Me.material_lbl.TabIndex = 51
        Me.material_lbl.Text = "Material :"
        '
        'material_val
        '
        Me.material_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.material_val.Location = New System.Drawing.Point(164, 157)
        Me.material_val.Name = "material_val"
        Me.material_val.Size = New System.Drawing.Size(100, 20)
        Me.material_val.TabIndex = 54
        '
        'repair_appr_lbl
        '
        Me.repair_appr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.repair_appr_lbl.AutoSize = True
        Me.repair_appr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.repair_appr_lbl.Location = New System.Drawing.Point(1154, 158)
        Me.repair_appr_lbl.Name = "repair_appr_lbl"
        Me.repair_appr_lbl.Size = New System.Drawing.Size(112, 13)
        Me.repair_appr_lbl.TabIndex = 52
        Me.repair_appr_lbl.Text = "S.Mat. Prev Appr.:"
        '
        'repair_appr_val
        '
        Me.repair_appr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.repair_appr_val.Location = New System.Drawing.Point(1300, 157)
        Me.repair_appr_val.Name = "repair_appr_val"
        Me.repair_appr_val.Size = New System.Drawing.Size(100, 20)
        Me.repair_appr_val.TabIndex = 55
        '
        'oem_feedback_lbl
        '
        Me.oem_feedback_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.oem_feedback_lbl.AutoSize = True
        Me.oem_feedback_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.oem_feedback_lbl.Location = New System.Drawing.Point(391, 205)
        Me.oem_feedback_lbl.Name = "oem_feedback_lbl"
        Me.oem_feedback_lbl.Size = New System.Drawing.Size(78, 13)
        Me.oem_feedback_lbl.TabIndex = 56
        Me.oem_feedback_lbl.Text = "OEM Fbck. :"
        '
        'oem_feedback_val
        '
        Me.oem_feedback_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.oem_feedback_val.FormattingEnabled = True
        Me.oem_feedback_val.Location = New System.Drawing.Point(542, 201)
        Me.oem_feedback_val.Name = "oem_feedback_val"
        Me.oem_feedback_val.Size = New System.Drawing.Size(100, 21)
        Me.oem_feedback_val.TabIndex = 57
        '
        'top_panel
        '
        Me.top_panel.AutoScroll = True
        Me.top_panel.AutoSize = True
        Me.top_panel.Controls.Add(Me.select_activity_grp)
        Me.top_panel.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel.Location = New System.Drawing.Point(0, 0)
        Me.top_panel.Name = "top_panel"
        Me.top_panel.Size = New System.Drawing.Size(1273, 55)
        Me.top_panel.TabIndex = 2
        '
        'select_activity_grp
        '
        Me.select_activity_grp.Controls.Add(Me.awq_archive_bt)
        Me.select_activity_grp.Controls.Add(Me.awq_form_bt)
        Me.select_activity_grp.Controls.Add(Me.map_sap_act_bt)
        Me.select_activity_grp.Controls.Add(Me.close_bt)
        Me.select_activity_grp.Controls.Add(Me.release_bt)
        Me.select_activity_grp.Controls.Add(Me.revise_bt)
        Me.select_activity_grp.Controls.Add(Me.cancel_bt)
        Me.select_activity_grp.Controls.Add(Me.awq_error_state_pic)
        Me.select_activity_grp.Controls.Add(Me.awq_select_right_pic)
        Me.select_activity_grp.Controls.Add(Me.awq_select_left_pic)
        Me.select_activity_grp.Controls.Add(Me.cb_awq)
        Me.select_activity_grp.Location = New System.Drawing.Point(0, 0)
        Me.select_activity_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.select_activity_grp.Name = "select_activity_grp"
        Me.select_activity_grp.Padding = New System.Windows.Forms.Padding(5)
        Me.select_activity_grp.Size = New System.Drawing.Size(1273, 50)
        Me.select_activity_grp.TabIndex = 0
        Me.select_activity_grp.TabStop = False
        Me.select_activity_grp.Text = "Select Activity"
        '
        'awq_archive_bt
        '
        Me.awq_archive_bt.Location = New System.Drawing.Point(348, 19)
        Me.awq_archive_bt.Name = "awq_archive_bt"
        Me.awq_archive_bt.Size = New System.Drawing.Size(53, 23)
        Me.awq_archive_bt.TabIndex = 11
        Me.awq_archive_bt.Text = "Archive"
        Me.awq_archive_bt.UseVisualStyleBackColor = True
        '
        'awq_form_bt
        '
        Me.awq_form_bt.Location = New System.Drawing.Point(275, 19)
        Me.awq_form_bt.Name = "awq_form_bt"
        Me.awq_form_bt.Size = New System.Drawing.Size(68, 23)
        Me.awq_form_bt.TabIndex = 10
        Me.awq_form_bt.Text = "AWQ Form"
        Me.awq_form_bt.UseVisualStyleBackColor = True
        '
        'map_sap_act_bt
        '
        Me.map_sap_act_bt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.map_sap_act_bt.Location = New System.Drawing.Point(1078, 12)
        Me.map_sap_act_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.map_sap_act_bt.Name = "map_sap_act_bt"
        Me.map_sap_act_bt.Size = New System.Drawing.Size(136, 32)
        Me.map_sap_act_bt.TabIndex = 9
        Me.map_sap_act_bt.Text = "Map With SAP Activity"
        Me.map_sap_act_bt.UseVisualStyleBackColor = True
        '
        'close_bt
        '
        Me.close_bt.Location = New System.Drawing.Point(119, 19)
        Me.close_bt.Name = "close_bt"
        Me.close_bt.Size = New System.Drawing.Size(41, 23)
        Me.close_bt.TabIndex = 8
        Me.close_bt.Text = "Close"
        Me.close_bt.UseVisualStyleBackColor = True
        '
        'release_bt
        '
        Me.release_bt.Location = New System.Drawing.Point(54, 19)
        Me.release_bt.Name = "release_bt"
        Me.release_bt.Size = New System.Drawing.Size(60, 23)
        Me.release_bt.TabIndex = 7
        Me.release_bt.Text = "Release"
        Me.release_bt.UseVisualStyleBackColor = True
        '
        'revise_bt
        '
        Me.revise_bt.Location = New System.Drawing.Point(220, 19)
        Me.revise_bt.Name = "revise_bt"
        Me.revise_bt.Size = New System.Drawing.Size(49, 23)
        Me.revise_bt.TabIndex = 6
        Me.revise_bt.Text = "Revise"
        Me.revise_bt.UseVisualStyleBackColor = True
        '
        'cancel_bt
        '
        Me.cancel_bt.Location = New System.Drawing.Point(165, 19)
        Me.cancel_bt.Name = "cancel_bt"
        Me.cancel_bt.Size = New System.Drawing.Size(49, 23)
        Me.cancel_bt.TabIndex = 5
        Me.cancel_bt.Text = "Cancel"
        Me.cancel_bt.UseVisualStyleBackColor = True
        '
        'awq_error_state_pic
        '
        Me.awq_error_state_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.awq_error_state_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.no_errors
        Me.awq_error_state_pic.Location = New System.Drawing.Point(12, 12)
        Me.awq_error_state_pic.Name = "awq_error_state_pic"
        Me.awq_error_state_pic.Size = New System.Drawing.Size(36, 32)
        Me.awq_error_state_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.awq_error_state_pic.TabIndex = 4
        Me.awq_error_state_pic.TabStop = False
        Me.awq_error_state_pic.Tag = ""
        '
        'awq_select_right_pic
        '
        Me.awq_select_right_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.awq_select_right_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.awq_select_right_pic.Location = New System.Drawing.Point(1020, 12)
        Me.awq_select_right_pic.Name = "awq_select_right_pic"
        Me.awq_select_right_pic.Size = New System.Drawing.Size(36, 32)
        Me.awq_select_right_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.awq_select_right_pic.TabIndex = 2
        Me.awq_select_right_pic.TabStop = False
        '
        'awq_select_left_pic
        '
        Me.awq_select_left_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.awq_select_left_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.awq_select_left_pic.Location = New System.Drawing.Point(547, 10)
        Me.awq_select_left_pic.Name = "awq_select_left_pic"
        Me.awq_select_left_pic.Size = New System.Drawing.Size(36, 32)
        Me.awq_select_left_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.awq_select_left_pic.TabIndex = 1
        Me.awq_select_left_pic.TabStop = False
        '
        'cb_awq
        '
        Me.cb_awq.FormattingEnabled = True
        Me.cb_awq.Location = New System.Drawing.Point(595, 17)
        Me.cb_awq.Name = "cb_awq"
        Me.cb_awq.Size = New System.Drawing.Size(421, 21)
        Me.cb_awq.TabIndex = 0
        '
        'act_lab_mat_panel
        '
        Me.act_lab_mat_panel.AutoScroll = True
        Me.act_lab_mat_panel.Controls.Add(Me.launch_awq_bt)
        Me.act_lab_mat_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_lab_mat_panel.Location = New System.Drawing.Point(5, 18)
        Me.act_lab_mat_panel.Name = "act_lab_mat_panel"
        Me.act_lab_mat_panel.Size = New System.Drawing.Size(1572, 336)
        Me.act_lab_mat_panel.TabIndex = 2
        '
        'launch_awq_bt
        '
        Me.launch_awq_bt.Controls.Add(Me.act_matlab_addlab_page)
        Me.launch_awq_bt.Controls.Add(Me.act_matlab_mat_page)
        Me.launch_awq_bt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.launch_awq_bt.Location = New System.Drawing.Point(0, 0)
        Me.launch_awq_bt.Name = "launch_awq_bt"
        Me.launch_awq_bt.SelectedIndex = 0
        Me.launch_awq_bt.Size = New System.Drawing.Size(1572, 336)
        Me.launch_awq_bt.TabIndex = 3
        '
        'act_matlab_addlab_page
        '
        Me.act_matlab_addlab_page.AutoScroll = True
        Me.act_matlab_addlab_page.Controls.Add(Me.act_add_lab_dgrid)
        Me.act_matlab_addlab_page.Location = New System.Drawing.Point(4, 22)
        Me.act_matlab_addlab_page.Name = "act_matlab_addlab_page"
        Me.act_matlab_addlab_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_matlab_addlab_page.Size = New System.Drawing.Size(1564, 310)
        Me.act_matlab_addlab_page.TabIndex = 0
        Me.act_matlab_addlab_page.Text = "Additional Labor"
        Me.act_matlab_addlab_page.UseVisualStyleBackColor = True
        '
        'act_add_lab_dgrid
        '
        Me.act_add_lab_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.act_add_lab_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_add_lab_dgrid.Location = New System.Drawing.Point(3, 3)
        Me.act_add_lab_dgrid.Name = "act_add_lab_dgrid"
        Me.act_add_lab_dgrid.Size = New System.Drawing.Size(1558, 304)
        Me.act_add_lab_dgrid.TabIndex = 0
        '
        'act_matlab_mat_page
        '
        Me.act_matlab_mat_page.AutoScroll = True
        Me.act_matlab_mat_page.Controls.Add(Me.act_materials_dgrid)
        Me.act_matlab_mat_page.Location = New System.Drawing.Point(4, 22)
        Me.act_matlab_mat_page.Name = "act_matlab_mat_page"
        Me.act_matlab_mat_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_matlab_mat_page.Size = New System.Drawing.Size(1564, 310)
        Me.act_matlab_mat_page.TabIndex = 2
        Me.act_matlab_mat_page.Text = "Materials"
        Me.act_matlab_mat_page.UseVisualStyleBackColor = True
        '
        'act_materials_dgrid
        '
        Me.act_materials_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.act_materials_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_materials_dgrid.Location = New System.Drawing.Point(3, 3)
        Me.act_materials_dgrid.Name = "act_materials_dgrid"
        Me.act_materials_dgrid.Size = New System.Drawing.Size(1558, 304)
        Me.act_materials_dgrid.TabIndex = 1
        '
        'act_lab_mat_grp
        '
        Me.act_lab_mat_grp.Controls.Add(Me.act_lab_mat_panel)
        Me.act_lab_mat_grp.Location = New System.Drawing.Point(5, 328)
        Me.act_lab_mat_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.act_lab_mat_grp.Name = "act_lab_mat_grp"
        Me.act_lab_mat_grp.Padding = New System.Windows.Forms.Padding(5)
        Me.act_lab_mat_grp.Size = New System.Drawing.Size(1582, 359)
        Me.act_lab_mat_grp.TabIndex = 3
        Me.act_lab_mat_grp.TabStop = False
        Me.act_lab_mat_grp.Text = "Labor And Material Applicabilities"
        '
        'dep_act_grp
        '
        Me.dep_act_grp.Controls.Add(Me.select_main_act_cb)
        Me.dep_act_grp.Controls.Add(Me.main_act_lbl)
        Me.dep_act_grp.Controls.Add(Me.select_act_right)
        Me.dep_act_grp.Controls.Add(Me.select_act_left)
        Me.dep_act_grp.Controls.Add(Me.act_dep_all_listb)
        Me.dep_act_grp.Controls.Add(Me.act_dep_selected_listb)
        Me.dep_act_grp.Location = New System.Drawing.Point(3, 3)
        Me.dep_act_grp.Name = "dep_act_grp"
        Me.dep_act_grp.Size = New System.Drawing.Size(1583, 317)
        Me.dep_act_grp.TabIndex = 2
        Me.dep_act_grp.TabStop = False
        Me.dep_act_grp.Text = "Select Dependant Activities"
        '
        'select_main_act_cb
        '
        Me.select_main_act_cb.FormattingEnabled = True
        Me.select_main_act_cb.Location = New System.Drawing.Point(1237, 29)
        Me.select_main_act_cb.Name = "select_main_act_cb"
        Me.select_main_act_cb.Size = New System.Drawing.Size(340, 21)
        Me.select_main_act_cb.TabIndex = 0
        '
        'main_act_lbl
        '
        Me.main_act_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.main_act_lbl.AutoSize = True
        Me.main_act_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.main_act_lbl.Location = New System.Drawing.Point(1136, 32)
        Me.main_act_lbl.Name = "main_act_lbl"
        Me.main_act_lbl.Size = New System.Drawing.Size(92, 13)
        Me.main_act_lbl.TabIndex = 9
        Me.main_act_lbl.Text = "Main Activity  :"
        '
        'select_act_right
        '
        Me.select_act_right.Cursor = System.Windows.Forms.Cursors.Hand
        Me.select_act_right.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.select_act_right.Location = New System.Drawing.Point(532, 94)
        Me.select_act_right.Name = "select_act_right"
        Me.select_act_right.Size = New System.Drawing.Size(36, 32)
        Me.select_act_right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.select_act_right.TabIndex = 4
        Me.select_act_right.TabStop = False
        '
        'select_act_left
        '
        Me.select_act_left.Cursor = System.Windows.Forms.Cursors.Hand
        Me.select_act_left.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.select_act_left.Location = New System.Drawing.Point(532, 155)
        Me.select_act_left.Name = "select_act_left"
        Me.select_act_left.Size = New System.Drawing.Size(36, 32)
        Me.select_act_left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.select_act_left.TabIndex = 3
        Me.select_act_left.TabStop = False
        '
        'act_dep_all_listb
        '
        Me.act_dep_all_listb.FormattingEnabled = True
        Me.act_dep_all_listb.Location = New System.Drawing.Point(3, 16)
        Me.act_dep_all_listb.Name = "act_dep_all_listb"
        Me.act_dep_all_listb.Size = New System.Drawing.Size(523, 290)
        Me.act_dep_all_listb.TabIndex = 1
        '
        'act_dep_selected_listb
        '
        Me.act_dep_selected_listb.FormattingEnabled = True
        Me.act_dep_selected_listb.Location = New System.Drawing.Point(574, 16)
        Me.act_dep_selected_listb.Name = "act_dep_selected_listb"
        Me.act_dep_selected_listb.Size = New System.Drawing.Size(543, 290)
        Me.act_dep_selected_listb.TabIndex = 0
        '
        'awr_applicability_ly
        '
        Me.awr_applicability_ly.AutoScroll = True
        Me.awr_applicability_ly.Controls.Add(Me.dep_act_grp)
        Me.awr_applicability_ly.Controls.Add(Me.act_lab_mat_grp)
        Me.awr_applicability_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.awr_applicability_ly.Location = New System.Drawing.Point(3, 10)
        Me.awr_applicability_ly.Name = "awr_applicability_ly"
        Me.awr_applicability_ly.Size = New System.Drawing.Size(1259, 302)
        Me.awr_applicability_ly.TabIndex = 10
        '
        'awq_lab_mat_ctrl
        '
        Me.awq_lab_mat_ctrl.Controls.Add(Me.awq_lab_mat_ctrl_dependant_act_selection_page)
        Me.awq_lab_mat_ctrl.Controls.Add(Me.awq_lab_mat_ctrl_awq_price_sum_page)
        Me.awq_lab_mat_ctrl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.awq_lab_mat_ctrl.Location = New System.Drawing.Point(5, 336)
        Me.awq_lab_mat_ctrl.Name = "awq_lab_mat_ctrl"
        Me.awq_lab_mat_ctrl.SelectedIndex = 0
        Me.awq_lab_mat_ctrl.Size = New System.Drawing.Size(1273, 341)
        Me.awq_lab_mat_ctrl.TabIndex = 10
        '
        'awq_lab_mat_ctrl_dependant_act_selection_page
        '
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.AutoScroll = True
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.Controls.Add(Me.awr_applicability_ly)
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.Location = New System.Drawing.Point(4, 22)
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.Name = "awq_lab_mat_ctrl_dependant_act_selection_page"
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.Padding = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.Size = New System.Drawing.Size(1265, 315)
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.TabIndex = 0
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.Text = "Activities Mateiral & Labor Applicability"
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.UseVisualStyleBackColor = True
        '
        'awq_lab_mat_ctrl_awq_price_sum_page
        '
        Me.awq_lab_mat_ctrl_awq_price_sum_page.AutoScroll = True
        Me.awq_lab_mat_ctrl_awq_price_sum_page.Controls.Add(Me.awq_summary_ly)
        Me.awq_lab_mat_ctrl_awq_price_sum_page.Location = New System.Drawing.Point(4, 22)
        Me.awq_lab_mat_ctrl_awq_price_sum_page.Name = "awq_lab_mat_ctrl_awq_price_sum_page"
        Me.awq_lab_mat_ctrl_awq_price_sum_page.Padding = New System.Windows.Forms.Padding(3)
        Me.awq_lab_mat_ctrl_awq_price_sum_page.Size = New System.Drawing.Size(1265, 320)
        Me.awq_lab_mat_ctrl_awq_price_sum_page.TabIndex = 1
        Me.awq_lab_mat_ctrl_awq_price_sum_page.Text = "AWQ Price Summary"
        Me.awq_lab_mat_ctrl_awq_price_sum_page.UseVisualStyleBackColor = True
        '
        'awq_summary_ly
        '
        Me.awq_summary_ly.Controls.Add(Me.Label1)
        Me.awq_summary_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.awq_summary_ly.Location = New System.Drawing.Point(3, 3)
        Me.awq_summary_ly.Name = "awq_summary_ly"
        Me.awq_summary_ly.Size = New System.Drawing.Size(1259, 314)
        Me.awq_summary_ly.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "T.B.A"
        '
        'FormAWQEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1283, 682)
        Me.Controls.Add(Me.awq_lab_mat_ctrl)
        Me.Controls.Add(Me.top_panel_activity)
        Me.Name = "FormAWQEdit"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.Text = "Edit Additional Work Quote Data"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.top_panel_activity.ResumeLayout(False)
        Me.top_panel_activity.PerformLayout()
        Me.awq_info.ResumeLayout(False)
        Me.awq_info.PerformLayout()
        Me.awq_info_grp.ResumeLayout(False)
        Me.product_details_lay.ResumeLayout(False)
        Me.product_details_lay.PerformLayout()
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel7.PerformLayout()
        CType(Me.offer_validity_val_cancel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ground_time_tb_ly.ResumeLayout(False)
        Me.ground_time_tb_ly.PerformLayout()
        Me.due_date_val_tb_ly.ResumeLayout(False)
        Me.due_date_val_tb_ly.PerformLayout()
        CType(Me.due_date_val_cancel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.issue_date_tb_ly.ResumeLayout(False)
        Me.issue_date_tb_ly.PerformLayout()
        CType(Me.issue_date_val_cancel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cust_sent_date_val_tb_ly.ResumeLayout(False)
        Me.cust_sent_date_val_tb_ly.PerformLayout()
        CType(Me.cust_sent_date_val_cancel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cust_fdbk_due_date_tb_ly.ResumeLayout(False)
        Me.cust_fdbk_due_date_tb_ly.PerformLayout()
        CType(Me.cust_fdbk_due_date_val_cancel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cust_feedback_actual_tb_ly.ResumeLayout(False)
        Me.cust_feedback_actual_tb_ly.PerformLayout()
        CType(Me.cust_feedback_actual_val_cancel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.top_panel.ResumeLayout(False)
        Me.select_activity_grp.ResumeLayout(False)
        CType(Me.awq_error_state_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.awq_select_right_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.awq_select_left_pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_lab_mat_panel.ResumeLayout(False)
        Me.launch_awq_bt.ResumeLayout(False)
        Me.act_matlab_addlab_page.ResumeLayout(False)
        CType(Me.act_add_lab_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_matlab_mat_page.ResumeLayout(False)
        CType(Me.act_materials_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_lab_mat_grp.ResumeLayout(False)
        Me.dep_act_grp.ResumeLayout(False)
        Me.dep_act_grp.PerformLayout()
        CType(Me.select_act_right, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.select_act_left, System.ComponentModel.ISupportInitialize).EndInit()
        Me.awr_applicability_ly.ResumeLayout(False)
        Me.awq_lab_mat_ctrl.ResumeLayout(False)
        Me.awq_lab_mat_ctrl_dependant_act_selection_page.ResumeLayout(False)
        Me.awq_lab_mat_ctrl_awq_price_sum_page.ResumeLayout(False)
        Me.awq_summary_ly.ResumeLayout(False)
        Me.awq_summary_ly.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents top_panel_activity As Panel
    Friend WithEvents awq_info_grp As GroupBox
    Friend WithEvents select_activity_grp As GroupBox
    Friend WithEvents awq_select_right_pic As PictureBox
    Friend WithEvents awq_select_left_pic As PictureBox
    Friend WithEvents cb_awq As ComboBox
    Friend WithEvents act_lab_mat_panel As Panel
    Friend WithEvents act_lab_mat_grp As GroupBox
    Friend WithEvents launch_awq_bt As TabControl
    Friend WithEvents act_matlab_addlab_page As TabPage
    Friend WithEvents act_matlab_mat_page As TabPage
    Friend WithEvents act_add_lab_dgrid As DataGridView
    Friend WithEvents act_materials_dgrid As DataGridView
    Friend WithEvents awq_error_state_pic As PictureBox
    Friend WithEvents product_details_lay As TableLayoutPanel
    Friend WithEvents weight_and_balance_val As TextBox
    Friend WithEvents comments_val As TextBox
    Friend WithEvents description_val As TextBox
    Friend WithEvents revision_val As TextBox
    Friend WithEvents reference_val As TextBox
    Friend WithEvents airworthiness_lbl As Label
    Friend WithEvents priority_lbl As Label
    Friend WithEvents weight_and_balance_lbl As Label
    Friend WithEvents issue_date_lbl As Label
    Friend WithEvents offer_validity_lbl As Label
    Friend WithEvents ground_time_lbl As Label
    Friend WithEvents comments_lbl As Label
    Friend WithEvents description_lbl As Label
    Friend WithEvents reference_lbl As Label
    Friend WithEvents revision_lbl As Label
    Friend WithEvents payement_terms_val As TextBox
    Friend WithEvents payement_terms_lbl As Label
    Friend WithEvents pricing_lbl As Label
    Friend WithEvents customer_comment_val As TextBox
    Friend WithEvents customer_comment_lbl As Label
    Friend WithEvents cust_feedback_actual_lbl As Label
    Friend WithEvents cust_feedback_lbl As Label
    Friend WithEvents cust_fdbk_due_date_lbl As Label
    Friend WithEvents cust_sent_date_lbl As Label
    Friend WithEvents dep_act_grp As GroupBox
    Friend WithEvents select_main_act_cb As ComboBox
    Friend WithEvents main_act_lbl As Label
    Friend WithEvents select_act_right As PictureBox
    Friend WithEvents select_act_left As PictureBox
    Friend WithEvents act_dep_all_listb As ListBox
    Friend WithEvents act_dep_selected_listb As ListBox
    Friend WithEvents awr_applicability_ly As FlowLayoutPanel
    Friend WithEvents offer_validity_val As DateTimePicker
    Friend WithEvents ground_time_val As TextBox
    Friend WithEvents ground_time_unit_val As ComboBox
    Friend WithEvents airworthiness_val As ComboBox
    Friend WithEvents issue_date_val As DateTimePicker
    Friend WithEvents due_date_val As DateTimePicker
    Friend WithEvents due_date_lbl As Label
    Friend WithEvents priority_val As ComboBox
    Friend WithEvents internal_status_val As ComboBox
    Friend WithEvents internal_status_lbl As Label
    Friend WithEvents cust_sent_date_val As DateTimePicker
    Friend WithEvents cust_feedback_val As ComboBox
    Friend WithEvents cust_fdbk_due_date_val As DateTimePicker
    Friend WithEvents cust_feedback_actual_val As DateTimePicker
    Friend WithEvents pricing_val As ComboBox
    Friend WithEvents dp_material_val As TextBox
    Friend WithEvents dp_material_lbl As Label
    Friend WithEvents dp_labor_val As TextBox
    Friend WithEvents dp_labor_lbl As Label
    Friend WithEvents is_latest_rev_lbl As Label
    Friend WithEvents is_latest_rev_calc_val As CheckBox
    Friend WithEvents ground_time_tb_ly As TableLayoutPanel
    Friend WithEvents TableLayoutPanel7 As TableLayoutPanel
    Friend WithEvents offer_validity_val_cancel As PictureBox
    Friend WithEvents cust_sent_date_val_tb_ly As TableLayoutPanel
    Friend WithEvents cust_sent_date_val_cancel As PictureBox
    Friend WithEvents cust_feedback_actual_tb_ly As TableLayoutPanel
    Friend WithEvents cust_feedback_actual_val_cancel As PictureBox
    Friend WithEvents issue_date_tb_ly As TableLayoutPanel
    Friend WithEvents issue_date_val_cancel As PictureBox
    Friend WithEvents due_date_val_tb_ly As TableLayoutPanel
    Friend WithEvents due_date_val_cancel As PictureBox
    Friend WithEvents cust_fdbk_due_date_tb_ly As TableLayoutPanel
    Friend WithEvents cust_fdbk_due_date_val_cancel As PictureBox
    Friend WithEvents awq_lab_mat_ctrl As TabControl
    Friend WithEvents awq_lab_mat_ctrl_dependant_act_selection_page As TabPage
    Friend WithEvents awq_lab_mat_ctrl_awq_price_sum_page As TabPage
    Friend WithEvents awq_summary_ly As FlowLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents total_material_val As TextBox
    Friend WithEvents labor_val As TextBox
    Friend WithEvents service_val As TextBox
    Friend WithEvents freight_val As TextBox
    Friend WithEvents repair_val As TextBox
    Friend WithEvents material_val As TextBox
    Friend WithEvents total_material_lbl As Label
    Friend WithEvents labor_lbl As Label
    Friend WithEvents service_lbl As Label
    Friend WithEvents freight_lbl As Label
    Friend WithEvents repair_lbl As Label
    Friend WithEvents material_lbl As Label
    Friend WithEvents freight_appr_lbl As Label
    Friend WithEvents service_appr_val As TextBox
    Friend WithEvents service_appr_lbl As Label
    Friend WithEvents freight_appr_val As TextBox
    Friend WithEvents total_material_appr_val As TextBox
    Friend WithEvents total_material_appr_lbl As Label
    Friend WithEvents appr_labor_val As TextBox
    Friend WithEvents appr_labor_lbl As Label
    Friend WithEvents mat_appr_val As TextBox
    Friend WithEvents mat_appr_lbl As Label
    Friend WithEvents repair_appr_val As TextBox
    Friend WithEvents repair_appr_lbl As Label
    Friend WithEvents release_bt As Button
    Friend WithEvents revise_bt As Button
    Friend WithEvents cancel_bt As Button
    Friend WithEvents close_bt As Button
    Friend WithEvents map_sap_act_bt As Button
    Friend WithEvents awq_form_bt As Button
    Friend WithEvents top_panel As Panel
    Friend WithEvents awq_info As Panel
    Friend WithEvents awq_archive_bt As Button
    Friend WithEvents pm_go_ahead_val As CheckBox
    Friend WithEvents pm_go_ahead_lbl As Label
    Friend WithEvents oem_feedback_lbl As Label
    Friend WithEvents oem_feedback_val As ComboBox
End Class

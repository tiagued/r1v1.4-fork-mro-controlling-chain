﻿
Imports Equin.ApplicationFramework

Public Class CDownPaymentRowValidator
    Public Shared instance As CDownPaymentRowValidator
    Public Shared Function getInstance() As CDownPaymentRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CDownPaymentRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(downp As CDownPayment)
        downp.calc_is_in_error_state = False
    End Sub

    Public Function lab_mat_applicability(downp As CDownPayment) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(downp.lab_mat_applicability) OrElse downp.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Lab/Mat Applicability cannot be empty"
            res = False
            downp.calc_is_in_error_state = True
        Else
            Dim isLabMatCurrSame = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr
            If isLabMatCurrSame And downp.lab_mat_applicability <> MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_LABMAT_APP_LABMAT_KEY).value Then
                err = "Lab/Mat Applicability Must be Labor & Material as the project labor and material currencies are the same"
                res = False
                downp.calc_is_in_error_state = True
            ElseIf Not isLabMatCurrSame And downp.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_LABMAT_APP_LABMAT_KEY).value Then
                err = "Labor and Material currencies are differents, applicability shall be either labor or material"
                res = False
                downp.calc_is_in_error_state = True
            End If
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'numner cannot be empty and is unique
    Public Function number(downp As CDownPayment) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(downp.number) Then
            err = "Number cannot be empty"
            res = False
            downp.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'value cannot be 0 
    Public Function dp_value(downp As CDownPayment) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If downp.dp_value <= 0 Then
            err = "Downpayment value shall be greather than 0"
            res = False
            downp.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'description cannot be empty 
    Public Function description(downp As CDownPayment) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(downp.description) Then
            err = "Description cannot be empty"
            res = False
            downp.calc_is_in_error_state = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'due date cannot be empty 
    Public Function due_date(downp As CDownPayment) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If downp.due_date = MConstants.APP_NULL_DATE Then
            err = "Due Date cannot be empty"
            res = False
            downp.calc_is_in_error_state = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class
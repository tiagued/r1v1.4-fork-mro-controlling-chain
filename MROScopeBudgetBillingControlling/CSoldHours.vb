﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CSoldHours
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Shared currentMaxID As Integer
    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_SOLDHRS_LOADED Then
            'set combo box
            'set first cc in list
            'use Me. to trigger object retrieval from id
            _cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
            'set object to avoid using Me.cc that will trigger calculation
            _cc_obj = MConstants.listOfCC(_cc)
            _payer_obj = MConstants.listOfPayer(_payer)
            _billing_work_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_BILL_STATUS_OPEN_KEY).value
            _discount_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _awq_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _nte_category_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _task_rate_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _fix_price_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_SOLDHRS_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_SOLD_HRS_DB_READ
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""
        Try
            If _is_sap_import Then
                res = False
                mess = "Item is imported from SAP so cannot be deleted"
            ElseIf Not IsNothing(_awq_obj) Then
                res = False
                mess = "Item cannot be deleted because it is linked to AWQ " & _awq_obj.reference_revision
            Else
                'remove sold hours from activity
                If Not IsNothing(_act_object) AndAlso _act_object.sold_hour_list.Contains(Me) Then
                    _act_object.sold_hour_list.Remove(Me)
                    _act_object.updateCal()
                End If
                Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                MConstants.GLB_MSTR_CTRL.add_scope_controller.deleteObjectSoldHours(Me)
                res = True
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not IsNothing(_awq_obj) AndAlso Not _awq_obj.is_in_work Then
            res = False
            mess = "Cannot be modifird cause it's linked to a released AWQ"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        If Not _calc_is_in_error_state AndAlso Not String.IsNullOrWhiteSpace(_calc_error_text_thrown) Then
            Return New KeyValuePair(Of Boolean, String)(True, _calc_error_text_thrown)
        Else
            Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
        End If
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_SOLDHRS_LOADED Then
                    CSoldHours.currentMaxID = Math.Max(CSoldHours.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _act_id As Long
    Public Property act_id() As Long
        Get
            Return _act_id
        End Get
        Set(ByVal value As Long)
            If Not _act_id = value Then
                _act_id = value
                'in case of sap import act object is already set and act id is set after activity creation
                If Not (Not IsNothing(_act_object) AndAlso _act_object.id = _act_id) Then
                    Me.act_object = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(_act_id)
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act_object As CScopeActivity
    Public Property act_object() As CScopeActivity
        Get
            Return _act_object
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(value, _act_object) Then
                _act_object = value
                If IsNothing(value) Then
                    _act_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _act_id = value.id
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'quote entry
    Private _init_quote_entry_obj As CSalesQuoteEntry
    Public Property init_quote_entry_obj() As CSalesQuoteEntry
        Get
            Return _init_quote_entry_obj
        End Get
        Set(ByVal value As CSalesQuoteEntry)
            If Not Object.Equals(_init_quote_entry_obj, value) Then
                _init_quote_entry_obj = value
                If IsNothing(value) Then
                    _init_quote_entry_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _init_quote_entry_id = _init_quote_entry_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _init_quote_entry_id As Long
    Public Property init_quote_entry_id() As Long
        Get
            Return _init_quote_entry_id
        End Get
        Set(ByVal value As Long)
            If Not _init_quote_entry_id = value Then
                _init_quote_entry_id = value
                If _init_quote_entry_id > 0 Then
                    _init_quote_entry_obj = MConstants.GLB_MSTR_CTRL.ini_scope_controller.getQuoteEntryObject(_init_quote_entry_id)
                Else
                    _init_quote_entry_obj = Nothing
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Property calc_quote_entry_str() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_init_quote_entry_obj) Then
                res = _init_quote_entry_obj.quote_obj.number & "/" & _init_quote_entry_obj.product
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _awq_id As Long
    Public Property awq_id() As Long
        Get
            Return _awq_id
        End Get
        Set(ByVal value As Long)
            If Not _awq_id = value Then
                _awq_id = value
                If MConstants.GLOB_AWQ_LOADED Then
                    If _awq_id > 0 Then
                        Me.awq_obj = MConstants.GLB_MSTR_CTRL.awq_controller.getAWQObject(_awq_id)
                    Else
                        Me.awq_obj = Nothing
                    End If
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _awq_obj As CAWQuotation
    Public Property awq_obj() As CAWQuotation
        Get
            Return _awq_obj
        End Get
        Set(ByVal value As CAWQuotation)
            If Not Object.Equals(value, _awq_obj) Then
                _awq_obj = value
                'set ID
                If IsNothing(_awq_obj) Then
                    _awq_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _awq_id = _awq_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'awq that cancel the current labor line
    Private _remove_awq_id As Long
    Public Property remove_awq_id() As Long
        Get
            Return _remove_awq_id
        End Get
        Set(ByVal value As Long)
            If Not _remove_awq_id = value Then
                _remove_awq_id = value
                If MConstants.GLOB_AWQ_LOADED Then
                    If remove_awq_id > 0 Then
                        Me.remove_awq_obj = MConstants.GLB_MSTR_CTRL.awq_controller.getAWQObject(_remove_awq_id)
                    Else
                        Me.remove_awq_obj = Nothing
                    End If
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _remove_awq_obj As CAWQuotation
    Public Property remove_awq_obj() As CAWQuotation
        Get
            Return _remove_awq_obj
        End Get
        Set(ByVal value As CAWQuotation)
            If Not Object.Equals(value, _remove_awq_obj) Then
                _remove_awq_obj = value
                'set ID
                If IsNothing(_remove_awq_obj) Then
                    _remove_awq_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _remove_awq_id = _remove_awq_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Property is_removed_awq As Boolean
        Get
            Return Not IsNothing(_remove_awq_obj)
        End Get
        Set(value As Boolean)
            NotifyPropertyChanged()
        End Set
    End Property
    Public Property is_awq As Boolean
        Get
            Return Not IsNothing(_awq_obj)
        End Get
        Set(value As Boolean)
            NotifyPropertyChanged()
        End Set
    End Property
    'to add quote summary with checkbox in init view
    Public Property is_quote_sum As Boolean
        Get
            Return Not IsNothing(_init_quote_entry_obj)
        End Get
        Set(value As Boolean)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _discount_id As Long
    Public Property discount_id() As Long
        Get
            Return _discount_id
        End Get
        Set(ByVal value As Long)
            If Not _discount_id = value Then
                _discount_id = value
                If _discount_id > 0 Then
                    Me.discount_obj = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getDiscountObject(_discount_id)
                Else
                    Me.discount_obj = Nothing
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _discount_obj As CDiscount
    Public Property discount_obj() As CDiscount
        Get
            Return _discount_obj
        End Get
        Set(ByVal value As CDiscount)
            If Not Object.Equals(value, _discount_obj) Then
                _discount_obj = value
                'set ID
                If IsNothing(_discount_obj) Then
                    _discount_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _discount_id = _discount_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property
    Private _nte_category_id As Long
    Public Property nte_category_id() As Long
        Get
            Return _nte_category_id
        End Get
        Set(ByVal value As Long)
            If Not _nte_category_id = value Then
                _nte_category_id = value
                'retrieve only for none empty
                If _nte_category_id > 0 Then
                    Me.nte_obj = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getNtetObject(_nte_category_id)
                Else
                    Me.nte_obj = Nothing
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _nte_obj As CNotToExceed
    Public Property nte_obj() As CNotToExceed
        Get
            Return _nte_obj
        End Get
        Set(ByVal value As CNotToExceed)
            If Not Object.Equals(value, _nte_obj) Then
                _nte_obj = value
                'set ID
                If IsNothing(_nte_obj) Then
                    _nte_category_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _nte_category_id = _nte_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'operation data
    Private _operation_desc As String
    Public Property operation_desc() As String
        Get
            Return _operation_desc
        End Get
        Set(ByVal value As String)
            If Not _operation_desc = value Then
                _operation_desc = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _billing_comment As String
    Public Property billing_comment() As String
        Get
            Return _billing_comment
        End Get
        Set(ByVal value As String)
            If Not _billing_comment = value Then
                _billing_comment = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _red_mark As String
    Public Property red_mark() As String
        Get
            Return _red_mark
        End Get
        Set(ByVal value As String)
            If Not _red_mark = value Then
                _red_mark = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'cc
    Private _cc As String
    Public Property cc() As String
        Get
            Return _cc
        End Get
        Set(ByVal value As String)
            If Not _cc = value Then
                _cc = value
                If MConstants.listOfCC.ContainsKey(_cc) Then
                    Me.cc_obj = MConstants.listOfCC(_cc)
                Else
                    Me.cc_obj = Nothing
                    Throw New Exception("Cost center does not exist in cc list : " & _cc)
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cc_obj As CCostCenter
    Public Property cc_obj() As CCostCenter
        Get
            Return _cc_obj
        End Get
        Set(ByVal value As CCostCenter)
            If Not Object.Equals(value, _cc_obj) Then
                _cc_obj = value
                If IsNothing(_cc_obj) Then
                    'use Me.cc to force obj value
                    Me.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _cc = _cc_obj.cc
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _plan_cc As String
    Public Property plan_cc() As String
        Get
            'if empty string cause some error in dictionaries with cc as key
            Return If(String.IsNullOrWhiteSpace(_plan_cc), MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value, _plan_cc)
        End Get
        Set(ByVal value As String)
            If Not _plan_cc = value Then
                _plan_cc = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _plan_hrs As Double
    Public Property plan_hrs() As Double
        Get
            Return _plan_hrs
        End Get
        Set(ByVal value As Double)
            If Not _plan_hrs = value Then
                _plan_hrs = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'billing data
    Private _sold_hrs As Double
    Public Property sold_hrs() As Double
        Get
            Return _sold_hrs
        End Get
        Set(ByVal value As Double)
            If Not _sold_hrs = value Then
                _sold_hrs = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _sold_hrs_adj As Double
    Public Property sold_hrs_adj() As Double
        Get
            Return _sold_hrs_adj
        End Get
        Set(ByVal value As Double)
            If Not _sold_hrs_adj = value Then
                _sold_hrs_adj = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer As String
    Public Property payer() As String
        Get
            Return _payer
        End Get
        Set(ByVal value As String)
            If Not _payer = value Then
                _payer = value
                If MConstants.listOfPayer.ContainsKey(_payer) Then
                    Me.payer_obj = MConstants.listOfPayer(_payer)
                Else
                    Throw New Exception("Payer does not exist in payer list : " & _payer)
                    Me.payer_obj = Nothing
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer_obj As CPayer
    Public Property payer_obj() As CPayer
        Get
            Return _payer_obj
        End Get
        Set(ByVal value As CPayer)
            If Not Object.Equals(_payer_obj, value) Then
                _payer_obj = value
                If IsNothing(_payer_obj) Then
                    'use Me to force a new value of payer_obj
                    Me.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _payer = _payer_obj.payer
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property payer_view_str As String
        Get
            If Not IsNothing(_payer_obj) Then
                Return payer_obj.label
            End If
            Return ""
        End Get
    End Property

    Private _task_rate_id As Long
    Public Property task_rate_id() As Long
        Get
            Return _task_rate_id
        End Get
        Set(ByVal value As Long)
            If Not _task_rate_id = value Then
                _task_rate_id = value
                'retrieve only for none empty
                If _task_rate_id > 0 Then
                    Me.task_rate_obj = MConstants.GLB_MSTR_CTRL.lab_rate_controller.getTaskRateObject(_task_rate_id)
                Else
                    Me.task_rate_obj = Nothing
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _task_rate_obj As CTaskRate
    Public Property task_rate_obj() As CTaskRate
        Get
            Return _task_rate_obj
        End Get
        Set(ByVal value As CTaskRate)
            If Not Object.Equals(_task_rate_obj, value) Then
                _task_rate_obj = value
                'set ID
                If IsNothing(_task_rate_obj) Then
                    _task_rate_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _task_rate_id = _task_rate_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Sub transferDataTo(soldHr As CSoldHours)
        soldHr.awq_obj = _awq_obj
        soldHr.remove_awq_obj = _remove_awq_obj
        soldHr.billing_comment = _billing_comment
        soldHr.cc = _cc
        soldHr.sold_hrs = _sold_hrs
        soldHr.payer_obj = _payer_obj
        soldHr.billing_work_status = _billing_work_status
        soldHr.discount_obj = _discount_obj
        soldHr.nte_obj = _nte_obj
        soldHr.is_subcontracted = _is_subcontracted
        soldHr.task_rate_obj = _task_rate_obj
        soldHr.fix_price = _fix_price
        soldHr.fix_price_curr = _fix_price_curr
        soldHr.is_reported = _is_reported
    End Sub

    'refresh all calculated values
    Public Sub updateCal(Optional calc_activity As Boolean = True)

        If MConstants.GLOB_SOLDHRS_LOADED AndAlso Not MConstants.GLOB_SAP_IMPOT_RUNNING Then
            _calc_error_text_thrown = ""
            Me.calc_price_before_disc = 0
            Me.calc_discount = 0
            Me.calc_nte = 0
            Me.calc_price = 0
            Try
                'done it only in runtime, not when reading DB
                If _system_status <> MConstants.CONST_SYS_ROW_DELETED AndAlso IsNothing(_init_quote_entry_obj) _
                AndAlso _red_mark <> MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_FOC_KEY).value _
                AndAlso Not IsNothing(act_object) Then

                    'calculate price before discount
                    If _fix_price_curr <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                        'if fix price use it
                        Me.calc_price_before_disc = MCalc.toLabCurr(_fix_price, _fix_price_curr)
                    ElseIf task_rate_id <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value Then
                        'if task rate
                        Me.calc_price_before_disc = MCalc.toLabCurr(_sold_hrs * _task_rate_obj.value, _task_rate_obj.curr)
                    ElseIf Not (IsNothing(act_object.warr_code)) AndAlso act_object.warr_code.Equals(MConstants.constantConf(MConstants.CONST_CONF_WARR_CODE_NTE_KEY).value, StringComparison.CurrentCultureIgnoreCase) Then
                        'if NTEY or NTEN warr code, use NTEY or NTEN rate
                        Me.calc_price_before_disc = MCalc.ccRateToLabCurr(MConstants.constantConf(MConstants.CONST_CONF_WARR_CODE_NTE_LAB_RATE_KEY).value, _sold_hrs, cc_obj, payer_obj)
                    ElseIf Not (IsNothing(act_object.warr_code)) AndAlso act_object.warr_code.Equals(MConstants.constantConf(MConstants.CONST_CONF_WARR_CODE_NTE_EXCL_KEY).value, StringComparison.CurrentCultureIgnoreCase) Then
                        'if NTEY or NTEN warr code, use NTEY or NTEN rate
                        Me.calc_price_before_disc = MCalc.ccRateToLabCurr(MConstants.constantConf(MConstants.CONST_CONF_WARR_CODE_NTE_EXC_LAB_RATEL_KEY).value, _sold_hrs, cc_obj, payer_obj)
                    Else
                        Me.calc_price_before_disc = MCalc.ccRateToLabCurr(_sold_hrs, cc_obj, payer_obj)
                    End If

                    'calculate discount
                    If Not (IsNothing(_discount_obj) OrElse _discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value) AndAlso _discount_obj.payer_obj.Equals(_payer_obj) Then
                        Me.calc_discount = -1 * _discount_obj.getDiscountValue(_calc_price_before_disc, MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr)
                    End If

                    'calculate price
                    Me.calc_price = Me.calc_price_before_disc + Me.calc_discount
                    If Me.calc_price < 0 Then
                        Me.calc_price = 0
                    End If

                    'update activity
                    If calc_activity Then
                        act_object.updateCal()
                    End If
                End If
            Catch ex As Exception
                _calc_error_text_thrown = "Calculation Error:" & Chr(10) & ex.Message
            End Try
        End If
    End Sub

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not _calc_error_text = value Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _calc_error_text_thrown As String
    Public Property calc_error_text_thrown() As String
        Get
            Return _calc_error_text_thrown
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    'text to check if there are any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    Public Property calc_awq() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_awq_obj) Then
                res = awq_obj.reference_revision
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property calc_remove_awq() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_remove_awq_obj) Then
                res = _remove_awq_obj.reference_revision
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_price As Double
    Public Property calc_price() As Double
        Get
            Return _calc_price
        End Get
        Set(ByVal value As Double)
            If Not value = _calc_price Then
                _calc_price = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _calc_discount As Double
    Public Property calc_discount() As Double
        Get
            Return _calc_discount
        End Get
        Set(ByVal value As Double)
            If Not value = _calc_discount Then
                _calc_discount = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _calc_nte As Double
    Public Property calc_nte() As Double
        Get
            Return _calc_nte
        End Get
        Set(ByVal value As Double)
            If Not value = _calc_nte Then
                _calc_nte = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _calc_price_before_disc As Double
    Public Property calc_price_before_disc() As Double
        Get
            Return _calc_price_before_disc
        End Get
        Set(ByVal value As Double)
            If Not value = _calc_price_before_disc Then
                _calc_price_before_disc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _fix_price As Double
    Public Property fix_price() As Double
        Get
            Return _fix_price
        End Get
        Set(ByVal value As Double)
            If Not _fix_price = value Then
                _fix_price = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _fix_price_curr As String
    Public Property fix_price_curr() As String
        Get
            Return _fix_price_curr
        End Get
        Set(ByVal value As String)
            If Not _fix_price_curr = value Then
                _fix_price_curr = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'revision control
    Private _revision_control_status As String
    Public Property revision_control_status() As String
        Get
            Return _revision_control_status
        End Get
        Set(ByVal value As String)
            If Not _revision_control_status = value Then
                _revision_control_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _revision_control_txt As String
    Public Property revision_control_txt() As String
        Get
            Return _revision_control_txt
        End Get
        Set(ByVal value As String)
            If Not _revision_control_txt = value Then
                _revision_control_txt = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _billing_work_status As String
    Public Property billing_work_status() As String
        Get
            Return _billing_work_status
        End Get
        Set(ByVal value As String)
            If Not _billing_work_status = value Then
                _billing_work_status = value
            End If
        End Set
    End Property

    'is_subcontracted
    Private _is_subcontracted As Boolean
    Public Property is_subcontracted() As Boolean
        Get
            Return _is_subcontracted
        End Get
        Set(ByVal value As Boolean)
            If Not _is_subcontracted = value Then
                _is_subcontracted = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    '_is_sap_importt
    Private _is_sap_import As Boolean
    Public Property is_sap_import() As Boolean
        Get
            Return _is_sap_import
        End Get
        Set(ByVal value As Boolean)
            If Not _is_sap_import = value Then
                _is_sap_import = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _is_reported As Boolean
    Public Property is_reported() As Boolean
        Get
            Return _is_reported
        End Get
        Set(ByVal value As Boolean)
            If Not _is_reported = value Then
                'trying to unused an AWQ related item
                If _is_reported AndAlso Not IsNothing(_awq_obj) AndAlso Not value Then
                    Exit Property
                End If
                _is_reported = value
                NotifyPropertyChanged()
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Public Sub UpdateCCReplaceFromSAP(hrsimport As CImportScopeObject)
        'use to replace a new cc with a not in sap cc
        _act_object.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
        _act_object.logRevisionControlText("Operations from CC " & _plan_cc & " replaced by CC " & hrsimport.work_center)

        Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED


        If String.IsNullOrWhiteSpace(_revision_control_txt) Then
            Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " CC " & _plan_cc & "=>" & hrsimport.work_center & ", plan hours" & _plan_hrs & "=>" & hrsimport.calculated_planned_hrs
        Else
            Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " CC " & _plan_cc & "=>" & hrsimport.work_center & ", plan hours" & _plan_hrs & "=>" & hrsimport.calculated_planned_hrs & Chr(10) & _revision_control_txt
        End If

        'update values
        Me.plan_hrs = hrsimport.calculated_planned_hrs
        Me.plan_cc = hrsimport.work_center
        If MConstants.listOfCC.ContainsKey(_plan_cc) Then
            Me.cc = _plan_cc
        Else
            Me.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        End If

        Me.operation_desc = hrsimport.calculated_operation_desc
    End Sub

    Public Function UpdateFromSAP(hrsimport As CImportScopeObject) As Boolean
        Dim updated As Boolean = False
        Dim mess As String = ""
        If _plan_hrs <> hrsimport.calculated_planned_hrs Then
            'log
            mess = _cc & " plan hours" & _plan_hrs & "=>" & hrsimport.calculated_planned_hrs
            updated = True
            'update values
            Me.plan_hrs = hrsimport.calculated_planned_hrs
        End If
        'log desc ?
        Me.operation_desc = hrsimport.calculated_operation_desc

        'log
        If updated Then
            'update activity and hrs status
            Me.act_object.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
            Me.act_object.logRevisionControlText(mess)

            logRevisionControlText(mess)
        Else
            Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOCHANGE
        End If

        Return updated
    End Function

    'check if labor is reportable in ^release to customer
    Public Function cust_release_disc_nte_reportable() As Boolean
        Try
            Dim res As Boolean = True
            If Not IsNothing(_init_quote_entry_obj) Then
                'no disc or nte on init
                res = False
            ElseIf Not IsNothing(_awq_obj) Then
                If Not IsNothing(_remove_awq_obj) AndAlso _remove_awq_obj.has_been_sent Then
                    res = False
                Else
                    If _awq_obj.has_been_sent Then
                        res = True
                    Else
                        res = False
                    End If
                End If
            ElseIf _is_reported Then
                If _payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value AndAlso (_act_object.fix_labor > 0 OrElse Not (String.IsNullOrWhiteSpace(_act_object.labor_sold_mark) OrElse _act_object.labor_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value)) Then
                    res = False
                Else
                    res = True
                End If
            Else
                'not reported
                res = False
            End If
            Return res
        Catch ex As Exception
            Return False
        End Try
    End Function

    'is nte
    Public Function isWarrCodeNTE() As Boolean
        If Not IsNothing(_act_object) Then
            Return _act_object.isWarrCodeNTE
        Else
            Return False
        End If
    End Function
    Public Function isWarrCodeNTEExckusion() As Boolean
        If Not IsNothing(_act_object) Then
            Return _act_object.isWarrCodeNTEExclusion
        Else
            Return False
        End If
    End Function

    'log revision
    Public Sub logRevisionControlText(mess As String)
        Try
            If String.IsNullOrWhiteSpace(_revision_control_txt) Then
                Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & mess
            Else
                Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & mess & Chr(10) & _revision_control_txt
            End If
        Catch ex As Exception
        End Try
    End Sub

    'to string
    Public Overrides Function tostring() As String
        Return "CC: " & _cc_obj.label & ", Payer: " & _payer_obj.label & ", hours " & _sold_hrs & ", reported ? " & _is_reported & " (" & _act_object.tostring & ")"
    End Function
End Class

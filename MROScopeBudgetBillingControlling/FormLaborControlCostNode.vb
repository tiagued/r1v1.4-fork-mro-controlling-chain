﻿Imports Equin.ApplicationFramework

Public Class FormLaborControlCostNode
    'navigate with arrows
    Private Sub act_cc_select_left_pic_Click(sender As Object, e As EventArgs) Handles act_cc_select_left_pic.Click
        GLB_MSTR_CTRL.labor_controlling_cost_node_ctrl.selectedObjectBackWard()
    End Sub
    'navigate with arrows
    Private Sub act_cc_select_right_pic_Click(sender As Object, e As EventArgs) Handles act_cc_select_right_pic.Click
        GLB_MSTR_CTRL.labor_controlling_cost_node_ctrl.selectedObjectForward()
    End Sub

    'object change event
    Private Sub cb_act_cc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_cost_nodes.SelectedIndexChanged
        GLB_MSTR_CTRL.labor_controlling_cost_node_ctrl.selectedCtrObjChanged()
    End Sub

    'activity error text
    Private Sub activity_error_state_pic_Click(sender As Object, e As EventArgs) Handles act_cc_error_state_pic.Click
        MsgBox(act_cc_error_state_pic.Tag)
    End Sub

    'select activity event
    Private Sub activities_list_SelectedIndexChanged(sender As Object, e As EventArgs) Handles activities_list.SelectedIndexChanged
        MConstants.GLB_MSTR_CTRL.labor_controlling_cost_node_ctrl.selectedActivitiesChanged()
    End Sub

    'select cc event
    Private Sub cost_center_list_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cost_center_list.SelectedIndexChanged
        MConstants.GLB_MSTR_CTRL.labor_controlling_cost_node_ctrl.selectedCostCentersChanged()
    End Sub

    'launch edit form
    'controlling main view, handle click on edit button
    Private Sub labor_ctrl_main_dgrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles labor_ctrl_main_dgrid.CellContentClick
        ' Ignore clicks that are not on button cells. 
        If e.RowIndex >= 0 AndAlso labor_ctrl_main_dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_LABOR_CTRL_EDIT Then
            Dim lab_ctrl As CLaborControlMainViewObj
            Try
                Dim costNode As CLaborControlMainViewObj = CType(cb_cost_nodes.SelectedItem, ObjectView(Of CLaborControlMainViewObj)).Object
                lab_ctrl = CType(labor_ctrl_main_dgrid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CLaborControlMainViewObj)).Object
                If Not lab_ctrl.is_ctrl_obj_editable Then
                    MGeneralFuntionsViewControl.displayMessage("Cannot Launch Edit Form", "This item is a readonly and calculated object and cannot be edited", MsgBoxStyle.Information)
                Else
                    MConstants.GLB_MSTR_CTRL.labor_controlling_controller_edit.launchEditForm(lab_ctrl, costNode)
                End If
            Catch ex As Exception
                MGeneralFuntionsViewControl.displayMessage("Cannot launch Cost Element Edit Form", "An error occured while laoding the cost element edit form" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
            End Try
        Else
            Return
        End If
    End Sub

    Private Sub add_scope_main_datagrid_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs)
        sender = sender
        e = e
    End Sub
End Class
﻿Public Class CProjCalculatorArgs
    Public project_details_scope_list As List(Of CScopeAtivityCalcEntry)
    Public nteDetailsActivitiesList As Dictionary(Of String, CScopeAtivityCalcEntry)
    Public nteDetailsNTEObjList As List(Of CNTEDetailsReportObj)

    Public Sub clearArgs()
        Try
            If Not IsNothing(project_details_scope_list) Then
                project_details_scope_list.Clear()
            End If
            If Not IsNothing(nteDetailsActivitiesList) Then
                nteDetailsActivitiesList.Clear()
            End If
            If Not IsNothing(nteDetailsNTEObjList) Then
                nteDetailsNTEObjList.Clear()
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class

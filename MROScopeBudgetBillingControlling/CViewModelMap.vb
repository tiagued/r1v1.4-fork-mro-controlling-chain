﻿Imports System.Data.OleDb

Public Class CViewModelMap
    Implements IGenericInterfaces.IDataGridViewRecordable

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(False, "")
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_ACTUAL_LABOR_DB_READ
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_DISCOUNT_LOADED Then
                    CDiscount.currentMaxID = Math.Max(CDiscount.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    'functional area used as a key to group mapping entries
    Private _func As String
    Public Property func() As String
        Get
            Return _func
        End Get
        Set(ByVal value As String)
            _func = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'excel sheet (table) to be queried
    Private _db_table As String
    Public Property db_table() As String
        Get
            Return _db_table
        End Get
        Set(ByVal value As String)
            _db_table = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'excel col
    Private _db_col As String
    Public Property db_col() As String
        Get
            Return _db_col
        End Get
        Set(ByVal value As String)
            _db_col = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'excel row. Use in the case where the mapping is used to retrieve not a list of objects but only one objects
    Private _key_value As String
    Public Property key_value() As String
        Get
            Return _key_value
        End Get
        Set(ByVal value As String)
            _key_value = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'mapping class
    Private _class As String
    Public Property class_name() As String
        Get
            Return _class
        End Get
        Set(ByVal value As String)
            _class = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'view control to display the data
    Private _view_control As String
    Public Property view_control() As String
        Get
            Return _view_control
        End Get
        Set(ByVal value As String)
            _view_control = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'view col in case of datagried
    Private _view_col As String
    Public Property view_col() As String
        Get
            Return _view_col
        End Get
        Set(ByVal value As String)
            _view_col = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property
    'view row in case of datagried
    Private _view_col_sys_name As String
    Public Property view_col_sys_name() As String
        Get
            Return _view_col_sys_name
        End Get
        Set(ByVal value As String)
            _view_col_sys_name = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'list in case of combo box list
    Private _view_list_sheet As String
    Public Property view_list_sheet() As String
        Get
            Return _view_list_sheet
        End Get
        Set(ByVal value As String)
            _view_list_sheet = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'list in case of combo box list
    Private _view_list_name As String
    Public Property view_list_name() As String
        Get
            Return _view_list_name
        End Get
        Set(ByVal value As String)
            _view_list_name = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'key to be used for default sorting in the dictionnary
    Private _key As String
    Public Property key() As String
        Get
            Return _key
        End Get
        Set(ByVal value As String)
            _key = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'field of the object to be mapped
    Private _class_field As String
    Public Property class_field() As String
        Get
            Return _class_field
        End Get
        Set(ByVal value As String)
            _class_field = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'used in case one object is to be retrieved. column that contains the value
    Private _key_col As String
    Public Property key_col() As String
        Get
            Return _key_col
        End Get
        Set(ByVal value As String)
            _key_col = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    Private _col_width As Integer
    Public Property col_width() As Integer
        Get
            Return _col_width
        End Get
        Set(ByVal value As Integer)
            _col_width = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    Private _col_editable As Boolean
    Public Property col_editable() As Boolean
        Get
            Return _col_editable
        End Get
        Set(ByVal value As Boolean)
            _col_editable = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    Private _col_control_type As String
    Public Property col_control_type() As String
        Get
            Return _col_control_type
        End Get
        Set(ByVal value As String)
            _col_control_type = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    Private _col_format As String
    Public Property col_format() As String
        Get
            Return _col_format
        End Get
        Set(ByVal value As String)
            _col_format = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    Private _ole_db_type As OleDbType
    Public Property ole_db_type() As OleDbType
        Get
            Return _ole_db_type
        End Get
        Set(ByVal value As OleDbType)
            _ole_db_type = value
        End Set
    End Property


End Class

﻿Public Class CSoldHoursAddRowValidator
    Public Shared instance As CSoldHoursAddRowValidator
    Public Shared Function getInstance() As CSoldHoursAddRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CSoldHoursAddRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(soldH As CSoldHours)
        soldH.calc_is_in_error_state = False
    End Sub

    ' Name of methods in this class should be the same as the property (class_field) binded to the datagridview in the applicable object
    'validate cost center.
    Public Function cc(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value OrElse IsNothing(soldH.cc_obj) Then
            err = "Cost Center Cannot be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate payer
    Public Function payer(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value OrElse IsNothing(soldH.payer_obj) Then
            err = "Payer Cannot be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate hours
    Public Function sold_hrs(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.sold_hrs <= 0 Then
            err = "Hours Shall be greather than 0"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate fix price curr
    Public Function fix_price_curr(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.fix_price > 0 AndAlso soldH.fix_price_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Fix price curr cannot be empty when a fix price has been set"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate hours
    Public Function fix_price(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.fix_price < 0 Then
            err = "Fix price cannot be negative value"
            res = False
        ElseIf soldH.fix_price_curr <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value AndAlso soldH.fix_price = 0 Then
            err = "Fix price cannot be zero. Use FOC Payer for FOC"
            res = False
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'check calculation ran without error
    Public Function errorText(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If Not String.IsNullOrWhiteSpace(soldH.calc_error_text_thrown) Then
            err = soldH.calc_error_text_thrown
            res = False
        Else
            res = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function


    'no discount on init items
    Public Function discount_obj(soldHr As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not IsNothing(soldHr.discount_obj) AndAlso soldHr.discount_obj.id <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value AndAlso soldHr.payer_obj.payer <> soldHr.discount_obj.payer_obj.payer Then
            err = "Discount Payer and Labor Payer are not matching"
            res = False
            soldHr.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function discount_id(soldHr As CSoldHours) As KeyValuePair(Of Boolean, String)
        Return discount_obj(soldHr)
    End Function

    'no NTE on init items
    Public Function nte_obj(soldHr As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not IsNothing(soldHr.nte_obj) AndAlso soldHr.nte_obj.id <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value AndAlso soldHr.payer_obj.payer <> soldHr.nte_obj.payer_obj.payer Then
            err = "NTE Payer and Labor Payer are not matching"
            res = False
            soldHr.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function nte_id(soldHr As CSoldHours) As KeyValuePair(Of Boolean, String)
        Return nte_obj(soldHr)
    End Function

    'validate hours adj
    Public Function calc_price_before_disc(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not soldH.is_reported AndAlso soldH.calc_price_before_disc > 0 AndAlso Not soldH.act_object.is_cost_collector Then
            err = "This entry hold a price but is not reported"
            res = False
            soldH.calc_is_in_error_state = True
        Else
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'check if labor is reported
    Public Function is_reported(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If soldH.act_object.is_cost_collector AndAlso soldH.is_reported AndAlso IsNothing(soldH.awq_obj) Then
            err = "Cost collector activities cannot support below threshold prices. Uncheck Object"
            res = False
            soldH.calc_is_in_error_state = True
        Else
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

End Class

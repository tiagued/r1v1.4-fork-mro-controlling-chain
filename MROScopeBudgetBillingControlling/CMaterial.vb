﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CMaterial
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    'used to created new ids
    Public Shared currentMaxID As Integer


    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_MATERIAL_LOADED Then
            'set combo emppty
            _nte_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _nte_obj = MConstants.EMPTY_NTE

            _billing_work_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _transac_condition = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value

            _discount_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _discount_obj = MConstants.EMPTY_DISC

            _awq_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _overwrite_freight_zfra_flag = False
            _overwrite_markup_zmar_flag = False
            _freight_zfrp = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _handling_zcus_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _zprm_zstm_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _freight_zfrp_overwriten_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
            _payer_obj = MConstants.listOfPayer(_payer)
            'Me.* at the end to avoid calcul triggering
            Me.activity_obj = MConstants.EMPTY_ACT
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_MATERIAL_DB_READ
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""
        Try
            If _is_sap_import Then
                res = False
                mess = "Item is imported from SAP so cannot be deleted"
            ElseIf Not IsNothing(_awq_obj) Then
                res = False
                mess = "Item cannot be deleted because it is linked to AWQ " & _awq_obj.reference_revision
            Else
                'remove material from activity
                If Not IsNothing(_activity_obj) AndAlso _activity_obj.material_list.Contains(Me) Then
                    _activity_obj.material_list.Remove(Me)
                    _activity_obj.updateCal()
                End If
                Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                MConstants.GLB_MSTR_CTRL.material_controller.deleteObjectMaterial(Me)
                res = True
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Sub setID()
        If MConstants.GLOB_MATERIAL_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        If Not IsNothing(MConstants.GLB_MSTR_CTRL.notifHelper) Then
            MConstants.GLB_MSTR_CTRL.notifHelper.OnPropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End If
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not IsNothing(_awq_obj) AndAlso Not _awq_obj.is_in_work Then
            res = False
            mess = "Cannot be modified cause it's linked to a released AWQ"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        If Not _calc_is_in_error_state AndAlso Not String.IsNullOrWhiteSpace(_calc_error_text_thrown) Then
            Return New KeyValuePair(Of Boolean, String)(True, _calc_error_text_thrown)
        Else
            Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
        End If
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_MATERIAL_LOADED Then
                    CMaterial.currentMaxID = Math.Max(CMaterial.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _activity_id As Long
    Public Property activity_id() As Long
        Get
            Return _activity_id
        End Get
        Set(ByVal value As Long)
            If Not _activity_id = value Then
                _activity_id = value
                Me.activity_obj = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(_activity_id)
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _activity_obj As CScopeActivity
    Public Property activity_obj() As CScopeActivity
        Get
            Return _activity_obj
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(value, _activity_obj) Then
                'update activity material lists
                If MConstants.GLOB_MATERIAL_LOADED Then
                    If Not IsNothing(_activity_obj) AndAlso _activity_obj.material_list.Contains(Me) Then
                        _activity_obj.material_list.Remove(Me)
                    End If
                    If Not IsNothing(value) And Not value.material_list.Contains(Me) And Me.id > 0 Then
                        value.material_list.Add(Me)
                    End If
                End If
                'set value
                _activity_obj = value
                If IsNothing(value) Then
                    _activity_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _activity_id = value.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'quote entry
    Private _init_quote_entry_obj As CSalesQuoteEntry
    Public Property init_quote_entry_obj() As CSalesQuoteEntry
        Get
            Return _init_quote_entry_obj
        End Get
        Set(ByVal value As CSalesQuoteEntry)
            If Not Object.Equals(_init_quote_entry_obj, value) Then
                _init_quote_entry_obj = value
                If IsNothing(value) Then
                    _init_quote_entry_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _init_quote_entry_id = _init_quote_entry_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _init_quote_entry_id As Long
    Public Property init_quote_entry_id() As Long
        Get
            Return _init_quote_entry_id
        End Get
        Set(ByVal value As Long)
            If Not _init_quote_entry_id = value Then
                _init_quote_entry_id = value
                If _init_quote_entry_id > 0 Then
                    _init_quote_entry_obj = MConstants.GLB_MSTR_CTRL.ini_scope_controller.getQuoteEntryObject(_init_quote_entry_id)
                Else
                    _init_quote_entry_obj = Nothing
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Property calc_quote_entry_str() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_init_quote_entry_obj) Then
                res = _init_quote_entry_obj.quote_obj.number & "/" & _init_quote_entry_obj.product
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _revision_control_txt As String
    Public Property revision_control_txt() As String
        Get
            Return _revision_control_txt
        End Get
        Set(ByVal value As String)
            If Not _revision_control_txt = value Then
                _revision_control_txt = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _revision_control_status As String
    Public Property revision_control_status() As String
        Get
            Return _revision_control_status
        End Get
        Set(ByVal value As String)
            If Not _revision_control_status = value Then
                _revision_control_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _po_created As Date
    Public Property po_created() As Date
        Get
            Return _po_created
        End Get
        Set(ByVal value As Date)
            If Not _po_created = value Then
                _po_created = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _project_activity As String
    Public Property project_activity() As String
        Get
            Return _project_activity
        End Get
        Set(ByVal value As String)
            If Not _project_activity = value Then
                _project_activity = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property project As String
        Get
            Return _activity_obj.proj
        End Get
    End Property

    Public ReadOnly Property act_notif_opcode As String
        Get
            If IsNothing(_activity_obj) Then
                Return ""
            Else
                Return activity_obj.act_notif_opcode
            End If
        End Get
    End Property


    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            If Not _qty = value Then
                _qty = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _uom As String
    Public Property uom() As String
        Get
            Return _uom
        End Get
        Set(ByVal value As String)
            If Not _uom = value Then
                _uom = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _part_number As String
    Public Property part_number() As String
        Get
            Return _part_number
        End Get
        Set(ByVal value As String)
            If Not _part_number = value Then
                _part_number = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _sn_on As String
    Public Property sn_on() As String
        Get
            Return _sn_on
        End Get
        Set(ByVal value As String)
            If Not _sn_on = value Then
                _sn_on = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _transac_condition As String
    Public Property transac_condition() As String
        Get
            Return _transac_condition
        End Get
        Set(ByVal value As String)
            If Not _transac_condition = value Then
                _transac_condition = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property transac_condition_view() As String
        Get
            Dim res As String = ""
            Try
                res = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_MATERIAL_TRANSAC_COND_LIST)(transac_condition).value
            Catch ex As Exception
            End Try
            Return res
        End Get
    End Property

    Private _material_type As String
    Public Property material_type() As String
        Get
            Return _material_type
        End Get
        Set(ByVal value As String)
            If Not _material_type = value Then
                _material_type = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_scope_init As Boolean
    Public ReadOnly Property is_scope_init() As Boolean
        Get
            Return Not IsNothing(_init_quote_entry_obj)
        End Get
    End Property

    Private _payer As String
    Public Property payer() As String
        Get
            Return _payer
        End Get
        Set(ByVal value As String)
            If Not _payer = value Then
                _payer = value
                If MConstants.listOfPayer.ContainsKey(_payer) Then
                    Me.payer_obj = MConstants.listOfPayer(_payer)
                Else
                    Me.payer_obj = Nothing
                    Throw New Exception("Payer does not exist in payer list : " & _payer)
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property
    Private _payer_obj As CPayer
    Public Property payer_obj() As CPayer
        Get
            Return _payer_obj
        End Get
        Set(ByVal value As CPayer)
            If Not Object.Equals(_payer_obj, value) Then
                _payer_obj = value
                If IsNothing(_payer_obj) Then
                    'use Me to force a new value of payer_obj
                    Me.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _payer = _payer_obj.payer
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_reported As Boolean
    Public Property is_reported() As Boolean
        Get
            Return _is_reported
        End Get
        Set(ByVal value As Boolean)
            If Not _is_reported = value Then
                _is_reported = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _po_number As String
    Public Property po_number() As String
        Get
            Return _po_number
        End Get
        Set(ByVal value As String)
            If Not _po_number = value Then
                _po_number = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _po_item As String
    Public Property po_item() As String
        Get
            Return _po_item
        End Get
        Set(ByVal value As String)
            If Not _po_item = value Then
                _po_item = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _or_rea As String
    Public Property or_rea() As String
        Get
            Return _or_rea
        End Get
        Set(ByVal value As String)
            If Not _or_rea = value Then
                _or_rea = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _warr_code As String
    Public Property warr_code() As String
        Get
            Return _warr_code
        End Get
        Set(ByVal value As String)
            If Not _warr_code = value Then
                _warr_code = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _po_cost As String
    Public Property po_cost() As String
        Get
            Return _po_cost
        End Get
        Set(ByVal value As String)
            If Not _po_cost = value Then
                _po_cost = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _po_curr As String
    Public Property po_curr() As String
        Get
            Return _po_curr
        End Get
        Set(ByVal value As String)
            If Not _po_curr = value Then
                _po_curr = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _po_estimated As String
    Public Property po_estimated() As String
        Get
            Return _po_estimated
        End Get
        Set(ByVal value As String)
            If Not _po_estimated = value Then
                _po_estimated = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _awq_id As Long
    Public Property awq_id() As Long
        Get
            Return _awq_id
        End Get
        Set(ByVal value As Long)
            If Not _awq_id = value Then
                _awq_id = value
                If MConstants.GLOB_AWQ_LOADED Then
                    If _awq_id > 0 Then
                        Me.awq_obj = MConstants.GLB_MSTR_CTRL.awq_controller.getAWQObject(_awq_id)
                    Else
                        Me.awq_obj = Nothing
                    End If
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _awq_obj As CAWQuotation
    Public Property awq_obj() As CAWQuotation
        Get
            Return _awq_obj
        End Get
        Set(ByVal value As CAWQuotation)
            If Not Object.Equals(value, _awq_obj) Then
                _awq_obj = value
                'set ID
                If IsNothing(_awq_obj) Then
                    _awq_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _awq_id = _awq_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'awq that cancel the current labor line
    Private _remove_awq_id As Long
    Public Property remove_awq_id() As Long
        Get
            Return _remove_awq_id
        End Get
        Set(ByVal value As Long)
            If Not _remove_awq_id = value Then
                _remove_awq_id = value
                If MConstants.GLOB_AWQ_LOADED Then
                    If remove_awq_id > 0 Then
                        Me.remove_awq_obj = MConstants.GLB_MSTR_CTRL.awq_controller.getAWQObject(_remove_awq_id)
                    Else
                        Me.remove_awq_obj = Nothing
                    End If
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _remove_awq_obj As CAWQuotation
    Public Property remove_awq_obj() As CAWQuotation
        Get
            Return _remove_awq_obj
        End Get
        Set(ByVal value As CAWQuotation)
            If Not Object.Equals(value, _remove_awq_obj) Then
                _remove_awq_obj = value
                'set ID
                If IsNothing(_remove_awq_obj) Then
                    _remove_awq_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _remove_awq_id = _remove_awq_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _discount_id As Long
    Public Property discount_id() As Long
        Get
            Return _discount_id
        End Get
        Set(ByVal value As Long)
            If Not _discount_id = value Then
                _discount_id = value
                If _discount_id > 0 Then
                    Me.discount_obj = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getDiscountObject(_discount_id)
                Else
                    _discount_obj = MConstants.EMPTY_DISC
                    _discount_id = MConstants.EMPTY_DISC.id
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _discount_obj As CDiscount
    Public Property discount_obj() As CDiscount
        Get
            Return _discount_obj
        End Get
        Set(ByVal value As CDiscount)
            If Not Object.Equals(_discount_obj, value) Then
                _discount_obj = value
                'set ID
                If IsNothing(_discount_obj) Then
                    _discount_obj = MConstants.EMPTY_DISC
                    _discount_id = MConstants.EMPTY_DISC.id
                Else
                    _discount_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _nte_id As Long
    Public Property nte_id() As Long
        Get
            Return _nte_id
        End Get
        Set(ByVal value As Long)
            If Not _nte_id = value Then
                _nte_id = value
                'retrieve only for none empty
                If _nte_id > 0 Then
                    Me.nte_obj = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getNtetObject(_nte_id)
                Else
                    Me.nte_obj = MConstants.EMPTY_NTE
                    _nte_id = MConstants.EMPTY_NTE.id
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _nte_obj As CNotToExceed
    Public Property nte_obj() As CNotToExceed
        Get
            Return _nte_obj
        End Get
        Set(ByVal value As CNotToExceed)
            If Not Object.Equals(_nte_obj, value) Then
                _nte_obj = value
                'set ID
                If IsNothing(_nte_obj) Then
                    _nte_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _nte_id = _nte_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _text5 As String
    Public Property text5() As String
        Get
            Return _text5
        End Get
        Set(ByVal value As String)
            If Not _text5 = value Then
                _text5 = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                updateCal()
            End If
        End Set
    End Property


    Private _total_sales_price_zprm_flag As Boolean
    Public Property total_sales_price_zprm_flag() As Boolean
        Get
            Return _total_sales_price_zprm_flag
        End Get
        Set(ByVal value As Boolean)
            If Not _total_sales_price_zprm_flag = value Then
                _total_sales_price_zprm_flag = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _total_sales_price_zprm As Double
    Public Property total_sales_price_zprm() As Double
        Get
            Return _total_sales_price_zprm
        End Get
        Set(ByVal value As Double)
            If Not _total_sales_price_zprm = value Then
                _total_sales_price_zprm = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _unit_list_price_zstm As Double
    Public Property unit_list_price_zstm() As Double
        Get
            Return _unit_list_price_zstm
        End Get
        Set(ByVal value As Double)
            If Not _unit_list_price_zstm = value Then
                _unit_list_price_zstm = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _zprm_zstm_curr As String
    Public Property zprm_zstm_curr() As String
        Get
            Return _zprm_zstm_curr
        End Get
        Set(ByVal value As String)
            If Not _zprm_zstm_curr = value Then
                _zprm_zstm_curr = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _overwrite_markup_zmar_flag As Boolean
    Public Property overwrite_markup_zmar_flag() As Boolean
        Get
            Return _overwrite_markup_zmar_flag
        End Get
        Set(ByVal value As Boolean)
            If Not _overwrite_markup_zmar_flag = value Then
                _overwrite_markup_zmar_flag = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _overwrite_markup_zmar As Double
    Public Property overwrite_markup_zmar() As Double
        Get
            Return _overwrite_markup_zmar
        End Get
        Set(ByVal value As Double)
            If Not _overwrite_markup_zmar = value Then
                _overwrite_markup_zmar = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _overwrite_freight_zfra_flag As Boolean
    Public Property overwrite_freight_zfra_flag() As Boolean
        Get
            Return _overwrite_freight_zfra_flag
        End Get
        Set(ByVal value As Boolean)
            If Not _overwrite_freight_zfra_flag = value Then
                _overwrite_freight_zfra_flag = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _overwrite_freight_zfra As Double
    Public Property overwrite_freight_zfra() As Double
        Get
            Return _overwrite_freight_zfra
        End Get
        Set(ByVal value As Double)
            If Not _overwrite_freight_zfra = value Then
                _overwrite_freight_zfra = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_zfrp As String
    Public Property freight_zfrp() As String
        Get
            Return _freight_zfrp
        End Get
        Set(ByVal value As String)
            If Not _freight_zfrp = value Then
                _freight_zfrp = value
                'retrieve only for none empty
                If _freight_zfrp <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                    freight_zfrp_obj = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getFreightObject(_freight_zfrp)
                Else
                    freight_zfrp_obj = Nothing
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_zfrp_obj As CMatFreight
    Public Property freight_zfrp_obj() As CMatFreight
        Get
            Return _freight_zfrp_obj
        End Get
        Set(ByVal value As CMatFreight)
            If Not Object.Equals(value, _freight_zfrp_obj) Then
                _freight_zfrp_obj = value
                'set ID
                If IsNothing(_freight_zfrp_obj) Then
                    _freight_zfrp = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _freight_zfrp = _freight_zfrp_obj.id_str
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_zfrp_overwriten_value As Double
    Public Property freight_zfrp_overwriten_value() As Double
        Get
            Return _freight_zfrp_overwriten_value
        End Get
        Set(ByVal value As Double)
            If Not _freight_zfrp_overwriten_value = value Then
                _freight_zfrp_overwriten_value = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_zfrp_overwriten_curr As String
    Public Property freight_zfrp_overwriten_curr() As String
        Get
            Return _freight_zfrp_overwriten_curr
        End Get
        Set(ByVal value As String)
            If Not _freight_zfrp_overwriten_curr = value Then
                _freight_zfrp_overwriten_curr = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _handling_zcus As Double
    Public Property handling_zcus() As Double
        Get
            Return _handling_zcus
        End Get
        Set(ByVal value As Double)
            If Not _handling_zcus = value Then
                _handling_zcus = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _handling_zcus_curr As String
    Public Property handling_zcus_curr() As String
        Get
            Return _handling_zcus_curr
        End Get
        Set(ByVal value As String)
            If Not _handling_zcus_curr = value Then
                _handling_zcus_curr = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_sap_import As Boolean
    Public Property is_sap_import() As Boolean
        Get
            Return _is_sap_import
        End Get
        Set(ByVal value As Boolean)
            If Not _is_sap_import = value Then
                _is_sap_import = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _billing_comment As String
    Public Property billing_comment() As String
        Get
            Return _billing_comment
        End Get
        Set(ByVal value As String)
            If Not _billing_comment = value Then
                _billing_comment = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Sub transferDataTo(mat As CMaterial)
        mat.awq_obj = _awq_obj
        mat.remove_awq_obj = _remove_awq_obj
        mat.billing_comment = _billing_comment
        mat.billing_work_status = _billing_work_status
        mat.qty = _qty
        mat.uom = _uom
        mat.sn_on = _sn_on
        mat.material_type = _material_type
        mat.payer_obj = _payer_obj
        mat.is_reported = _is_reported
        mat.discount_obj = _discount_obj
        mat.nte_obj = _nte_obj
        mat.text5 = _text5
        mat.total_sales_price_zprm_flag = _total_sales_price_zprm_flag
        mat.total_sales_price_zprm = _total_sales_price_zprm
        mat.unit_list_price_zstm = _unit_list_price_zstm
        mat.zprm_zstm_curr = _zprm_zstm_curr
        mat.overwrite_markup_zmar_flag = _overwrite_markup_zmar_flag
        mat.overwrite_markup_zmar = _overwrite_markup_zmar
        mat.overwrite_freight_zfra_flag = _overwrite_freight_zfra_flag
        mat.overwrite_freight_zfra = _overwrite_freight_zfra
        mat.freight_zfrp = _freight_zfrp
        mat.freight_zfrp_overwriten_value = _freight_zfrp_overwriten_value
        mat.freight_zfrp_overwriten_curr = _freight_zfrp_overwriten_curr
        mat.handling_zcus = _handling_zcus
        mat.handling_zcus_curr = _handling_zcus_curr
        mat.is_sap_import = _is_sap_import
    End Sub

    Public Property calc_awq() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_awq_obj) Then
                res = awq_obj.reference_revision
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property is_removed_awq As Boolean
        Get
            Return Not IsNothing(_remove_awq_obj)
        End Get
        Set(value As Boolean)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property is_awq As Boolean
        Get
            Return Not IsNothing(_awq_obj)
        End Get
        Set(value As Boolean)
            NotifyPropertyChanged()
        End Set
    End Property

    'to add quote summary with checkbox in init view
    Public Property is_quote_sum As Boolean
        Get
            Return Not IsNothing(_init_quote_entry_obj)
        End Get
        Set(value As Boolean)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property calc_remove_awq() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_remove_awq_obj) Then
                res = _remove_awq_obj.reference_revision
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _billing_work_status As String
    Public Property billing_work_status() As String
        Get
            Return _billing_work_status
        End Get
        Set(ByVal value As String)
            If Not _billing_work_status = value Then
                _billing_work_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'calculated values
    Private _calc_tagk_zprm_zstm_to_matcurr As Double
    Public Property calc_tagk_zprm_zstm_to_matcurr() As Double
        Get
            Return _calc_tagk_zprm_zstm_to_matcurr
        End Get
        Set(ByVal value As Double)
            _calc_tagk_zprm_zstm_to_matcurr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_markup_percent As Double
    Public Property calc_markup_percent() As Double
        Get
            Return _calc_markup_percent
        End Get
        Set(ByVal value As Double)
            _calc_markup_percent = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_markup_value_in_mat_curr As Double
    Public Property calc_markup_value_in_mat_curr() As Double
        Get
            Return _calc_markup_value_in_mat_curr
        End Get
        Set(ByVal value As Double)
            _calc_markup_value_in_mat_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_freight_zfra_percent As Double
    Public Property calc_freight_zfra_percent() As Double
        Get
            Return _calc_freight_zfra_percent
        End Get
        Set(ByVal value As Double)
            _calc_freight_zfra_percent = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_freight_zfra_value_in_freight_curr As Double
    Public Property calc_freight_zfra_value_in_freight_curr() As Double
        Get
            Return _calc_freight_zfra_value_in_freight_curr
        End Get
        Set(ByVal value As Double)
            _calc_freight_zfra_value_in_freight_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_total_material_in_mat_curr As Double
    Public Property calc_total_material_in_mat_curr() As Double
        Get
            Return _calc_total_material_in_mat_curr
        End Get
        Set(ByVal value As Double)
            _calc_total_material_in_mat_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_total_material_in_mat_curr_bef_disc As Double
    Public Property calc_total_material_in_mat_curr_bef_disc() As Double
        Get
            Return _calc_total_material_in_mat_curr_bef_disc
        End Get
        Set(ByVal value As Double)
            _calc_total_material_in_mat_curr_bef_disc = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_discount_total_material_in_mat_curr As Double
    Public Property calc_discount_total_material_in_mat_curr() As Double
        Get
            Return _calc_discount_total_material_in_mat_curr
        End Get
        Set(ByVal value As Double)
            _calc_discount_total_material_in_mat_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_tagk_freight_to_freight_curr As Double
    Public Property calc_tagk_freight_to_freight_curr() As Double
        Get
            Return _calc_tagk_freight_to_freight_curr
        End Get
        Set(ByVal value As Double)
            _calc_tagk_freight_to_freight_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_total_freight_befor_disc_in_freight_curr As Double
    Public Property calc_total_freight_befor_disc_in_freight_curr() As Double
        Get
            Return _calc_total_freight_befor_disc_in_freight_curr
        End Get
        Set(ByVal value As Double)
            _calc_total_freight_befor_disc_in_freight_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_total_freight_disc_in_freight_curr As Double
    Public Property calc_total_freight_disc_in_freight_curr() As Double
        Get
            Return _calc_total_freight_disc_in_freight_curr
        End Get
        Set(ByVal value As Double)
            _calc_total_freight_disc_in_freight_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_total_freight_in_freight_curr As Double
    Public Property calc_total_freight_in_freight_curr() As Double
        Get
            Return _calc_total_freight_in_freight_curr
        End Get
        Set(ByVal value As Double)
            _calc_total_freight_in_freight_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_tagk_handling_zcus_to_mat_curr As Double
    Public Property calc_tagk_handling_zcus_to_mat_curr() As Double
        Get
            Return _calc_tagk_handling_zcus_to_mat_curr
        End Get
        Set(ByVal value As Double)
            _calc_tagk_handling_zcus_to_mat_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_handling_zcus_value_bef_disc_in_mat_curr As Double
    Public Property calc_handling_zcus_value_bef_disc_in_mat_curr() As Double
        Get
            Return _calc_handling_zcus_value_bef_disc_in_mat_curr
        End Get
        Set(ByVal value As Double)
            _calc_handling_zcus_value_bef_disc_in_mat_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_handling_zcus_disc_in_mat_curr As Double
    Public Property calc_handling_zcus_disc_in_mat_curr() As Double
        Get
            Return _calc_handling_zcus_disc_in_mat_curr
        End Get
        Set(ByVal value As Double)
            _calc_handling_zcus_disc_in_mat_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _calc_handling_zcus_value_in_mat_curr As Double
    Public Property calc_handling_zcus_value_in_mat_curr() As Double
        Get
            Return _calc_handling_zcus_value_in_mat_curr
        End Get
        Set(ByVal value As Double)
            _calc_handling_zcus_value_in_mat_curr = value
            NotifyPropertyChanged()
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not _calc_error_text = value Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _calc_error_text_thrown As String
    Public Property calc_error_text_thrown() As String
        Get
            Return _calc_error_text_thrown
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    'material info
    Private _calc_material_info As String
    Public Property calc_material_info() As String
        Get
            Return _calc_material_info
        End Get
        Set(ByVal value As String)
            _calc_material_info = value
            NotifyPropertyChanged()
        End Set
    End Property

    'text to check if there are any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    Public ReadOnly Property is_not_null_price() As Double
        Get
            Return _calc_total_freight_befor_disc_in_freight_curr <> 0 OrElse _calc_handling_zcus_value_bef_disc_in_mat_curr <> 0 OrElse _calc_total_material_in_mat_curr_bef_disc
        End Get
    End Property

    'refresh all calculated values
    Public Sub updateCal(Optional calc_activity As Boolean = True)
        If MConstants.GLOB_MATERIAL_LOADED AndAlso Not MConstants.GLOB_SAP_IMPOT_RUNNING Then
            'error
            _calc_error_text_thrown = ""
            'zfrp
            Me.calc_tagk_freight_to_freight_curr = 0

            Me.calc_total_freight_befor_disc_in_freight_curr = 0
            Me.calc_total_freight_disc_in_freight_curr = 0
            Me.calc_total_freight_in_freight_curr = 0

            'zcus
            Me.calc_tagk_zprm_zstm_to_matcurr = 0
            Me.calc_handling_zcus_value_bef_disc_in_mat_curr = 0
            Me.calc_handling_zcus_disc_in_mat_curr = 0
            Me.calc_handling_zcus_value_in_mat_curr = 0

            'zprm curr
            Me.calc_tagk_zprm_zstm_to_matcurr = 0
            'markup
            Me.calc_markup_percent = 0
            Me.calc_markup_value_in_mat_curr = 0
            'zfra
            Me.calc_freight_zfra_percent = 0
            Me.calc_freight_zfra_value_in_freight_curr = 0
            'mat price
            Me.calc_total_material_in_mat_curr_bef_disc = 0
            Me.calc_discount_total_material_in_mat_curr = 0
            Me.calc_total_material_in_mat_curr = 0
            'mat info
            Me.calc_material_info = ""

            Try
                'done it only in runtime, not when reading DB
                If _system_status <> MConstants.CONST_SYS_ROW_DELETED Then

                    'zfrp freight
                    'standard freight choosen in dropdown
                    Dim zfrp_exist As Boolean
                    If _freight_zfrp_overwriten_curr <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                        'manual freight
                        Me.calc_tagk_freight_to_freight_curr = GLB_MSTR_CTRL.billing_cond_controller.getTagk(_freight_zfrp_overwriten_curr, GLB_MSTR_CTRL.project_controller.project.freight_curr)
                        Me.calc_total_freight_befor_disc_in_freight_curr = MCalc.toFreightCurr(_freight_zfrp_overwriten_value, _freight_zfrp_overwriten_curr)
                        zfrp_exist = True
                    ElseIf Not IsNothing(_freight_zfrp_obj) AndAlso _freight_zfrp_obj.id_str <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                        Me.calc_tagk_freight_to_freight_curr = GLB_MSTR_CTRL.billing_cond_controller.getTagk(_freight_zfrp_obj.curr, GLB_MSTR_CTRL.project_controller.project.freight_curr)
                        Me.calc_total_freight_befor_disc_in_freight_curr = MCalc.toFreightCurr(_freight_zfrp_obj.price, _freight_zfrp_obj.curr)
                        zfrp_exist = True
                    End If
                    If zfrp_exist Then
                        'freight discount ?
                        Me.calc_total_freight_disc_in_freight_curr = 0
                        If Not (IsNothing(_discount_obj) OrElse _discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value) AndAlso _discount_obj.payer_obj.Equals(_payer_obj) Then
                            If MUtils.isFreightApplicableLabMat(_discount_obj.lab_mat_applicability) Then
                                Me.calc_total_freight_disc_in_freight_curr = -1 * discount_obj.getDiscountValue(_calc_total_freight_befor_disc_in_freight_curr, GLB_MSTR_CTRL.project_controller.project.freight_curr)
                            End If
                        End If

                        'after discount
                        Me.calc_total_freight_in_freight_curr = _calc_total_freight_befor_disc_in_freight_curr + _calc_total_freight_disc_in_freight_curr
                        If Me.calc_total_freight_in_freight_curr < 0 Then
                            Me.calc_total_freight_in_freight_curr = 0
                        End If
                    End If


                    'zcus service
                    If _handling_zcus_curr <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                        Me.calc_tagk_handling_zcus_to_mat_curr = GLB_MSTR_CTRL.billing_cond_controller.getTagk(_handling_zcus_curr, GLB_MSTR_CTRL.project_controller.project.third_party_curr)
                        Me.calc_handling_zcus_value_bef_disc_in_mat_curr = MCalc.toServCurr(_handling_zcus, _handling_zcus_curr)

                        'zcus service discount ?
                        Me.calc_total_freight_disc_in_freight_curr = 0
                        If Not (IsNothing(_discount_obj) OrElse _discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value) AndAlso _discount_obj.payer_obj.Equals(_payer_obj) Then
                            If MUtils.isServApplicableLabMat(discount_obj.lab_mat_applicability) Then
                                Me.calc_handling_zcus_disc_in_mat_curr = -1 * discount_obj.getDiscountValue(_calc_handling_zcus_value_bef_disc_in_mat_curr, GLB_MSTR_CTRL.project_controller.project.third_party_curr)
                            End If
                        End If
                        'after discount
                        Me.calc_handling_zcus_value_in_mat_curr = _calc_handling_zcus_value_bef_disc_in_mat_curr + _calc_handling_zcus_disc_in_mat_curr
                        If Me.calc_handling_zcus_value_in_mat_curr < 0 Then
                            Me.calc_handling_zcus_value_in_mat_curr = 0
                        End If
                    End If


                    'zprm zstm tagk to mat curr if not empty
                    If _zprm_zstm_curr <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then

                        Me.calc_tagk_zprm_zstm_to_matcurr = GLB_MSTR_CTRL.billing_cond_controller.getTagk(zprm_zstm_curr, GLB_MSTR_CTRL.project_controller.project.mat_curr)

                        'unit price or flat price?
                        If _total_sales_price_zprm <> 0 And _unit_list_price_zstm = 0 Then
                            Me.total_sales_price_zprm_flag = True
                        ElseIf _total_sales_price_zprm = 0 And _unit_list_price_zstm <> 0 Then
                            Me.total_sales_price_zprm_flag = False
                        End If

                        'catalog price per unit
                        If Not _total_sales_price_zprm_flag Then
                            'default markup %
                            Dim markupO As CMatMarkup = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getMarkupRate(_unit_list_price_zstm)
                            Me.calc_markup_percent = markupO.percent
                            'markup in material curr
                            Dim notCapedMkp As Double
                            'if default markup is overwriten
                            If _overwrite_markup_zmar_flag Then
                                notCapedMkp = MCalc.toMatCurr(_overwrite_markup_zmar * _unit_list_price_zstm * _qty, _zprm_zstm_curr)
                            Else
                                notCapedMkp = MCalc.toMatCurr(_calc_markup_percent * _unit_list_price_zstm * _qty, _zprm_zstm_curr)
                            End If
                            'check markup cap
                            If MConstants.GLB_MSTR_CTRL.project_controller.project.is_line_mat_markup_cap Then
                                Dim mkp_cap As Double
                                mkp_cap = MCalc.toMatCurr(MConstants.GLB_MSTR_CTRL.project_controller.project.line_mat_markup_cap_value, MConstants.GLB_MSTR_CTRL.project_controller.project.line_mat_markup_cap_curr)
                                Me.calc_markup_value_in_mat_curr = Math.Min(mkp_cap, notCapedMkp)
                            Else
                                Me.calc_markup_value_in_mat_curr = notCapedMkp
                            End If

                            'default freight %
                            If Not zfrp_exist Then
                                Me._calc_freight_zfra_percent = MConstants.GLB_MSTR_CTRL.project_controller.project.freight_rate
                                'freight in material curr
                                Dim notCapedFrght As Double
                                'if default freight is overwriten
                                If _overwrite_freight_zfra_flag Then
                                    notCapedFrght = MCalc.toMatCurr(_overwrite_freight_zfra * _unit_list_price_zstm * _qty, _zprm_zstm_curr)
                                Else
                                    notCapedFrght = MCalc.toMatCurr(_calc_freight_zfra_percent * _unit_list_price_zstm * _qty, _zprm_zstm_curr)
                                End If
                                'check freight cap
                                If MConstants.GLB_MSTR_CTRL.project_controller.project.is_line_freight_cap Then
                                    Dim freight_cap As Double
                                    freight_cap = MCalc.toMatCurr(MConstants.GLB_MSTR_CTRL.project_controller.project.line_freight_cap_value, MConstants.GLB_MSTR_CTRL.project_controller.project.line_freight_cap_curr)
                                    Me.calc_freight_zfra_value_in_freight_curr = Math.Min(freight_cap, notCapedFrght)
                                Else
                                    Me.calc_freight_zfra_value_in_freight_curr = notCapedFrght
                                End If
                            End If

                            'final price before discount
                            _calc_total_material_in_mat_curr_bef_disc = MCalc.toMatCurr(_unit_list_price_zstm * _qty, _zprm_zstm_curr)
                            If zfrp_exist Then
                                _calc_total_material_in_mat_curr_bef_disc = _calc_total_material_in_mat_curr_bef_disc + _calc_markup_value_in_mat_curr
                            Else
                                _calc_total_material_in_mat_curr_bef_disc = _calc_total_material_in_mat_curr_bef_disc + _calc_markup_value_in_mat_curr + _calc_freight_zfra_value_in_freight_curr
                            End If

                        Else
                            _calc_total_material_in_mat_curr_bef_disc = MCalc.toMatCurr(total_sales_price_zprm, _zprm_zstm_curr)
                        End If
                        Dim isApplicableDiscount As Boolean = False
                        If Not (IsNothing(_discount_obj) OrElse _discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value) AndAlso _discount_obj.payer_obj.Equals(_payer_obj) Then
                            If _material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_MAT_KEY).value AndAlso
                                MUtils.isMaterialApplicableLabMat(discount_obj.lab_mat_applicability) Then
                                isApplicableDiscount = True
                                'discount on material 
                            ElseIf _material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_REP_KEY).value AndAlso
                                MUtils.isRepairApplicableLabMat(discount_obj.lab_mat_applicability) Then
                                isApplicableDiscount = True
                                'discount on repair
                            ElseIf _material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value AndAlso
                                    MUtils.isFreightApplicableLabMat(discount_obj.lab_mat_applicability) Then
                                isApplicableDiscount = True
                                'discount on freight
                            ElseIf _material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value AndAlso
                                   MUtils.isServApplicableLabMat(discount_obj.lab_mat_applicability) Then
                                isApplicableDiscount = True
                                'discount on service
                            End If
                        End If
                        If isApplicableDiscount Then
                            Me.calc_discount_total_material_in_mat_curr = -1 * discount_obj.getDiscountValue(_calc_total_material_in_mat_curr_bef_disc, GLB_MSTR_CTRL.project_controller.project.mat_curr)
                            Me.calc_total_material_in_mat_curr = _calc_total_material_in_mat_curr_bef_disc + _calc_discount_total_material_in_mat_curr
                            If Me.calc_total_material_in_mat_curr < 0 Then
                                Me.calc_total_material_in_mat_curr = 0
                            End If
                        Else
                            Me.calc_total_material_in_mat_curr = _calc_total_material_in_mat_curr_bef_disc
                        End If
                    End If

                    'material info text
                    _calc_material_info = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value
                    'qty
                    If _qty > 0 And Not String.IsNullOrWhiteSpace(_uom) Then
                        _calc_material_info = _calc_material_info & " " & _qty & " " & _uom
                    End If

                    'desc
                    If Not String.IsNullOrWhiteSpace(_description) Then
                        _calc_material_info = _calc_material_info & " " & _description
                    End If

                    'pn
                    If Not String.IsNullOrWhiteSpace(_part_number) Then
                        _calc_material_info = _calc_material_info & " " & _part_number
                    End If
                    'sn
                    If Not String.IsNullOrWhiteSpace(_sn_on) Then
                        _calc_material_info = _calc_material_info & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SN_PREFIX).value & " " & _sn_on
                    End If

                    'text5
                    If Not String.IsNullOrWhiteSpace(_text5) Then
                        _calc_material_info = _calc_material_info & " " & _text5
                    Else
                        _calc_material_info = _calc_material_info & " " & FormatNumber(_calc_total_material_in_mat_curr, 2) & " " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr).value
                    End If
                    'freight
                    If _calc_total_freight_in_freight_curr <> 0 Then
                        _calc_material_info = _calc_material_info & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_FREIGHT_PREFIX).value & " " & FormatNumber(_calc_total_freight_in_freight_curr, 2) & " " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.freight_curr).value
                    End If
                    'service
                    If _calc_handling_zcus_value_in_mat_curr <> 0 Then
                        _calc_material_info = _calc_material_info & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_ZCUS_PREFIX).value & " " & FormatNumber(_calc_handling_zcus_value_in_mat_curr, 2) & " " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.third_party_curr).value
                    End If
                End If
                'update activity
                If calc_activity Then
                    activity_obj.updateCal()
                End If
            Catch ex As Exception
                _calc_error_text_thrown = "Calculation Error:" & Chr(10) & ex.Message
            End Try
        End If
    End Sub

    'to string
    Public Overrides Function tostring() As String
        Return "Act:" & _activity_obj.act & "PN:" & _part_number & " desc:" & _description & " P/O: " & po_number
    End Function

    Public Function getUniqueSAPID() As String
        Return _project_activity & "_" & po_number & "_" & po_item
    End Function

    Public Function cust_release_disc_nte_reportable() As Boolean
        Try
            Dim res As Boolean = True
            If Not IsNothing(_init_quote_entry_obj) Then
                'no disc or nte on init
                res = False
            ElseIf Not IsNothing(_awq_obj) Then
                If Not IsNothing(_remove_awq_obj) AndAlso _remove_awq_obj.has_been_sent Then
                    res = False
                Else
                    If _awq_obj.has_been_sent Then
                        res = True
                    Else
                        res = False
                    End If
                End If
            ElseIf _is_reported Then
                res = True
            Else
                'not reported
                res = False
            End If
            Return res
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Public Sub reset_revision_control()
    '    _revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOSAP
    '    _revision_control_txt = ""
    'End Sub

    Public Function UpdateFromSAP(mat As CImportMatObject) As Boolean
        Dim updated As Boolean
        Dim mess As String = ""
        'qty
        If Not _qty = mat.quantity Then
            mess = mess & "qty:" & _qty & "=>" & mat.quantity & ", "
            Me.qty = mat.quantity
            updated = True
        End If
        'uom
        If Not _uom = mat.unit Then
            mess = mess & "UoM:" & _uom & "=>" & mat.unit & ", "
            Me.uom = mat.unit
            updated = True
        End If
        'partnumber
        If Not _part_number = mat.part_number Then
            mess = mess & "part number:" & _part_number & "=>" & mat.part_number & ", "
            Me.part_number = mat.part_number
            updated = True
        End If
        'or_rea
        If Not _or_rea = mat.or_rea Then
            mess = mess & "or_rea:" & _or_rea & "=>" & mat.or_rea & ", "
            Me.or_rea = mat.or_rea
            updated = True
        End If
        'warr code
        If Not _warr_code = mat.w_sp_code Then
            mess = mess & "warr_code:" & _warr_code & "=>" & mat.w_sp_code & ", "
            Me.warr_code = mat.w_sp_code
            updated = True
        End If
        'po cost
        If Not _po_cost = mat.price Then
            mess = mess & "po_cost:" & _po_cost & "=>" & mat.price & ", "
            Me.po_cost = mat.price
            updated = True
        End If

        'curr
        If Not _po_curr = mat.curr Then
            mess = mess & "po_curr:" & _po_curr & "=>" & mat.curr & ", "
            Me.po_curr = mat.curr
            updated = True
        End If

        'po cost
        If Not _po_estimated = mat.estimate Then
            mess = mess & "po_est:" & _po_estimated & "=>" & mat.estimate & ", "
            Me.po_estimated = mat.estimate
            updated = True
        End If
        If updated Then
            If mess.EndsWith(", ") Then
                mess = mess.Substring(0, mess.Length - 2)
            End If
            logRevisionControlText(mess)
            Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
            'act
            _activity_obj.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
            _activity_obj.logRevisionControlText("Material Updated " & Me.getUniqueSAPID)
        Else
            Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOCHANGE
        End If

        Return updated
    End Function

    'is nte
    Public Function isWarrCodeNTE() As Boolean
        If Not IsNothing(_activity_obj) Then
            Return _activity_obj.isWarrCodeNTE
        Else
            Return False
        End If
    End Function

    Public Function isWarrCodeNTEExckusion() As Boolean
        If Not IsNothing(_activity_obj) Then
            Return _activity_obj.isWarrCodeNTEExclusion
        Else
            Return False
        End If
    End Function

    Public Sub logRevisionControlText(mess As String)
        Try
            If String.IsNullOrWhiteSpace(_revision_control_txt) Then
                Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & mess
            Else
                Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & mess & Chr(10) & _revision_control_txt
            End If
        Catch ex As Exception
        End Try
    End Sub

End Class

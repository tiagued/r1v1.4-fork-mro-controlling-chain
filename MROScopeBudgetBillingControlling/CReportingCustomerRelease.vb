﻿Imports System.Text
Imports Equin.ApplicationFramework
Imports Microsoft.Office.Interop.Excel

Public Class CReportingCustomerRelease
    Private Shared instance As CReportingCustomerRelease

    Public form As FormReportCustomerRelease

    'project
    Private ProjectSummaryConf As List(Of CReportConfObject)
    'awq
    Private AWQTableConf As List(Of CReportConfObject)
    Private AWQSummaryConf As List(Of CReportConfObject)
    'project detail
    Private ProjectDetailsTableConf As List(Of CReportConfObject)
    Private ProjectDetailsDiscTableConf As List(Of CReportConfObject)
    Private ProjectDetailsNTETableConf As List(Of CReportConfObject)
    Private ProjectDetailsSummaryConf As List(Of CReportConfObject)
    'down payment
    Private DownPayTableConf As List(Of CReportConfObject)
    Private DownPaySummaryConf As List(Of CReportConfObject)
    'nte details
    Private NTEDetailsTableConf As List(Of CReportConfObject)
    Private NTEDetailsActTableConf As List(Of CReportConfObject)
    Private NTEDetailsSummaryConf As List(Of CReportConfObject)

    Private reportWB As Microsoft.Office.Interop.Excel.Workbook
    'list of tabs of the report
    Private tabList As List(Of String)

    'project details list
    Private calc_args As CProjCalculatorArgs
    Private awq_list As List(Of CAWQuotationReportingObject)

    Private project_lab_curr As String
    Private project_mat_curr As String

    'defalut
    Public defaultCustPayer As CPayer

    Public Shared Function getInstance() As CReportingCustomerRelease
        Try
            If IsNothing(instance) Then
                instance = New CReportingCustomerRelease
                instance.calc_args = New CProjCalculatorArgs
                instance.is_proj_summary_tab = True
                instance.is_proj_details_tab = True
                instance.is_awq_list_tab = True
                instance.is_dp_list_tab = True
                instance.is_nte_details_list_tab = False

                instance.is_awq_latest_rev_only = True
                instance.defaultCustPayer = MConstants.listOfPayer(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value)

                instance.tabList = New List(Of String)
                'proj summ
                instance.ProjectSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_PROJ_SUM_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                'proj details
                instance.ProjectDetailsTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_PROJ_DET_SHEET_NAME)(MConstants.REPORT_CONF_CUST_REL_PROJ_DET_LIST_NAME)
                instance.ProjectDetailsDiscTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_PROJ_DET_SHEET_NAME)(MConstants.REPORT_CONF_CUST_REL_PROJ_DET_DISC_LIST_NAME)
                instance.ProjectDetailsSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_PROJ_DET_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                instance.ProjectDetailsNTETableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_PROJ_DET_SHEET_NAME)(MConstants.REPORT_CONF_CUST_REL_PROJ_DET_NTE_LIST_NAME)
                'proj awq
                instance.AWQTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_AWQ_SHEET_NAME)(MConstants.REPORT_CONF_CUST_REL_AWQ_LIST_NAME)
                instance.AWQSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_AWQ_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                'dp
                instance.DownPayTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_DP_SHEET_NAME)(MConstants.REPORT_CONF_CUST_REL_DP_LIST_NAME)
                instance.DownPaySummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_DP_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                'nte details
                instance.NTEDetailsActTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_NTE_DET_SHEET_NAME)(MConstants.REPORT_CONF_CUST_REL_NTE_DET_ACT_LIST_NAME)
                instance.NTEDetailsTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_NTE_DET_SHEET_NAME)(MConstants.REPORT_CONF_CUST_REL_NTE_DET_LIST_NAME)
                instance.NTEDetailsSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_CUST_REL_NTE_DET_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)

            End If
        Catch ex As Exception
            instance = Nothing
            MGeneralFuntionsViewControl.displayMessage("Customer Release Error", "An error occured while preparing customer release report view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return instance
    End Function

    Private _is_proj_summary_tab As Boolean
    Public Property is_proj_summary_tab() As Boolean
        Get
            Return _is_proj_summary_tab
        End Get
        Set(ByVal value As Boolean)
            If Not _is_proj_summary_tab = value Then
                _is_proj_summary_tab = value
            End If
        End Set
    End Property

    Private _is_proj_details_tab As Boolean
    Public Property is_proj_details_tab() As Boolean
        Get
            Return _is_proj_details_tab
        End Get
        Set(ByVal value As Boolean)
            If Not _is_proj_details_tab = value Then
                _is_proj_details_tab = value
            End If
        End Set
    End Property

    Private _is_awq_list_tab As Boolean
    Public Property is_awq_list_tab() As Boolean
        Get
            Return _is_awq_list_tab
        End Get
        Set(ByVal value As Boolean)
            If Not _is_awq_list_tab = value Then
                _is_awq_list_tab = value
            End If
        End Set
    End Property

    Private _is_dp_list_tab As Boolean
    Public Property is_dp_list_tab() As Boolean
        Get
            Return _is_dp_list_tab
        End Get
        Set(ByVal value As Boolean)
            If Not _is_dp_list_tab = value Then
                _is_dp_list_tab = value
            End If
        End Set
    End Property

    Private _is_nte_details_list_tab As Boolean
    Public Property is_nte_details_list_tab() As Boolean
        Get
            Return _is_nte_details_list_tab
        End Get
        Set(ByVal value As Boolean)
            If Not _is_nte_details_list_tab = value Then
                _is_nte_details_list_tab = value
            End If
        End Set
    End Property

    Private _is_awq_latest_rev_only As Boolean
    Public Property is_awq_latest_rev_only() As Boolean
        Get
            Return _is_awq_latest_rev_only
        End Get
        Set(ByVal value As Boolean)
            If Not _is_awq_latest_rev_only = value Then
                _is_awq_latest_rev_only = value
            End If
        End Set
    End Property

    Private _is_proj_details_fill_corr_act As Boolean
    Public Property is_proj_details_fill_corr_act() As Boolean
        Get
            Return _is_proj_details_fill_corr_act
        End Get
        Set(ByVal value As Boolean)
            If Not _is_proj_details_fill_corr_act = value Then
                _is_proj_details_fill_corr_act = value
            End If
        End Set
    End Property

    Private _is_lab_mat_curr_enabled As Boolean
    Public Property is_lab_mat_curr_enabled() As Boolean
        Get
            Return _is_lab_mat_curr_enabled
        End Get
        Set(ByVal value As Boolean)
            If Not _is_lab_mat_curr_enabled = value Then
                _is_lab_mat_curr_enabled = value
            End If
        End Set
    End Property

    Private _project_report_payer As CPayer
    Public Property project_report_payer() As CPayer
        Get
            Return _project_report_payer
        End Get
        Set(ByVal value As CPayer)
            If Not Object.Equals(_project_report_payer, value) Then
                Me.is_lab_mat_curr_enabled = True
                _project_report_payer = value
                'manage currencies
                If Object.Equals(_project_report_payer, defaultCustPayer) Then
                    Me.report_lab_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr
                    Me.report_mat_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr
                    Me.is_lab_mat_curr_enabled = False
                Else
                    If MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_LAB_CURR).ContainsKey(_project_report_payer.payer) Then
                        If MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_MAT_CURR).ContainsKey(_project_report_payer.payer) Then
                            Me.report_lab_curr = MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_LAB_CURR)(_project_report_payer.payer)(0)
                            Me.report_mat_curr = MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_MAT_CURR)(_project_report_payer.payer)(0)
                            Me.is_lab_mat_curr_enabled = False
                        End If
                    End If
                End If
            End If
        End Set
    End Property

    Private _report_lab_curr As String
    Public Property report_lab_curr() As String
        Get
            Return _report_lab_curr
        End Get
        Set(ByVal value As String)
            _report_lab_curr = value
        End Set
    End Property

    Private _report_mat_curr As String
    Public Property report_mat_curr() As String
        Get
            Return _report_mat_curr
        End Get
        Set(ByVal value As String)
            _report_mat_curr = value
        End Set
    End Property

    Public Sub log_text(text As String)
        Try
            form.log_bt.Text = text
        Catch ex As Exception
            Throw New Exception("An error occured while logging progress" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    'bind form controls to report object properties
    Public Sub prepareCustomerReleaseView()
        Try
            form = New FormReportCustomerRelease

            'payer selection
            'defalut value
            _project_report_payer = defaultCustPayer

            form.cb_payer.DataBindings.Clear()
            form.cb_payer.AutoCompleteMode = AutoCompleteMode.None
            form.cb_payer.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_payer)
            Dim bds As New BindingSource
            bds.DataSource = MConstants.listOfPayer.Values
            form.cb_payer.DataSource = bds
            form.cb_payer.DisplayMember = "label"
            form.cb_payer.DataBindings.Add("SelectedItem", Me, "project_report_payer", False, DataSourceUpdateMode.OnPropertyChanged)

            'currencies selection
            Me.is_lab_mat_curr_enabled = False
            'defalut value
            _report_lab_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr

            form.cb_lab_curr.DataBindings.Clear()
            form.cb_lab_curr.AutoCompleteMode = AutoCompleteMode.None
            form.cb_lab_curr.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_lab_curr)
            bds = New BindingSource
            bds.DataSource = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR).Values
            form.cb_lab_curr.DataSource = bds
            form.cb_lab_curr.DisplayMember = "Value"
            form.cb_lab_curr.ValueMember = "Key"
            form.cb_lab_curr.DataBindings.Add("SelectedValue", Me, "report_lab_curr", False, DataSourceUpdateMode.OnPropertyChanged)
            form.cb_lab_curr.DataBindings.Add("Enabled", Me, "is_lab_mat_curr_enabled", False, DataSourceUpdateMode.OnPropertyChanged)

            'defalut value
            _report_mat_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

            form.cb_mat_curr.DataBindings.Clear()
            form.cb_mat_curr.AutoCompleteMode = AutoCompleteMode.None
            form.cb_mat_curr.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_mat_curr)
            bds = New BindingSource
            bds.DataSource = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR).Values
            form.cb_mat_curr.DataSource = bds
            form.cb_mat_curr.DisplayMember = "Value"
            form.cb_mat_curr.ValueMember = "Key"
            form.cb_mat_curr.DataBindings.Add("SelectedValue", Me, "report_mat_curr", False, DataSourceUpdateMode.OnPropertyChanged)
            form.cb_mat_curr.DataBindings.Add("Enabled", Me, "is_lab_mat_curr_enabled", False, DataSourceUpdateMode.OnPropertyChanged)

            'summary page
            form.select_page_proj_sum_chk.DataBindings.Clear()
            form.select_page_proj_sum_chk.DataBindings.Add("Checked", Me, "is_proj_summary_tab", False, DataSourceUpdateMode.OnPropertyChanged)
            'proj details page
            form.select_page_proj_details_chk.DataBindings.Clear()
            form.select_page_proj_details_chk.DataBindings.Add("Checked", Me, "is_proj_details_tab", False, DataSourceUpdateMode.OnPropertyChanged)
            'awq page
            form.select_page_awq_list_chk.DataBindings.Clear()
            form.select_page_awq_list_chk.DataBindings.Add("Checked", Me, "is_awq_list_tab", False, DataSourceUpdateMode.OnPropertyChanged)
            'DP page
            form.select_page_down_payment_chk.DataBindings.Clear()
            form.select_page_down_payment_chk.DataBindings.Add("Checked", Me, "is_dp_list_tab", False, DataSourceUpdateMode.OnPropertyChanged)
            'NTE Details page
            form.select_page_nte_details_chk.DataBindings.Clear()
            form.select_page_nte_details_chk.DataBindings.Add("Checked", Me, "is_nte_details_list_tab", False, DataSourceUpdateMode.OnPropertyChanged)

            'awq latest rev only ?
            form.awq_report_latest_only_chk.DataBindings.Clear()
            form.awq_report_latest_only_chk.DataBindings.Add("Checked", Me, "is_awq_latest_rev_only", False, DataSourceUpdateMode.OnPropertyChanged)
            'fill corrective actions ?
            form.proj_details_fill_corr_act_chk.DataBindings.Clear()
            form.proj_details_fill_corr_act_chk.DataBindings.Add("Checked", Me, "is_proj_details_fill_corr_act", False, DataSourceUpdateMode.OnPropertyChanged)

            'log
            log_text("Click Run to generate report")

            form.ShowDialog()
            form.BringToFront()
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Customer Release Error", "An error occured while preparing customer release report view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub launch()
        Dim curr_altereded As Boolean = False
        Try
            'log
            log_text("Starting...")
            'check currencies
            If String.IsNullOrWhiteSpace(_report_lab_curr) OrElse MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value = _report_lab_curr _
                OrElse String.IsNullOrWhiteSpace(_report_mat_curr) OrElse MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value = _report_mat_curr Then
                Throw New Exception("Lab Currency or Material Currency cannot be empty")
            End If

            'calculate if not default customer as payer
            If defaultCustPayer.payer <> _project_report_payer.payer Then
                'log
                log_text("Calculating project for chosen payer")
                'to block project curr update in DB
                MConstants.GLOB_APP_LOADED = False
                project_lab_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr
                project_mat_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

                MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr = _report_lab_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr = _report_mat_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.repair_curr = _report_mat_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.freight_curr = _report_mat_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.third_party_curr = _report_mat_curr

                MConstants.GLOB_APP_LOADED = True

                curr_altereded = True

                'refresh calc
                MConstants.GLB_MSTR_CTRL.calculation_controller.calculateInMainThread(False)
            End If

            'log
            log_text("Checking Template")

            Dim template As String = MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_TMPL_FILE).value
            If Not System.IO.File.Exists(template) Then
                Throw New Exception("The Customer Release Template File " & template & " Does not exists")
            End If

            'clear
            clearData()

            'generate excelf file
            Dim reportName As String = MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_NAME).value
            'replace variable in report name
            reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, MConstants.GLB_MSTR_CTRL.project_controller.project.project_def)
            reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_DATE_PARAM).value, DateTime.Now.ToString("yyyy.MM.dd hh.mm.ss"))

            'if not for customer
            If _project_report_payer.payer = defaultCustPayer.payer Then
                reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_PAYER_PARAM).value, "")
            Else
                reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_PAYER_PARAM).value, "_" & _project_report_payer.label)
            End If

            reportName = MConstants.GLB_MSTR_CTRL.csFile.db_folder & "\" & reportName

            'log
            log_text("Copy and Open template")

            reportWB = MConstants.GLB_MSTR_CTRL.csFile.userxlAppOpenTemplate(template, reportName)

            'update project
            'get act details ?
            calc_args.project_details_scope_list = Nothing
            If _is_proj_details_tab Then
                calc_args.project_details_scope_list = New List(Of CScopeAtivityCalcEntry)
            End If
            'get NTE details ?
            calc_args.nteDetailsActivitiesList = Nothing
            calc_args.nteDetailsNTEObjList = Nothing
            If _is_nte_details_list_tab Then
                calc_args.nteDetailsActivitiesList = New Dictionary(Of String, CScopeAtivityCalcEntry)
                calc_args.nteDetailsNTEObjList = New List(Of CNTEDetailsReportObj)
            End If
            Try
                'log
                log_text("Calculating the project")

                MConstants.GLB_MSTR_CTRL.proj_calculator.update(_project_report_payer.payer, calc_args)

                If Not String.IsNullOrWhiteSpace(MConstants.GLB_MSTR_CTRL.proj_calculator.calc_error_text) Then
                    Throw New Exception("An error occured while calculating the whole project :" & Chr(10) & MConstants.GLB_MSTR_CTRL.proj_calculator.calc_error_text)
                End If
                MConstants.GLB_MSTR_CTRL.csFile.OffCalc(reportWB.Application)

                'load tabs
                If _is_proj_summary_tab Then
                    'log
                    log_text("Filling project summary")
                    loadProjectSumData()
                End If
                If _is_proj_details_tab AndAlso calc_args.project_details_scope_list.Count > 0 Then
                    'log
                    log_text("Filling project details")
                    loadProjectDetailsData()
                End If
                If _is_awq_list_tab Then
                    'log
                    log_text("Generate AWQ List")
                    awq_list = New List(Of CAWQuotationReportingObject)
                    MConstants.GLB_MSTR_CTRL.proj_calculator.updateAWQ(_project_report_payer.payer, awq_list)
                    'log
                    log_text("Filling AWQ List")
                    loadAWQData()
                End If
                If _is_dp_list_tab Then
                    'log
                    log_text("Filling down payment")
                    loadDownPaymentsData()
                End If
                If is_nte_details_list_tab AndAlso calc_args.nteDetailsNTEObjList.Count > 0 Then
                    'log
                    log_text("Filling NTE Details")
                    loadNTEDetailsData()
                End If

                'log
                log_text("Deleting unused sheets")
                'remove uncessary sheets
                For Each sheet As Microsoft.Office.Interop.Excel.Worksheet In reportWB.Worksheets
                    If Not tabList.Contains(sheet.Name) Then
                        sheet.Delete()
                    End If
                Next
            Catch ex As Exception
                Throw New Exception("Error " & Chr(10) & ex.Message & ex.StackTrace)
            Finally
                MConstants.GLB_MSTR_CTRL.csFile.OnCalc(reportWB.Application)
            End Try
            'log
            log_text("Saving report")
            MConstants.GLB_MSTR_CTRL.csFile.safeWorkbookSave(reportWB)
            reportWB.Activate()

            If tabList.Contains(MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_PROJSUM_TAB_NAME).value) Then
                Dim proj_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_PROJSUM_TAB_NAME).value)
                proj_sum_ws.Activate()
            End If
            calc_args.clearArgs()

            If Not IsNothing(awq_list) Then
                awq_list.Clear()
            End If
            'log
            log_text("Report Successfully Generated")
            MGeneralFuntionsViewControl.displayMessage("Customer Release Success", "The customer release report has been generated successfully :" & reportWB.FullName, MsgBoxStyle.Information)
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Customer Release Error", "An error occured while preparing customer release report view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        Finally
            'replace customer values if needed
            'calculate if not default customer as payer
            If curr_altereded Then
                MConstants.GLOB_APP_LOADED = False
                MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr = project_lab_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr = project_mat_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.repair_curr = project_mat_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.freight_curr = project_mat_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.third_party_curr = project_mat_curr
                MConstants.GLOB_APP_LOADED = True
                'refresh calc
                MConstants.GLB_MSTR_CTRL.refreshCalculation()
            End If

            form.Close()
        End Try
    End Sub

    Public Sub loadProjectSumData()
        Try
            Dim proj_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_PROJSUM_TAB_NAME).value)
            tabList.Add(proj_sum_ws.Name)
            Dim value As Object
            'fill summary values
            For Each confO As CReportConfObject In ProjectSummaryConf
                If confO.item_object = "CReportingCustomerRelease" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    proj_sum_ws.Cells(confO.item_row, confO.item_column) = value
                ElseIf confO.item_object.StartsWith("CProjectCalculator.", StringComparison.OrdinalIgnoreCase) Then
                    Dim prop As String = Trim(confO.item_object.Split(".")(1))
                    value = MConstants.GLB_MSTR_CTRL.proj_calculator.getlistOfCalcItem(prop, confO.item_property)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    proj_sum_ws.Cells(confO.item_row, confO.item_column) = value
                Else
                    Throw New Exception("Project Summary Report, conf object type not found " & confO.item_object)
                End If
            Next
        Catch ex As Exception
            Throw New Exception("An error occured while generating Project Summary view of the report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadProjectDetailsData()
        Try

            Dim proj_details_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_PROJDETAIL_TAB_NAME).value)

            tabList.Add(proj_details_ws.Name)
            Dim value As Object


            'fill summary values
            For Each confO As CReportConfObject In ProjectDetailsSummaryConf
                If confO.item_object = "CReportingCustomerRelease" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    proj_details_ws.Cells(confO.item_row, confO.item_column) = value
                ElseIf confO.item_object.StartsWith("CProjectCalculator.", StringComparison.OrdinalIgnoreCase) Then
                    Dim prop As String = Trim(confO.item_object.Split(".")(1))
                    value = MConstants.GLB_MSTR_CTRL.proj_calculator.getlistOfCalcItem(prop, confO.item_property)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    proj_details_ws.Cells(confO.item_row, confO.item_column) = value
                Else
                    Throw New Exception("Project Details Report, conf object type not found " & confO.item_object)
                End If
            Next

            'properties to be converted to double
            Dim prop_to_double As New List(Of String)
            prop_to_double.Add("hours_sold_calc_str")
            prop_to_double.Add("labor_sold_calc_str")
            prop_to_double.Add("mat_sold_calc_str")
            prop_to_double.Add("rep_sold_calc_str")
            prop_to_double.Add("freight_sold_calc_str")
            prop_to_double.Add("serv_sold_calc_str")

            'act list
            Dim reportArray(calc_args.project_details_scope_list.Count - 1, ProjectDetailsTableConf.Count - 1) As Object
            Dim i As Integer = 0
            'build array
            For Each entry As CScopeAtivityCalcEntry In calc_args.project_details_scope_list
                For Each confO As CReportConfObject In ProjectDetailsTableConf
                    'fill corrective actions ?
                    If confO.item_property = "corrective_action" And Not is_proj_details_fill_corr_act Then
                        Continue For
                    End If

                    If confO.item_object = "CScopeActivity" Then
                        value = CallByName(entry.act_obj, confO.item_property, CallType.Get)
                    ElseIf confO.item_object = "CReportingCustomerRelease" Then
                        value = CallByName(Me, confO.item_property, CallType.Get, New Object() {entry})
                    Else
                        value = CallByName(entry, confO.item_property, CallType.Get)
                        'get double rather than string
                        If prop_to_double.Contains(confO.item_property) Then
                            If IsNumeric(value) Then
                                Dim dbl_val As Double
                                'ignore case where str val is 0.00
                                If Double.TryParse(value, dbl_val) AndAlso Not (dbl_val = 0 AndAlso CStr(value).Length > 1) Then
                                    value = dbl_val
                                End If
                            End If
                        End If
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next
            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(ProjectDetailsTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & ProjectDetailsTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(proj_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If calc_args.project_details_scope_list.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, proj_details_ws, xlrg, calc_args.project_details_scope_list.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(proj_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = proj_details_ws.Range(xlrg, xlrg.Offset(calc_args.project_details_scope_list.Count - 1, ProjectDetailsTableConf.Count - 1))
            xlrg.Value = reportArray


            'Discount Values
            Dim discountList = MConstants.GLB_MSTR_CTRL.proj_calculator.listOfDiscount.Values.ToList

            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(ProjectDetailsDiscTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange

            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & ProjectDetailsDiscTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            If discountList.Count > 0 Then
                Dim disc_reportArray(discountList.Count - 1, ProjectDetailsDiscTableConf.Count - 1) As Object
                i = 0
                'build array
                For Each entry As CScopeAtivityCalcEntry In discountList

                    For Each confO As CReportConfObject In ProjectDetailsDiscTableConf
                        If String.IsNullOrWhiteSpace(confO.item_object) OrElse confO.item_object.ToLower.Equals("skip") Then
                            Continue For
                        End If
                        value = CallByName(entry, confO.item_property, CallType.Get)
                        If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                            If CDate(value) = MConstants.APP_NULL_DATE Then
                                value = ""
                            Else
                                Dim _d As Date = CDate(value)
                                value = _d.ToString(confO.item_format)
                            End If
                        End If
                        disc_reportArray(i, confO.item_column - 1) = value
                    Next
                    i = i + 1
                Next
                'insert
                'startrow + 1 to insert from the line below to propagate the good formating
                xlrg = CType(proj_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
                'insert rows
                If discountList.Count > 1 Then
                    MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, proj_details_ws, xlrg, discountList.Count - 1)
                End If
                'reset range as lines have been added
                xlrg = CType(proj_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
                xlrg = proj_details_ws.Range(xlrg, xlrg.Offset(discountList.Count - 1, ProjectDetailsDiscTableConf.Count - 1))
                xlrg.Value = disc_reportArray
            Else
                'delete discount line
                xlrg = CType(proj_details_ws.Rows(xlrg_start.Row), Microsoft.Office.Interop.Excel.Range)
                xlrg.Delete()
            End If


            'NTE Values
            'calculate NTE
            Dim nteList = MConstants.GLB_MSTR_CTRL.proj_calculator.listOfNTE.Values.ToList
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(ProjectDetailsNTETableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange

            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & ProjectDetailsNTETableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            If nteList.Count > 0 Then
                Dim nte_reportArray(nteList.Count - 1, ProjectDetailsNTETableConf.Count - 1) As Object
                i = 0
                'build array
                For Each entry As CScopeAtivityCalcEntry In nteList

                    For Each confO As CReportConfObject In ProjectDetailsNTETableConf
                        If String.IsNullOrWhiteSpace(confO.item_object) OrElse confO.item_object.ToLower.Equals("skip") Then
                            Continue For
                        End If
                        value = CallByName(entry, confO.item_property, CallType.Get)
                        If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                            If CDate(value) = MConstants.APP_NULL_DATE Then
                                value = ""
                            Else
                                Dim _d As Date = CDate(value)
                                value = _d.ToString(confO.item_format)
                            End If
                        End If
                        nte_reportArray(i, confO.item_column - 1) = value
                    Next
                    i = i + 1
                Next
                'insert
                'startrow + 1 to insert from the line below to propagate the good formating
                xlrg = CType(proj_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
                'insert rows
                If nteList.Count > 1 Then
                    MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, proj_details_ws, xlrg, nteList.Count - 1)
                End If
                'reset range as lines have been added
                xlrg = CType(proj_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
                xlrg = proj_details_ws.Range(xlrg, xlrg.Offset(nteList.Count - 1, ProjectDetailsNTETableConf.Count - 1))
                xlrg.Value = nte_reportArray
            Else
                'delete discount line
                xlrg = CType(proj_details_ws.Rows(xlrg_start.Row), Microsoft.Office.Interop.Excel.Range)
                xlrg.Delete()
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while generating Project Details view of the report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadAWQData()
        Try

            'init array
            Dim _awq_reporting_list As List(Of CAWQuotationReportingObject)
            If _is_awq_latest_rev_only Then
                _awq_reporting_list = New List(Of CAWQuotationReportingObject)
                For Each repO As CAWQuotationReportingObject In awq_list
                    If repO.is_latest_sent Then
                        _awq_reporting_list.Add(repO)
                    End If
                Next
            Else
                _awq_reporting_list = awq_list
            End If


            Dim reportArray(_awq_reporting_list.Count - 1, AWQTableConf.Count - 1) As Object
            Dim i As Integer = 0

            Dim value As Object
            'build array
            For Each awq_report_o As CAWQuotationReportingObject In _awq_reporting_list
                For Each confO As CReportConfObject In AWQTableConf
                    If confO.item_object = "CAWQuotationReportingObject" Then
                        value = CallByName(awq_report_o, confO.item_property, CallType.Get)
                    Else
                        value = CallByName(awq_report_o.awq, confO.item_property, CallType.Get)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next

            Dim awq_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_AWQ_TAB_NAME).value)
            tabList.Add(awq_ws.Name)

            'fill summary values
            For Each confO As CReportConfObject In AWQSummaryConf
                If confO.item_object = "CReportingCustomerRelease" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)

                ElseIf confO.item_object.StartsWith("CProjectCalculator.", StringComparison.OrdinalIgnoreCase) Then
                    Dim prop As String = Trim(confO.item_object.Split(".")(1))
                    value = MConstants.GLB_MSTR_CTRL.proj_calculator.getlistOfCalcItem(prop, confO.item_property)

                Else
                    Throw New Exception("AWQ Report Summary Data, conf object type not found " & confO.item_object)
                End If
                If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                    If CDate(value) = MConstants.APP_NULL_DATE Then
                        value = ""
                    Else
                        Dim _d As Date = CDate(value)
                        value = _d.ToString(confO.item_format)
                    End If
                End If
                awq_ws.Cells(confO.item_row, confO.item_column) = value
            Next


            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(AWQTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & AWQTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try

            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(awq_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If _awq_reporting_list.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, awq_ws, xlrg, _awq_reporting_list.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(awq_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = awq_ws.Range(xlrg, xlrg.Offset(_awq_reporting_list.Count - 1, AWQTableConf.Count - 1))
            xlrg.Value = reportArray

        Catch ex As Exception
            Throw New Exception("An error occured while generating AWQ view of the report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
    'load downpayment data
    Public Sub loadDownPaymentsData()
        Try
            Dim dp_list = MConstants.GLB_MSTR_CTRL.proj_calculator.listDP.Values.ToList

            If dp_list.Count = 0 Then
                Exit Sub
            End If

            Dim dowm_pay_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_DOWN_PAY_TAB_NAME).value)

            tabList.Add(dowm_pay_ws.Name)
            Dim value As Object

            'fill summary values
            For Each confO As CReportConfObject In DownPaySummaryConf
                If confO.item_object = "CReportingCustomerRelease" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                ElseIf confO.item_object.StartsWith("CProjectCalculator.", StringComparison.OrdinalIgnoreCase) Then
                    Dim prop As String = Trim(confO.item_object.Split(".")(1))
                    value = MConstants.GLB_MSTR_CTRL.proj_calculator.getlistOfCalcItem(prop, confO.item_property)
                Else
                    Throw New Exception("Downpayment Report, conf object type not found " & confO.item_object)
                End If
                If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                    If CDate(value) = MConstants.APP_NULL_DATE Then
                        value = ""
                    Else
                        Dim _d As Date = CDate(value)
                        value = _d.ToString(confO.item_format)
                    End If
                End If
                dowm_pay_ws.Cells(confO.item_row, confO.item_column) = value
            Next

            'dp list
            Dim reportArray(dp_list.Count - 1, DownPayTableConf.Count - 1) As Object
            Dim i As Integer = 0
            'build array
            For Each entry As CScopeAtivityCalcEntry In dp_list

                For Each confO As CReportConfObject In DownPayTableConf
                    If confO.item_object = "CScopeAtivityCalcEntry" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        value = CallByName(entry.business_object, confO.item_property, CallType.Get)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next
            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(DownPayTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & DownPayTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(dowm_pay_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If dp_list.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, dowm_pay_ws, xlrg, dp_list.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(dowm_pay_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = dowm_pay_ws.Range(xlrg, xlrg.Offset(dp_list.Count - 1, DownPayTableConf.Count - 1))
            xlrg.Value = reportArray

        Catch ex As Exception
            Throw New Exception("An error occured while generating Down Payment view of the report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
    'load NTE detailslist
    Public Sub loadNTEDetailsData()
        Try
            Dim nte_details_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_NTE_DETAILS_TAB_NAME).value)

            tabList.Add(nte_details_ws.Name)
            Dim value As Object

            'fill summary values
            For Each confO As CReportConfObject In NTEDetailsSummaryConf
                If confO.item_object = "CReportingCustomerRelease" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    nte_details_ws.Cells(confO.item_row, confO.item_column) = value
                ElseIf confO.item_object.StartsWith("CProjectCalculator.", StringComparison.OrdinalIgnoreCase) Then
                    Dim prop As String = Trim(confO.item_object.Split(".")(1))
                    value = MConstants.GLB_MSTR_CTRL.proj_calculator.getlistOfCalcItem(prop, confO.item_property)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    nte_details_ws.Cells(confO.item_row, confO.item_column) = value
                Else
                    Throw New Exception("NTE Details Report, conf object type not found " & confO.item_object)
                End If
            Next

            'act list
            Dim reportArray(calc_args.nteDetailsActivitiesList.Count - 1, NTEDetailsActTableConf.Count - 1) As Object
            Dim i As Integer = 0
            'build array
            For Each entry As CScopeAtivityCalcEntry In calc_args.nteDetailsActivitiesList.Values
                For Each confO As CReportConfObject In NTEDetailsActTableConf
                    If String.IsNullOrWhiteSpace(confO.item_object) OrElse confO.item_object.ToLower.Equals("skip") Then
                        Continue For
                    End If
                    If confO.item_object = "CScopeActivity" Then
                        value = CallByName(entry.act_obj, confO.item_property, CallType.Get)
                    ElseIf confO.item_object = "CScopeAtivityCalcEntry" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("NTE Details Report, Act List, conf object type not found " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next

            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(NTEDetailsActTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & NTEDetailsActTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(nte_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If calc_args.nteDetailsActivitiesList.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, nte_details_ws, xlrg, calc_args.nteDetailsActivitiesList.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(nte_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = nte_details_ws.Range(xlrg, xlrg.Offset(calc_args.nteDetailsActivitiesList.Count - 1, NTEDetailsActTableConf.Count - 1))
            xlrg.Value = reportArray


            'NTE Objects list
            Dim reportNTEArray(calc_args.nteDetailsNTEObjList.Count - 1, NTEDetailsTableConf.Count - 1) As Object
            i = 0
            'build array
            For Each entry As CNTEDetailsReportObj In calc_args.nteDetailsNTEObjList
                For Each confO As CReportConfObject In NTEDetailsTableConf
                    If String.IsNullOrWhiteSpace(confO.item_object) OrElse confO.item_object.ToLower.Equals("skip") Then
                        Continue For
                    End If
                    If confO.item_object = "CNTEDetailsReportObj" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("NTE Details Report, NTE Obj List, conf object type not found " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportNTEArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next

            'insert
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(NTEDetailsTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & NTEDetailsTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(nte_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If calc_args.nteDetailsNTEObjList.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, nte_details_ws, xlrg, calc_args.nteDetailsNTEObjList.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(nte_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = nte_details_ws.Range(xlrg, xlrg.Offset(calc_args.nteDetailsNTEObjList.Count - 1, NTEDetailsTableConf.Count - 1))
            xlrg.Value = reportNTEArray
        Catch ex As Exception
            Throw New Exception("An error occured while generating Project Details view of the report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    'clear report
    Public Sub clearData()
        _ProjectAWQPendingLabor = 0
        _ProjectAWQApprovedLabor = 0
        _ProjectAWQTotalLabor = 0
        _ProjectAWQPendingMarerial = 0
        _ProjectAWQApprovedMarerial = 0
        _ProjectAWQTotalMarerial = 0
    End Sub

    Private _ProjectAWQPendingLabor As Double
    Public ReadOnly Property getProjectAWQPendingLabor() As Double
        Get
            Return _ProjectAWQPendingLabor
        End Get
    End Property

    Private _ProjectAWQApprovedLabor As Double
    Public ReadOnly Property getProjectAWQApprovedLabor() As Double
        Get
            Return _ProjectAWQApprovedLabor
        End Get
    End Property

    Private _ProjectAWQTotalLabor As Double
    Public ReadOnly Property getProjectAWQTotalLabor() As Double
        Get
            Return _ProjectAWQTotalLabor
        End Get
    End Property

    Private _ProjectAWQPendingMarerial As Double
    Public ReadOnly Property getProjectAWQPendingMarerial() As Double
        Get
            Return _ProjectAWQPendingMarerial
        End Get
    End Property

    Private _ProjectAWQApprovedMarerial As Double
    Public ReadOnly Property getProjectAWQApprovedMarerial() As Double
        Get
            Return _ProjectAWQApprovedMarerial
        End Get
    End Property

    Private _ProjectAWQTotalMarerial As Double
    Public ReadOnly Property getProjectAWQTotalMarerial() As Double
        Get
            Return _ProjectAWQTotalMarerial
        End Get
    End Property

    Public ReadOnly Property Customer() As String
        Get
            If defaultCustPayer.payer = project_report_payer.payer Then
                Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer
            Else
                Return project_report_payer.friendly_label
            End If
        End Get
    End Property

    Public ReadOnly Property Aircraft() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.ac_reg & "(" & MConstants.GLB_MSTR_CTRL.project_controller.project.ac_master & ")"
        End Get
    End Property

    Public ReadOnly Property Project() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_list_str
        End Get
    End Property

    Public ReadOnly Property ProjectDescription() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_desc
        End Get
    End Property

    Public ReadOnly Property getDate() As Date
        Get
            Return DateTime.Now
        End Get
    End Property

    Public ReadOnly Property AWQSpecialist() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.awq_specialist
        End Get
    End Property

    Public ReadOnly Property getLaborCurr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr).value
        End Get
    End Property

    Public ReadOnly Property getMatCurr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr).value
        End Get
    End Property

    Public ReadOnly Property getProjectQuotation() As String
        Get
            If defaultCustPayer.payer = project_report_payer.payer Then
                Return MConstants.GLB_MSTR_CTRL.project_controller.project.quotation_number
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property getProjectPurchaseOrder() As String
        Get
            If defaultCustPayer.payer = project_report_payer.payer Then
                Return MConstants.GLB_MSTR_CTRL.project_controller.project.purchase_order
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property getProjectStartDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.input_date
        End Get
    End Property

    Public ReadOnly Property getProjectFinishDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.fcst_finish_date
        End Get
    End Property

    Public ReadOnly Property getProjectDearCustomer() As String
        Get
            Dim sb As New StringBuilder
            sb.AppendFormat(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_DEAR_CUSTOMER_TXT).value, MConstants.GLB_MSTR_CTRL.project_controller.project.customer_rep_name)
            Return sb.ToString
        End Get
    End Property
    'to calculate ratio
    Public ReadOnly Property getAddVsInitAdd() As Double
        Get
            Return MConstants.GLB_MSTR_CTRL.proj_calculator.sub_total_scope_add_total.lab_mat_in_lab_cur_calc
        End Get
    End Property
    Public ReadOnly Property getAddVsInitIni() As Double
        Get
            Dim entry = CScopeAtivityCalcEntry.getEmpty
            entry.addValuesFrom(MConstants.GLB_MSTR_CTRL.proj_calculator.sub_total_scope_init)
            entry.addValuesFrom(MConstants.GLB_MSTR_CTRL.proj_calculator.sub_total_scope_init_adj)
            Return entry.lab_mat_in_lab_cur_calc
        End Get
    End Property

    Public ReadOnly Property getApprVsPendAppr() As Double
        Get
            'avoid division by 0
            If MConstants.GLB_MSTR_CTRL.proj_calculator.sub_total_scope_add_appr.lab_mat_in_lab_cur_calc = 0 _
                AndAlso MConstants.GLB_MSTR_CTRL.proj_calculator.sub_total_scope_add_pending.lab_mat_in_lab_cur_calc = 0 Then
                Return 1
            End If
            Return MConstants.GLB_MSTR_CTRL.proj_calculator.sub_total_scope_add_appr.lab_mat_in_lab_cur_calc
        End Get
    End Property
    Public ReadOnly Property getApprVsPendPend() As Double
        Get
            Return MConstants.GLB_MSTR_CTRL.proj_calculator.sub_total_scope_add_pending.lab_mat_in_lab_cur_calc
        End Get
    End Property

    Public ReadOnly Property getPaidVsUnPaidPaid() As Double
        Get
            If MConstants.GLB_MSTR_CTRL.proj_calculator.dp_paid.lab_mat_in_lab_cur_calc = 0 AndAlso
                     MConstants.GLB_MSTR_CTRL.proj_calculator.dp_un_paid.lab_mat_in_lab_cur_calc = 0 Then
                Return 1
            End If
            Return MConstants.GLB_MSTR_CTRL.proj_calculator.dp_paid.lab_mat_in_lab_cur_calc
        End Get
    End Property
    Public ReadOnly Property getPaidVsUnPaidUPaid() As Double
        Get
            Return MConstants.GLB_MSTR_CTRL.proj_calculator.dp_un_paid.lab_mat_in_lab_cur_calc
        End Get
    End Property

    Public ReadOnly Property getProjDetailsScope(entry As CScopeAtivityCalcEntry) As String
        Get
            Return entry.init_add_label
        End Get
    End Property

    Public ReadOnly Property getProjDetailsCustomerFeedback(entry As CScopeAtivityCalcEntry) As String
        Get
            Dim val As String
            If entry.init_add_label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD_REPORT_NO_PRICE).value Then
                val = MConstants.constantConf(MConstants.CONST_CONF_REPORT_CUST_FEEDBACK_NA).value
            ElseIf entry.init_add = CScopeActivityCalculator.INIT OrElse entry.init_add = CScopeActivityCalculator.INIT_ADJ Then
                val = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY).value).value
            ElseIf entry.init_add = CScopeActivityCalculator.ADD Then
                val = MConstants.constantConf(MConstants.CONST_CONF_RELEASE_CUST_REPORT_PROJDETAIL_BEL_THR_CUST_STAT).value
            Else
                'case AWQ
                If entry.customer_status = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY).value).value Then
                    val = entry.customer_status
                Else
                    val = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_PENDING_KEY).value).value
                End If
            End If
            Return val
        End Get
    End Property

    Public ReadOnly Property getProjDetailsQuotation(entry As CScopeAtivityCalcEntry) As String
        Get
            Dim val As String
            If entry.init_add = CScopeActivityCalculator.ADD Then
                val = ""
            Else
                'case AWQ
                val = entry.entry_scope_label
            End If
            Return val
        End Get
    End Property

End Class

﻿
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CDiscount
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    'used to created new ids
    Public Shared currentMaxID As Integer


    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_DISCOUNT_LOADED Then
            Me.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_PROJ_KEY).value
            Me.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value
            'do it on internal private property , cause the setter will alterate the values
            _cap_value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _is_editable = True
            _payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
            _payer_obj = MConstants.listOfPayer(_payer)
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_DISCOUNT_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
        MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshDiscountDependantViews()
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_DISCOUNT_DB_READ
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""

        Try
            If Not _is_editable Then
                res = False
                mess = "Item is not editable by end user so cannot be deleted"
            Else
                Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isDiscountUsed(Me)
                If isUsed.Key Then
                    res = False
                    mess = "Item " & Me.ToString & " Cannot be deleted because it is used in " & isUsed.Value.Count & " Activity(ies)/Labor CC(s) / Material(s) " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value)
                Else
                    Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.deleteObjectDiscount(Me)
                    res = True
                End If
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.ToString & Chr(10) & ex.Message
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function


    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not _is_editable Then
            res = False
            mess = "Cannot be modified"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Shared Function getEmptyDisc() As CDiscount
        Dim currState As Boolean
        currState = MConstants.GLOB_DISCOUNT_LOADED
        MConstants.GLOB_DISCOUNT_LOADED = False
        Dim empty As New CDiscount
        empty.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
        'if label nothing it will cause issue in combobox selection
        empty.label = " "
        MConstants.GLOB_DISCOUNT_LOADED = currState
        Return empty
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_DISCOUNT_LOADED Then
                    CDiscount.currentMaxID = Math.Max(CDiscount.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                'refresh combo boxes
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshDiscountDependantViews()
                End If
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _scope_applicability As String
    Public Property scope_applicability() As String
        Get
            Return _scope_applicability
        End Get
        Set(ByVal value As String)
            If Not _scope_applicability = value Then
                'check if used before changing scope applicability
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isDiscountUsed(Me)
                    If isUsed.Key Then
                        MGeneralFuntionsViewControl.displayMessage("Discount is used", "Cannot change discount scope cause it's already used by the following items " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value), MsgBoxStyle.Critical)
                        Exit Property
                    End If
                    _scope_applicability = value
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshDiscountDependantViews()
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                    NotifyPropertyChanged()
                Else
                    _scope_applicability = value
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _lab_mat_applicability As String
    Public Property lab_mat_applicability() As String
        Get
            Return _lab_mat_applicability
        End Get
        Set(ByVal value As String)
            'lab mat app set only if scope is proj or act
            If Not _lab_mat_applicability = value Then
                _lab_mat_applicability = value
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshDiscountDependantViews()
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _rate As Double
    Public Property rate() As Double
        Get
            Return _rate
        End Get
        Set(ByVal value As Double)
            If Not _rate = value Then
                _rate = value
                NotifyPropertyChanged()
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _discount_value As Double
    Public Property discount_value() As Double
        Get
            Return _discount_value
        End Get
        Set(ByVal value As Double)
            'n/a if is flat is not checked
            If Not is_flat Then
                Exit Property
            End If
            If Not _discount_value = value Then
                _discount_value = value
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _value_curr As String
    Public Property value_curr() As String
        Get
            Return _value_curr
        End Get
        Set(ByVal value As String)
            'n/a if is flat is not checked
            If Not is_flat Then
                NotifyPropertyChanged()
                Exit Property
            End If
            If Not _value_curr = value Then
                _value_curr = value
                NotifyPropertyChanged()
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer As String
    Public Property payer() As String
        Get
            Return _payer
        End Get
        Set(ByVal value As String)
            If Not _payer = value Then
                _payer = value
                If MConstants.listOfPayer.ContainsKey(_payer) Then
                    Me.payer_obj = MConstants.listOfPayer(_payer)
                Else
                    Me.payer_obj = Nothing
                    Throw New Exception("Payer does not exist in payer list : " & _payer)
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer_obj As CPayer
    Public Property payer_obj() As CPayer
        Get
            Return _payer_obj
        End Get
        Set(ByVal value As CPayer)
            If Not Object.Equals(_payer_obj, value) Then
                _payer_obj = value
                If IsNothing(_payer_obj) Then
                    'use Me to force a new value of payer_obj
                    Me.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _payer = _payer_obj.payer
                End If
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cap_value As Double
    Public Property cap_value() As Double
        Get
            Return _cap_value
        End Get
        Set(ByVal value As Double)
            'n/a if is flat is checked
            If is_flat Then
                Exit Property
            End If
            If Not _cap_value = value Then
                _cap_value = value
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cap_value_curr As String
    Public Property cap_value_curr() As String
        Get
            Return _cap_value_curr
        End Get
        Set(ByVal value As String)
            'n/a if is flat is checked
            If is_flat Then
                Exit Property
            End If
            If Not _cap_value_curr = value Then
                _cap_value_curr = value
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_flat As Boolean
    Public Property is_flat() As Boolean
        Get
            Return _is_flat
        End Get
        Set(ByVal value As Boolean)
            If Not _is_flat = value Then
                'reset values before seting _is_flat. because setter of property to be reset will not work properly
                If Not value Then
                    Me.discount_value = 0
                    Me.value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    Me.rate = 0
                    Me.cap_value = 0
                    Me.cap_value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                End If
                _is_flat = value
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _exclude_children As Boolean
    Public Property exclude_children() As Boolean
        Get
            Return _exclude_children
        End Get
        Set(ByVal value As Boolean)
            If Not _exclude_children = value Then
                _exclude_children = value
                If MConstants.GLOB_DISCOUNT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_editable As Boolean
    Public Property is_editable() As Boolean
        Get
            Return _is_editable
        End Get
        Set(ByVal value As Boolean)
            If Not _is_editable = value Then
                _is_editable = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Function getDiscountValue(gross_value As Double, curr As String) As Double
        'calculate discount
        Dim res As Double
        If Me.is_flat Then
            res = Math.Min(MCalc.toCurr(Me.discount_value, Me.value_curr, curr), gross_value)
        ElseIf Me.cap_value_curr <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            'if capped
            res = Math.Min(MCalc.toCurr(Me.cap_value, Me.cap_value_curr, curr), gross_value * Me.rate)
        Else
            res = gross_value * Me.rate
        End If
        Return res
    End Function

    Public Function getDiscountValueExclusive(value As Double, valueNoDisc As Double, curr As String) As Double
        If _exclude_children Then
            Return getDiscountValue(valueNoDisc, curr)
        Else
            Return getDiscountValue(value, curr)
        End If
    End Function

    'to string
    Public Overrides Function tostring() As String
        Return "Discount: " & _label & ", Description: " & _description
    End Function
End Class
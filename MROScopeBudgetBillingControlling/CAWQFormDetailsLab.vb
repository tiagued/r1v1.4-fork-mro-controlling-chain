﻿Public Class CAWQFormDetailsLab
    Private _skill As String
    Public Property skill() As String
        Get
            Return _skill
        End Get
        Set(ByVal value As String)
            _skill = value
        End Set
    End Property

    Private _skill_key As String
    Public Property skill_key() As String
        Get
            Return _skill_key
        End Get
        Set(ByVal value As String)
            _skill_key = value
        End Set
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            _qty = value
        End Set
    End Property

    Private _UoM As String
    Public Property UoM() As String
        Get
            Return _UoM
        End Get
        Set(ByVal value As String)
            _UoM = value
        End Set
    End Property

    Private _price As Double
    Public Property price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = value
        End Set
    End Property


    Private _approved_price As Double
    Public Property approved_price() As Double
        Get
            Return _approved_price
        End Get
        Set(ByVal value As Double)
            _approved_price = value
        End Set
    End Property


    Public Sub addApprLabor(soldH As CSoldHours, isNegative As Boolean)
        If isNegative Then
            _qty = _qty - soldH.sold_hrs
            _approved_price = _approved_price - soldH.calc_price_before_disc
        Else
            _qty = _qty + soldH.sold_hrs
            _approved_price = _approved_price + soldH.calc_price_before_disc
        End If
    End Sub

    Public Sub addLabor(soldH As CSoldHours, isNegative As Boolean)
        If isNegative Then
            _qty = _qty - soldH.sold_hrs
            _price = _price - soldH.calc_price_before_disc
        Else
            _qty = _qty + soldH.sold_hrs
            _price = _price + soldH.calc_price_before_disc
        End If
    End Sub

End Class

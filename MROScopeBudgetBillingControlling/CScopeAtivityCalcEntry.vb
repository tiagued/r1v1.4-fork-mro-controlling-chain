﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CScopeAtivityCalcEntry
    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    'list of items used to calculate before discount price
    Public laborList As List(Of CSoldHours)
    Public materialList As List(Of CMaterial)
    Public repairList As List(Of CMaterial)
    Public freightList As List(Of CMaterial)
    Public servList As List(Of CMaterial)
    Public labMatDeletedList As List(Of Object)

    Public Shared Function getEmpty() As CScopeAtivityCalcEntry
        Return New CScopeAtivityCalcEntry(CScopeActivityCalculator.EMPTY_LINE)
    End Function

    Public Sub New(id As String)
        _entry_id = id
    End Sub

    Private _act_obj As CScopeActivity
    Public Property act_obj() As CScopeActivity
        Get
            Return _act_obj
        End Get
        Set(ByVal value As CScopeActivity)
            _act_obj = value
        End Set
    End Property

    Private _entry_id As String
    Public Property entry_id() As String
        Get
            Return _entry_id
        End Get
        Set(ByVal value As String)
            _entry_id = value
        End Set
    End Property

    Private _calc_type As String
    Public Property calc_type() As String
        Get
            Return _calc_type
        End Get
        Set(ByVal value As String)
            _calc_type = value
        End Set
    End Property

    Private _init_add As String
    Public Property init_add() As String
        Get
            Return _init_add
        End Get
        Set(ByVal value As String)
            _init_add = value
        End Set
    End Property

    Private _init_add_label As String
    Public Property init_add_label() As String
        Get
            Dim val As String
            If Not String.IsNullOrWhiteSpace(_init_add_label) Then
                val = _init_add_label
            Else
                If _init_add = CScopeActivityCalculator.INIT Then
                    Val = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_INI).value
                ElseIf _init_add = CScopeActivityCalculator.INIT_ADJ Then
                    Val = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_INI_ADJ).value
                ElseIf _init_add = CScopeActivityCalculator.ADD Then
                    Val = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value
                Else
                    'case AWQ
                    Val = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_AWQ_QUOTE_TXT).value
                End If
            End If
            Return val
        End Get
        Set(ByVal value As String)
            _init_add_label = value
        End Set
    End Property

    Private _entry_scope As String
    Public Property entry_scope() As String
        Get
            Return _entry_scope
        End Get
        Set(ByVal value As String)
            _entry_scope = value
        End Set
    End Property

    Private _customer_status As String
    Public Property customer_status() As String
        Get
            Return _customer_status
        End Get
        Set(ByVal value As String)
            _customer_status = value
        End Set
    End Property

    Private _entry_scope_label As String
    Public Property entry_scope_label() As String
        Get
            Return _entry_scope_label
        End Get
        Set(ByVal value As String)
            _entry_scope_label = value
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
            End If
        End Set
    End Property

    Private _payer As CPayer
    Public Property payer() As CPayer
        Get
            Return _payer
        End Get
        Set(ByVal value As CPayer)
            _payer = value
        End Set
    End Property

    Public ReadOnly Property payer_view() As String
        Get
            Return _payer.label
        End Get
    End Property

    Public Function is_labor_and_mat_text() As Boolean
        Return Not IsNumeric(_labor_sold_calc_str) AndAlso Not IsNumeric(_mat_sold_calc_str) AndAlso Not IsNumeric(_rep_sold_calc_str) AndAlso
            Not IsNumeric(_serv_sold_calc_str) AndAlso Not IsNumeric(_freight_sold_calc_str)
    End Function

    Private _hours_sold_calc As Double
    Public Property hours_sold_calc() As Double
        Get
            Return _hours_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _hours_sold_calc Then
                _hours_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _labor_sold_calc As Double
    Public Property labor_sold_calc() As Double
        Get
            Return _labor_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _labor_sold_calc Then
                _labor_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _labor_sold_no_disc_calc As Double
    Public Property labor_sold_no_disc_calc() As Double
        Get
            Return _labor_sold_no_disc_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _labor_sold_no_disc_calc Then
                _labor_sold_no_disc_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _mat_sold_calc As Double
    Public Property mat_sold_calc() As Double
        Get
            Return _mat_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _mat_sold_calc Then
                _mat_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _mat_sold_no_disc_calc As Double
    Public Property mat_sold_no_disc_calc() As Double
        Get
            Return _mat_sold_no_disc_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _mat_sold_no_disc_calc Then
                _mat_sold_no_disc_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _rep_sold_calc As Double
    Public Property rep_sold_calc() As Double
        Get
            Return _rep_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _rep_sold_calc Then
                _rep_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _rep_sold_no_disc_calc As Double
    Public Property rep_sold_no_disc_calc() As Double
        Get
            Return _rep_sold_no_disc_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _rep_sold_no_disc_calc Then
                _rep_sold_no_disc_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _serv_sold_calc As Double
    Public Property serv_sold_calc() As Double
        Get
            Return _serv_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _serv_sold_calc Then
                _serv_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Public ReadOnly Property all_mat_calc() As Double
        Get
            Return _mat_sold_calc + _rep_sold_calc + _freight_sold_calc + _serv_sold_calc
        End Get
    End Property

    Public ReadOnly Property lab_mat_in_lab_cur_calc() As Double
        Get
            Return _labor_sold_calc + MCalc.toLabCurr(_mat_sold_calc + _rep_sold_calc + _freight_sold_calc + _serv_sold_calc, MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr)
        End Get
    End Property

    Private _serv_sold_no_disc_calc As Double
    Public Property serv_sold_no_disc_calc() As Double
        Get
            Return _serv_sold_no_disc_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _serv_sold_no_disc_calc Then
                _serv_sold_no_disc_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _freight_sold_calc As Double
    Public Property freight_sold_calc() As Double
        Get
            Return _freight_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _freight_sold_calc Then
                _freight_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _comment As String
    Public Property comment() As String
        Get
            Return _comment
        End Get
        Set(ByVal value As String)
            _comment = value
        End Set
    End Property

    Private _freight_sold_no_disc_calc As Double
    Public Property freight_sold_no_disc_calc() As Double
        Get
            Return _freight_sold_no_disc_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _freight_sold_no_disc_calc Then
                _freight_sold_no_disc_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _mat_info_calc As String
    Public Property mat_info_calc() As String
        Get
            Return _mat_info_calc
        End Get
        Set(ByVal value As String)
            _mat_info_calc = value
            NotifyPropertyChanged()
        End Set
    End Property

    Public Sub copyValuesTo(toEntry As CScopeAtivityCalcEntry)
        toEntry.hours_sold_calc = _hours_sold_calc
        toEntry.mat_sold_calc = _mat_sold_calc
        toEntry.freight_sold_calc = _freight_sold_calc
        toEntry.serv_sold_calc = _serv_sold_calc
        toEntry.rep_sold_calc = _rep_sold_calc
        toEntry.labor_sold_calc = _labor_sold_calc
    End Sub

    Public Sub copyPropertiesTo(toEntry As CScopeAtivityCalcEntry)
        toEntry.act_obj = _act_obj
        toEntry.entry_id = _entry_id
        toEntry.calc_type = _calc_type
        toEntry.init_add = _init_add
        toEntry.init_add_label = _init_add_label
        toEntry.entry_scope = _entry_scope
        toEntry.entry_scope_label = _entry_scope_label
        toEntry.label = _label
        toEntry.payer = _payer
        toEntry.comment = _comment

        toEntry.hours_sold_calc = _hours_sold_calc
        toEntry.mat_sold_calc = _mat_sold_calc
        toEntry.freight_sold_calc = _freight_sold_calc
        toEntry.serv_sold_calc = _serv_sold_calc
        toEntry.rep_sold_calc = _rep_sold_calc
        toEntry.labor_sold_calc = _labor_sold_calc

        toEntry.hours_sold_calc_str = _hours_sold_calc_str
        toEntry.mat_sold_calc_str = _mat_sold_calc_str
        toEntry.freight_sold_calc_str = _freight_sold_calc_str
        toEntry.serv_sold_calc_str = _serv_sold_calc_str
        toEntry.rep_sold_calc_str = _rep_sold_calc_str
        toEntry.labor_sold_calc_str = _labor_sold_calc_str

        toEntry.mat_info_calc = _mat_info_calc
    End Sub

    Public Sub addValuesFrom(fromEntry As CScopeAtivityCalcEntry)
        If IsNothing(fromEntry) Then
            Exit Sub
        End If
        _hours_sold_calc = _hours_sold_calc + fromEntry.hours_sold_calc
        _mat_sold_calc = _mat_sold_calc + fromEntry.mat_sold_calc
        _freight_sold_calc = _freight_sold_calc + fromEntry.freight_sold_calc
        _serv_sold_calc = _serv_sold_calc + fromEntry.serv_sold_calc
        _rep_sold_calc = _rep_sold_calc + fromEntry.rep_sold_calc
        _labor_sold_calc = _labor_sold_calc + fromEntry.labor_sold_calc

        If String.IsNullOrWhiteSpace(_mat_info_calc) Then
            _mat_info_calc = fromEntry.mat_info_calc
        Else
            _mat_info_calc = mat_info_calc & Chr(10) & fromEntry.mat_info_calc
        End If

        'str values. In case there are an entry with text is added to an empty entry
        If String.IsNullOrWhiteSpace(_hours_sold_calc_str) AndAlso Not IsNumeric(fromEntry.hours_sold_calc_str) Then
            _hours_sold_calc_str = fromEntry.hours_sold_calc
        Else
            _hours_sold_calc_str = _hours_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
        End If

        If String.IsNullOrWhiteSpace(_labor_sold_calc_str) AndAlso Not IsNumeric(fromEntry.labor_sold_calc_str) Then
            _labor_sold_calc_str = fromEntry.labor_sold_calc_str
        Else
            _labor_sold_calc_str = _labor_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
        End If

        If String.IsNullOrWhiteSpace(_mat_sold_calc_str) AndAlso Not IsNumeric(fromEntry.mat_sold_calc_str) Then
            _mat_sold_calc_str = fromEntry.mat_sold_calc_str
        Else
            _mat_sold_calc_str = _mat_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
        End If

        If String.IsNullOrWhiteSpace(_freight_sold_calc_str) AndAlso Not IsNumeric(fromEntry.freight_sold_calc_str) Then
            _freight_sold_calc_str = fromEntry.freight_sold_calc_str
        Else
            _freight_sold_calc_str = _freight_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
        End If

        If String.IsNullOrWhiteSpace(_serv_sold_calc_str) AndAlso Not IsNumeric(fromEntry.serv_sold_calc_str) Then
            _serv_sold_calc_str = fromEntry.serv_sold_calc_str
        Else
            _serv_sold_calc_str = _serv_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
        End If

        If String.IsNullOrWhiteSpace(_rep_sold_calc_str) AndAlso Not IsNumeric(fromEntry.rep_sold_calc_str) Then
            _rep_sold_calc_str = fromEntry.rep_sold_calc_str
        Else
            _rep_sold_calc_str = _rep_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
        End If
    End Sub

    Public Sub substractValuesFrom(fromEntry As CScopeAtivityCalcEntry)
        If IsNothing(fromEntry) Then
            Exit Sub
        End If
        _hours_sold_calc = _hours_sold_calc - fromEntry.hours_sold_calc
        _mat_sold_calc = _mat_sold_calc - fromEntry.mat_sold_calc
        _freight_sold_calc = _freight_sold_calc - fromEntry.freight_sold_calc
        _serv_sold_calc = _serv_sold_calc - fromEntry.serv_sold_calc
        _rep_sold_calc = _rep_sold_calc - fromEntry.rep_sold_calc
        _labor_sold_calc = _labor_sold_calc - fromEntry.labor_sold_calc
    End Sub
    'set to 0
    Public Sub setEntryToZero()
        _hours_sold_calc = 0
        _labor_sold_calc = 0
        _mat_sold_calc = 0
        _freight_sold_calc = 0
        _rep_sold_calc = 0
        _serv_sold_calc = 0
        _mat_info_calc = ""
        'items where a discount was not applied for a given the payer-scope(awr)
        _labor_sold_no_disc_calc = 0
        _mat_sold_no_disc_calc = 0
        _rep_sold_no_disc_calc = 0
        _serv_sold_no_disc_calc = 0
        _freight_sold_no_disc_calc = 0
        'displayed values
        _labor_sold_calc_str = ""
        _mat_sold_calc_str = ""
        _freight_sold_calc_str = ""
        _rep_sold_calc_str = ""
        _serv_sold_calc_str = ""
        _hours_sold_calc_str = ""
    End Sub

    'use to check if refer to quote need to be calculated
    Public Function is_zero() As Boolean
        Return _labor_sold_calc = 0 AndAlso _mat_sold_calc = 0 AndAlso _freight_sold_calc = 0 AndAlso _rep_sold_calc = 0 AndAlso _serv_sold_calc = 0
    End Function


    'string values
    Private _hours_sold_calc_str As String
    Public Property hours_sold_calc_str() As String
        Get
            Return _hours_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _hours_sold_calc_str Then
                _hours_sold_calc_str = value
            End If
        End Set
    End Property

    Private _labor_sold_calc_str As String
    Public Property labor_sold_calc_str() As String
        Get
            Return _labor_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _labor_sold_calc_str Then
                _labor_sold_calc_str = value
            End If
        End Set
    End Property

    Private _mat_sold_calc_str As String
    Public Property mat_sold_calc_str() As String
        Get
            Return _mat_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _mat_sold_calc_str Then
                _mat_sold_calc_str = value
            End If
        End Set
    End Property

    Private _rep_sold_calc_str As String
    Public Property rep_sold_calc_str() As String
        Get
            Return _rep_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _rep_sold_calc_str Then
                _rep_sold_calc_str = value
            End If
        End Set
    End Property

    Private _serv_sold_calc_str As String
    Public Property serv_sold_calc_str() As String
        Get
            Return _serv_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _serv_sold_calc_str Then
                _serv_sold_calc_str = value
            End If
        End Set
    End Property

    Private _freight_sold_calc_str As String
    Public Property freight_sold_calc_str() As String
        Get
            Return _freight_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _freight_sold_calc_str Then
                _freight_sold_calc_str = value
            End If
        End Set
    End Property

    Private _business_object As Object
    Public Property business_object() As Object
        Get
            Return _business_object
        End Get
        Set(ByVal value As Object)
            _business_object = value
        End Set
    End Property

End Class

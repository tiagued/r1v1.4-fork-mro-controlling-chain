﻿Imports System.ComponentModel
Imports System.Threading
Imports System.Timers
Imports Equin.ApplicationFramework

Public Class CRefreshCalculationBackGroundWorker
    Public Sub New()

    End Sub

    Public Sub init()
        Try
            _worker = New BackgroundWorker
            _worker.WorkerReportsProgress = True
            _worker.WorkerSupportsCancellation = True

            AddHandler _worker.DoWork, AddressOf DoWork
            AddHandler _worker.ProgressChanged, AddressOf ProgressChanged
            AddHandler _worker.RunWorkerCompleted, AddressOf RunWorkerCompleted

            _startAgain = False
        Catch ex As Exception
            Throw New Exception("An exception occured whine initializing calculation controller" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Private _startAgain As Boolean

    Private WithEvents _worker As BackgroundWorker
    Public ReadOnly Property worker() As BackgroundWorker
        Get
            Return _worker
        End Get
    End Property


    Public Sub startAsync()
        Try
            If _worker.IsBusy Then
                _startAgain = True
                Me.cancelAsync()
            Else
                ' Start the asynchronous operation.
                _worker.RunWorkerAsync()
            End If
        Catch ex As Exception
            'busy
        End Try
    End Sub

    Public Sub cancelAsync()
        If _worker.WorkerSupportsCancellation = True Then
            ' Cancel the asynchronous operation.
            _worker.CancelAsync()
        End If
    End Sub
    'same as dowork without progress
    Public Sub calculateInMainThread(updateProj As Boolean)
        Try

            'update activities labor and material
            For Each view As ObjectView(Of CScopeActivity) In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList
                view.Object.updateMatLabCal()
            Next
            For Each view As ObjectView(Of CAWQuotation) In MConstants.GLB_MSTR_CTRL.awq_controller.awq_list
                view.Object.updateCal()
            Next
            If updateProj Then
                'update proj
                MConstants.GLB_MSTR_CTRL.proj_calculator.update(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value, Nothing)
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while calculating project" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
    ' This event handler is where the time-consuming work is done.
    Public Sub DoWork(ByVal sender As System.Object, ByVal e As DoWorkEventArgs)
        Try

            Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)

            Dim countAct As Integer = MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.Count
            Dim countAWQ As Integer = MConstants.GLB_MSTR_CTRL.awq_controller.awq_list.Count / 4
            Dim countProjCalc As Integer = countAct / 4
            Dim countSum = countAct + countAWQ + countProjCalc
            Dim totalProgrAct As Integer = 0
            Dim totalProgrAWQ As Integer = 0
            Dim totalProgProjCalc As Integer = 0

            Dim progress_unit As Integer = 0
            Dim progress_unit_items As Integer = 0

            Dim progress As Integer = 0
            Dim count As Integer = 0
            If countSum = 0 Then
                worker.ReportProgress(100)
            Else
                totalProgrAct = 100 * countAct / countSum
                totalProgrAWQ = 100 * countAWQ / countSum
                totalProgProjCalc = 100 * countProjCalc / countSum
            End If
            progress = 0
            worker.ReportProgress(progress)

            MConstants.GLB_MSTR_CTRL.statusBar.calc_progress_bar_visible = True
            MConstants.GLB_MSTR_CTRL.statusBar.calc_status_label_visible = True

            progress_unit = totalProgrAct / 10
            progress_unit_items = countAct / 10
            'update activities labor and material
            For Each view As ObjectView(Of CScopeActivity) In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList
                count = count + 1
                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                    Exit Sub
                Else
                    view.Object.updateMatLabCal()
                    If (progress_unit_items <> 0 AndAlso progress_unit <> 0) AndAlso count Mod progress_unit_items = 0 Then
                        progress = progress + progress_unit
                        worker.ReportProgress(progress)
                    End If

                End If
            Next
            progress = totalProgrAct
            worker.ReportProgress(progress)

            progress_unit = totalProgrAWQ / 10
            progress_unit_items = countAWQ / 10
            count = 0
            For Each view As ObjectView(Of CAWQuotation) In MConstants.GLB_MSTR_CTRL.awq_controller.awq_list
                count = count + 1
                If (worker.CancellationPending = True) Then
                    e.Cancel = True
                    Exit Sub
                Else
                    view.Object.updateCal()
                    If (progress_unit_items <> 0 AndAlso progress_unit <> 0) AndAlso count Mod progress_unit_items = 0 Then
                        progress = progress + progress_unit
                        worker.ReportProgress(progress)
                    End If

                End If
            Next
            progress = totalProgrAWQ + totalProgrAct
            worker.ReportProgress(progress)

            progress = totalProgrAWQ + totalProgrAct + totalProgProjCalc
            worker.ReportProgress(progress)

            worker.ReportProgress(100)

        Catch ex As Exception
            Throw New Exception("An error occured while calculating project" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    ' This event handler updates the progress.
    Private Sub ProgressChanged(ByVal sender As System.Object, ByVal e As ProgressChangedEventArgs)
        If e.ProgressPercentage > 100 Then
            Exit Sub
        End If
        MConstants.GLB_MSTR_CTRL.statusBar.calc_progress_bar_value = e.ProgressPercentage
    End Sub

    ' This event handler deals with the results of the background operation.
    Private Sub RunWorkerCompleted(ByVal sender As System.Object, ByVal e As RunWorkerCompletedEventArgs)
        If e.Cancelled = True Then
            If _startAgain Then
                _startAgain = False
                Me.startAsync()
            End If
        ElseIf e.Error IsNot Nothing Then
        Else
        End If
        MConstants.GLB_MSTR_CTRL.statusBar.calc_progress_bar_visible = False
        MConstants.GLB_MSTR_CTRL.statusBar.calc_status_label_visible = False
    End Sub

End Class

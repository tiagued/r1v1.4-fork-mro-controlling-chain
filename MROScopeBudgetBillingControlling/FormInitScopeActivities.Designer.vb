﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormInitScopeActivities
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.top_panel_product = New System.Windows.Forms.Panel()
        Me.product_info_grp = New System.Windows.Forms.GroupBox()
        Me.product_details_lay = New System.Windows.Forms.TableLayoutPanel()
        Me.product_freight_lbl = New System.Windows.Forms.Label()
        Me.product_freight_val = New System.Windows.Forms.Label()
        Me.product_third_p_lbl = New System.Windows.Forms.Label()
        Me.product_third_p_val = New System.Windows.Forms.Label()
        Me.product_repair_lbl = New System.Windows.Forms.Label()
        Me.product_repair_val = New System.Windows.Forms.Label()
        Me.product_mat_lbl = New System.Windows.Forms.Label()
        Me.product_mat_val = New System.Windows.Forms.Label()
        Me.product_labor_lbl = New System.Windows.Forms.Label()
        Me.product_labor_val = New System.Windows.Forms.Label()
        Me.product_number_val = New System.Windows.Forms.Label()
        Me.product_number_lbl = New System.Windows.Forms.Label()
        Me.product_quote_lbl = New System.Windows.Forms.Label()
        Me.product_quote_val = New System.Windows.Forms.Label()
        Me.select_product_grp = New System.Windows.Forms.GroupBox()
        Me.product_select_right_pic = New System.Windows.Forms.PictureBox()
        Me.product_select_left_pic = New System.Windows.Forms.PictureBox()
        Me.cb_product = New System.Windows.Forms.ComboBox()
        Me.top_panel_dependant_act = New System.Windows.Forms.Panel()
        Me.select_dependant_act_grp = New System.Windows.Forms.GroupBox()
        Me.filter_not_mapped_val = New System.Windows.Forms.CheckBox()
        Me.filter_not_mapped_lbl = New System.Windows.Forms.Label()
        Me.select_main_act_cb = New System.Windows.Forms.ComboBox()
        Me.main_act_lbl = New System.Windows.Forms.Label()
        Me.select_act_right = New System.Windows.Forms.PictureBox()
        Me.select_act_left = New System.Windows.Forms.PictureBox()
        Me.act_dep_all_listb = New System.Windows.Forms.ListBox()
        Me.act_dep_selected_listb = New System.Windows.Forms.ListBox()
        Me.main_panel_set_hours = New System.Windows.Forms.Panel()
        Me.tab_control_act_hours = New System.Windows.Forms.TabControl()
        Me.init_scope_page = New System.Windows.Forms.TabPage()
        Me.init_scope_hours_dgrid = New System.Windows.Forms.DataGridView()
        Me.add_mat_page = New System.Windows.Forms.TabPage()
        Me.add_scope_mat_dgrid = New System.Windows.Forms.DataGridView()
        Me.init_mat_page = New System.Windows.Forms.TabPage()
        Me.init_scope_mat_dgrid = New System.Windows.Forms.DataGridView()
        Me.add_scope_page = New System.Windows.Forms.TabPage()
        Me.add_scope_hrs_dgrid = New System.Windows.Forms.DataGridView()
        Me.main_scrollable_init_panel = New System.Windows.Forms.Panel()
        Me.top_panel_product.SuspendLayout()
        Me.product_info_grp.SuspendLayout()
        Me.product_details_lay.SuspendLayout()
        Me.select_product_grp.SuspendLayout()
        CType(Me.product_select_right_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.product_select_left_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.top_panel_dependant_act.SuspendLayout()
        Me.select_dependant_act_grp.SuspendLayout()
        CType(Me.select_act_right, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.select_act_left, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.main_panel_set_hours.SuspendLayout()
        Me.tab_control_act_hours.SuspendLayout()
        Me.init_scope_page.SuspendLayout()
        CType(Me.init_scope_hours_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.add_mat_page.SuspendLayout()
        CType(Me.add_scope_mat_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.init_mat_page.SuspendLayout()
        CType(Me.init_scope_mat_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.add_scope_page.SuspendLayout()
        CType(Me.add_scope_hrs_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.main_scrollable_init_panel.SuspendLayout()
        Me.SuspendLayout()
        '
        'top_panel_product
        '
        Me.top_panel_product.Controls.Add(Me.product_info_grp)
        Me.top_panel_product.Controls.Add(Me.select_product_grp)
        Me.top_panel_product.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel_product.Location = New System.Drawing.Point(4, 5)
        Me.top_panel_product.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_panel_product.Name = "top_panel_product"
        Me.top_panel_product.Size = New System.Drawing.Size(2102, 195)
        Me.top_panel_product.TabIndex = 0
        '
        'product_info_grp
        '
        Me.product_info_grp.Controls.Add(Me.product_details_lay)
        Me.product_info_grp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.product_info_grp.Location = New System.Drawing.Point(0, 77)
        Me.product_info_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.product_info_grp.Name = "product_info_grp"
        Me.product_info_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.product_info_grp.Size = New System.Drawing.Size(2102, 118)
        Me.product_info_grp.TabIndex = 1
        Me.product_info_grp.TabStop = False
        Me.product_info_grp.Text = "Product Details"
        '
        'product_details_lay
        '
        Me.product_details_lay.ColumnCount = 10
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.613048!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.88561!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.36193!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.58356!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.736372!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.67292!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.16622!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.31546!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.540661!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.94549!))
        Me.product_details_lay.Controls.Add(Me.product_freight_lbl, 2, 1)
        Me.product_details_lay.Controls.Add(Me.product_freight_val, 3, 1)
        Me.product_details_lay.Controls.Add(Me.product_third_p_lbl, 0, 1)
        Me.product_details_lay.Controls.Add(Me.product_third_p_val, 1, 1)
        Me.product_details_lay.Controls.Add(Me.product_repair_lbl, 8, 0)
        Me.product_details_lay.Controls.Add(Me.product_repair_val, 9, 0)
        Me.product_details_lay.Controls.Add(Me.product_mat_lbl, 6, 0)
        Me.product_details_lay.Controls.Add(Me.product_mat_val, 7, 0)
        Me.product_details_lay.Controls.Add(Me.product_labor_lbl, 4, 0)
        Me.product_details_lay.Controls.Add(Me.product_labor_val, 5, 0)
        Me.product_details_lay.Controls.Add(Me.product_number_val, 1, 0)
        Me.product_details_lay.Controls.Add(Me.product_number_lbl, 0, 0)
        Me.product_details_lay.Controls.Add(Me.product_quote_lbl, 2, 0)
        Me.product_details_lay.Controls.Add(Me.product_quote_val, 3, 0)
        Me.product_details_lay.Dock = System.Windows.Forms.DockStyle.Fill
        Me.product_details_lay.Location = New System.Drawing.Point(4, 24)
        Me.product_details_lay.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.product_details_lay.Name = "product_details_lay"
        Me.product_details_lay.RowCount = 2
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.27586!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.72414!))
        Me.product_details_lay.Size = New System.Drawing.Size(2094, 89)
        Me.product_details_lay.TabIndex = 0
        '
        'product_freight_lbl
        '
        Me.product_freight_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_freight_lbl.AutoSize = True
        Me.product_freight_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.product_freight_lbl.Location = New System.Drawing.Point(391, 55)
        Me.product_freight_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_freight_lbl.Name = "product_freight_lbl"
        Me.product_freight_lbl.Size = New System.Drawing.Size(80, 20)
        Me.product_freight_lbl.TabIndex = 12
        Me.product_freight_lbl.Text = "Freight :"
        '
        'product_freight_val
        '
        Me.product_freight_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_freight_val.AutoSize = True
        Me.product_freight_val.Location = New System.Drawing.Point(503, 55)
        Me.product_freight_val.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_freight_val.Name = "product_freight_val"
        Me.product_freight_val.Size = New System.Drawing.Size(28, 20)
        Me.product_freight_val.TabIndex = 13
        Me.product_freight_val.Text = "val"
        '
        'product_third_p_lbl
        '
        Me.product_third_p_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_third_p_lbl.AutoSize = True
        Me.product_third_p_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.product_third_p_lbl.Location = New System.Drawing.Point(4, 55)
        Me.product_third_p_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_third_p_lbl.Name = "product_third_p_lbl"
        Me.product_third_p_lbl.Size = New System.Drawing.Size(98, 20)
        Me.product_third_p_lbl.TabIndex = 10
        Me.product_third_p_lbl.Text = "3rd Party :"
        '
        'product_third_p_val
        '
        Me.product_third_p_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_third_p_val.AutoSize = True
        Me.product_third_p_val.Location = New System.Drawing.Point(142, 55)
        Me.product_third_p_val.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_third_p_val.Name = "product_third_p_val"
        Me.product_third_p_val.Size = New System.Drawing.Size(28, 20)
        Me.product_third_p_val.TabIndex = 11
        Me.product_third_p_val.Text = "val"
        '
        'product_repair_lbl
        '
        Me.product_repair_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_repair_lbl.AutoSize = True
        Me.product_repair_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.product_repair_lbl.Location = New System.Drawing.Point(1580, 11)
        Me.product_repair_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_repair_lbl.Name = "product_repair_lbl"
        Me.product_repair_lbl.Size = New System.Drawing.Size(86, 20)
        Me.product_repair_lbl.TabIndex = 8
        Me.product_repair_lbl.Text = "S. Mat.  :"
        '
        'product_repair_val
        '
        Me.product_repair_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_repair_val.AutoSize = True
        Me.product_repair_val.Location = New System.Drawing.Point(1696, 11)
        Me.product_repair_val.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_repair_val.Name = "product_repair_val"
        Me.product_repair_val.Size = New System.Drawing.Size(28, 20)
        Me.product_repair_val.TabIndex = 9
        Me.product_repair_val.Text = "val"
        '
        'product_mat_lbl
        '
        Me.product_mat_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_mat_lbl.AutoSize = True
        Me.product_mat_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.product_mat_lbl.Location = New System.Drawing.Point(1172, 11)
        Me.product_mat_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_mat_lbl.Name = "product_mat_lbl"
        Me.product_mat_lbl.Size = New System.Drawing.Size(89, 20)
        Me.product_mat_lbl.TabIndex = 6
        Me.product_mat_lbl.Text = "Material :"
        '
        'product_mat_val
        '
        Me.product_mat_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_mat_val.AutoSize = True
        Me.product_mat_val.Location = New System.Drawing.Point(1301, 11)
        Me.product_mat_val.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_mat_val.Name = "product_mat_val"
        Me.product_mat_val.Size = New System.Drawing.Size(28, 20)
        Me.product_mat_val.TabIndex = 7
        Me.product_mat_val.Text = "val"
        '
        'product_labor_lbl
        '
        Me.product_labor_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_labor_lbl.AutoSize = True
        Me.product_labor_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.product_labor_lbl.Location = New System.Drawing.Point(787, 11)
        Me.product_labor_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_labor_lbl.Name = "product_labor_lbl"
        Me.product_labor_lbl.Size = New System.Drawing.Size(69, 20)
        Me.product_labor_lbl.TabIndex = 4
        Me.product_labor_lbl.Text = "Labor :"
        '
        'product_labor_val
        '
        Me.product_labor_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_labor_val.AutoSize = True
        Me.product_labor_val.Location = New System.Drawing.Point(886, 11)
        Me.product_labor_val.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_labor_val.Name = "product_labor_val"
        Me.product_labor_val.Size = New System.Drawing.Size(28, 20)
        Me.product_labor_val.TabIndex = 5
        Me.product_labor_val.Text = "val"
        '
        'product_number_val
        '
        Me.product_number_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_number_val.AutoSize = True
        Me.product_number_val.Location = New System.Drawing.Point(142, 11)
        Me.product_number_val.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_number_val.Name = "product_number_val"
        Me.product_number_val.Size = New System.Drawing.Size(28, 20)
        Me.product_number_val.TabIndex = 1
        Me.product_number_val.Text = "val"
        '
        'product_number_lbl
        '
        Me.product_number_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_number_lbl.AutoSize = True
        Me.product_number_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.product_number_lbl.Location = New System.Drawing.Point(4, 11)
        Me.product_number_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_number_lbl.Name = "product_number_lbl"
        Me.product_number_lbl.Size = New System.Drawing.Size(86, 20)
        Me.product_number_lbl.TabIndex = 0
        Me.product_number_lbl.Text = "Product :"
        '
        'product_quote_lbl
        '
        Me.product_quote_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_quote_lbl.AutoSize = True
        Me.product_quote_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.product_quote_lbl.Location = New System.Drawing.Point(391, 11)
        Me.product_quote_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_quote_lbl.Name = "product_quote_lbl"
        Me.product_quote_lbl.Size = New System.Drawing.Size(71, 20)
        Me.product_quote_lbl.TabIndex = 2
        Me.product_quote_lbl.Text = "Quote :"
        '
        'product_quote_val
        '
        Me.product_quote_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.product_quote_val.AutoSize = True
        Me.product_quote_val.Location = New System.Drawing.Point(503, 11)
        Me.product_quote_val.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.product_quote_val.Name = "product_quote_val"
        Me.product_quote_val.Size = New System.Drawing.Size(28, 20)
        Me.product_quote_val.TabIndex = 3
        Me.product_quote_val.Text = "val"
        '
        'select_product_grp
        '
        Me.select_product_grp.Controls.Add(Me.product_select_right_pic)
        Me.select_product_grp.Controls.Add(Me.product_select_left_pic)
        Me.select_product_grp.Controls.Add(Me.cb_product)
        Me.select_product_grp.Dock = System.Windows.Forms.DockStyle.Top
        Me.select_product_grp.Location = New System.Drawing.Point(0, 0)
        Me.select_product_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_product_grp.Name = "select_product_grp"
        Me.select_product_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_product_grp.Size = New System.Drawing.Size(2102, 77)
        Me.select_product_grp.TabIndex = 0
        Me.select_product_grp.TabStop = False
        Me.select_product_grp.Text = "Select Product"
        '
        'product_select_right_pic
        '
        Me.product_select_right_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.product_select_right_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.product_select_right_pic.Location = New System.Drawing.Point(844, 18)
        Me.product_select_right_pic.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.product_select_right_pic.Name = "product_select_right_pic"
        Me.product_select_right_pic.Size = New System.Drawing.Size(54, 49)
        Me.product_select_right_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.product_select_right_pic.TabIndex = 2
        Me.product_select_right_pic.TabStop = False
        '
        'product_select_left_pic
        '
        Me.product_select_left_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.product_select_left_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.product_select_left_pic.Location = New System.Drawing.Point(274, 18)
        Me.product_select_left_pic.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.product_select_left_pic.Name = "product_select_left_pic"
        Me.product_select_left_pic.Size = New System.Drawing.Size(54, 49)
        Me.product_select_left_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.product_select_left_pic.TabIndex = 1
        Me.product_select_left_pic.TabStop = False
        '
        'cb_product
        '
        Me.cb_product.FormattingEnabled = True
        Me.cb_product.Location = New System.Drawing.Point(336, 29)
        Me.cb_product.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_product.Name = "cb_product"
        Me.cb_product.Size = New System.Drawing.Size(500, 28)
        Me.cb_product.TabIndex = 0
        '
        'top_panel_dependant_act
        '
        Me.top_panel_dependant_act.AutoScroll = True
        Me.top_panel_dependant_act.Controls.Add(Me.select_dependant_act_grp)
        Me.top_panel_dependant_act.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel_dependant_act.Location = New System.Drawing.Point(0, 0)
        Me.top_panel_dependant_act.Margin = New System.Windows.Forms.Padding(4, 5, 4, 31)
        Me.top_panel_dependant_act.Name = "top_panel_dependant_act"
        Me.top_panel_dependant_act.Size = New System.Drawing.Size(2106, 545)
        Me.top_panel_dependant_act.TabIndex = 2
        '
        'select_dependant_act_grp
        '
        Me.select_dependant_act_grp.Controls.Add(Me.filter_not_mapped_val)
        Me.select_dependant_act_grp.Controls.Add(Me.filter_not_mapped_lbl)
        Me.select_dependant_act_grp.Controls.Add(Me.select_main_act_cb)
        Me.select_dependant_act_grp.Controls.Add(Me.main_act_lbl)
        Me.select_dependant_act_grp.Controls.Add(Me.select_act_right)
        Me.select_dependant_act_grp.Controls.Add(Me.select_act_left)
        Me.select_dependant_act_grp.Controls.Add(Me.act_dep_all_listb)
        Me.select_dependant_act_grp.Controls.Add(Me.act_dep_selected_listb)
        Me.select_dependant_act_grp.Dock = System.Windows.Forms.DockStyle.Top
        Me.select_dependant_act_grp.Location = New System.Drawing.Point(0, 0)
        Me.select_dependant_act_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_dependant_act_grp.Name = "select_dependant_act_grp"
        Me.select_dependant_act_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_dependant_act_grp.Size = New System.Drawing.Size(2106, 538)
        Me.select_dependant_act_grp.TabIndex = 0
        Me.select_dependant_act_grp.TabStop = False
        Me.select_dependant_act_grp.Text = "Select Dependant Activities"
        '
        'filter_not_mapped_val
        '
        Me.filter_not_mapped_val.AutoSize = True
        Me.filter_not_mapped_val.Location = New System.Drawing.Point(210, 23)
        Me.filter_not_mapped_val.Name = "filter_not_mapped_val"
        Me.filter_not_mapped_val.Size = New System.Drawing.Size(22, 21)
        Me.filter_not_mapped_val.TabIndex = 11
        Me.filter_not_mapped_val.UseVisualStyleBackColor = True
        '
        'filter_not_mapped_lbl
        '
        Me.filter_not_mapped_lbl.AutoSize = True
        Me.filter_not_mapped_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.filter_not_mapped_lbl.Location = New System.Drawing.Point(9, 25)
        Me.filter_not_mapped_lbl.Name = "filter_not_mapped_lbl"
        Me.filter_not_mapped_lbl.Size = New System.Drawing.Size(195, 15)
        Me.filter_not_mapped_lbl.TabIndex = 10
        Me.filter_not_mapped_lbl.Text = "Show Only Activities Not Mapped ?"
        '
        'select_main_act_cb
        '
        Me.select_main_act_cb.FormattingEnabled = True
        Me.select_main_act_cb.Location = New System.Drawing.Point(1621, 114)
        Me.select_main_act_cb.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_main_act_cb.Name = "select_main_act_cb"
        Me.select_main_act_cb.Size = New System.Drawing.Size(456, 28)
        Me.select_main_act_cb.TabIndex = 0
        '
        'main_act_lbl
        '
        Me.main_act_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.main_act_lbl.AutoSize = True
        Me.main_act_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.main_act_lbl.Location = New System.Drawing.Point(1756, 58)
        Me.main_act_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.main_act_lbl.Name = "main_act_lbl"
        Me.main_act_lbl.Size = New System.Drawing.Size(135, 20)
        Me.main_act_lbl.TabIndex = 9
        Me.main_act_lbl.Text = "Main Activity  :"
        '
        'select_act_right
        '
        Me.select_act_right.Cursor = System.Windows.Forms.Cursors.Hand
        Me.select_act_right.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.select_act_right.Location = New System.Drawing.Point(768, 169)
        Me.select_act_right.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_act_right.Name = "select_act_right"
        Me.select_act_right.Size = New System.Drawing.Size(54, 49)
        Me.select_act_right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.select_act_right.TabIndex = 4
        Me.select_act_right.TabStop = False
        '
        'select_act_left
        '
        Me.select_act_left.Cursor = System.Windows.Forms.Cursors.Hand
        Me.select_act_left.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.select_act_left.Location = New System.Drawing.Point(768, 263)
        Me.select_act_left.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.select_act_left.Name = "select_act_left"
        Me.select_act_left.Size = New System.Drawing.Size(54, 49)
        Me.select_act_left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.select_act_left.TabIndex = 3
        Me.select_act_left.TabStop = False
        '
        'act_dep_all_listb
        '
        Me.act_dep_all_listb.FormattingEnabled = True
        Me.act_dep_all_listb.ItemHeight = 20
        Me.act_dep_all_listb.Location = New System.Drawing.Point(4, 45)
        Me.act_dep_all_listb.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.act_dep_all_listb.Name = "act_dep_all_listb"
        Me.act_dep_all_listb.Size = New System.Drawing.Size(718, 484)
        Me.act_dep_all_listb.TabIndex = 1
        '
        'act_dep_selected_listb
        '
        Me.act_dep_selected_listb.FormattingEnabled = True
        Me.act_dep_selected_listb.ItemHeight = 20
        Me.act_dep_selected_listb.Location = New System.Drawing.Point(844, 45)
        Me.act_dep_selected_listb.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.act_dep_selected_listb.Name = "act_dep_selected_listb"
        Me.act_dep_selected_listb.Size = New System.Drawing.Size(727, 484)
        Me.act_dep_selected_listb.TabIndex = 0
        '
        'main_panel_set_hours
        '
        Me.main_panel_set_hours.Controls.Add(Me.tab_control_act_hours)
        Me.main_panel_set_hours.Dock = System.Windows.Forms.DockStyle.Fill
        Me.main_panel_set_hours.Location = New System.Drawing.Point(0, 545)
        Me.main_panel_set_hours.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.main_panel_set_hours.Name = "main_panel_set_hours"
        Me.main_panel_set_hours.Size = New System.Drawing.Size(2106, 537)
        Me.main_panel_set_hours.TabIndex = 3
        '
        'tab_control_act_hours
        '
        Me.tab_control_act_hours.Controls.Add(Me.init_scope_page)
        Me.tab_control_act_hours.Controls.Add(Me.init_mat_page)
        Me.tab_control_act_hours.Controls.Add(Me.add_scope_page)
        Me.tab_control_act_hours.Controls.Add(Me.add_mat_page)
        Me.tab_control_act_hours.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab_control_act_hours.Location = New System.Drawing.Point(0, 0)
        Me.tab_control_act_hours.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_control_act_hours.Name = "tab_control_act_hours"
        Me.tab_control_act_hours.SelectedIndex = 0
        Me.tab_control_act_hours.Size = New System.Drawing.Size(2106, 537)
        Me.tab_control_act_hours.TabIndex = 0
        '
        'init_scope_page
        '
        Me.init_scope_page.Controls.Add(Me.init_scope_hours_dgrid)
        Me.init_scope_page.Location = New System.Drawing.Point(4, 29)
        Me.init_scope_page.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_page.Name = "init_scope_page"
        Me.init_scope_page.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_page.Size = New System.Drawing.Size(2098, 504)
        Me.init_scope_page.TabIndex = 0
        Me.init_scope_page.Text = "Initial Labor"
        Me.init_scope_page.UseVisualStyleBackColor = True
        '
        'init_scope_hours_dgrid
        '
        Me.init_scope_hours_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.init_scope_hours_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.init_scope_hours_dgrid.Location = New System.Drawing.Point(4, 5)
        Me.init_scope_hours_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_hours_dgrid.Name = "init_scope_hours_dgrid"
        Me.init_scope_hours_dgrid.Size = New System.Drawing.Size(2090, 494)
        Me.init_scope_hours_dgrid.TabIndex = 0
        '
        'add_mat_page
        '
        Me.add_mat_page.Controls.Add(Me.add_scope_mat_dgrid)
        Me.add_mat_page.Location = New System.Drawing.Point(4, 29)
        Me.add_mat_page.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_mat_page.Name = "add_mat_page"
        Me.add_mat_page.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_mat_page.Size = New System.Drawing.Size(2098, 504)
        Me.add_mat_page.TabIndex = 3
        Me.add_mat_page.Text = "Additional Materials"
        Me.add_mat_page.UseVisualStyleBackColor = True
        '
        'add_scope_mat_dgrid
        '
        Me.add_scope_mat_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.add_scope_mat_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.add_scope_mat_dgrid.Location = New System.Drawing.Point(4, 5)
        Me.add_scope_mat_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_scope_mat_dgrid.Name = "add_scope_mat_dgrid"
        Me.add_scope_mat_dgrid.Size = New System.Drawing.Size(2090, 494)
        Me.add_scope_mat_dgrid.TabIndex = 0
        '
        'init_mat_page
        '
        Me.init_mat_page.Controls.Add(Me.init_scope_mat_dgrid)
        Me.init_mat_page.Location = New System.Drawing.Point(4, 29)
        Me.init_mat_page.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_mat_page.Name = "init_mat_page"
        Me.init_mat_page.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_mat_page.Size = New System.Drawing.Size(2098, 504)
        Me.init_mat_page.TabIndex = 2
        Me.init_mat_page.Text = "Init Materials"
        Me.init_mat_page.UseVisualStyleBackColor = True
        '
        'init_scope_mat_dgrid
        '
        Me.init_scope_mat_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.init_scope_mat_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.init_scope_mat_dgrid.Location = New System.Drawing.Point(4, 5)
        Me.init_scope_mat_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_mat_dgrid.Name = "init_scope_mat_dgrid"
        Me.init_scope_mat_dgrid.Size = New System.Drawing.Size(2090, 494)
        Me.init_scope_mat_dgrid.TabIndex = 0
        '
        'add_scope_page
        '
        Me.add_scope_page.Controls.Add(Me.add_scope_hrs_dgrid)
        Me.add_scope_page.Location = New System.Drawing.Point(4, 29)
        Me.add_scope_page.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_scope_page.Name = "add_scope_page"
        Me.add_scope_page.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_scope_page.Size = New System.Drawing.Size(2098, 504)
        Me.add_scope_page.TabIndex = 1
        Me.add_scope_page.Text = "Additional Labor"
        Me.add_scope_page.UseVisualStyleBackColor = True
        '
        'add_scope_hrs_dgrid
        '
        Me.add_scope_hrs_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.add_scope_hrs_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.add_scope_hrs_dgrid.Location = New System.Drawing.Point(4, 5)
        Me.add_scope_hrs_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_scope_hrs_dgrid.Name = "add_scope_hrs_dgrid"
        Me.add_scope_hrs_dgrid.Size = New System.Drawing.Size(2090, 494)
        Me.add_scope_hrs_dgrid.TabIndex = 0
        '
        'main_scrollable_init_panel
        '
        Me.main_scrollable_init_panel.AutoScroll = True
        Me.main_scrollable_init_panel.Controls.Add(Me.main_panel_set_hours)
        Me.main_scrollable_init_panel.Controls.Add(Me.top_panel_dependant_act)
        Me.main_scrollable_init_panel.Location = New System.Drawing.Point(4, 215)
        Me.main_scrollable_init_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.main_scrollable_init_panel.Name = "main_scrollable_init_panel"
        Me.main_scrollable_init_panel.Size = New System.Drawing.Size(2106, 1082)
        Me.main_scrollable_init_panel.TabIndex = 4
        '
        'FormInitScopeActivities
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1842, 1050)
        Me.Controls.Add(Me.main_scrollable_init_panel)
        Me.Controls.Add(Me.top_panel_product)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "FormInitScopeActivities"
        Me.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Text = "Set Initial Scope Activities"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.top_panel_product.ResumeLayout(False)
        Me.product_info_grp.ResumeLayout(False)
        Me.product_details_lay.ResumeLayout(False)
        Me.product_details_lay.PerformLayout()
        Me.select_product_grp.ResumeLayout(False)
        CType(Me.product_select_right_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.product_select_left_pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.top_panel_dependant_act.ResumeLayout(False)
        Me.select_dependant_act_grp.ResumeLayout(False)
        Me.select_dependant_act_grp.PerformLayout()
        CType(Me.select_act_right, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.select_act_left, System.ComponentModel.ISupportInitialize).EndInit()
        Me.main_panel_set_hours.ResumeLayout(False)
        Me.tab_control_act_hours.ResumeLayout(False)
        Me.init_scope_page.ResumeLayout(False)
        CType(Me.init_scope_hours_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.add_mat_page.ResumeLayout(False)
        CType(Me.add_scope_mat_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.init_mat_page.ResumeLayout(False)
        CType(Me.init_scope_mat_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.add_scope_page.ResumeLayout(False)
        CType(Me.add_scope_hrs_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.main_scrollable_init_panel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents top_panel_product As Panel
    Friend WithEvents select_product_grp As GroupBox
    Friend WithEvents cb_product As ComboBox
    Friend WithEvents product_info_grp As GroupBox
    Friend WithEvents product_select_right_pic As PictureBox
    Friend WithEvents product_select_left_pic As PictureBox
    Friend WithEvents product_details_lay As TableLayoutPanel
    Friend WithEvents product_freight_lbl As Label
    Friend WithEvents product_freight_val As Label
    Friend WithEvents product_third_p_lbl As Label
    Friend WithEvents product_third_p_val As Label
    Friend WithEvents product_repair_lbl As Label
    Friend WithEvents product_repair_val As Label
    Friend WithEvents product_mat_lbl As Label
    Friend WithEvents product_mat_val As Label
    Friend WithEvents product_labor_lbl As Label
    Friend WithEvents product_labor_val As Label
    Friend WithEvents product_number_val As Label
    Friend WithEvents product_number_lbl As Label
    Friend WithEvents product_quote_lbl As Label
    Friend WithEvents product_quote_val As Label
    Friend WithEvents top_panel_dependant_act As Panel
    Friend WithEvents select_dependant_act_grp As GroupBox
    Friend WithEvents select_main_act_cb As ComboBox
    Friend WithEvents act_dep_all_listb As ListBox
    Friend WithEvents act_dep_selected_listb As ListBox
    Friend WithEvents select_act_right As PictureBox
    Friend WithEvents select_act_left As PictureBox
    Friend WithEvents main_act_lbl As Label
    Friend WithEvents main_panel_set_hours As Panel
    Friend WithEvents tab_control_act_hours As TabControl
    Friend WithEvents init_scope_page As TabPage
    Friend WithEvents add_scope_page As TabPage
    Friend WithEvents init_scope_hours_dgrid As DataGridView
    Friend WithEvents add_scope_hrs_dgrid As DataGridView
    Friend WithEvents main_scrollable_init_panel As Panel
    Friend WithEvents filter_not_mapped_val As CheckBox
    Friend WithEvents filter_not_mapped_lbl As Label
    Friend WithEvents init_mat_page As TabPage
    Friend WithEvents init_scope_mat_dgrid As DataGridView
    Friend WithEvents add_mat_page As TabPage
    Friend WithEvents add_scope_mat_dgrid As DataGridView
End Class

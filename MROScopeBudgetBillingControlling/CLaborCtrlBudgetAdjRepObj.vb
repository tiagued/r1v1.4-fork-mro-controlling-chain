﻿
Public Class CLaborCtrlBudgetAdjRepObj

    Public Sub New(ctrl_o As CLaborControlMainViewObj)
        _proj = ctrl_o.act_obj.proj
        _activity = ctrl_o.act_obj.act
        _activity_desc = ctrl_o.act_obj.act_desc
        _notification = ctrl_o.act_obj.notification
        _cc = ctrl_o.cc_object.cc
        _cc_desc = ctrl_o.cc_object.friendly_description
    End Sub

    Private _proj As String
    Public Property proj() As String
        Get
            Return _proj
        End Get
        Set(ByVal value As String)
            If Not _proj = value Then
                _proj = value
            End If
        End Set
    End Property


    Private _activity As String
    Public Property activity() As String
        Get
            Return _activity
        End Get
        Set(ByVal value As String)
            If Not _activity = value Then
                _activity = value
            End If
        End Set
    End Property

    Private _activity_desc As String
    Public Property activity_desc() As String
        Get
            Return _activity_desc
        End Get
        Set(ByVal value As String)
            If Not _activity_desc = value Then
                _activity_desc = value
            End If
        End Set
    End Property

    Private _notification As String
    Public Property notification() As String
        Get
            Return _notification
        End Get
        Set(ByVal value As String)
            If Not _notification = value Then
                _notification = value
            End If
        End Set
    End Property

    Private _cc As String
    Public Property cc() As String
        Get
            Return _cc
        End Get
        Set(ByVal value As String)
            If Not _cc = value Then
                _cc = value
            End If
        End Set
    End Property

    Private _cc_desc As String
    Public Property cc_desc() As String
        Get
            Return _cc_desc
        End Get
        Set(ByVal value As String)
            If Not _cc_desc = value Then
                _cc_desc = value
            End If
        End Set
    End Property

    Private _dev_scope As String
    Public Property dev_scope() As String
        Get
            Return _dev_scope
        End Get
        Set(ByVal value As String)
            If Not _dev_scope = value Then
                _dev_scope = value
            End If
        End Set
    End Property

    Private _dev_type As String
    Public Property dev_type() As String
        Get
            Return _dev_type
        End Get
        Set(ByVal value As String)
            If Not _dev_type = value Then
                _dev_type = value
            End If
        End Set
    End Property

    Private _dev_desc As String
    Public Property dev_desc() As String
        Get
            Return _dev_desc
        End Get
        Set(ByVal value As String)
            If Not _dev_desc = value Then
                _dev_desc = value
            End If
        End Set
    End Property

    Private _dev_reason As String
    Public Property dev_reason() As String
        Get
            Return _dev_reason
        End Get
        Set(ByVal value As String)
            If Not _dev_reason = value Then
                _dev_reason = value
            End If
        End Set
    End Property

    Private _claimed_by As String
    Public Property claimed_by() As String
        Get
            Return _claimed_by
        End Get
        Set(ByVal value As String)
            If Not _claimed_by = value Then
                _claimed_by = value
            End If
        End Set
    End Property

    Private _approved_on As Date
    Public Property approved_on() As Date
        Get
            Return _approved_on
        End Get
        Set(ByVal value As Date)
            If Not _approved_on = value Then
                _approved_on = value
            End If
        End Set
    End Property

    Private _approved_by As String
    Public Property approved_by() As String
        Get
            Return _approved_by
        End Get
        Set(ByVal value As String)
            If Not _approved_by = value Then
                _approved_by = value
            End If
        End Set
    End Property

    Private _is_opening_eac As Boolean
    Public Property is_opening_eac() As Boolean
        Get
            Return _is_opening_eac
        End Get
        Set(ByVal value As Boolean)
            If Not _is_opening_eac = value Then
                _is_opening_eac = value
            End If
        End Set
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            If Not _qty = value Then
                _qty = value
            End If
        End Set
    End Property

End Class

﻿
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CSurcharge
    Implements IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not _is_editable Then
            res = False
            mess = "Cannot be modified"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = "It is not allowed to delete " & _surcharge_type & " objects"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_SURCHARGE_DB_READ
    End Function

    Public Shared Function getEmptyGUM() As CSurcharge
        Dim gum As New CSurcharge
        gum.label = "No GUM"
        Return gum
    End Function

    Public Shared Function getEmptyECO() As CSurcharge
        Dim eco As New CSurcharge
        eco.label = "No ECO Tax"
        Return eco
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property
    'use for view
    Public ReadOnly Property label_view() As String
        Get
            Return If(surcharge_type.Equals(MConstants.constantConf(MConstants.CONST_CONF_SURCHARGE_TYPE_GUM).value), MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_GUM)(_label).value, MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_ECOTAX)(_label).value)
        End Get
    End Property

    Private _surcharge_type As String
    Public Property surcharge_type() As String
        Get
            Return _surcharge_type
        End Get
        Set(ByVal value As String)
            If Not _surcharge_type = value Then
                _surcharge_type = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _rate As Double
    Public Property rate() As Double
        Get
            Return _rate
        End Get
        Set(ByVal value As Double)
            If Not _rate = value Then
                _rate = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cap_value As Double
    Public Property cap_value() As Double
        Get
            Return _cap_value
        End Get
        Set(ByVal value As Double)
            If Not _cap_value = value Then
                _cap_value = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _curr As String
    Public Property curr() As String
        Get
            Return _curr
        End Get
        Set(ByVal value As String)
            If Not _curr = value Then
                _curr = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_editable As Boolean
    Public Property is_editable() As Boolean
        Get
            Return _is_editable
        End Get
        Set(ByVal value As Boolean)
            If Not _is_editable = value Then
                _is_editable = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Function getSurchargeValue(gross_value As Double, curr As String) As Double
        'calculate discount
        Dim res As Double
        res = Math.Min(MCalc.toCurr(_cap_value, _curr, curr), gross_value * Me.rate)
        Return res
    End Function

    'to string
    Public Overrides Function tostring() As String
        Return "label: " & _label & ", rate: " & _rate & ",  cap: " & _cap_value & " " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_curr).value
    End Function

End Class

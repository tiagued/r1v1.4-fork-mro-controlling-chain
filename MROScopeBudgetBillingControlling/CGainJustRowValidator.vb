﻿Public Class CGainJustRowValidator
    Public Shared instance As CGainJustRowValidator
    Public Shared Function getInstance() As CGainJustRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CGainJustRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(devJ As CDevJust)
        devJ.calc_is_in_error_state = False
    End Sub

    'validate hour just
    Public Function qty(devJ As CDevJust) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If devJ.qty <= 0 Then
            err = "Hour(s) quantity must be greather than 0"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate reason
    Public Function reason(devJ As CDevJust) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(devJ.reason) OrElse devJ.reason = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Reason shall not be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'approved by
    Public Function approved_by(devJ As CDevJust) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(devJ.approved_by) OrElse devJ.approved_by = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Approver shall not be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function


    'description
    Public Function description(devJ As CDevJust) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(devJ.description) OrElse devJ.description = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Description shall not be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'approved on
    Public Function approved_on(devJ As CDevJust) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If devJ.approved_on <= MConstants.APP_NULL_DATE Then
            err = "Approved on date cannot be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'scope
    Public Function scope(devJ As CDevJust) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(devJ.scope) OrElse devJ.scope = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Scope shall not be empty"
            res = False
        Else
            'If Not IsNothing(devJ.ctrl_obj) AndAlso Not devJ.ctrl_obj.dev_just_scope_list.Contains(devJ.scope) Then
            '    err = "Scope " & devJ.scope & " is not part of the cost list of the current object"
            '    res = False
            'End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class

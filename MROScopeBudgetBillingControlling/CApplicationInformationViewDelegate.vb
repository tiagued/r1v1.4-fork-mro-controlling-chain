﻿Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CApplicationInformationViewDelegate

    Private form As FormAdminAboutApp

    Public Sub New()

    End Sub

    Public Sub launch()
        form = New FormAdminAboutApp
        form.ctrl = Me
        loadInfoView()
        form.ShowDialog()
        form.BringToFront()
    End Sub

    Public Sub loadInfoView()
        Try
            Dim sys_viewModel As CViewModelMapList = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_SYSTEM_DATA)
            'read db entries
            Dim listO As List(Of Object) = GLB_MSTR_CTRL.csDB.performSelectObject(sys_viewModel.getDefaultSelect, sys_viewModel, True)

            'bind view
            'get lov map view
            Dim mapLOVEdit As CViewModelMapList
            mapLOVEdit = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_APPLICATION_INFO_VIEW)

            form.admin_about_appli_dgrid.AllowUserToAddRows = False
            form.admin_about_appli_dgrid.AutoGenerateColumns = False

            MViewEventHandler.addDefaultDatagridEventHandler(form.admin_about_appli_dgrid)

            For Each map As CViewModelMap In mapLOVEdit.list.Values
                Dim dgcol As New DataGridViewTextBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                If Not String.IsNullOrWhiteSpace(map.col_format) Then
                    dgcol.DefaultCellStyle.Format = map.col_format
                    Dim propInf As PropertyInfo
                    propInf = mapLOVEdit.object_class.GetProperty(map.class_field)
                    'format as numeric only for double
                    If propInf.PropertyType.Name = "Double" Then
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                form.admin_about_appli_dgrid.Columns.Add(dgcol)
            Next

            Dim listSys As New List(Of CConfProperty)
            For Each obj As CConfProperty In listO
                listSys.Add(obj)
            Next

            'bind datasource
            form.admin_about_appli_dgrid.DataSource = New BindingListView(Of CConfProperty)(listSys)
            MGeneralFuntionsViewControl.resizeDataGridWidth(form.admin_about_appli_dgrid)

        Catch ex As Exception
            MsgBox("An error occured while loading information about the application view " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub resetSession()
        Try
            MConstants.GLB_MSTR_CTRL.sys_data_ctrl.db_log_reset_session_data()
            MsgBox("Reset Done !!")
        Catch ex As Exception
            MsgBox("An error occured while reset session data " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class

﻿Public Class CImportCostCollectorDelegate
    Private Shared instance As CImportCostCollectorDelegate

    Public Shared Function getInstance() As CImportCostCollectorDelegate
        If IsNothing(instance) Then
            instance = New CImportCostCollectorDelegate
        End If
        Return instance
    End Function
    'list of items
    Private proj_list As List(Of String)
    Private actList As Dictionary(Of String, CImportScopeObject)

    Private cnt_zimro_lines As Long
    Private cnt_zimro_duplicated_lines As Long
    Private cnt_added As Long
    Private cnt_updated As Long
    Private cnt_deleted As Long
    Private cnt_constant As Long
    Private cnt_initial As Long

    Dim progressForm As FormProgressBar

    'log message
    Delegate Sub safeCallDelegate(message As String)
    Private Sub appendLogMessage(message As String)
        Try
            If progressForm.final_message_txt.InvokeRequired Then
                Dim deleg As New safeCallDelegate(AddressOf appendLogMessage)
                progressForm.final_message_txt.Invoke(deleg, New Object() {message})
            Else
                progressForm.final_message_txt.AppendText(message & Environment.NewLine & Environment.NewLine)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub launchScopeImport()
        Dim dialogRes As DialogResult
        Dim filepath As String = ""
        cnt_zimro_lines = 0
        cnt_zimro_duplicated_lines = 0
        cnt_added = 0
        cnt_updated = 0
        cnt_deleted = 0
        cnt_constant = 0
        Try

            GLB_MSTR_CTRL.statusBar.status_strip_main_label = "Import SAP CN47N..."
            'open file dialog
            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Select CN47N file..."
            GLB_MSTR_CTRL.mainForm.open_file_dialog.AddExtension = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckFileExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckPathExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Multiselect = False
            GLB_MSTR_CTRL.mainForm.open_file_dialog.DereferenceLinks = True
            'filter file types
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Filter = "Excel Documents|*" & MConstants.constantConf(MConstants.CONST_CONF_CN47N_IMP_FILE_EXT).value

            dialogRes = GLB_MSTR_CTRL.mainForm.open_file_dialog.ShowDialog()

            If dialogRes = DialogResult.OK Then
                'check file extention
                filepath = GLB_MSTR_CTRL.mainForm.open_file_dialog.FileName
                'if file is mhtml
                If Not filepath.EndsWith(MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_EXT).value, StringComparison.OrdinalIgnoreCase) Then
                    MsgBox("The file you selected is not a " & MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_EXT).value & " file", MsgBoxStyle.Critical, "Wrong file type")
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("An error occured when intializing the import " & Chr(10) & ex.Message, MsgBoxStyle.Critical, "Error when picking CN47N file")
            Exit Sub
        End Try

        Dim xlHandler As CDBHandler = Nothing
        Try
            'new form
            progressForm = New FormProgressBar

            Dim conn As String
            Dim listO As List(Of Object)
            Dim warnMess As String = ""
            'block calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = True

            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Load CN47N file..."
            'get file and query data
            conn = MConstants.constantConf(MConstants.CONST_CONF_CN47N_IMP_FILE_CONN_STR).value.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, filepath)
            xlHandler = New CDBHandler(conn, filepath)
            listO = xlHandler.performSelectObject(GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_COST_COLLECTOR_IMPORT).getExcelDefaultSelect, GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_COST_COLLECTOR_IMPORT), True)

            'check data consistency
            Dim checkResult As KeyValuePair(Of Boolean, String)
            checkResult = checkDataConsistency(listO)
            If checkResult.Key Then
                MsgBox(checkResult.Value, MsgBoxStyle.Critical, "Cannot load data from the file")
                Exit Sub
            End If

            'if ok, continue import
            loadData()

            'save in db
            Dim arg_save As CSaveWorkerArgs
            arg_save = CSaveWorkerArgs.getInsertAll
            arg_save.progBar = progressForm.progress_bar_new
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)
            'updated items.
            arg_save = CSaveWorkerArgs.getUpdateDeleteAll
            arg_save.progBar = progressForm.progress_bar_update
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)

            appendLogMessage("CN47N Import successfully done !" & Chr(10) & Chr(10))
            appendLogMessage("CN47N File_ Activities=" & cnt_zimro_lines)
            appendLogMessage("Initial_______ Activities=" & cnt_initial)
            appendLogMessage("Added______ Activities=" & cnt_added)
            appendLogMessage("Updated____ Activities=" & cnt_updated)
            appendLogMessage("Unchanged_ Activities=" & cnt_constant)
            appendLogMessage("Not in SAP__ Activities=" & (cnt_initial - cnt_updated - cnt_constant))

            progressForm.ShowDialog()
            progressForm.BringToFront()

            'refresh view
            GLB_MSTR_CTRL.add_scope_controller.scopeList.Refresh()
        Catch ex As Exception
            MsgBox("An error occured when reading data from CN47N file. The file might not be in the right template, please check and try again" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical, "Error when loading ZIMRO file")
        Finally
            'allow calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = False
            If Not IsNothing(xlHandler) Then
                Try
                    xlHandler.closeConn()
                Catch ex As Exception
                End Try
            End If
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_main_label = "END Import SAP Scope"
    End Sub

    Private Function checkDataConsistency(listO As List(Of Object)) As KeyValuePair(Of Boolean, String)

        Dim errMessage As String
        Dim is_error As Boolean = False

        actList = New Dictionary(Of String, CImportScopeObject)
        proj_list = New List(Of String)
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Check CN47N Data consistency ..."

        Try
            For Each act_imp As CImportScopeObject In listO
                'get proj
                If Not proj_list.Contains(act_imp.network) Then
                    proj_list.Add(act_imp.network)
                End If

                'convert to Hierarchy structure

                cnt_zimro_lines = cnt_zimro_lines + 1
                'handle activity
                If Not actList.ContainsKey(act_imp.getActKey) Then
                    actList.Add(act_imp.getActKey, act_imp)
                Else
                    cnt_zimro_duplicated_lines = cnt_zimro_duplicated_lines + 1
                End If
            Next

            'free memory
            listO.Clear()
            listO = Nothing
            errMessage = "Scope upload failed. Error details : "

            ''check that there is at least one project
            If Not (proj_list.Count > 0 AndAlso Not String.IsNullOrWhiteSpace(proj_list.Item(0))) Then
                errMessage = errMessage & Chr(10) & "-No network (or Project) were found in the SAP Extract"
                is_error = True
            End If

            For Each proj As String In proj_list
                If Not MConstants.GLB_MSTR_CTRL.project_controller.project.project_list.Contains(proj) Then
                    errMessage = errMessage & Chr(10) & "-Network " & proj & " which is part of the current file being imported, is not part of the current customer statement scope : " & String.Join(MConstants.SEP_COMMA, MConstants.GLB_MSTR_CTRL.project_controller.project.project_list)
                    is_error = True
                End If
            Next

        Catch ex As Exception
            Throw New Exception("An error occured when checking CN47N data consistency" & Chr(10) & ex.Message & ex.StackTrace)
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Check SAP Data consistency"

        Return New KeyValuePair(Of Boolean, String)(is_error, errMessage)
    End Function

    Public Sub loadData()
        'proj ref
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Merging SAP data with current customer statement data..."
        Try
            Dim proj As CProjectModel = GLB_MSTR_CTRL.project_controller.project

            'Merge data
            'organize activities in key object mode to ease comparison
            Dim activities As New Dictionary(Of String, CScopeActivity)
            Dim activitiesNotInSAP As New List(Of CScopeActivity)

            For Each act_ As CScopeActivity In GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource
                If act_.is_cost_collector AndAlso proj_list.Contains(act_.proj) Then
                    'handle items only concerned with the ongoing import
                    If Not activities.ContainsKey(act_.getActKey) Then
                        activities.Add(act_.getActKey, act_)
                        activitiesNotInSAP.Add(act_)
                        'log count
                        cnt_initial = cnt_initial + 1
                        'act_.reset_revision_control()
                    End If
                End If
            Next
            Dim act As CScopeActivity

            For Each import As CImportScopeObject In actList.Values

                'handle activity update
                If Not activities.ContainsKey(import.getActKey) Then
                    act = import.getCostCollectorActivity
                    act.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NEW
                    act.logRevisionControlText(act.revision_control_status)
                    'add to view
                    GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource.Add(act)
                    cnt_added = cnt_added + 1
                    'add to list for saving
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted(act)
                Else
                    act = activities(import.getActKey)
                    'update only items that have not yet been updated, because one activity is repeated if there are several cost centers
                    If (act.UpdateCostCollectorFromSAP(import)) Then
                        cnt_updated = cnt_updated + 1
                    Else
                        cnt_constant = cnt_constant + 1
                    End If
                    'remove from act not in sap
                    If activitiesNotInSAP.Contains(act) Then
                        activitiesNotInSAP.Remove(act)
                    End If
                End If
            Next

            'log items not in sap
            For Each noSAPAct As CScopeActivity In activitiesNotInSAP
                noSAPAct.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOSAP
                noSAPAct.logRevisionControlText("Actitity Not Found in SAP")
            Next

        Catch ex As Exception
            Throw New Exception("An error occured while mergin SAP data with customer statement data" & Chr(10) & ex.Message, ex)
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "END Merging SAP data with current customer statement data"
    End Sub

End Class
﻿Imports Equin.ApplicationFramework

Module MDataConsistency

    Public Function isTaskRateUsed(task As CTaskRate) As KeyValuePair(Of Boolean, List(Of String))
        Dim key As Boolean = False
        Dim list As New List(Of String)

        For Each actView As ObjectView(Of CScopeActivity) In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList
            For Each soldH As CSoldHours In actView.Object.sold_hour_list
                If Not IsNothing(soldH.task_rate_obj) AndAlso soldH.task_rate_obj.Equals(task) AndAlso soldH.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                    key = True
                    list.Add(soldH.tostring)
                End If
            Next
        Next
        Return New KeyValuePair(Of Boolean, List(Of String))(key, list)
    End Function

    'check if sales entry contains init sold hours
    Public Function isSalesEntryUsed(salesE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, List(Of String))
        Dim key As Boolean = False
        Dim list As New List(Of String)

        For Each lk As CSaleQuoteEntryToActLink In salesE.quote_entry_link_list
            If Not IsNothing(lk.act_obj) Then
                For Each soldH As CSoldHours In lk.act_obj.sold_hour_list
                    If lk.act_obj.system_status <> MConstants.CONST_SYS_ROW_DELETED AndAlso soldH.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                        If Not IsNothing(soldH.init_quote_entry_id) AndAlso soldH.init_quote_entry_id = salesE.id Then
                            key = True
                            list.Add(soldH.tostring)
                        End If
                    End If
                Next
            End If
        Next
        Return New KeyValuePair(Of Boolean, List(Of String))(key, list)
    End Function

    'check if sales quote is used by sales quote entry
    Public Function isSalesQuoteUsed(salesQ As CSalesQuote) As KeyValuePair(Of Boolean, List(Of String))
        Dim key As Boolean = False
        Dim list As New List(Of String)

        For Each salesEView As ObjectView(Of CSalesQuoteEntry) In MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list
            If Not IsNothing(salesEView.Object.quote_obj) AndAlso salesEView.Object.quote_obj.Equals(salesQ) Then
                list.Add(salesQ.tostring)
                key = True
            End If
        Next
        Return New KeyValuePair(Of Boolean, List(Of String))(key, list)
    End Function

    'check if discount is used
    Public Function isDiscountUsed(disc As CDiscount) As KeyValuePair(Of Boolean, List(Of String))
        Dim key As Boolean = False
        Dim list As New List(Of String)

        For Each actView As ObjectView(Of CScopeActivity) In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList

            If Not IsNothing(actView.Object.discount_obj) AndAlso actView.Object.discount_obj.Equals(disc) Then
                key = True
                list.Add(actView.ToString)
            End If

            For Each soldH As CSoldHours In actView.Object.sold_hour_list
                If Not IsNothing(soldH.discount_obj) AndAlso soldH.discount_obj.Equals(disc) AndAlso soldH.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                    key = True
                    list.Add(soldH.tostring)
                End If
            Next

            For Each mat As CMaterial In actView.Object.material_list
                If Not IsNothing(mat.discount_obj) AndAlso mat.discount_obj.Equals(disc) AndAlso mat.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                    key = True
                    list.Add(mat.tostring)
                End If
            Next
        Next
        Return New KeyValuePair(Of Boolean, List(Of String))(key, list)
    End Function

    'check if NTE is used
    Public Function isNTEUsed(nte As CNotToExceed) As KeyValuePair(Of Boolean, List(Of String))
        Dim key As Boolean = False
        Dim list As New List(Of String)

        For Each actView As ObjectView(Of CScopeActivity) In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList

            If Not IsNothing(actView.Object.nte_obj) AndAlso actView.Object.discount_obj.Equals(nte) Then
                key = True
                list.Add(actView.ToString)
            End If

            For Each soldH As CSoldHours In actView.Object.sold_hour_list
                If Not IsNothing(soldH.nte_obj) AndAlso soldH.nte_obj.Equals(nte) AndAlso soldH.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                    key = True
                    list.Add(soldH.tostring)
                End If
            Next

            For Each mat As CMaterial In actView.Object.material_list
                If Not IsNothing(mat.nte_obj) AndAlso mat.nte_obj.Equals(nte) AndAlso mat.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                    key = True
                    list.Add(mat.tostring)
                End If
            Next
        Next
        Return New KeyValuePair(Of Boolean, List(Of String))(key, list)
    End Function


    'check if Activity can be deleted is used
    Public Function isFakeActivityUsed(act As CScopeActivity) As KeyValuePair(Of Boolean, List(Of String))
        Dim key As Boolean = False
        Dim list As New List(Of String)
        'also check if awq master >0 ^for fake activities for which creatin has been gaven up
        If Not IsNothing(act.awq_master_obj) AndAlso act.awq_master_obj.id > 0 Then
            key = True
            list.Add(act.awq_master_obj.reference_revision)
        End If

        For Each lk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
            If Not IsNothing(lk.init_quote_entry_obj) Then
                key = True
                list.Add(lk.init_quote_entry_obj.tostring)
            End If
        Next
        Return New KeyValuePair(Of Boolean, List(Of String))(key, list)
    End Function
End Module

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAdminAboutApp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.top_panel = New System.Windows.Forms.Panel()
        Me.jet_logo = New System.Windows.Forms.PictureBox()
        Me.main_panel = New System.Windows.Forms.Panel()
        Me.admin_about_appli_dgrid = New System.Windows.Forms.DataGridView()
        Me.bottom_panel = New System.Windows.Forms.Panel()
        Me.reset_session_bt = New System.Windows.Forms.Button()
        Me.top_panel.SuspendLayout()
        CType(Me.jet_logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.main_panel.SuspendLayout()
        CType(Me.admin_about_appli_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bottom_panel.SuspendLayout()
        Me.SuspendLayout()
        '
        'top_panel
        '
        Me.top_panel.Controls.Add(Me.jet_logo)
        Me.top_panel.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel.Location = New System.Drawing.Point(0, 0)
        Me.top_panel.Name = "top_panel"
        Me.top_panel.Size = New System.Drawing.Size(679, 100)
        Me.top_panel.TabIndex = 0
        '
        'jet_logo
        '
        Me.jet_logo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.jet_logo.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.jetLogo
        Me.jet_logo.InitialImage = Nothing
        Me.jet_logo.Location = New System.Drawing.Point(0, 0)
        Me.jet_logo.Name = "jet_logo"
        Me.jet_logo.Size = New System.Drawing.Size(679, 100)
        Me.jet_logo.TabIndex = 0
        Me.jet_logo.TabStop = False
        '
        'main_panel
        '
        Me.main_panel.Controls.Add(Me.admin_about_appli_dgrid)
        Me.main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.main_panel.Location = New System.Drawing.Point(0, 100)
        Me.main_panel.Name = "main_panel"
        Me.main_panel.Size = New System.Drawing.Size(679, 433)
        Me.main_panel.TabIndex = 1
        '
        'admin_about_appli_dgrid
        '
        Me.admin_about_appli_dgrid.AllowUserToAddRows = False
        Me.admin_about_appli_dgrid.AllowUserToDeleteRows = False
        Me.admin_about_appli_dgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.admin_about_appli_dgrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.admin_about_appli_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.admin_about_appli_dgrid.DefaultCellStyle = DataGridViewCellStyle4
        Me.admin_about_appli_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.admin_about_appli_dgrid.Location = New System.Drawing.Point(0, 0)
        Me.admin_about_appli_dgrid.Name = "admin_about_appli_dgrid"
        Me.admin_about_appli_dgrid.Size = New System.Drawing.Size(679, 433)
        Me.admin_about_appli_dgrid.TabIndex = 4
        '
        'bottom_panel
        '
        Me.bottom_panel.Controls.Add(Me.reset_session_bt)
        Me.bottom_panel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.bottom_panel.Location = New System.Drawing.Point(0, 533)
        Me.bottom_panel.Name = "bottom_panel"
        Me.bottom_panel.Size = New System.Drawing.Size(679, 100)
        Me.bottom_panel.TabIndex = 5
        '
        'reset_session_bt
        '
        Me.reset_session_bt.Location = New System.Drawing.Point(3, 6)
        Me.reset_session_bt.Name = "reset_session_bt"
        Me.reset_session_bt.Size = New System.Drawing.Size(88, 23)
        Me.reset_session_bt.TabIndex = 0
        Me.reset_session_bt.Text = "Reset Session"
        Me.reset_session_bt.UseVisualStyleBackColor = True
        '
        'FormAdminAboutApp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 633)
        Me.Controls.Add(Me.main_panel)
        Me.Controls.Add(Me.bottom_panel)
        Me.Controls.Add(Me.top_panel)
        Me.Name = "FormAdminAboutApp"
        Me.Text = "About Application"
        Me.top_panel.ResumeLayout(False)
        CType(Me.jet_logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.main_panel.ResumeLayout(False)
        CType(Me.admin_about_appli_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bottom_panel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents top_panel As Panel
    Friend WithEvents jet_logo As PictureBox
    Friend WithEvents main_panel As Panel
    Friend WithEvents admin_about_appli_dgrid As DataGridView
    Friend WithEvents bottom_panel As Panel
    Friend WithEvents reset_session_bt As Button
End Class

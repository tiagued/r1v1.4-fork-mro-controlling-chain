﻿Public Class CAWQFormDetailsMat
    Private _material As String
    Public Property material() As String
        Get
            Return _material
        End Get
        Set(ByVal value As String)
            _material = value
        End Set
    End Property

    Private _pn As String
    Public Property pn() As String
        Get
            Return _pn
        End Get
        Set(ByVal value As String)
            _pn = value
        End Set
    End Property

    Private _type As String
    Public Property type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    Private _transaction As String
    Public Property transaction() As String
        Get
            Return _transaction
        End Get
        Set(ByVal value As String)
            _transaction = value
        End Set
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            _qty = value
        End Set
    End Property

    Public ReadOnly Property qty_view() As Double
        Get
            Return If(_qty > 0, _qty, 1)
        End Get
    End Property

    Private _UoM As String
    Public Property UoM() As String
        Get
            Return _UoM
        End Get
        Set(ByVal value As String)
            _UoM = value
        End Set
    End Property

    Public ReadOnly Property UoM_view() As String
        Get
            Return If(Not String.IsNullOrWhiteSpace(_UoM), _UoM, MConstants.constantConf(MConstants.CONST_CONF_UOM_EA_LABEL).value)
        End Get
    End Property

    Private _price As Double
    Public Property price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = value
        End Set
    End Property


    Private _approved_price As Double
    Public Property approved_price() As Double
        Get
            Return _approved_price
        End Get
        Set(ByVal value As Double)
            _approved_price = value
        End Set
    End Property


    Public Sub addApprMat(mat As CMaterial, isNegative As Boolean)
        If isNegative Then
            _qty = _qty - mat.qty
            _approved_price = _approved_price - mat.calc_total_material_in_mat_curr_bef_disc
        Else
            _qty = _qty + mat.qty
            _approved_price = _approved_price + mat.calc_total_material_in_mat_curr_bef_disc
        End If
    End Sub

    Public Sub addMat(mat As CMaterial, isNegative As Boolean)
        If isNegative Then
            _qty = _qty - mat.qty
            _price = _price - mat.calc_total_material_in_mat_curr_bef_disc
        Else
            _qty = _qty + mat.qty
            _price = _price + mat.calc_total_material_in_mat_curr_bef_disc
        End If
    End Sub

    Public Sub addApprFreight(mat As CMaterial, isNegative As Boolean)
        If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value Then
            _UoM = mat.uom
            If isNegative Then
                _qty = _qty - mat.qty
                _approved_price = _approved_price - mat.calc_total_material_in_mat_curr_bef_disc
            Else
                _qty = _qty + mat.qty
                _approved_price = _approved_price + mat.calc_total_material_in_mat_curr_bef_disc
            End If
        End If
        If isNegative Then
            _approved_price = _approved_price - mat.calc_total_freight_befor_disc_in_freight_curr
        Else
            _approved_price = _approved_price + mat.calc_total_freight_befor_disc_in_freight_curr
        End If
    End Sub

    Public Sub addFreight(mat As CMaterial, isNegative As Boolean)
        If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value Then
            _UoM = mat.uom
            If isNegative Then
                _qty = _qty - mat.qty
                _price = _price - mat.calc_total_material_in_mat_curr_bef_disc
            Else
                _qty = _qty + mat.qty
                _price = _price + mat.calc_total_material_in_mat_curr_bef_disc
            End If
        End If
        If isNegative Then
            _price = _price - mat.calc_total_freight_befor_disc_in_freight_curr
        Else
            _price = _price + mat.calc_total_freight_befor_disc_in_freight_curr
        End If
    End Sub

    Public Sub addApprServ(mat As CMaterial, isNegative As Boolean)
        If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value Then
            _UoM = mat.uom
            If isNegative Then
                _qty = _qty - mat.qty
                _approved_price = _approved_price - mat.calc_total_material_in_mat_curr_bef_disc
            Else
                _qty = _qty + mat.qty
                _approved_price = _approved_price + mat.calc_total_material_in_mat_curr_bef_disc
            End If
        End If
        If isNegative Then
            _approved_price = _approved_price - mat.calc_handling_zcus_value_bef_disc_in_mat_curr
        Else
            _approved_price = _approved_price + mat.calc_handling_zcus_value_bef_disc_in_mat_curr
        End If
    End Sub

    Public Sub addServ(mat As CMaterial, isNegative As Boolean)
        If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value Then
            _UoM = mat.uom
            If isNegative Then
                _qty = _qty - mat.qty
                _price = _price - mat.calc_total_material_in_mat_curr_bef_disc
            Else
                _qty = _qty + mat.qty
                _price = _price + mat.calc_total_material_in_mat_curr_bef_disc
            End If
        End If
        If isNegative Then
            _price = _price - mat.calc_handling_zcus_value_bef_disc_in_mat_curr
        Else
            _price = _price + mat.calc_handling_zcus_value_bef_disc_in_mat_curr
        End If
    End Sub

End Class

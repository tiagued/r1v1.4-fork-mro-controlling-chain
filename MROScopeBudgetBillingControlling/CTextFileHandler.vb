﻿Imports System.Reflection

Public Class CTextFileHandler
    Public Function performSelectObject(filePath As String, firstCol As String, delimiter As String, mapping As CViewModelMapList) As List(Of Object)

        Dim objReader As New System.IO.StreamReader(filePath)
        Dim line As String
        Dim lineArr() As String
        Dim headerFound As Boolean
        Dim firstColFound As Boolean
        Dim firstColIdx As Integer
        Dim maxColIdx As Integer = 0
        Dim entry As Object
        Dim propInf As PropertyInfo
        Dim value As Object
        Dim res As New List(Of Object)
        Try
            Dim colIndex As New Dictionary(Of String, Integer)
            Do While objReader.Peek() <> -1

                line = objReader.ReadLine()
                lineArr = line.Split(delimiter)

                If Not headerFound Then
                    'catch first line
                    For Each col As String In lineArr
                        If Trim(col) = Trim(firstCol) Then
                            firstColFound = True
                            Exit For
                        End If
                    Next

                    If firstColFound Then
                        For idx As Integer = 0 To lineArr.Count - 1
                            Dim col = Trim(lineArr(idx))
                            If mapping.list.ContainsKey(Trim(col)) Then
                                If Not colIndex.ContainsKey(col) Then
                                    colIndex.Add(col, idx)
                                    maxColIdx = Math.Max(maxColIdx, idx)
                                Else
                                    Throw New Exception("Column " & col & " Appears more than once in the txt file")
                                End If
                            End If
                        Next
                        'check that all col in the mapping are in the txt file
                        For Each mapCol As String In mapping.list.Keys
                            If Not colIndex.ContainsKey(mapCol) Then
                                Throw New Exception("The col " & mapCol & " contained in the mapping model cannot be found in the txt file")
                            End If
                        Next
                        firstColIdx = colIndex(firstCol)
                        headerFound = True
                    End If

                Else
                    'header already found
                    If lineArr.Length > maxColIdx AndAlso Not String.IsNullOrWhiteSpace(lineArr(firstColIdx)) Then
                        entry = Activator.CreateInstance(mapping.object_class)

                        For Each map As CViewModelMap In mapping.list.Values
                            propInf = mapping.object_class.GetProperty(map.class_field)
                            value = MGeneralFunctionsDB.formatValue(Trim(lineArr(colIndex(map.db_col))), propInf.PropertyType.Name)
                            CallByName(entry, map.class_field, CallType.Let, New Object() {value})
                        Next
                        res.Add(entry)
                    End If
                End If
            Loop
        Catch ex As Exception
            Throw New Exception("An error occured when reading TXT File " & filePath & Chr(10) & ex.Message)
        Finally
            objReader.Close()
        End Try
        Return res
    End Function
End Class

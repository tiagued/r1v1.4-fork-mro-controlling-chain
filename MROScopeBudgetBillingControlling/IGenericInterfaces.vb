﻿Module IGenericInterfaces
    'interface to block items that are not editable
    Interface IDataGridViewEditable
        Function isEditable() As KeyValuePair(Of Boolean, String)
    End Interface

    'interface used to take the empty item of a list of objects when triggering the clear action
    Interface IDataGridViewListable
        Function isEmptyItem() As Boolean
    End Interface

    'interface used to delete a row item
    Interface IDataGridViewDeletable
        Function deleteItem() As KeyValuePair(Of Boolean, String)
    End Interface

    'interface used to save a row item
    Interface IDataGridViewRecordable
        'if id > 0
        Function isRecordable() As Boolean
        'get the mapper that contains the list of attributes
        Function getDBMapperName() As String
        'get the mapper the object id
        Function getID() As Long
        'set the object id
        Sub setID(_id As Long)
        'set error
        Sub setErrorState(isError As Boolean, errorText As String)
        'get error
        Function getErrorState() As KeyValuePair(Of Boolean, String)

    End Interface
End Module

﻿Imports System.Text
Imports Equin.ApplicationFramework
Imports Microsoft.Office.Interop.Excel

Public Class CReportingAWQForm
    Private Shared instance As CReportingAWQForm

    Public form As FormReportAWQForm

    'awq front page
    Private frontPageSummaryConf As List(Of CReportConfObject)
    'awq details
    Private detailsPageSummaryConf As List(Of CReportConfObject)
    Private detailsPageLaborTableConf As List(Of CReportConfObject)
    Private detailsPageMaterialTableConf As List(Of CReportConfObject)
    Private reportWB As Microsoft.Office.Interop.Excel.Workbook
    Private awq As CAWQuotation
    Private project_lab_curr As String
    Private project_mat_curr As String

    'defalut
    Public defaultCustPayer As CPayer

    Public Shared Function getInstance() As CReportingAWQForm
        Try
            If IsNothing(instance) Then
                instance = New CReportingAWQForm

                'proj summ
                instance.frontPageSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_AWQ_FORM_FROMT_PAGE_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                'proj details
                instance.detailsPageLaborTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_AWQ_FORM_DETAILS_PAGE_SHEET_NAME)(MConstants.REPORT_CONF_AWQ_FORM_DETAILS_PAGE_LABOR_LIST)
                instance.detailsPageMaterialTableConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_AWQ_FORM_DETAILS_PAGE_SHEET_NAME)(MConstants.REPORT_CONF_AWQ_FORM_DETAILS_PAGE_MAT_LIST)
                instance.detailsPageSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_AWQ_FORM_DETAILS_PAGE_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                instance.defaultCustPayer = MConstants.listOfPayer(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value)
            End If
        Catch ex As Exception
            instance = Nothing
            MGeneralFuntionsViewControl.displayMessage("AWQ Form Error", "An error occured while preparing AWQ form view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return instance
    End Function

    Private _project_report_payer As CPayer
    Public Property project_report_payer() As CPayer
        Get
            Return _project_report_payer
        End Get
        Set(ByVal value As CPayer)
            If Not Object.Equals(_project_report_payer, value) Then
                Me.is_lab_mat_curr_enabled = True
                _project_report_payer = value
                'manage currencies
                If Object.Equals(_project_report_payer, defaultCustPayer) Then
                    Me.report_lab_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr
                    Me.report_mat_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr
                    Me.is_lab_mat_curr_enabled = False
                Else
                    If MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_LAB_CURR).ContainsKey(_project_report_payer.payer) Then
                        If MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_MAT_CURR).ContainsKey(_project_report_payer.payer) Then
                            Me.report_lab_curr = MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_LAB_CURR)(_project_report_payer.payer)(0)
                            Me.report_mat_curr = MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_DEP_PAYER_REPORT_MAT_CURR)(_project_report_payer.payer)(0)
                            Me.is_lab_mat_curr_enabled = False
                        End If
                    End If
                End If
            End If
        End Set
    End Property

    Private _is_lab_mat_curr_enabled As Boolean
    Public Property is_lab_mat_curr_enabled() As Boolean
        Get
            Return _is_lab_mat_curr_enabled
        End Get
        Set(ByVal value As Boolean)
            If Not _is_lab_mat_curr_enabled = value Then
                _is_lab_mat_curr_enabled = value
            End If
        End Set
    End Property


    Private _report_lab_curr As String
    Public Property report_lab_curr() As String
        Get
            Return _report_lab_curr
        End Get
        Set(ByVal value As String)
            _report_lab_curr = value
        End Set
    End Property

    Private _report_mat_curr As String
    Public Property report_mat_curr() As String
        Get
            Return _report_mat_curr
        End Get
        Set(ByVal value As String)
            _report_mat_curr = value
        End Set
    End Property

    Public Sub log_text(text As String)
        Try
            form.log_bt.Text = text
        Catch ex As Exception
            Throw New Exception("An error occured while logging progress" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    'bind form controls to report object properties
    Public Sub prepareAWQFormView(_awq As CAWQuotation)
        Try

            awq = _awq
            form = New FormReportAWQForm

            'payer selection
            'defalut value
            _project_report_payer = MConstants.listOfPayer(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value)

            form.cb_payer.DataBindings.Clear()
            form.cb_payer.AutoCompleteMode = AutoCompleteMode.None
            form.cb_payer.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_payer)
            Dim bds As New BindingSource
            bds.DataSource = MConstants.listOfPayer.Values
            form.cb_payer.DataSource = bds
            form.cb_payer.DisplayMember = "label"
            form.cb_payer.DataBindings.Add("SelectedItem", Me, "project_report_payer", False, DataSourceUpdateMode.OnPropertyChanged)


            'currencies selection
            Me.is_lab_mat_curr_enabled = False
            'defalut value
            _report_lab_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr

            form.cb_lab_curr.DataBindings.Clear()
            form.cb_lab_curr.AutoCompleteMode = AutoCompleteMode.None
            form.cb_lab_curr.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_lab_curr)
            bds = New BindingSource
            bds.DataSource = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR).Values
            form.cb_lab_curr.DataSource = bds
            form.cb_lab_curr.DisplayMember = "Value"
            form.cb_lab_curr.ValueMember = "Key"
            form.cb_lab_curr.DataBindings.Add("SelectedValue", Me, "report_lab_curr", False, DataSourceUpdateMode.OnPropertyChanged)
            form.cb_lab_curr.DataBindings.Add("Enabled", Me, "is_lab_mat_curr_enabled", False, DataSourceUpdateMode.OnPropertyChanged)

            'defalut value
            _report_mat_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

            form.cb_mat_curr.DataBindings.Clear()
            form.cb_mat_curr.AutoCompleteMode = AutoCompleteMode.None
            form.cb_mat_curr.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_mat_curr)
            bds = New BindingSource
            bds.DataSource = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR).Values
            form.cb_mat_curr.DataSource = bds
            form.cb_mat_curr.DisplayMember = "Value"
            form.cb_mat_curr.ValueMember = "Key"
            form.cb_mat_curr.DataBindings.Add("SelectedValue", Me, "report_mat_curr", False, DataSourceUpdateMode.OnPropertyChanged)
            form.cb_mat_curr.DataBindings.Add("Enabled", Me, "is_lab_mat_curr_enabled", False, DataSourceUpdateMode.OnPropertyChanged)
            'log
            log_text("Click Run to generate AWQ Form")

            form.ShowDialog()
            form.BringToFront()
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("AWQ Form Error", "An error occured while preparing AWQ Form generation view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    Public Sub launch()
        Dim curr_altereded As Boolean = False
        Try
            'log
            log_text("Starting...")
            'check currencies
            If String.IsNullOrWhiteSpace(_report_lab_curr) OrElse MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value = _report_lab_curr _
                OrElse String.IsNullOrWhiteSpace(_report_mat_curr) OrElse MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value = _report_mat_curr Then
                Throw New Exception("Lab Currency or Material Currency cannot be empty")
            End If

            'calculate if not default customer as payer
            If defaultCustPayer.payer <> _project_report_payer.payer Then
                'log
                log_text("Calculate Project with chosen payer currencies...")
                'to block project curr update in DB
                MConstants.GLOB_APP_LOADED = False
                project_lab_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr
                project_mat_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

                MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr = _report_lab_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr = _report_mat_curr
                MConstants.GLOB_APP_LOADED = True

                curr_altereded = True

                'refresh calc
                MConstants.GLB_MSTR_CTRL.calculation_controller.calculateInMainThread(False)
            End If

            'log
            log_text("Refresh AWQ Values with current payer")
            'UPDATE CALCULATION ON AWQ WITH SPECIFIED PAYER. SET TRUE To fill sold hours and material lists for awq details
            awq.updateCal(_project_report_payer.payer, True)

            'log
            log_text("Check Template File")

            Dim template As String = MConstants.constantConf(MConstants.CONST_CONF_AWQ_FORM_REPORT_TMPL_FILE).value
            If Not System.IO.File.Exists(template) Then
                Throw New Exception("The AWQ Form Template File " & template & " Does not exists")
            End If

            'generate excel file
            Dim reportName As String = MConstants.constantConf(MConstants.CONST_CONF_AWQ_FORM_REPORT_NAME).value
            'replace variable in report name
            reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_AWQ_PARAM).value, awq.reference_revision)
            'if not for customer
            If _project_report_payer.payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value Then
                reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_PAYER_PARAM).value, "")
            Else
                reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_PAYER_PARAM).value, "_" & _project_report_payer.label)
            End If

            reportName = MConstants.GLB_MSTR_CTRL.csFile.db_folder & "\" & reportName

            'log
            log_text("Copy and Open Template File")
            reportWB = MConstants.GLB_MSTR_CTRL.csFile.userxlAppOpenTemplate(template, reportName)

            Try
                MConstants.GLB_MSTR_CTRL.csFile.OffCalc(reportWB.Application)
                'log
                log_text("Filling AWQ Summary")
                loadFrontPageSumData(awq)
                'log
                log_text("Filling AWQ Details")
                loadAWQDetailsData(awq)

            Catch ex As Exception
                Throw New Exception("Error " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            Finally
                MConstants.GLB_MSTR_CTRL.csFile.OnCalc(reportWB.Application)
            End Try
            'log
            log_text("Saving Form")
            MConstants.GLB_MSTR_CTRL.csFile.safeWorkbookSave(reportWB)
            reportWB.Activate()
            Dim awq_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_AWQ_FORM_FRONT_PAGE_TAB_NAME).value)
            awq_sum_ws.Activate()
            'log
            log_text("AWQ Form Successfully Generated...")
            MGeneralFuntionsViewControl.displayMessage("AWQ Form Success", "The AWQ Form has been generated successfully :" & reportWB.FullName, MsgBoxStyle.Information)
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("AWQ FormError", "An error occured while preparing the AWQ Form view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        Finally
            'replace customer values if needed
            'calculate if not default customer as payer
            If curr_altereded Then
                MConstants.GLOB_APP_LOADED = False
                MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr = project_lab_curr
                MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr = project_mat_curr
                MConstants.GLOB_APP_LOADED = True
                'refresh calc
                MConstants.GLB_MSTR_CTRL.refreshCalculation()
            End If
            form.Close()
            'reset default calculation view of awq on the customer
            awq.updateCal(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value, False)
        End Try
    End Sub

    Public Sub loadFrontPageSumData(awq As CAWQuotation)
        Try
            Dim awq_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_AWQ_FORM_FRONT_PAGE_TAB_NAME).value)
            Dim value As Object

            'fill summary values
            For Each confO As CReportConfObject In frontPageSummaryConf
                If confO.item_object = "CReportingAWQForm" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                ElseIf confO.item_object = "CAWQuotation" Then
                    value = CallByName(awq, confO.item_property, CallType.Get)
                Else
                    Throw New Exception("AWQ Form Report, conf object type not found " & confO.item_object)
                End If
                If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                    If CDate(value) = MConstants.APP_NULL_DATE Then
                        value = ""
                    Else
                        Dim _d As Date = CDate(value)
                        value = _d.ToString(confO.item_format)
                    End If
                End If
                awq_sum_ws.Cells(confO.item_row, confO.item_column) = value
            Next
        Catch ex As Exception
            Throw New Exception("An error occured while generating AWQ Form View" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadAWQDetailsData(awq As CAWQuotation)
        Try
            Dim awq_details_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_AWQ_FORM_DETAILS_PAGE_TAB_NAME).value)

            Dim value As Object

            'fill summary values
            For Each confO As CReportConfObject In detailsPageSummaryConf
                If confO.item_object = "CReportingAWQForm" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                ElseIf confO.item_object = "CAWQuotation" Then
                    value = CallByName(awq, confO.item_property, CallType.Get)
                Else
                    Throw New Exception("AWQ Form, conf object type not found " & confO.item_object)
                End If
                If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                    If CDate(value) = MConstants.APP_NULL_DATE Then
                        value = ""
                    Else
                        Dim _d As Date = CDate(value)
                        value = _d.ToString(confO.item_format)
                    End If
                End If
                awq_details_ws.Cells(confO.item_row, confO.item_column) = value
            Next

            'prepare labor and material list (make sure awq calculation has been updated with filllabmat list option
            Dim releasedAWQ As List(Of String) = awq.getPreviouApprovedRevs
            Dim entryList As List(Of CScopeAtivityCalcEntry)
            Dim awqLaborList As New Dictionary(Of String, CAWQFormDetailsLab)
            Dim awqAllMatListDic As New Dictionary(Of String, CAWQFormDetailsMat)

            Dim matTypeList As Dictionary(Of String, CConfProperty) = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_MATERIAL_TYPE_LIST)

            'apprv labor & mat
            For Each act As CScopeActivity In awq.master_obj.dependantActList
                entryList = act.calculator.getEntry(_project_report_payer.payer, releasedAWQ, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                For Each entry As CScopeAtivityCalcEntry In entryList
                    For Each soldH As CSoldHours In entry.laborList
                        Dim newLab = New CAWQFormDetailsLab
                        If Not IsNothing(soldH.cc_obj) Then
                            newLab.skill_key = soldH.cc_obj.cc
                            newLab.skill = soldH.cc_obj.friendly_description
                        End If
                        newLab.UoM = MConstants.constantConf(MConstants.CONST_CONF_UOM_HOURS_LABEL).value
                        awqLaborList.Add(CStr(awqLaborList.Count), newLab)

                        'add labor
                        newLab.addApprLabor(soldH, entry.labMatDeletedList.Contains(soldH))
                    Next
                    For Each mat As CMaterial In entry.materialList
                        Dim newMat = New CAWQFormDetailsMat
                        newMat.pn = mat.part_number
                        newMat.material = mat.description
                        newMat.UoM = mat.uom
                        newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_MAT_KEY).value).value
                        newMat.transaction = mat.transac_condition_view
                        awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)
                        newMat.addApprMat(mat, entry.labMatDeletedList.Contains(mat))
                    Next
                    For Each mat As CMaterial In entry.repairList
                        Dim newMat = New CAWQFormDetailsMat
                        newMat.pn = mat.part_number
                        newMat.material = mat.description
                        newMat.UoM = mat.uom
                        newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_REP_KEY).value).value
                        newMat.transaction = mat.transac_condition_view
                        awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)
                        newMat.addApprMat(mat, entry.labMatDeletedList.Contains(mat))
                    Next
                    For Each mat As CMaterial In entry.freightList
                        Dim newMat = New CAWQFormDetailsMat
                        newMat.pn = mat.part_number
                        newMat.UoM = mat.uom
                        newMat.material = mat.description
                        newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value).value
                        awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)

                        newMat.addApprFreight(mat, entry.labMatDeletedList.Contains(mat))
                    Next
                    For Each mat As CMaterial In entry.servList
                        Dim newMat = New CAWQFormDetailsMat
                        newMat.UoM = mat.uom
                        newMat.pn = mat.part_number
                        newMat.material = mat.description
                        newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value).value
                        awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)
                        newMat.addApprServ(mat, entry.labMatDeletedList.Contains(mat))
                    Next
                Next
            Next
            'current AWQ
            For Each act As CScopeActivity In awq.master_obj.dependantActList
                Dim entry As CScopeAtivityCalcEntry = act.calculator.getEntry(_project_report_payer.payer, awq.reference_revision, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                If IsNothing(entry) Then
                    Continue For
                End If
                For Each soldH As CSoldHours In entry.laborList
                    Dim newLab = New CAWQFormDetailsLab
                    If Not IsNothing(soldH.cc_obj) Then
                        newLab.skill_key = soldH.cc_obj.cc
                        newLab.skill = soldH.cc_obj.friendly_description
                    End If
                    newLab.UoM = MConstants.constantConf(MConstants.CONST_CONF_UOM_HOURS_LABEL).value
                    awqLaborList.Add(CStr(awqLaborList.Count), newLab)

                    newLab.addLabor(soldH, entry.labMatDeletedList.Contains(soldH))
                Next
                For Each mat As CMaterial In entry.materialList
                    Dim newMat = New CAWQFormDetailsMat
                    newMat.pn = mat.part_number
                    newMat.UoM = mat.uom
                    newMat.material = mat.description
                    newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_MAT_KEY).value).value
                    newMat.transaction = mat.transac_condition_view
                    awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)
                    newMat.addMat(mat, entry.labMatDeletedList.Contains(mat))
                Next
                For Each mat As CMaterial In entry.repairList
                    Dim newMat = New CAWQFormDetailsMat
                    newMat.pn = mat.part_number
                    newMat.UoM = mat.uom
                    newMat.material = mat.description
                    newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_REP_KEY).value).value
                    newMat.transaction = mat.transac_condition_view
                    awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)
                    newMat.addMat(mat, entry.labMatDeletedList.Contains(mat))
                Next
                For Each mat As CMaterial In entry.freightList
                    Dim newMat = New CAWQFormDetailsMat
                    newMat.pn = mat.part_number
                    newMat.material = mat.description
                    newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value).value
                    awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)
                    newMat.addFreight(mat, entry.labMatDeletedList.Contains(mat))
                Next
                For Each mat As CMaterial In entry.servList
                    Dim newMat = New CAWQFormDetailsMat
                    newMat.pn = mat.part_number
                    newMat.material = mat.description
                    newMat.type = matTypeList(MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value).value
                    awqAllMatListDic.Add(CStr(awqAllMatListDic.Count), newMat)
                    newMat.addServ(mat, entry.labMatDeletedList.Contains(mat))
                Next
            Next
            'sort list
            Dim sortHandler As New BindingListView(Of CAWQFormDetailsMat)(awqAllMatListDic.Values.ToList)
            sortHandler.ApplySort("pn")
            Dim awqAllMatList As List(Of CAWQFormDetailsMat) = MGeneralFuntionsViewControl.getAWQMaterialViewFromListView(sortHandler)
            'labor list
            Dim reportArray(awqLaborList.Count - 1, detailsPageLaborTableConf.Count - 1) As Object
            Dim i As Integer = 0
            'build array
            For Each entry As CAWQFormDetailsLab In awqLaborList.Values
                For Each confO As CReportConfObject In detailsPageLaborTableConf
                    If String.IsNullOrWhiteSpace(confO.item_object) OrElse confO.item_object.ToLower.Equals("skip") Then
                        Continue For
                    End If
                    If confO.item_object = "CAWQFormDetailsLab" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("Unknow item_object for awq details labor " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next
            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(detailsPageLaborTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & detailsPageLaborTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(awq_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If awqLaborList.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, awq_details_ws, xlrg, awqLaborList.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(awq_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = awq_details_ws.Range(xlrg, xlrg.Offset(awqLaborList.Count - 1, detailsPageLaborTableConf.Count - 1))
            xlrg.Value = reportArray


            'Material Values

            Dim mat_reportArray(awqAllMatList.Count - 1, detailsPageMaterialTableConf.Count - 1) As Object
            i = 0
            'build array
            For Each entry As CAWQFormDetailsMat In awqAllMatList

                For Each confO As CReportConfObject In detailsPageMaterialTableConf
                    If String.IsNullOrWhiteSpace(confO.item_object) OrElse confO.item_object.ToLower.Equals("skip") Then
                        Continue For
                    End If
                    If confO.item_object = "CAWQFormDetailsMat" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("Unknow item_object for awq details materials " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    mat_reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next
            'insert
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(detailsPageMaterialTableConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & detailsPageMaterialTableConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(awq_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If awqAllMatList.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, awq_details_ws, xlrg, awqAllMatList.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(awq_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = awq_details_ws.Range(xlrg, xlrg.Offset(awqAllMatList.Count - 1, detailsPageMaterialTableConf.Count - 1))
            xlrg.Value = mat_reportArray
        Catch ex As Exception
            Throw New Exception("An error occured while generating AWQ Form" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public ReadOnly Property CustomerCompany() As String
        Get
            If _project_report_payer.payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value Then
                Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer
            Else
                Return _project_report_payer.label
            End If
        End Get
    End Property
    Public ReadOnly Property CustomerName() As String
        Get
            If _project_report_payer.payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value Then
                Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer_rep_name
            Else
                Return _project_report_payer.friendly_label
            End If
        End Get
    End Property
    Public ReadOnly Property CustomerEmail() As String
        Get
            If _project_report_payer.payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value Then
                Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer_email
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property AircraftType() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.ac_master
        End Get
    End Property

    Public ReadOnly Property AircraftReg() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.ac_reg
        End Get

    End Property
    Public ReadOnly Property Project() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_list_str
        End Get
    End Property

    Public ReadOnly Property ProjectDescription() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_desc
        End Get
    End Property

    Public ReadOnly Property getDate() As Date
        Get
            Return DateTime.Now
        End Get
    End Property

    Public ReadOnly Property AWQSpecialist() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.awq_specialist
        End Get
    End Property

    Public ReadOnly Property getLaborCurr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr).value
        End Get
    End Property

    Public ReadOnly Property getMatCurr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr).value
        End Get
    End Property

    Public ReadOnly Property getRepCurr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.repair_curr).value
        End Get
    End Property

    Public ReadOnly Property getFreightCurr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.freight_curr).value
        End Get
    End Property

    Public ReadOnly Property getServCurr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.third_party_curr).value
        End Get
    End Property

    Public ReadOnly Property getProjectQuotation() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.ini_scope_controller.getProjectQuoteNumber
        End Get
    End Property

    Public ReadOnly Property getCurrenciesTagkToMatCurr() As String
        Get
            Dim res As String = ""

            Dim matcurr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr
            Dim matcurrView = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(matcurr).value
            Dim chf_curr = MConstants.constantConf(MConstants.CONST_CONF_CURR_CHF_KEY).value
            Dim chf_currView = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(chf_curr).value
            Dim eur_curr = MConstants.constantConf(MConstants.CONST_CONF_CURR_EUR_KEY).value
            Dim eur_currView = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(eur_curr).value
            Dim usd_curr = MConstants.constantConf(MConstants.CONST_CONF_CURR_USD_KEY).value
            Dim usd_currView = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(usd_curr).value


            res = "1 " & chf_currView & " to " & MConstants.GLB_MSTR_CTRL.billing_cond_controller.getTagk(chf_curr, matcurr) & " " & matcurrView
            res = res & " / 1 " & eur_currView & " to " & MConstants.GLB_MSTR_CTRL.billing_cond_controller.getTagk(eur_curr, matcurr) & " " & matcurrView
            res = res & " / 1 " & usd_currView & " to " & MConstants.GLB_MSTR_CTRL.billing_cond_controller.getTagk(usd_curr, matcurr) & " " & matcurrView

            Return res
        End Get
    End Property




End Class

﻿Public Class CExchangeRateRowValidator
    Public Shared instance As CExchangeRateRowValidator
    Public Shared Function getInstance() As CExchangeRateRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CExchangeRateRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(exchang As CExchangeRate)
        exchang.calc_is_in_error_state = False
    End Sub

    Public Function exchange_rate(exchang As CExchangeRate) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If exchang.exchange_rate <= 0 Then
            err = "Exchange Rate Value shall be between greather than 0"
            res = False
            exchang.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class
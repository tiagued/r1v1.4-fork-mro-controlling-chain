﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CStatusStripView
    Private mctrl As CMasterController
    Public Sub New(_mcrl As CMasterController)
        mctrl = _mcrl
        initialize()
    End Sub

    'raise event if change occures
    Public Sub notifyPropertyChange(<CallerMemberName()> Optional ByVal prop As String = "")
        'check that no waitOne event on the main thread is active to avoid freezing main thread
        notifyPropUpdateSafe(prop)
    End Sub

    Delegate Sub safeCallDelegate(prop As String)

    'sub notify
    Public Sub notifyPropUpdate(prop As String)
        Try
            If prop = "status_strip_main_label" Then
                mctrl.mainForm.status_strip_main_label.Text = Me.status_strip_main_label
            ElseIf prop = "status_strip_sub_label" Then
                mctrl.mainForm.status_strip_sub_label.Text = Me.status_strip_sub_label
            ElseIf prop = "progress_bar_visible" Then
                mctrl.mainForm.status_strip_progress_bar.Visible = Me.progress_bar_visible
            ElseIf prop = "progress_bar_value" Then
                mctrl.mainForm.status_strip_progress_bar.Value = Me.progress_bar_value
            ElseIf prop = "latest_update_date_label" Then
                mctrl.mainForm.status_strip_latest_update.Text = Me.latest_update_date_label
            ElseIf prop = "calc_progress_bar_visible" Then
                mctrl.mainForm.calculation_progress_bar.Visible = Me.calc_progress_bar_visible
            ElseIf prop = "calc_progress_bar_value" Then
                mctrl.mainForm.calculation_progress_bar.Value = Me.calc_progress_bar_value
            ElseIf prop = "calc_status_label_visible" Then
                mctrl.mainForm.calculation_label.Visible = Me.calc_status_label_visible
            ElseIf prop = "" Then
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub notifyPropUpdateSafe(prop As String)
        If mctrl.mainForm.Visible Then
            If mctrl.mainForm.DownStatusStrip.InvokeRequired Then
                Dim deleg As New safeCallDelegate(AddressOf notifyPropUpdate)
                mctrl.mainForm.DownStatusStrip.Invoke(deleg, New Object() {prop})
            Else
                notifyPropUpdate(prop)
            End If
        End If
    End Sub

    'label of the main ongoing function
    Private _status_strip_main_label As String
    Public Property status_strip_main_label() As String
        Get
            Return _status_strip_main_label
        End Get
        Set(ByVal value As String)
            If Not _status_strip_main_label = value Then
                _status_strip_main_label = value
                notifyPropertyChange()
            End If
        End Set
    End Property

    'logs of the ongoing functions
    Private _status_strip_sub_label As String
    Public Property status_strip_sub_label() As String
        Get
            Return _status_strip_sub_label
        End Get
        Set(ByVal value As String)
            If Not _status_strip_sub_label = value Then
                _status_strip_sub_label = value
                notifyPropertyChange()
            End If
        End Set
    End Property

    'progress bar display
    Private _progress_bar_visible As Boolean
    Public Property progress_bar_visible() As Boolean
        Get
            Return _progress_bar_visible
        End Get
        Set(ByVal value As Boolean)
            If Not _progress_bar_visible = value Then
                _progress_bar_visible = value
                notifyPropertyChange()
            End If
        End Set
    End Property

    'progress bar value
    Private _progress_bar_value As Long
    Public Property progress_bar_value() As Long
        Get
            Return _progress_bar_value
        End Get
        Set(ByVal value As Long)
            _progress_bar_value = value
            If _progress_bar_value = 0 Then
                Me.progress_bar_visible = False
            Else
                Me.progress_bar_visible = True
            End If
            notifyPropertyChange()
        End Set
    End Property

    'calc progress bar display
    Private _calc_progress_bar_visible As Boolean
    Public Property calc_progress_bar_visible() As Boolean
        Get
            Return _calc_progress_bar_visible
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_progress_bar_visible = value Then
                _calc_progress_bar_visible = value
                notifyPropertyChange()
            End If
        End Set
    End Property

    'calc progress bar value
    Private _calc_progress_bar_value As Long
    Public Property calc_progress_bar_value() As Long
        Get
            Return _calc_progress_bar_value
        End Get
        Set(ByVal value As Long)
            If Not _calc_progress_bar_value = value Then
                _calc_progress_bar_value = value
                notifyPropertyChange()
            End If
        End Set
    End Property
    'calc progress status visible
    Private _calc_status_label_visible As Boolean
    Public Property calc_status_label_visible() As Boolean
        Get
            Return _calc_status_label_visible
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_status_label_visible = value Then
                _calc_status_label_visible = value
                notifyPropertyChange()
            End If
        End Set
    End Property

    'latest date update
    Private _latest_update_date_label As String
    Public Property latest_update_date_label() As String
        Get
            Return _latest_update_date_label
        End Get
        Set(ByVal value As String)
            _latest_update_date_label = value
            notifyPropertyChange()
        End Set
    End Property

    Public Sub initialize()
        Me.status_strip_main_label = ""
        Me.status_strip_sub_label = ""
        Me.progress_bar_visible = False
        Me.progress_bar_value = 0
        Me.latest_update_date_label = ""
    End Sub
End Class

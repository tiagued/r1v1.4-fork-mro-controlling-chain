﻿
Public Class CProjectView
    Private proj As CProjectModel
    Public Sub New(p As CProjectModel)

        proj = p
        _applicable_material_markup = New BindingSource
    End Sub









    'update markup list depending on ac group
    Private _applicable_material_markup As BindingSource
    Public Property applicable_material_markup() As BindingSource
        Get
            Return _applicable_material_markup
        End Get
        Set(ByVal value As BindingSource)
            Dim res As New Dictionary(Of String, String)
            If MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_AC_GRP_DEP_MAT_MRKUP).ContainsKey(Me.proj.ac_group) Then
                For Each str As String In MConstants.lisOfValueDependencies(MConstants.CONST_CONF_LOV_AC_GRP_DEP_MAT_MRKUP)(Me.proj.ac_group)
                    If MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_MAT_MRKUP).ContainsKey(str) Then
                        res.Add(str, MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_MAT_MRKUP)(str).value)
                    End If
                Next
            Else
                'take all markups if ac-group is null
                For Each prop As CConfProperty In MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_MAT_MRKUP).Values
                    res.Add(prop.key, prop.value)
                Next
            End If
            applicable_material_markup.DataSource = New BindingSource(res, Nothing)
        End Set
    End Property

End Class
